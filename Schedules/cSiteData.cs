﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Windows.Forms;
using System.Data.Common;
using Oracle.ManagedDataAccess.Client;
using System.Configuration;


namespace Schedules
{
  ///get site data ie  downtime hours and production figures
        
   class cSiteData
   {


    cCal cal = new cCal();


    //parameters from index html
    string sSite = "";
    string sAreaID = "";
    int iSappiYear = 0;


    public void RunMonthly(string pSite, string pAreaID, int pRunYear = 0, int pStartMonth = 0)
    {
      int iMonth = 0;     // oracle month
      int iSQLMonthID = 0;   // this is the ID used in MSSQLSERVER    kpiMonthly

      string sStartDate = "";
      string sEndDate = "";
      int currMonth = 0;
      int currYear = 0;

      int iDayStart = 0;
      int iDayEnd = 0;

      sSite = pSite;
      sAreaID = pAreaID;

      try
      { 

        cal = cal ?? new cCal();



        cal.GetCal(cmath.getDateNowStr(-1));
        currYear = cal.iSappiYear;
        currMonth = cal.iCurrMonthOfYear;
        sStartDate = cal.sMonthStartDate;
        sEndDate = cal.sMonthEndDate;


        iMonth = pStartMonth;     //start from the month number
        iSappiYear = pRunYear;    //run year

        if (iSappiYear < currYear)
        {
           currMonth = 12;     //set to last month of year
        }


          //then loop thru until the current month
          while (iMonth <= currMonth)
          {
            
            cSQL.OpenDB("ZAMDW");

            cal.GetMonthMSSQL(iSappiYear, iMonth, ref sStartDate, ref sEndDate);      //get start date and end date for the month

            cal.GetCal(cmath.getDateStr(sStartDate));
            iDayStart = cal.iDayOfYear;

            cal.GetCal(cmath.getDateStr( sEndDate));
            iDayEnd = cal.iDayOfYear;

            iSQLMonthID = cal.GetSappiMonthID(iSappiYear, iMonth);               // from sql server to save at sql server

            if ((iMonth <= 0) || (iSQLMonthID <= 0))
            {
              cFG.LogIt( "SiteData Invalid Month ID : " + iMonth + " , " + iSQLMonthID);
              break;
            }

            if (iSappiYear == currYear &&  iMonth == currMonth)     //enddate is up to yesterday
            {
              sEndDate = cmath.getDateEODStr(cmath.getDateStr(DateTime.Now.ToString(), -1));       //yesterday date   
              cal.GetCal(cmath.getDateStr(sEndDate));
              iDayEnd = cal.iDayOfYear;
          }

            cSQL.CloseDB();

            Application.DoEvents();

            cFG.LogIt( "SiteData Process Month# " + iMonth + " Date Start : " + sStartDate + " , Date End " + sEndDate);


            if (sSite == "NGX")
            {
                  if (sAreaID == "13")       //uptake machine
                  {
                     DoNGX_UT(iSQLMonthID, sStartDate, sEndDate, 0);
                     SheetBreaks(iSQLMonthID, sStartDate, sEndDate, 0);
                     GradeChanges(iSQLMonthID, sStartDate, sEndDate, 0);       
                  }

                  if ((sAreaID == "25") || (sAreaID == "139"))
                  {
                    DoNGX_PM(iSQLMonthID, sStartDate, sEndDate, 0);
                    SheetBreaks(iSQLMonthID, sStartDate, sEndDate, 0);
                    GradeChanges(iSQLMonthID, sStartDate, sEndDate, 0);       

                  }

                  if (sAreaID == "161")       //
                  {
                    DoNGX_FL3(iSQLMonthID, sStartDate, sEndDate, 0);
                  }

                  if (sAreaID == "5")       //
                  {
                    DoNGX_DIG2(iSQLMonthID, sStartDate, sEndDate, 0); 
                  }
            }

          if (sSite == "SAI")
          {
                  if ((sAreaID == "4") || (sAreaID == "5") || (sAreaID == "6"))
                  { 
                      DoSAI_Continua(iSQLMonthID, sStartDate, sEndDate, 0);
                      SheetBreaks(iSQLMonthID, sStartDate, sEndDate, 0);
                      GradeChanges(iSQLMonthID, sStartDate, sEndDate, 0);      
                  }

                  if ((sAreaID == "1") || (sAreaID == "2") || (sAreaID == "3") || (sAreaID == "65") )
                  {
                      DoSAI_DIG(iSQLMonthID, sStartDate, sEndDate, 0);
                  }


          }

            if (sSite == "STA")
            {
                  if (sAreaID == "6" || sAreaID == "12")
                  {
                    DoStanger_PM(iSQLMonthID, sStartDate, sEndDate, 0);
                    SheetBreaks(iSQLMonthID, sStartDate, sEndDate, 0);
                    GradeChanges(iSQLMonthID, sStartDate, sEndDate, 0);       //
                  }

                  if (sAreaID == "1")
                  {
                    DoSTA_DIG(iSQLMonthID, sStartDate, sEndDate, 0);       //digester
                  }

            }

            if (sSite == "TUG")
            {
                  if (sAreaID == "118")
                  {
                    DoTugela_PM(iSQLMonthID, sStartDate, sEndDate, 0);
                    SheetBreaks(iSQLMonthID, sStartDate, sEndDate, 0);
                    GradeChanges(iSQLMonthID, sStartDate, sEndDate, 0);       //
                  }

                  if (sAreaID == "126")
                  {
                    DoTUG_DIG(iSQLMonthID, sStartDate, sEndDate, 0);       //digester
                  }
            }


            DoPLS(iMonth, iDayStart, iDayEnd, iSQLMonthID, 0);      // the old availability kpi's
            DoMPPD(iMonth, iDayStart, iDayEnd, iSQLMonthID, 0);     // the old availability kpi's

            DoCMDT(iMonth, iDayStart, iDayEnd, iSQLMonthID, 0);     // the new availability kpi's
            DoPTS(iMonth, iDayStart, iDayEnd, iSQLMonthID, 0);
            DoPLO(iMonth, iDayStart, iDayEnd, iSQLMonthID, 0);
            DoPLMT(iMonth, iDayStart, iDayEnd, iSQLMonthID, 0);
            DoPLPS(iMonth, iDayStart, iDayEnd, iSQLMonthID, 0);
            DoPIBP(iMonth, iDayStart, iDayEnd, iSQLMonthID, 0);
            DoUPLE(iMonth, iDayStart, iDayEnd, iSQLMonthID, 0);
            DoPROC(iMonth, iDayStart, iDayEnd, iSQLMonthID, 0);

            DoPLAP(iMonth, iDayStart, iDayEnd, iSQLMonthID, 0);     //2021-02-22
            DoTCSS(iMonth, iDayStart, iDayEnd, iSQLMonthID, 0);     //2021-02-22
            DoTISS(iMonth, iDayStart, iDayEnd, iSQLMonthID, 0);     //2021-02-22
            DoBudgetProduction(iSQLMonthID, sStartDate, sEndDate, 0, 0);
            DoSAIBudgetProduction(iSQLMonthID, sStartDate, sEndDate, 0, 0);
            DoSAIBudgetProduction83(iSQLMonthID, sStartDate, sEndDate, 0, 0);

          iMonth++;

          } //loop

      }
      catch (Exception ee)
      {
        cFG.LogIt( "DoMonthly " + ee.Message);
        //throw ee;
      }
      finally
      {
        cSQL.CloseDB();
        cORA.CloseDB();
      }

    }// 


    public void RunWeekly(string pSite, string pAreaID, int pSappiYear, int pWeekNum)
    {


      string sStartDate = "";
      string sEndDate = "";
      int currMonth = 0;
      int currWeek = 0;
      int numDaysInWeek = 0;

      int iDayStart = 0;
      int iDayEnd = 0;
      int iSQLWeekID = 0;   // this is the ID used in MSSQLSERVER    kpiWeekly


      sSite = pSite;
      sAreaID = pAreaID;
      currWeek = pWeekNum;
      iSappiYear = pSappiYear;

      try
      {

        cal = cal ?? new cCal();


        cal.GetWeekMSSQL(iSappiYear, currWeek, ref currMonth, ref sStartDate, ref sEndDate, ref numDaysInWeek);


        int thisWeek = cal.iCurrWeekOfYear;

        if (thisWeek == currWeek)
            sEndDate = cmath.getDateEODStr(cmath.getDateStr(DateTime.Now.ToString(), -1));       //run up to yesterday if this week

        

        cal.GetCal(cmath.getDateStr(sStartDate));
        iDayStart = cal.iDayOfYear;

        cal.GetCal(cmath.getDateStr(sEndDate));
        iDayEnd = cal.iDayOfYear;


        iSQLWeekID = cal.GetSappiWeekID(iSappiYear, currWeek);               // from sql server to save at sql server

        if  (iSQLWeekID <= 0)
        {
          cFG.LogIt("SiteData Invalid Week ID : " + currWeek);
          return;
        }


        Application.DoEvents();

        cFG.LogIt("SiteData Process Week# " + currWeek + " Date Start : " + sStartDate + " , Date End " + sEndDate);

        if (sSite == "NGX")
        {
            if (sAreaID == "13")       //uptake machine
            {
              DoNGX_UT(0, sStartDate, sEndDate, 0, iSQLWeekID);
              SheetBreaks(0, sStartDate, sEndDate, 0, iSQLWeekID);       //sheet breaks
              GradeChanges(0, sStartDate, sEndDate, 0, iSQLWeekID);       //grade changes
            }

            if ((sAreaID == "25") || (sAreaID == "139"))                  //
            {
              DoNGX_PM(0, sStartDate, sEndDate, 0, iSQLWeekID);
              SheetBreaks(0, sStartDate, sEndDate, 0, iSQLWeekID);       //sheet breaks
              GradeChanges(0, sStartDate, sEndDate, 0, iSQLWeekID);       //grade changes
            }

            if (sAreaID == "161")       //
            {
              DoNGX_FL3(0, sStartDate, sEndDate, 0, iSQLWeekID);
            }

            if (sAreaID == "5")       //
            {
              DoNGX_DIG2(0, sStartDate, sEndDate, 0, iSQLWeekID);
            }
        }



        if (sSite == "SAI")
        {
            if ((sAreaID == "4") || (sAreaID == "5") || (sAreaID == "6"))
            {
              DoSAI_Continua(0, sStartDate, sEndDate, 0, iSQLWeekID);
              SheetBreaks(0, sStartDate, sEndDate, 0, iSQLWeekID);      //sheet breaks
              GradeChanges(0, sStartDate, sEndDate, 0, iSQLWeekID);     //grade changes
            //DoSAI_NSPPM(0, sStartDate, sEndDate, 0, iSQLWeekID);      //2025-02-19 daily and weekly now as a calculation see cKPIJob for NSPPM

            }

            if ((sAreaID == "1") || (sAreaID == "2") || (sAreaID == "3") || (sAreaID == "65"))
            {
              DoSAI_DIG(0, sStartDate, sEndDate, 0, iSQLWeekID);
            }
        }


        if (sSite == "STA")
        {
            if (sAreaID == "6" || sAreaID == "12")
            {
              DoStanger_PM(0, sStartDate, sEndDate, 0, iSQLWeekID);
              SheetBreaks(0, sStartDate, sEndDate, 0, iSQLWeekID);       //sheet breaks
              GradeChanges(0, sStartDate, sEndDate, 0, iSQLWeekID);       //grade changes
            }

            if (sAreaID == "1")
            {
              DoSTA_DIG(0, sStartDate, sEndDate, 0, iSQLWeekID);
            }

        }


        if (sSite == "TUG")
        {
          if (sAreaID == "118")
          {
            DoTugela_PM(0, sStartDate, sEndDate, 0, iSQLWeekID);
            SheetBreaks(0, sStartDate, sEndDate, 0, iSQLWeekID);       //sheet breaks
            GradeChanges(0, sStartDate, sEndDate, 0, iSQLWeekID);       //grade changes
          }

          if (sAreaID == "126")
            DoTUG_DIG(0, sStartDate, sEndDate, 0, iSQLWeekID);       //digester

        }


        DoPLS(currMonth, iDayStart, iDayEnd, 0, 0, iSQLWeekID);      // the old availability kpi's
        DoMPPD(currMonth, iDayStart, iDayEnd, 0, 0, iSQLWeekID);     // the old availability kpi's

        DoCMDT(currMonth, iDayStart, iDayEnd, 0, 0, iSQLWeekID);     // the new availability kpi's
        DoPTS(currMonth, iDayStart, iDayEnd, 0,0, iSQLWeekID);
        DoPLO(currMonth, iDayStart, iDayEnd, 0, 0, iSQLWeekID);
        DoPLMT(currMonth, iDayStart, iDayEnd, 0, 0, iSQLWeekID);
        DoPLPS(currMonth, iDayStart, iDayEnd, 0, 0, iSQLWeekID);
        DoPIBP(currMonth, iDayStart, iDayEnd, 0, 0, iSQLWeekID);
        DoUPLE(currMonth, iDayStart, iDayEnd, 0, 0, iSQLWeekID);
        DoPROC(currMonth, iDayStart, iDayEnd, 0, 0, iSQLWeekID);

        DoPLAP(currMonth, iDayStart, iDayEnd, 0, 0, iSQLWeekID);     
        DoTCSS(currMonth, iDayStart, iDayEnd, 0, 0, iSQLWeekID);    
        DoTISS(currMonth, iDayStart, iDayEnd, 0, 0, iSQLWeekID);
        DoBudgetProduction(0, sStartDate, sEndDate, 0, iSQLWeekID);
        DoSAIBudgetProduction(0, sStartDate, sEndDate, 0, iSQLWeekID);
        DoSAIBudgetProduction83(0, sStartDate, sEndDate, 0, iSQLWeekID);

      }
      catch (Exception ee)
      {
        cFG.LogIt("Do Weekly " + ee.Message);
        //throw ee;
      }
      finally
      {
        cSQL.CloseDB();
        cORA.CloseDB();
      }

    }// 



    public void RunDaily(string pSite, string pAreaID, string pRunDate)
    {
      string sStartDate = "";
      string sEndDate = "";

      int iSQLDayID = 0;     // this is the ID used in MSSQLSERVER    kpiDaily

      int iDayStart = 0;
      int iDayEnd = 0;
      int currMonth = 0;

      sSite = pSite;
      sAreaID = pAreaID;

      try
      {
        cal = cal ?? new cCal();

        iSQLDayID = cal.GetSappiDayID(pRunDate);

        cal.GetCal(pRunDate);

        iSappiYear = cal.iSappiYear;
        currMonth = cal.iCurrMonthOfYear;
        sStartDate = cal.sDayStart;
        sEndDate = cal.sDayEnd;



        cal.GetCal(cmath.getDateStr(sStartDate));
        iDayStart = cal.iDayOfYear;

        cal.GetCal(cmath.getDateStr(sEndDate));
        iDayEnd = cal.iDayOfYear;


        cFG.LogIt( "SiteData Process Date Start : " + sStartDate + " , Date End " + sEndDate
        + " , Day Start " + iDayStart.ToString() + " , Day End " + iDayEnd.ToString());


        if (sSite == "SAI")
        {
              if ((sAreaID == "4") || (sAreaID == "5") || (sAreaID == "6"))
              {
                DoSAI_Continua(0, sStartDate, sEndDate, iSQLDayID);
                SheetBreaks(0, sStartDate, sEndDate, iSQLDayID);
                GradeChanges(0, sStartDate, sEndDate, iSQLDayID);
                //DoSAI_NSPPM(0, sStartDate, sEndDate, iSQLDayID);   //2025-02-19 daily and weekly now as a calculation see cKPIJob for NSPPM
          }

              if ((sAreaID == "1") || (sAreaID == "2") || (sAreaID == "3") || (sAreaID == "65"))
              {
                DoSAI_DIG(0, sStartDate, sEndDate, iSQLDayID);
              }
        }


        if (sSite == "NGX")
        {
              if (sAreaID == "13")       //uptake machine
              {
                DoNGX_UT(0, sStartDate, sEndDate, iSQLDayID);
                SheetBreaks(0, sStartDate, sEndDate, iSQLDayID);
                GradeChanges(0, sStartDate, sEndDate, iSQLDayID);
              }

              if ((sAreaID == "25") || (sAreaID == "139"))
              {
                DoNGX_PM(0, sStartDate, sEndDate, iSQLDayID);
                SheetBreaks(0, sStartDate, sEndDate, iSQLDayID);
                GradeChanges(0, sStartDate, sEndDate, iSQLDayID);
              }

              if (sAreaID == "161")       //
              {
                DoNGX_FL3(0, sStartDate, sEndDate, iSQLDayID);
              }

              if (sAreaID == "5")       //
              {
                DoNGX_DIG2(0, sStartDate, sEndDate, iSQLDayID);
              }
        }


        if (sSite == "STA")
        {
              if (sAreaID == "6" || sAreaID == "12")
              {
                DoStanger_PM(0, sStartDate, sEndDate, iSQLDayID);
                SheetBreaks(0, sStartDate, sEndDate, iSQLDayID);
                GradeChanges(0, sStartDate, sEndDate, iSQLDayID);
              }

              if (sAreaID == "1")
              {
                DoSTA_DIG(0, sStartDate, sEndDate, iSQLDayID);
              }
        }


        if (sSite == "TUG")
        {
            if (sAreaID == "118")
            {
              DoTugela_PM(0, sStartDate, sEndDate, iSQLDayID);
              SheetBreaks(0, sStartDate, sEndDate, iSQLDayID);
              GradeChanges(0, sStartDate, sEndDate, iSQLDayID);
            }

            if (sAreaID == "126")
              DoTUG_DIG(0, sStartDate, sEndDate, iSQLDayID);     
        }



        DoPLS(currMonth, iDayStart, iDayEnd, 0, iSQLDayID);
        DoMPPD(currMonth, iDayStart, iDayEnd, 0, iSQLDayID);

        DoCMDT(currMonth, iDayStart, iDayEnd, 0, iSQLDayID);
        DoPTS(currMonth, iDayStart, iDayEnd, 0, iSQLDayID);
        DoPLO(currMonth, iDayStart, iDayEnd, 0, iSQLDayID);
        DoPLMT(currMonth, iDayStart, iDayEnd, 0, iSQLDayID);
        DoPLPS(currMonth, iDayStart, iDayEnd, 0, iSQLDayID);
        DoPIBP(currMonth, iDayStart, iDayEnd, 0, iSQLDayID);
        DoUPLE(currMonth, iDayStart, iDayEnd, 0, iSQLDayID);
        DoPROC(currMonth, iDayStart, iDayEnd, 0, iSQLDayID);
        DoPLAP(currMonth, iDayStart, iDayEnd, 0, iSQLDayID);
        DoTCSS(currMonth, iDayStart, iDayEnd, 0, iSQLDayID);
        DoTISS(currMonth, iDayStart, iDayEnd, 0, iSQLDayID);
        DoBudgetProduction(0, sStartDate, sEndDate, iSQLDayID);
        DoSAIBudgetProduction(0, sStartDate, sEndDate, iSQLDayID);
        DoSAIBudgetProduction83(0, sStartDate, sEndDate, iSQLDayID);

      }
      catch (Exception ee)
      {
        cFG.LogIt( "DoDaily " + ee.Message);
        //throw ee;
      }
      finally
      {
        cSQL.CloseDB();
        cORA.CloseDB();
      }

    } //





    private void SaveMonths(int sqlmonthNum, string kpiCode, double pValue)
    {

      //int col = 0;
      Application.DoEvents();

      string sql = "";

      cSQL.OpenDB("ZAMDW");       

      try
      {
        sql = "UPDATE KPIMonthly set KPI_VALUE = " + pValue;
        sql += " , LASTUPDATED = '" + cmath.getDateTimeNowStr() + "'";
        sql += " where Mill_Code = '" + sSite + "'";
        sql += " and KPI_CODE = '" + kpiCode + "'";
        sql += " and SappiMonth_ID  = " + sqlmonthNum;
        sql += " and FLID = " + sAreaID;
        sql += " IF @@ROWCOUNT=0";
        sql += " INSERT INTO KPIMonthly (MILL_CODE, KPI_CODE, SAPPIMONTH_ID,KPI_VALUE, LASTUPDATED,FLID)";
        sql += " VALUES ("
             + "'" + sSite + "'"
             + ",'" + kpiCode + "'"
             + "," + sqlmonthNum
             + "," + pValue
             + ",'" + cmath.getDateTimeNowStr() + "'"
             + "," + sAreaID
             + " )";

        cSQL.ExecuteQuery(sql);


      }
      catch (Exception er)
      {
        cFG.LogIt( "SiteData SaveMonths: " + er.Message);
        throw new Exception("SiteData SaveMonths: " + er.Message);
      }
      finally
      {
        cSQL.CloseDB();
      }


    }//



    private void SaveDaily(int iSqlDayID , string kpiCode, double pValue)
    {

      //int col = 0;
      Application.DoEvents();

      string sql = "";

      cSQL.OpenDB("ZAMDW");    


      try
      {
        sql = "UPDATE KPIDaily set KPIVALUE = " + pValue;
        sql += " , LASTUPDATED = '" + cmath.getDateTimeNowStr() + "'";
        sql += " where MillCode = '" + sSite + "'";
        sql += " and KPICODE = '" + kpiCode + "'";
        sql += " and SappiDayID  = " + iSqlDayID;
        sql += " and FLID = " + sAreaID;
        sql += " IF @@ROWCOUNT=0";
        sql += " INSERT INTO KPIDaily (MILLCODE, KPICODE, SAPPIDAYID, KPIVALUE, LASTUPDATED, FLID)";
        sql += " VALUES ("
             + "'" + sSite + "'"
             + ",'" + kpiCode + "'"
             + "," + iSqlDayID
             + "," + pValue
             + ",'" + cmath.getDateTimeNowStr() + "'"
             + "," + sAreaID
             + " )";

        cSQL.ExecuteQuery(sql);


      }
      catch (Exception er)
      {
        cFG.LogIt( "SaveDaily: " + er.Message);
        throw new Exception("SaveDaily: " + er.Message);
      }
      finally
      {
        cSQL.CloseDB();
      }

    }//



    private void SaveWeekly(int iSqlWeekID, string kpiCode, double pValue)
    {

      //int col = 0;
      Application.DoEvents();

      string sql = "";

      cSQL.OpenDB("ZAMDW");


      try
      {
        sql = "UPDATE KPIWeekly set KPIVALUE = " + pValue;
        sql += " , LASTUPDATED = '" + cmath.getDateTimeNowStr() + "'";
        sql += " where MillCode = '" + sSite + "'";
        sql += " and KPICODE = '" + kpiCode + "'";
        sql += " and SappiWeekID  = " + iSqlWeekID;
        sql += " and FLID = " + sAreaID;
        sql += " IF @@ROWCOUNT=0";
        sql += " INSERT INTO KPIWeekly (MILLCODE, KPICODE, SAPPIWEEKID, KPIVALUE, LASTUPDATED, FLID)";
        sql += " VALUES ("
             + "'" + sSite + "'"
             + ",'" + kpiCode + "'"
             + "," + iSqlWeekID
             + "," + pValue
             + ",'" + cmath.getDateTimeNowStr() + "'"
             + "," + sAreaID
             + " )";

        cSQL.ExecuteQuery(sql);


      }
      catch (Exception er)
      {
        cFG.LogIt("SaveWeekly: " + er.Message);
        throw new Exception("SaveWeekly: " + er.Message);
      }
      finally
      {
        cSQL.CloseDB();
      }

    }//




    public void RunLomati(int pRunYear = 0, int pStartMonth = 0)
    {
      int iMonth = 0;     // oracle month
      int iSQLMonthID = 0;   // this is the ID used in MSSQLSERVER    kpiMonthly

      string sStartDate = "";
      string sEndDate = "";
      int currMonth = 0;
      int currYear = 0;

      int iDayStart = 0;
      int iDayEnd = 0;

      try
      {

        cal = cal ?? new cCal();


        cal.GetCal(cmath.getDateNowStr(-1));
        currYear = cal.iSappiYear;
        currMonth = cal.iCurrMonthOfYear;
        sStartDate = cal.sMonthStartDate;
        sEndDate = cal.sMonthEndDate;


        iMonth = pStartMonth;     //start from the month number
        iSappiYear = pRunYear;    //run year

        if (iMonth > currMonth)
          iMonth = currMonth;

        //if running for previous years
        if (iSappiYear < currYear)
        {
          currMonth = 12;     //set to last month of year
        }

        if (iSappiYear > currYear)
        {
          cFG.LogIt("SiteData RunLomati  RunYear > current year");
          return;
        }


        //then loop thru until the current month
        while (iMonth <= currMonth)
        {
          cSQL.OpenDB("ZAMDW");
          cal.GetMonthMSSQL(iSappiYear, iMonth, ref sStartDate, ref sEndDate);

          cal.GetCal(cmath.getDateStr(sStartDate));
          iDayStart = cal.iDayOfYear;

          cal.GetCal(cmath.getDateStr(sEndDate));
          iDayEnd = cal.iDayOfYear;


          iSQLMonthID = cal.GetSappiMonthID(iSappiYear, iMonth);               // from sql server to save at sql server



          if ((iMonth <= 0) || (iSQLMonthID <= 0))
          {
            cFG.LogIt("SiteData RunLomati Invalid Month ID : " + iMonth + " , " + iSQLMonthID);
            break;
          }

          if (iMonth == currMonth)     //enddate is up to yesterday
          {
            sEndDate = cmath.getDateEODStr(cmath.getDateStr(DateTime.Now.ToString(), -1));       //yesterday
            cal.GetCal(cmath.getDateStr(sEndDate));
            iDayEnd = cal.iDayOfYear;
          }


          Application.DoEvents();

          cFG.LogIt("SiteData RunLomati Process Month# " + iMonth + " Date Start : " + sStartDate + " , Date End " + sEndDate);

          //DoLomati(iSQLMonthID, iSappiYear.ToString(), iMonth.ToString(), sStartDate, sEndDate);
          DoLomati(iSappiYear.ToString(), iMonth.ToString(), sStartDate, sEndDate, "month");


          //quarterly figures
          cal.GetCal(cmath.getDateStr(sStartDate));   //get the quarter start / end dates
          sStartDate = cal.sQuartertartDate;
          //sEndDate = is the actual month end date for the quarter calculations
          DoLomati(iSappiYear.ToString(), iMonth.ToString(), sStartDate, sEndDate, "quarter");


          //YTD figures
          sStartDate = cal.sYearStartDate;
          //sEndDate = is the actual month end date from the last date given in the loop
          DoLomati(iSappiYear.ToString(), iMonth.ToString(), sStartDate, sEndDate, "ytd");


          iMonth++;

        } //loop




      }
      catch (Exception ee)
      {
        cFG.LogIt("RunLomati " + ee.Message);
        //throw ee;
      }
      finally
      {
        cSQL.CloseDB();
        cORA.CloseDB();
      }

    }// 




    private void DoLomati(string pSappiYear, string pMonth, string pStartDate, string pEndDate, string runType)
    {

      string sql = "";

      double dOEEBudget = 0;
      double dOEEActual = 0;

      double dOMEBudget = 0;
      double dOMEActual = 0;

      double dTonsBudget = 0;
      double dTonsActual = 0;

      double dSubstandardBudget = 0;
      double dSubstandardActual = 0;


      //for the OEE %
      double Budget_UptimePercentage = 0;
      double Budget_Volume = 0;
      double Budget_Volume_Intake = 0;

      double OMEBudget_UptimePercentage = 0;
      double OMEBudget_Volume = 0;
      double OMEBudget_Volume_Intake = 0;

      int reporting_Year = 0;

      string sStartDate = pStartDate;     //dd/mm/yyyy
      string sEndDate = pEndDate;



      DataTable dt = null;


      try
      {
        //----------------------------
        //Saleable production
        //----------------------------
        if (runType == "month")
        {
              sql = "select round(sum(volume),5) as SaleableProduction"
              + " from   Scanners_History.dbo.vwTicketProcessed"
              + " where FinancialPeriod = " + pSappiYear + pMonth.PadLeft(2, '0')
              + " and ProcessCode = 'OUT'"
              //+ " and ItemGroup in ('STR', 'IND', 'SLV')"
              + " and ItemGroup in ('STR', 'IND', 'VAL')"
              + "";

              cSQL.OpenDB("SCANNERS");

              dt = cSQL.GetDataTable(sql);

              foreach (DataRow item in dt.Rows)
              {
                dTonsActual = cmath.getd(item["SaleableProduction"].ToString());
              }
        }
      }
      catch (Exception ee)
      {
        cFG.LogIt("SiteData DoLomati SaleableProduction: " + ee.Message);
      }

      //----------------------------
      //Saleable production budget
      //----------------------------
      try {
        if (runType == "month")
        {

          sql = "select round(sum(budget_volume),5)  SaleableBudget"
            + " from     vwNon_financial_Budgets"
            + " where    BudgetLink_ToEvolution in ('SLV', 'IND', 'FJP')"
            + " and BudgetDate >= '" + cmath.getDateStr(sStartDate) + "'"
            + " and BudgetDate <= '" + cmath.getDateStr(sEndDate) + "'"
            + "";

          dt = cSQL.GetDataTable(sql);

          foreach (DataRow item in dt.Rows)
          {
            dTonsBudget = cmath.getd(item["SaleableBudget"].ToString());
          }
        }
      }
      catch (Exception ee)
      {
        cFG.LogIt("SiteData DoLomati SaleableProductionBudget: " + ee.Message);
      }


      cORA.CloseDB();



      //----------------------------
      // OEE  actual  percent 2022-06-30
      //----------------------------
      cSQL.OpenDB("OEELOMATI");

      try
      {
        sql = "EXECUTE [OEE].[dbo].[spLomatiOEEActual] '" + cmath.getDateStr(sStartDate) + "','" + cmath.getDateStr(sEndDate) + "'";

        dt = cSQL.GetDataTable(sql);

        foreach (DataRow item in dt.Rows)
        {
          dOEEActual = Math.Round(cmath.getd(item["OEEActual"].ToString()) * 100, 5);
        }
      }
      catch (Exception ee)
      {
        cFG.LogIt("SiteData DoLomati OEE Actual: " + ee.Message);
      }


      cSQL.CloseDB();


      // --------------------------------
      // OEE BUDGET PERCENT
      // ------------------------------
      cSQL.OpenDB("SCANNERS");

        dOEEBudget = 0;
        
        //get the reporting year ID
        sql = "select max(idyear) idyear from Scanners_History.dbo.vwSappiPeriods"
          + " where ReportingDate between '" + cmath.getDateStr(sStartDate) + "'"
          + "   and '" + cmath.getDateStr(sEndDate) + "'"
          + "";

        dt = cSQL.GetDataTable(sql);

        foreach (DataRow item in dt.Rows)
        {
          reporting_Year  = cmath.geti(item["idYear"].ToString());
        }


        sql = "select	Avg(Budget_UptimePercentage) Budget_UptimePercentage"
            + " from	Scanners_History.dbo.vwNon_financial_Budgets"
            + " where	BudgetLink_ToOEE = 1"
            + " and	BudgetDate between '" + cmath.getDateStr(sStartDate) + "'"
            + "   and '" + cmath.getDateStr(sEndDate) + "'"
            + "";

        dt = cSQL.GetDataTable(sql);

        foreach (DataRow item in dt.Rows)
        {
          Budget_UptimePercentage = cmath.getd(item["Budget_UptimePercentage"].ToString());
        }



        sql = "select sum(Budget_Volume)  Budget_Volume"
          + " from  Scanners_History.dbo.tblNon_financial_Budgets"
          + " left join vwSappiPeriods on BudgetDate = ReportingDate"
          + " where BudgetLink_ToEvolution in ('FJP', 'IND', 'SLV')"
          + "  and idYear = " + reporting_Year
          + "";

          dt = cSQL.GetDataTable(sql);

          foreach (DataRow item in dt.Rows)
          {
            Budget_Volume = cmath.getd(item["Budget_Volume"].ToString());
          }


          sql = "select 	sum(Budget_Volume) Budget_Volume"
              + " from 	Scanners_History.dbo.tblNon_financial_Budgets"
              + " left join Scanners_History.dbo.vwSappiPeriods on BudgetDate = ReportingDate"
              + " where	BudgetLink_ToEvolution = 'WET'"
              + " and 	idYear = " + reporting_Year
              + "";

          dt = cSQL.GetDataTable(sql);

          foreach (DataRow item in dt.Rows)
          {
            Budget_Volume_Intake = cmath.getd(item["Budget_Volume"].ToString());
          }

          dOEEBudget = Math.Round( (Budget_UptimePercentage * (Budget_Volume / Budget_Volume_Intake) / 0.5) * 100, 5);




        //------------------------
        //substandard budget
        //------------------------
        dSubstandardBudget = 0;

        try { 
          sql = "select round(AVG(budget_xxratio),5) as substandardBudget"
              + " from	Scanners_History.dbo.vwNon_financial_Budgets"
              + " where	BudgetLink_ToOEE = 1"
              + " and		BudgetDate between  '" + cmath.getDateStr(sStartDate) + "'"
              + "  and '" + cmath.getDateStr(sEndDate) + "'"
              + "";


          dt = cSQL.GetDataTable(sql);

          foreach (DataRow item in dt.Rows)
          {
            dSubstandardBudget = Math.Round(cmath.getd(item["substandardBudget"].ToString()) * 100, 5) ;
          }
        }
         catch (Exception ee)
         {
            cFG.LogIt("SiteData DoLomati Substandard Budget: " + ee.Message);
         }

          //----------------------------
          //Substandard Actual
          //----------------------------
          try
          {
            sql = "select ("
              + " (  Select	round(sum(volume),5) from	Scanners_History.dbo.vwTicketProcessed"
              + "     where		ItemGrade = 'XX'"
              + "     and		processcode = 'OUT'"
              + "     and		ReportingDate between  '" + cmath.getDateStr(sStartDate) + "'"
              + "     and '" + cmath.getDateStr(sEndDate) + "'"
              + " ) / "         //division
              + " (select round(sum(volume),5) from	Scanners_History.dbo.vwTicketProcessed"
              + "   where	ProcessCode = 'OUT'"
              + "   and ItemGroup = 'STR' "
              + "   and		ReportingDate between  '" + cmath.getDateStr(sStartDate) + "'"
              + "   and '" + cmath.getDateStr(sEndDate) + "'"
              + " ) "
              + " )  as substandardActual";

             dt = cSQL.GetDataTable(sql);

            foreach (DataRow item in dt.Rows)
            {
              dSubstandardActual = Math.Round(cmath.getd(item["substandardActual"].ToString()) * 100, 5);
            }
          }
          catch (Exception ee)
          {
            cFG.LogIt("SiteData DoLomati Substandard Actual: " + ee.Message);
          }



      cSQL.CloseDB();



      //----------------------------
      // OME  actual  2022-12-12
      //----------------------------
      cSQL.OpenDB("OEELOMATI");

      try
      {
        sql = "EXECUTE [OEE].[dbo].[spLomatiOMEActual] '" + cmath.getDateStr(sStartDate) + "','" + cmath.getDateStr(sEndDate) + "'";

        dt = cSQL.GetDataTable(sql);

        foreach (DataRow item in dt.Rows)
        {
          dOMEActual = Math.Round(cmath.getd(item["OMEActual"].ToString()) , 5);
        }
      }
      catch (Exception ee)
      {
        cFG.LogIt("SiteData DoLomati OME Actual: " + ee.Message);
      }

      cSQL.CloseDB();



      // --------------------------------
      // OME BUDGET PERCENT   2022-12-12
      // ------------------------------
      cSQL.OpenDB("SCANNERS");


      sql = "select	Avg(Budget_UptimePercentage) Budget_UptimePercentage"
          + " from	Scanners_History.dbo.vwNon_financial_Budgets"
          + " where	BudgetLink_ToOEE = 1"
          + " and	BudgetDate between '" + cmath.getDateStr(sStartDate) + "'"
          + "   and '" + cmath.getDateStr(sEndDate) + "'"
          + "";

      dt = cSQL.GetDataTable(sql);

      foreach (DataRow item in dt.Rows)
      {
        OMEBudget_UptimePercentage = cmath.getd(item["Budget_UptimePercentage"].ToString());
      }



      sql = "select sum(Budget_Volume)  Budget_Volume"
        + " from  Scanners_History.dbo.tblNon_financial_Budgets"
        + " left join vwSappiPeriods on BudgetDate = ReportingDate"
        + " where BudgetLink_ToEvolution in ('FJP', 'IND', 'SLV')"
        + "  and idYear = " + reporting_Year
        + "";

      dt = cSQL.GetDataTable(sql);

      foreach (DataRow item in dt.Rows)
      {
        OMEBudget_Volume = cmath.getd(item["Budget_Volume"].ToString());
      }


      sql = "select 	sum(Budget_Volume) Budget_Volume"
          + " from 	Scanners_History.dbo.tblNon_financial_Budgets"
          + " left join Scanners_History.dbo.vwSappiPeriods on BudgetDate = ReportingDate"
          + " where	BudgetLink_ToEvolution = 'WET'"
          + " and 	idYear = " + reporting_Year
          + "";

      dt = cSQL.GetDataTable(sql);

      foreach (DataRow item in dt.Rows)
      {
        OMEBudget_Volume_Intake = cmath.getd(item["Budget_Volume"].ToString());
      }

      dOMEBudget = Math.Round((OMEBudget_UptimePercentage * (OMEBudget_Volume / OMEBudget_Volume_Intake) / 0.5) * 100, 5);



      cSQL.CloseDB();


      if (runType == "month")
      {

        sql = "update OEELomati set Lastupdated = getdate()"
            + " , OEEBudget = " + dOEEBudget
            + " , OEEActual = " + dOEEActual
            + " , OMEBudget = " + dOMEBudget
            + " , OMEActual = " + dOMEActual
            + " , TonsBudget = " + dTonsBudget
            + " , TonsActual = " + dTonsActual
            + " , SubstandardBudget = " + dSubstandardBudget
            + " , SubstandardActual = " + dSubstandardActual
            + " where SappiYear = " + pSappiYear
            + " and SappiMonth = " + pMonth
            + " if @@rowcount = 0"
            + "   insert into OEELomati(sappiyear, sappimonth, oeebudget, oeeactual, tonsbudget, tonsactual" +
            "         , substandardbudget, substandardactual, OMEBudget, OMEActual, lastupdated)"
            + " values("
            + " " + pSappiYear
           + " , " + pMonth
           + " , " + dOEEBudget
           + " , " + dOEEActual
           + " , " + dTonsBudget
           + " , " + dTonsActual
           + " , " + dSubstandardBudget
           + " , " + dSubstandardActual
           + " , " + dOMEBudget
           + " , " + dOMEActual
           + " , getDate()"
          + " )"
          + " ";

        try
        {
          cSQL.OpenDB("ZAMDW");
          cSQL.ExecuteQuery(sql);
        }
        catch (Exception ee)
        {
          cFG.LogIt("SiteData DoLomati  SaveData: " + ee.Message);
        }

      } // month        


      if (runType == "quarter")
      {

        sql = "update OEELomati set Lastupdated = getdate()"
            + " , OEEBudgetQtr = " + dOEEBudget
            + " , OEEActualQtr = " + dOEEActual
            + " , OMEBudgetQtr = " + dOMEBudget
            + " , OMEActualQtr = " + dOMEActual
            //+ " , TonsBudget = " + dTonsBudget
            //+ " , TonsActual = " + dTonsActual
            + " , SubstandardBudgetQtr = " + dSubstandardBudget
            + " , SubstandardActualQtr = " + dSubstandardActual
            + " where SappiYear = " + pSappiYear
            + " and SappiMonth = " + pMonth
            + " if @@rowcount = 0"
            + "   insert into OEELomati(sappiyear, sappimonth, oeebudgetQtr, oeeactualQtr " +
            "         , substandardbudgetQtr, substandardactualQtr, OMEBudgetQtr, OMEActualQtr, lastupdated)"
            + " values("
            + " " + pSappiYear
           + " , " + pMonth
           + " , " + dOEEBudget
           + " , " + dOEEActual
           + " , " + dSubstandardBudget
           + " , " + dSubstandardActual
           + " , " + dOMEBudget
           + " , " + dOMEActual
           + " , getDate()"
          + " )"
          + " ";

        try
        {
          cSQL.OpenDB("ZAMDW");
          cSQL.ExecuteQuery(sql);
        }
        catch (Exception ee)
        {
          cFG.LogIt("SiteData DoLomati  SaveDataQtr: " + ee.Message);
        }

      } // quarter


      if (runType == "ytd")
      {

        sql = "update OEELomati set Lastupdated = getdate()"
            + " , OEEBudgetYTD = " + dOEEBudget
            + " , OEEActualYTD = " + dOEEActual
            + " , OMEBudgetYTD = " + dOMEBudget
            + " , OMEActualYTD = " + dOMEActual
            //+ " , TonsBudget = " + dTonsBudget
            //+ " , TonsActual = " + dTonsActual
            + " , SubstandardBudgetYTD = " + dSubstandardBudget
            + " , SubstandardActualYTD = " + dSubstandardActual
            + " where SappiYear = " + pSappiYear
            + " and SappiMonth = " + pMonth
            + " if @@rowcount = 0"
            + "   insert into OEELomati(sappiyear, sappimonth, oeebudgetYTD, oeeactualYTD " +
            "         , substandardbudgetYTD, substandardactualYTD, OMEBudgetYTD, OMEActualYTD, lastupdated)"
            + " values("
            + " " + pSappiYear
           + " , " + pMonth
           + " , " + dOEEBudget
           + " , " + dOEEActual
           + " , " + dSubstandardBudget
           + " , " + dSubstandardActual
           + " , " + dOMEBudget
           + " , " + dOMEActual
           + " , getDate()"
          + " )"
          + " ";

        try
        {
          cSQL.OpenDB("ZAMDW");
          cSQL.ExecuteQuery(sql);
        }
        catch (Exception ee)
        {
          cFG.LogIt("SiteData DoLomati  SaveDataYTD: " + ee.Message);
        }

      } // month        


      cORA.CloseDB();
         
      cSQL.CloseDB();




    }//




    private void DoNGX_PM(int sqlMonthID, string pStartDate, string pEndDate, int sqlDayID = 0, int sqlWeekID = 0)
    {
      short shPM = 0;
      double dGross = 0;
      double dPrimeProd = 0;
      double dBrokeShrinkage = 0;
      double dDKL = 0;
      double dPPAG = 0;


      if (sAreaID == "139")
        shPM = 1;

      if (sAreaID == "25")
        shPM = 2;

      pStartDate = cmath.getDateStr(pStartDate);
      pEndDate = cmath.getDateStr(pEndDate);

      try
      {
    
        cORA.OpenDB("SAPXMIINGX");

        cORA.cmd = cORA.con.CreateCommand();
        cORA.cmd.CommandText = "sapxmii.api_manufacturing_efficiency_model.quality_and_rft_production_of";
        cORA.cmd.CommandType = CommandType.StoredProcedure;
        
        OracleCommandBuilder.DeriveParameters(cORA.cmd);
        cORA.cmd.BindByName = true;
        cORA.cmd.Parameters["I_PM_NO"].Value = shPM;
        cORA.cmd.Parameters["I_DATE_FROM"].Value = pStartDate;
        cORA.cmd.Parameters["I_DATE_TO"].Value = pEndDate;


        //using (
        //    var dataReader = cORA.cmd.Parameters["RETURN_VALUE"].Value as DbDataReader
        //    ) {
        //cORA.cmd.ExecuteNonQuery();

          DataTable dt = cORA.ExecuteQuerySp(cORA.cmd);
          foreach (DataRow item in dt.Rows)
          {
            dGross = cmath.getd(item["gp"].ToString());
            dDKL = cmath.getd(item["dkl"].ToString());
            dPrimeProd = cmath.getd(item["rft"].ToString());
            dPPAG = cmath.getd(item["ppag"].ToString());
            dBrokeShrinkage = cmath.getd(item["tbs"].ToString());
          }
        


        if ((sqlDayID == 0) && (sqlMonthID > 0))
        {
          SaveMonths(sqlMonthID, "GP", dGross);
          SaveMonths(sqlMonthID, "DKL", dDKL);
          SaveMonths(sqlMonthID, "PPAG", dPPAG);
          SaveMonths(sqlMonthID, "TBS", dBrokeShrinkage);
          SaveMonths(sqlMonthID, "RFT", dPrimeProd);

        }

        if ((sqlDayID > 0) && (sqlMonthID == 0))
        {
          SaveDaily(sqlDayID, "GP", dGross);
          SaveDaily(sqlDayID, "DKL", dDKL);
          SaveDaily(sqlDayID, "PPAG", dPPAG);
          SaveDaily(sqlDayID, "TBS", dBrokeShrinkage);
          SaveDaily(sqlDayID, "RFT", dPrimeProd);
        }

        if (sqlWeekID > 0)
        {
          SaveWeekly(sqlWeekID, "GP", dGross);
          SaveWeekly(sqlWeekID, "DKL", dDKL);
          SaveWeekly(sqlWeekID, "PPAG", dPPAG);
          SaveWeekly(sqlWeekID, "TBS", dBrokeShrinkage);
          SaveWeekly(sqlWeekID, "RFT", dPrimeProd);
        }

      }
      catch (Exception ee)
      {
        cFG.LogIt( "SiteData DoNGX_PM: " + ee.Message);
        throw ee;
      }
      finally
      {
        cORA.CloseDB();
      }

    }//



    private void DoNGX_UT(int sqlMonthID, string pStartDate, string pEndDate, int sqlDayID = 0, int sqlWeekID = 0)
    {
      //2021-03-10  Now using the storec procedure supplied by saiccor

      double dGrossWeightAll = 0D;
      double dGrossWeightReject = 0D;
      double dGrossWeightWriteOff = 0D;
      double dGrossWeightReleasedA = 0D;
      double dGrossWeightRFT = 0D;
      double dBGrade = 0D;
      double dNett = 0D;


      try
      {
        pStartDate = cmath.getDateTimeStr(pStartDate);
        pEndDate = cmath.getDateTimeStr(pEndDate);

        string sql = "EXECUTE [sts].[Sp_OEE_Production_Totals_NGX] '" + pStartDate + "','" + pEndDate + "'";

        cSQL.OpenDB("NGXMES");
        DataTable dt = cSQL.GetDataTable(sql);

        foreach (DataRow item in dt.Rows)
        {
          dGrossWeightAll = cmath.getd(item["ADMT_PlusWriteoff"].ToString());             //GP
          dGrossWeightWriteOff = cmath.getd(item["ADMT_WriteOff"].ToString());            //TBS
          dBGrade = cmath.getd(item["ADMT_Released_B-Grade"].ToString());                 //B-Grade
          dNett += cmath.getd(item["ADMT"].ToString());                                   //2021-03-11 for calcs only
          //dGrossWeightReleasedA = cmath.getd(item["ADMT_Released_A-Grade"].ToString()); //PPAG
          dGrossWeightReleasedA = dNett - dBGrade;                                        //PPAG 2021-03-11

          //dGrossWeightRFT = cmath.getd(item["ADMT_RFT_ProdHit"].ToString());            //RFT
          dGrossWeightRFT = dGrossWeightReleasedA;                                        //RFT 2021-03-11
        }

        cSQL.CloseDB();

        if ((sqlDayID == 0) && (sqlMonthID > 0))
        {
          SaveMonths(sqlMonthID, "GP", dGrossWeightAll);
          SaveMonths(sqlMonthID, "DKL", dGrossWeightReject);
          SaveMonths(sqlMonthID, "TBS", dGrossWeightWriteOff);
          SaveMonths(sqlMonthID, "PPAG", dGrossWeightReleasedA);
          SaveMonths(sqlMonthID, "RFT", dGrossWeightRFT);
        }

        if ((sqlDayID > 0) && (sqlMonthID == 0))
        {
          SaveDaily(sqlDayID, "GP", dGrossWeightAll);
          SaveDaily(sqlDayID, "DKL", dGrossWeightReject);
          SaveDaily(sqlDayID, "TBS", dGrossWeightWriteOff);
          SaveDaily(sqlDayID, "PPAG", dGrossWeightReleasedA);
          SaveDaily(sqlDayID, "RFT", dGrossWeightRFT);
        }

        if (sqlWeekID > 0)
        {
          SaveWeekly(sqlWeekID, "GP", dGrossWeightAll);
          SaveWeekly(sqlWeekID, "DKL", dGrossWeightReject);
          SaveWeekly(sqlWeekID, "TBS", dGrossWeightWriteOff);
          SaveWeekly(sqlWeekID, "PPAG", dGrossWeightReleasedA);
          SaveWeekly(sqlWeekID, "RFT", dGrossWeightRFT);
        }

      }
      catch (Exception ee)
      {
        cFG.LogIt( "SiteData DoNGX_UT: " + ee.Message);
        throw ee;
      }
      finally
      {
        cSQL.CloseDB();
      }

    }//



    private void DoNGX_DIG2(int sqlMonthID, string pStartDate, string pEndDate, int sqlDayID = 0, int sqlWeekID = 0)
    {

      double dPulpNetProdAct = 0;
      double dQuality = 0;

      double dTotalCooksMTD = 0;
      double dTotalCooksWTD = 0;
      double dTotalCooksDay = 0;

      string oraDateFrom = cmath.OraDateSlash(pStartDate);
      string oraDateTo  = cmath.OraDateSlash(pEndDate);


      //production-----------------------------------------------------------
      try
      {

        cORA.OpenDB("SAPXMIINGX2");

        cORA.cmd = cORA.con.CreateCommand();
        cORA.cmd.CommandText = "sapxmii.api_manufacturing_efficiency_model.budget_and_actual_production_of";
        cORA.cmd.CommandType = CommandType.StoredProcedure;

        OracleCommandBuilder.DeriveParameters(cORA.cmd);
        cORA.cmd.BindByName = true;
        cORA.cmd.Parameters["I_PLANTID"].Value = "DG2";
        cORA.cmd.Parameters["i_date_from"].Value = oraDateFrom;
        cORA.cmd.Parameters["i_date_to"].Value = oraDateTo;

        DataTable dt = cORA.ExecuteQuerySp(cORA.cmd);

        cORA.CloseDB();

        foreach (DataRow item in dt.Rows)
        {
          dPulpNetProdAct += cmath.getd(item["Actual_Production"].ToString());
        }



        //quality---------------------------------------------------------------
        //taken from the package in OEE    "PCK_OEE1.getQualityTypeQ";

        oraDateFrom = cmath.OraDateMon(pStartDate);
        oraDateTo = cmath.OraDateMon(pEndDate);

        string sql = "select round((sum(e.quality) / sum(100) * 100),2) quality , round(sum(e.quality),5) qualitytotal,  sum(100) recs"
          + " from event e"
          + " where e.flid = " + sAreaID
          + " and e.eventtype = 'Q'"
          + " and e.quality <> 0"
          + " and trunc(e.datefrom) >= " + oraDateFrom
          + " and trunc(e.datefrom) <= " + oraDateTo
          + "";

        dt = null;
        cORA.OpenDB("NGX");
        dt = cORA.GetDataTable(sql);
        foreach (DataRow item in dt.Rows)
        {
          dQuality = cmath.getd(item["quality"].ToString());
        }

        cORA.CloseDB();


        //total cooks -------------------------------------
        pStartDate = cmath.getDateStr(pStartDate);
        pEndDate = cmath.getDateStr(pEndDate);

        dTotalCooksWTD = 0;
        dTotalCooksMTD = 0;
        dTotalCooksDay = 0;
        dt = null;
        
        cSQL.OpenDB("NGXVSS04");

        if ((sqlDayID == 0) && (sqlMonthID > 0))        //month
        {
          sql = "SELECT  sum(dig2) cooks_per_day  FROM Ngx_Cooks"
            + " where cooks_date >= '" + pStartDate + "'"
            + " and cooks_date <= '" + pEndDate + "'"
            + "";

          dt = cSQL.GetDataTable(sql);
          foreach (DataRow item in dt.Rows)
          {
            dTotalCooksMTD = cmath.getd(item["cooks_per_day"].ToString());
          }
        }



        if ((sqlDayID > 0) && (sqlMonthID == 0))        // day
        {
          sql = "SELECT dig2 as cooks_per_day FROM Ngx_Cooks"
            + " where cooks_date = '" + pStartDate + "'"
            + "";

          dt = cSQL.GetDataTable(sql);
          foreach (DataRow item in dt.Rows)
          {
            dTotalCooksDay = cmath.getd(item["cooks_per_day"].ToString());
          }

        }

        if (sqlWeekID > 0)                          // week
        {
          sql = "SELECT  sum(dig2) cooks_per_day  FROM Ngx_Cooks"
            + " where cooks_date >= '" + pStartDate + "'"
            + " and cooks_date <= '" + pEndDate + "'"
            + "";

          dt = cSQL.GetDataTable(sql);
          foreach (DataRow item in dt.Rows)
          {
            dTotalCooksWTD = cmath.getd(item["cooks_per_day"].ToString());
          }

        }


        cSQL.CloseDB();


        if ((sqlDayID == 0) && (sqlMonthID > 0))
        {
          SaveMonths(sqlMonthID, "GP", dPulpNetProdAct);
          SaveMonths(sqlMonthID, "AGDC", dTotalCooksMTD);
          SaveMonths(sqlMonthID, "DKL", 0);
          SaveMonths(sqlMonthID, "ADQPP", dQuality);
        }

        if ((sqlDayID > 0) && (sqlMonthID == 0))
        {
          SaveDaily(sqlDayID, "GP", dPulpNetProdAct);
          SaveDaily(sqlDayID, "AGDC", dTotalCooksDay);
          SaveDaily(sqlDayID, "DKL", 0);
          SaveDaily(sqlDayID, "ADQPP", dQuality);
        }

        if (sqlWeekID > 0)
        {
          SaveWeekly(sqlWeekID, "GP", dPulpNetProdAct);
          SaveWeekly(sqlWeekID, "AGDC", dTotalCooksWTD);
          SaveWeekly(sqlWeekID, "DKL", 0);
          SaveWeekly(sqlWeekID, "ADQPP", dQuality);
        }

      }
      catch (Exception ee)
      {
        cFG.LogIt("SiteData DoNGX_DIG2: " + ee.Message);
        throw ee;
      }
      finally
      {
        cORA.CloseDB();
      }

    }//



    private void DoNGX_FL3(int sqlMonthID, string pStartDate, string pEndDate, int sqlDayID = 0, int sqlWeekID = 0)
    {
      // 2022-01-21  C-2110-0242 MEM digesters Gross production for NGX No 3 Fibreline now reading from NIS Production (TAGS)

      string sql = "";

      string oraDateFrom  = cmath.OraDateMon(pStartDate);
      string oraDateTo = cmath.OraDateMon(pEndDate);

      double dTotalCooksMTD = 0;
      double dTotalCooksWTD = 0;
      double dTotalCooksDay = 0;

      double dDigProdTons = 0;
      double dQuality = 0;


      pStartDate = cmath.getDateStr(pStartDate);
      pEndDate = cmath.getDateStr(pEndDate);

      try
      {

        //the GP total

        sql = "Select sum(production_figure) as prodfig  from PRODUCTION_FIGURES"
              + " where production_date >= " + oraDateFrom + " And production_date <= " + oraDateTo
              + " and plantid = 'PI'"
              + " and grade = 'FL3'";


        cORA.OpenDB("NGXNIS");
        DataTable dt = cORA.GetDataTable(sql);
        cORA.CloseDB();
        foreach (DataRow item in dt.Rows)
        {
          dDigProdTons = cmath.getd(item["prodfig"].ToString());
        }


        //quality-------------------------------------

        //taken from the package in OEE    "PCK_OEE1.getQualityTypeQ";
        cORA.OpenDB("NGX");
        sql = "select round((sum(e.quality) / sum(100) * 100),2) quality ,  sum(e.quality) qualitytotal,  sum(100) recs"
          + " from event e"
          + " where e.flid = " + sAreaID
          + " and e.eventtype = 'Q'"
          + " and e.quality <> 0"
          + " and trunc(e.datefrom) >= " + oraDateFrom
          + " and trunc(e.datefrom) <= " + oraDateTo
          + "";

        dt = null;
        dt = cORA.GetDataTable(sql);
        cORA.CloseDB();

        foreach (DataRow item in dt.Rows)
        {
          dQuality = cmath.getd(item["quality"].ToString());
        }


        //------------------total cooks -------------------------------------
        dTotalCooksWTD = 0;
        dTotalCooksMTD = 0;
        dTotalCooksDay = 0;
        dt = null;

        cSQL.OpenDB("NGXVSS04");

        if ((sqlDayID == 0) && (sqlMonthID > 0))        //month
        {
          sql = "SELECT  sum(cooks_per_day) cooks_per_day  FROM Ngx_Cooks"
            + " where cooks_date >= '" + pStartDate + "'"
            + " and cooks_date <= '" + pEndDate + "'"
            + "";

          dt = cSQL.GetDataTable(sql);
          foreach (DataRow item in dt.Rows)
          {
            dTotalCooksMTD = cmath.getd(item["cooks_per_day"].ToString());
          }
        }



        if ((sqlDayID > 0) && (sqlMonthID == 0))        // day
        {
          sql = "SELECT cooks_per_day  FROM Ngx_Cooks"
            + " where cooks_date = '" + pStartDate + "'"
            + "";

          dt = cSQL.GetDataTable(sql);
          foreach (DataRow item in dt.Rows)
          {
            dTotalCooksDay = cmath.getd(item["cooks_per_day"].ToString());
          }

        }

        if (sqlWeekID > 0)                          // week
        {
          sql = "SELECT  sum(cooks_per_day) cooks_per_day  FROM Ngx_Cooks"
            + " where cooks_date >= '" + pStartDate + "'"
            + " and cooks_date <= '" + pEndDate + "'"
            + "";

          dt = cSQL.GetDataTable(sql);
          foreach (DataRow item in dt.Rows)
          {
            dTotalCooksWTD = cmath.getd(item["cooks_per_day"].ToString());
          }

        }

        cSQL.CloseDB();


        if ((sqlDayID == 0) && (sqlMonthID > 0))
        {
          SaveMonths(sqlMonthID, "GP", dDigProdTons);
          SaveMonths(sqlMonthID, "AGDC", dTotalCooksMTD);
          SaveMonths(sqlMonthID, "DKL", 0);
          SaveMonths(sqlMonthID, "ADQPP", dQuality);
        }

        if ((sqlDayID > 0) && (sqlMonthID == 0))
        {
          SaveDaily(sqlDayID, "GP", dDigProdTons);
          SaveDaily(sqlDayID, "AGDC", dTotalCooksDay);
          SaveDaily(sqlDayID, "DKL", 0);
          SaveDaily(sqlDayID, "ADQPP", dQuality);
        }

        if (sqlWeekID > 0)
        {
          SaveWeekly(sqlWeekID, "GP", dDigProdTons);
          SaveWeekly(sqlWeekID, "AGDC", dTotalCooksWTD);
          SaveWeekly(sqlWeekID, "DKL", 0);
          SaveWeekly(sqlWeekID, "ADQPP", dQuality);
        }

      }
      catch (Exception ee)
      {
        cFG.LogIt("SiteData DoNGX_FL3: " + ee.Message);
        throw ee;
      }
      finally
      {
        cORA.CloseDB();
        cSQL.CloseDB();
      }

    }//



    private void DoSAI_Continua(int sqlMonthID, string pStartDate, string pEndDate, int sqlDayID = 0, int sqlWeekID = 0)
    {
      /*
       * FIELDS RETURNED FROM QUERY
       *  prodLine
          PulpType
          GrossWeight
          GrossWeight_Reject
          GrossWeight_All 
          GrossWeight_WriteOff
          GrossWeight_Released_A - Grade
          GrossWeight_Released_B - Grade
          GrossWeight_CampaignProd_Total
          GrossWeight_RFT_ProdHit
          RFT_Perc
     */

      double dGrossWeightAll = 0D;
      double dGrossWeightReject = 0D;
      double dGrossWeightWriteOff = 0D;
      double dGrossWeightReleasedA = 0D;
      double dGrossWeightRFT = 0D;
      double dChinaBGrade = 0D;           //2021-02-16
      double dBGrade = 0D;                //2021-03-11  
      double dNett = 0D;                  //2021-03-11

      pStartDate = cmath.getDateTimeStr(pStartDate);
      pEndDate = cmath.getDateTimeStr(pEndDate);

      string sql = "EXECUTE [sts].[Sp_OEE_Production_Totals_Saiccor] '" + pStartDate + "','" + pEndDate + "'";

      try
      {
        cSQL.OpenDB("SAIMES");
        DataTable dt = cSQL.GetDataTable(sql, true);
        cSQL.CloseDB();

        foreach (DataRow item in dt.Rows)
        {

          if ((sAreaID == "4") && (item["prodLine"].ToString() == "PM3"))
          {
            dGrossWeightAll += cmath.getd(item["ADMT_PlusWriteoff"].ToString());           //GP
            dGrossWeightWriteOff += cmath.getd(item["ADMT_WriteOff"].ToString());          //TBS    shrinkage

            //dGrossWeightReleasedA += cmath.getd(item["ADMT_Released_A-Grade"].ToString()); //PPAG  old
            //2021-03-11 prime production = Nett - bGrade
            dNett += cmath.getd(item["ADMT"].ToString());                                 //2021-03-11 for calcs only
            dBGrade += cmath.getd(item["ADMT_Released_B-Grade"].ToString());              //bgrade 2021-03-11 for calcs only
            dGrossWeightReleasedA = dNett - dBGrade;                                      //PPAG   2021-03-11

            dGrossWeightRFT += cmath.getd(item["ADMT_RFT_ProdHit"].ToString());            //RFT
            dChinaBGrade += cmath.getd(item["ADMT_Released_China_B-Grade"].ToString());    //K136
          }


          if ((sAreaID == "5") && (item["prodLine"].ToString() == "PM4"))
          { //B batches and R rolls
            dGrossWeightAll += cmath.getd(item["ADMT_PlusWriteoff"].ToString());           //GP
            dGrossWeightWriteOff += cmath.getd(item["ADMT_WriteOff"].ToString());          //TBS    shrinkage

            //dGrossWeightReleasedA += cmath.getd(item["ADMT_Released_A-Grade"].ToString()); //PPAG  old
            //2021-03-11 prime production = Nett - bGrade
            dNett += cmath.getd(item["ADMT"].ToString());                                 //2021-03-11 for calcs only
            dBGrade += cmath.getd(item["ADMT_Released_B-Grade"].ToString());              //bgrade 2021-03-11 for calcs only
            dGrossWeightReleasedA = dNett - dBGrade;                                      //PPAG   2021-03-11

            dGrossWeightRFT += cmath.getd(item["ADMT_RFT_ProdHit"].ToString());            //RFT
            dChinaBGrade += cmath.getd(item["ADMT_Released_China_B-Grade"].ToString());    //K136
          }


          if ((sAreaID == "6") && (item["prodLine"].ToString() == "PM5"))
          {
            dGrossWeightAll += cmath.getd(item["ADMT_PlusWriteoff"].ToString());           //GP
            dGrossWeightWriteOff += cmath.getd(item["ADMT_WriteOff"].ToString());          //TBS    shrinkage

            //dGrossWeightReleasedA += cmath.getd(item["ADMT_Released_A-Grade"].ToString()); //PPAG  old
            //2021-03-11 prime production = Nett - bGrade
            dNett += cmath.getd(item["ADMT"].ToString());                                 //2021-03-11 for calcs only
            dBGrade += cmath.getd(item["ADMT_Released_B-Grade"].ToString());              //bgrade 2021-03-11 for calcs only
            dGrossWeightReleasedA = dNett - dBGrade;                                      //PPAG   2021-03-11

            dGrossWeightRFT += cmath.getd(item["ADMT_RFT_ProdHit"].ToString());            //RFT
            dChinaBGrade += cmath.getd(item["ADMT_Released_China_B-Grade"].ToString());    //K136
          }

        }


        if ((sqlDayID == 0) && (sqlMonthID > 0))
        {
          SaveMonths(sqlMonthID, "GP", dGrossWeightAll);
          SaveMonths(sqlMonthID, "DKL", dGrossWeightReject);
          SaveMonths(sqlMonthID, "TBS", dGrossWeightWriteOff);
          SaveMonths(sqlMonthID, "PPAG", dGrossWeightReleasedA);
          SaveMonths(sqlMonthID, "RFT", dGrossWeightRFT);
          SaveMonths(sqlMonthID, "KPI0136", dChinaBGrade);
          //cFG.LogIt("month=" + sqlMonthID + " startdate=" + pStartDate + " enddate=" + pEndDate +  " GP=" + dGrossWeightAll + " TBS=" + dGrossWeightWriteOff + " kpi0033 " + (dGrossWeightAll - dGrossWeightWriteOff));
        }


        if ((sqlDayID > 0) && (sqlMonthID == 0))
        {
          SaveDaily(sqlDayID, "GP", dGrossWeightAll);
          SaveDaily(sqlDayID, "DKL", dGrossWeightReject);
          SaveDaily(sqlDayID, "TBS", dGrossWeightWriteOff);
          SaveDaily(sqlDayID, "PPAG", dGrossWeightReleasedA);
          SaveDaily(sqlDayID, "RFT", dGrossWeightRFT);
          SaveDaily(sqlDayID, "KPI0136", dChinaBGrade);
        }

        if (sqlWeekID > 0)
        {
          SaveWeekly(sqlWeekID, "GP", dGrossWeightAll);
          SaveWeekly(sqlWeekID, "DKL", dGrossWeightReject);
          SaveWeekly(sqlWeekID, "TBS", dGrossWeightWriteOff);
          SaveWeekly(sqlWeekID, "PPAG", dGrossWeightReleasedA);
          SaveWeekly(sqlWeekID, "RFT", dGrossWeightRFT);
          SaveWeekly(sqlWeekID, "KPI0136", dChinaBGrade);
        }

      }
      catch (Exception ee)
      {
        cFG.LogIt("SiteData DoSAI: " + ee.Message);
        throw ee;
      }
      finally
      {
        cSQL.CloseDB();
      }

    }//



    private void DoSAI_DIG(int sqlMonthID, string pStartDate, string pEndDate, int sqlDayID = 0, int sqlWeekID = 0)
    {

      double dPulpNetProdAct = 0D;
      double dQuality = 0;
      int iNumCooks = 0;
      
      pStartDate = cmath.getDateStr(pStartDate);
      pEndDate = cmath.getDateStr(pEndDate);


      string sql = "EXECUTE [sts].[Sp_MEM_Digester_Upload] '" + pStartDate + "','" + pEndDate + "'";

      try
      {
        cSQL.OpenDB("SAIMES");
        DataTable dt = cSQL.GetDataTable(sql);
        cSQL.CloseDB();

        foreach (DataRow item in dt.Rows)
        {
          string str = item["DigesterBatch"].ToString();

          if ((sAreaID == "1") && (item["DigesterBatch"].ToString() == "MGO2"))
          {
            dPulpNetProdAct = cmath.getd(item["GrossDigesterPulpProduction"].ToString());
            iNumCooks = cmath.geti(item["CookCount"].ToString());
            dQuality = cmath.getd(item["Quality"].ToString());
          }


          if ((sAreaID == "2") && (item["DigesterBatch"].ToString() == "MGO1"))
          {
            dPulpNetProdAct = cmath.getd(item["GrossDigesterPulpProduction"].ToString());
            iNumCooks = cmath.geti(item["CookCount"].ToString());
            dQuality = cmath.getd(item["Quality"].ToString());
          }


          if ((sAreaID == "3") && (item["DigesterBatch"].ToString() == "CAO"))
          {
            dPulpNetProdAct = cmath.getd(item["GrossDigesterPulpProduction"].ToString());
            iNumCooks = cmath.geti(item["CookCount"].ToString());
            dQuality = cmath.getd(item["Quality"].ToString());
          }

          if ((sAreaID == "65") && (item["DigesterBatch"].ToString() == "MGO3"))
          {
            dPulpNetProdAct = cmath.getd(item["GrossDigesterPulpProduction"].ToString());
            iNumCooks = cmath.geti(item["CookCount"].ToString());
            dQuality = cmath.getd(item["Quality"].ToString());
          }

        }


        if ((sqlDayID == 0) && (sqlMonthID > 0))
        {
          SaveMonths(sqlMonthID, "GP", dPulpNetProdAct);
          SaveMonths(sqlMonthID, "AGDC", iNumCooks);
          SaveMonths(sqlMonthID, "DKL", 0);
          SaveMonths(sqlMonthID, "ADQPP", dQuality);
        }


        if ((sqlDayID > 0) && (sqlMonthID == 0))
        {
          SaveDaily(sqlDayID, "GP", dPulpNetProdAct);
          SaveDaily(sqlDayID, "AGDC", iNumCooks);
          SaveDaily(sqlDayID, "DKL", 0);
          SaveDaily(sqlDayID, "ADQPP", dQuality);
        }

        if (sqlWeekID > 0)
        {
          SaveWeekly(sqlWeekID, "GP", dPulpNetProdAct);
          SaveWeekly(sqlWeekID, "AGDC", iNumCooks);
          SaveWeekly(sqlWeekID, "DKL", 0);
          SaveWeekly(sqlWeekID, "ADQPP", dQuality);
        }

      }
      catch (Exception ee)
      {
        cFG.LogIt("SiteData DoSAI_DIG: " + ee.Message);
        throw ee;
      }
      finally
      {
        cSQL.CloseDB();
      }

    }//



    private void DoStanger_PM(int sqlMonthID, string pStartDate, string pEndDate, int sqlDayID = 0, int sqlWeekID = 0)
    {
      string sql = "";

      int flid = cmath.geti(sAreaID);
      double dGross = 0;
      double dPrimeProd = 0;
      double dBrokeShrinkage = 0;
      double dDKL = 0;
      double dStockMovement = 0D;

      pStartDate = cmath.OraDate(pStartDate);
      pEndDate = cmath.OraDate(pEndDate);


      sql = " Select p.machine_code"
       + ", Sum(nvl(p.machine_gross, 0)) Gross"
       + ", Sum(nvl(p.broke, 0))  Broke"
       + ", sum(p.stock_movement) stock_movement"
       + ", sum(nvl(P.Output, 0)) as ActualNetProd"
       + " From daily_production p"
       + " where decode(p.machine_code, 'ST', 12, 'P1', 6) = " + sAreaID
       + " and trunc(p.report_date) >= " + pStartDate 
       + " and trunc(p.report_date) <= " + pEndDate
       + " group by p.machine_code"
       + "";

      try
      {
        cORA.OpenDB("SPIS");
        DataTable dt = cORA.GetDataTable(sql);
        foreach (DataRow item in dt.Rows)
        {

          dGross = cmath.getd(item["Gross"].ToString());
          dPrimeProd = cmath.getd(item["ActualNetProd"].ToString());
          dBrokeShrinkage = cmath.getd(item["Broke"].ToString());
          dStockMovement = cmath.getd(item["stock_movement"].ToString());
        }


        if (flid == 6)
        {//2020-12-01
          sql = "Select round(sum(TMP_DKL.DklLossMass),5) DKL"
          + " FROM    ("
          + "       select w.mps_number,"
          + "         pck_paper_mps.fncGetMaterial(w.mps_number) Material,"
          + "         pck_sap_material.GetMaterialDescription(pck_paper_mps.fncGetMaterial(w.mps_number)) Product,"
          + "         w.item_number,"
          + "         pck_pmdeckle.fncGetPWDeckle(w.mps_number, w.item_number) MpsItemDeckle,"
          + "         pck_pmdeckle.fncGetPMMaxDeckle(w.mps_number) MpsMaxDeckle,"
          + "         pck_pmdeckle.fncGetPWDklPerc(w.mps_number, w.item_number) DklPerc,"
          + "         (sum(w.nett_mass) / 1000) GCP,"
          + "         (sum(w.nett_mass) / 1000) * (1 + pck_pmdeckle.fncGetPWDklPerc(w.mps_number, w.item_number) / 100) FullDklMass,"
          + "         (sum(w.nett_mass) / 1000) * (1 + pck_pmdeckle.fncGetPWDklPerc(w.mps_number, w.item_number) / 100) - (sum(w.nett_mass) / 1000)DklLossMass"
          + "         from paper_webs w"
          + "         where w.jumbo_tappi in"
          + "           (  select cj.jumbo_tappi"
          + "               From current_jumbo_status cj"
          + "               where trunc(cj.status_date) >=  " + pStartDate
          + "               and trunc(cj.status_date) <= " + pEndDate
          + "               and cj.current_status = 'CP'"
          + "           )"
          + "       group by w.mps_number, w.item_number"
          + " ) TMP_DKL"
          + "";

          dt = cORA.GetDataTable(sql);


          foreach (DataRow item in dt.Rows)
          {
            dDKL = cmath.getd(item["DKL"].ToString());
          }

          dBrokeShrinkage -= dDKL;

        }//


        if (flid == 12)
        {

          sql = " select round(sum(TMP_DKL.DklLossMass),5) DeckleLoss"
            + " FROM"
            + " (select w.mps_number,"
            + "    (sum(w.nett_mass) / 1000) * (1 + (select   100 - (tm.ave_deckle / tm.max_deckle * 100)"
            + "                                              from tissue_mps tm"
            + "                                              where tm.mps_number = w.mps_number"
            + "                                      ) / 100)  - (sum(w.nett_mass) / 1000) DklLossMass"
            + " from   tissue_bundles w"
            + " where w.jumbo_tappi_1 in"
            + " ("
            + "   select cj.jumbo_tappi"
            + "   From current_jumbo_status cj"
            + "   where trunc(cj.status_date) >= " + pStartDate
            + "         and trunc(cj.status_date) <= " + pEndDate
            + "         and cj.current_status = 'CT')"
            + "        or w.jumbo_tappi_2 in"
            + "         ("
            + "            select cj.jumbo_tappi From current_jumbo_status cj"
            + "            Where trunc(cj.status_date) >= " + pStartDate
            + "            and trunc(cj.status_date) <= " + pEndDate
            + "            and cj.current_status = 'CT'"
            + "         )"
            + " group by w.mps_number"
            + " ) TMP_DKL";


          dt = cORA.GetDataTable(sql);


          foreach (DataRow item in dt.Rows)
          {
            dDKL = cmath.getd(item["DeckleLoss"].ToString());
          }

          dBrokeShrinkage -= dDKL;

        }//12

        cORA.CloseDB();


        if ((sqlDayID == 0) && (sqlMonthID > 0))
        {
          SaveMonths(sqlMonthID, "GP", dGross);
          SaveMonths(sqlMonthID, "PPAG", dPrimeProd);
          SaveMonths(sqlMonthID, "TBS", dBrokeShrinkage);
          SaveMonths(sqlMonthID, "RFT", dPrimeProd);
          SaveMonths(sqlMonthID, "DKL", dDKL);
        }

        if ((sqlDayID > 0) && (sqlMonthID == 0))
        {
          SaveDaily(sqlDayID, "GP", dGross);
          SaveDaily(sqlDayID, "PPAG", dPrimeProd);
          SaveDaily(sqlDayID, "TBS", dBrokeShrinkage);
          SaveDaily(sqlDayID, "RFT", dPrimeProd);
          SaveDaily(sqlDayID, "DKL", dDKL);
        }


        if (sqlWeekID > 0)
        {
          SaveWeekly(sqlWeekID, "GP", dGross);
          SaveWeekly(sqlWeekID, "PPAG", dPrimeProd);
          SaveWeekly(sqlWeekID, "TBS", dBrokeShrinkage);
          SaveWeekly(sqlWeekID, "RFT", dPrimeProd);
          SaveWeekly(sqlWeekID, "DKL", dDKL);
        }

      }
      catch (Exception ee)
      {
        cFG.LogIt( "SiteData DoStanger_PM: " + ee.Message);
        throw ee;
      }
      finally
      {
        cORA.CloseDB();
      }

    }//



    private void DoSTA_DIG(int sqlMonthID, string pStartDate, string pEndDate, int sqlDayID = 0, int sqlWeekID = 0)
    {

      double dPulpNetProdAct = 0;
      double dQuality = 0;


      pStartDate = cmath.OraDate(pStartDate);
      pEndDate = cmath.OraDate(pEndDate);


      try
      {
        //ACTUAL PRODUCTION and quality
        string sql = "select  round(sum(gross_mass),5) gross_mass, round(avg(quality_rate),5) quality_rate"
            + " from pulp_production"
            + " where  creation_date >= " + pStartDate
            + " and creation_date <= " +  pEndDate
            + "";

        cORA.OpenDB("SPIS");
        DataTable dt = cORA.GetDataTable(sql);
        cORA.CloseDB();

        foreach (DataRow item in dt.Rows)
        {
          dPulpNetProdAct = cmath.getd(item["gross_mass"].ToString());
          dQuality = cmath.getd(item["quality_rate"].ToString());
        }


        if ((sqlDayID == 0) && (sqlMonthID > 0))
        {
          SaveMonths(sqlMonthID, "GP", dPulpNetProdAct);
          SaveMonths(sqlMonthID, "AGDC", 0);
          SaveMonths(sqlMonthID, "DKL", 0);
          SaveMonths(sqlMonthID, "ADQPP", dQuality);
        }

        if ((sqlDayID > 0) && (sqlMonthID == 0))
        {
          SaveDaily(sqlDayID, "GP", dPulpNetProdAct);
          SaveDaily(sqlDayID, "AGDC", 0);
          SaveDaily(sqlDayID, "DKL", 0);
          SaveDaily(sqlDayID, "ADQPP", dQuality);
        }

        if (sqlWeekID > 0)
        {
          SaveWeekly(sqlWeekID, "GP", dPulpNetProdAct);
          SaveWeekly(sqlWeekID, "AGDC", 0);
          SaveWeekly(sqlWeekID, "DKL", 0);
          SaveWeekly(sqlWeekID, "ADQPP", dQuality);
        }

      }
      catch (Exception ee)
      {
        cFG.LogIt("SiteData DoSTA_DIG: " + ee.Message);
        throw ee;
      }
      finally
      {
        cORA.CloseDB();
      }

    }//



    private void DoTugela_PM(int sqlMonthID, string pStartDate, string pEndDate, int sqlDayID = 0, int sqlWeekID = 0)
    {
      string sql = "";

      double dGross = 0;
      double dPrimeProd = 0;
      double dShrinkage = 0;
      double dDKL = 0;


      //--GP gross
      //--TBS = broke
      //--PPAG nettProd
      //--RFT same as nettProd
      //--DKL = 0

      pStartDate = cmath.OraDate(pStartDate);
      pEndDate = cmath.OraDate(pEndDate);

      sql = "Select p.station_code"
        + " ,sum(nvl(p.product_gross, 0) / 1000) as Gross"
        + " ,sum(nvl(p.product_gross, 0) / 1000) - sum(p.passed + p.substd - p.returns) / 1000 as Broke"
        + " ,sum(p.passed) / 1000 as NettProd"
        + " from  pprd_commodity p"
        + " where decode(p.station_code ,'PM2', 118) =  " + sAreaID
       + " and trunc(p.report_date) >= " + pStartDate
       + " and trunc(p.report_date) <= " + pEndDate
       + " group by p.station_code"
       + "";

      try
      {
        cORA.OpenDB("PROG_REP");
        DataTable dt = cORA.GetDataTable(sql);
        cORA.CloseDB();
        foreach (DataRow item in dt.Rows)
        {
          dGross = cmath.getd(item["Gross"].ToString());
          //dPrimeProd = cmath.getd(item["NettProd"].ToString());   //2021-02-01 
        }


        //get the rest from SAPXMII
        sql = "Select p.station_code"
          + " , sum(nvl(p.gross, 0) / 1000) as gross"
          + " , sum(nvl(p.pm_mkt_loss, 0) / 1000) as deckle_market_loss"
          + " , sum(nvl(p.shr, 0) / 1000) as shrinkage"
          + " from pts_grade_summary p"
          + " where p.station_code = 'PM2'"
          + " and trunc(p.date_production) >= " + pStartDate
          + " and trunc(p.date_production) <= " + pEndDate
          + " group by p.station_code"
          + "";

        cORA.OpenDB("SAPXMII");
        dt = cORA.GetDataTable(sql);
        cORA.CloseDB();
        foreach (DataRow item in dt.Rows)
        {
          dDKL = cmath.getd(item["deckle_market_loss"].ToString());
          dShrinkage = cmath.getd(item["shrinkage"].ToString());
        }

        dShrinkage = dShrinkage - dDKL;

        //2021-02-01 as per eric email PPAG == nett saleable ( also RFT = nett saleable)
        dPrimeProd = dGross - dDKL - dShrinkage;  

        //--GP gross
        //--TBS = broke
        //--PPAG nettProd
        //--RFT same as nettProd
        //--DKL = 0

        if ((sqlDayID == 0) && (sqlMonthID > 0))
        {
          SaveMonths(sqlMonthID, "GP", dGross);
          SaveMonths(sqlMonthID, "PPAG", dPrimeProd);
          SaveMonths(sqlMonthID, "TBS", dShrinkage);
          SaveMonths(sqlMonthID, "RFT", dPrimeProd);
          SaveMonths(sqlMonthID, "DKL", dDKL);
        }


        if ((sqlDayID > 0) && (sqlMonthID == 0))
        {
          SaveDaily(sqlDayID, "GP", dGross);
          SaveDaily(sqlDayID, "PPAG", dPrimeProd);
          SaveDaily(sqlDayID, "TBS", dShrinkage);
          SaveDaily(sqlDayID, "RFT", dPrimeProd);
          SaveDaily(sqlDayID, "DKL", dDKL);
        }


        if (sqlWeekID > 0)
        {
          SaveWeekly(sqlWeekID, "GP", dGross);
          SaveWeekly(sqlWeekID, "PPAG", dPrimeProd);
          SaveWeekly(sqlWeekID, "TBS", dShrinkage);
          SaveWeekly(sqlWeekID, "RFT", dPrimeProd);
          SaveWeekly(sqlWeekID, "DKL", dDKL);
        }


      }
      catch (Exception ee)
      {
        cFG.LogIt( "SiteData DoTugela_PM: " + ee.Message);
        throw ee;
      }
      finally
      {
        cORA.CloseDB();
      }

    }//



    private void DoTUG_DIG(int sqlMonthID, string pStartDate, string pEndDate, int sqlDayID = 0, int sqlWeekID = 0)
    {

      double dPulpNetProdAct = 0;
      double dQuality = 0;
      double dCationic = 0;
      double dFreeness = 0;

      string sql = "";

      string sSQLStartDate = cmath.getDateStr(pStartDate);
      string sSQLEndDate = cmath.getDateStr(pEndDate);

      pStartDate = cmath.OraDate(pStartDate);
      pEndDate = cmath.OraDate(pEndDate);

      try
      {
        //ACTUAL PRODUCTION
        sql = "select sum(NSSC)NSSC, sum(Waste_Prod) Waste "
          + " from  Daily_Production_Figures"
          + " where Date_production >= " + pStartDate
          + " and Date_production <= " + pEndDate;

        cORA.OpenDB("SAPXMII");
        DataTable dt = cORA.GetDataTable(sql);
        cORA.CloseDB();

        foreach (DataRow item in dt.Rows)
        {
          dPulpNetProdAct = cmath.getd(item["NSSC"].ToString());
        }


        //Actual Digester Quality Pulp Performance Index
        //quality = cationic + freeness / 2    from MSSQL TUGVSS04 -LIS

        cSQL.OpenDB("TUGVSS04");

        sql = "SELECT [dbo].[InSpecVal_ST](5998, + '" + sSQLStartDate + "','" + sSQLEndDate + "', '', '', '')  Cationic";
        DataTable dtCationic = cSQL.GetDataTable(sql);

        sql = "SELECT [dbo].[InSpecVal_ST](268, + '" + sSQLStartDate + "','" + sSQLEndDate + "', '', '', '') Freeness";
        DataTable dtFreeness = cSQL.GetDataTable(sql);

        cSQL.CloseDB();

        foreach (DataRow item in dtCationic.Rows)
        {
          dCationic = cmath.getd(item["Cationic"].ToString());
        }

        foreach (DataRow item in dtFreeness.Rows)
        {
          dFreeness = cmath.getd(item["Freeness"].ToString());
        }

        dQuality = (dCationic + dFreeness) / 2;


        if ((sqlDayID == 0) && (sqlMonthID > 0))
        {
          SaveMonths(sqlMonthID, "GP", dPulpNetProdAct);
          SaveMonths(sqlMonthID, "AGDC", 0);
          SaveMonths(sqlMonthID, "DKL", 0);
          SaveMonths(sqlMonthID, "ADQPP", dQuality);
        }

        if ((sqlDayID > 0) && (sqlMonthID == 0))
        {
          SaveDaily(sqlDayID, "GP", dPulpNetProdAct);
          SaveDaily(sqlDayID, "AGDC", 0);
          SaveDaily(sqlDayID, "DKL", 0);
          SaveDaily(sqlDayID, "ADQPP", dQuality);
        }

        if (sqlWeekID > 0)
        {
          SaveWeekly(sqlWeekID, "GP", dPulpNetProdAct);
          SaveWeekly(sqlWeekID, "AGDC", 0);
          SaveWeekly(sqlWeekID, "DKL", 0);
          SaveWeekly(sqlWeekID, "ADQPP", dQuality);
        }

      }
      catch (Exception ee)
      {
        cFG.LogIt("SiteData : DoTUG_Production" + ee.Message);
        throw ee;
      }
      finally
      {
        cORA.CloseDB();
        cSQL.CloseDB();
      }

    }//



    private void DoPLS(int monthNum, int dayStart, int dayEnd, int sqlMonthID,  int sqlDayID, int sqlWeekID = 0)
    {
      string sql = "";
      double rv = 0D;

      //technical , commercial and other standstills

      byte dec = 1;           //this was the decimal rounding as at end of april 2023
      if ((monthNum >= 8 && iSappiYear >= 2023) || iSappiYear >= 2024)    //from May 2023
      {
        dec = 2;
      }

      cORA.OpenDB(sSite);


      //if (sSite == "SAI" && monthNum >= 5 && iSappiYear >=2022)      //from Feb 2022
      if ((sSite == "SAI" && monthNum >= 5 && iSappiYear >= 2022) || (sSite == "SAI" && iSappiYear >= 2023))      //from Feb 2022
        {
          //Change Request C-2202-0086 : 2022-02-03- Only pull through events >= 5 min from OEE into MEM from February 2022 onwards for all Saiccor Plant in MEM

          sql = " select sum(DTHrs) DTHrs , sum(DTMins) DTMins From  ("
                  + " select SE.EventID, round(nvl(sum(SE.Minutes) / 60, 0), " + dec + " ) as DTHrs"
                  + " , round(nvl(sum(SE.Minutes) , 0), 1) as DTMins"
                  + " from Event E, SubEvent SE, Reasons R"
                  + " where E.FLID = " + sAreaID
                  + " and E.Eventid = SE.EVENTID"
                  + " and E.EVENTTYPE = 'E'"
                  + " and E.ReasonID = R.ReasonID"
                  + " and lower(R.REASON) = 'available time adjustment'"
                  + " and R.Planunplan = " + cFG.CAT_PLANNED
                  + " and SE.Sappi_Year = " + iSappiYear
                  + " and SE.Sappi_month = " + monthNum
                  + " and SE.SAPPI_DAY >= " + dayStart
                  + " and SE.SAPPI_DAY <= " + dayEnd
                  + " group by SE.EventID"
                  + ") s1 "
             + " where  s1.DTMins >= 5"
             + "";
      }
      else
      {
        sql = " select  round(nvl(sum(SE.Minutes) / 60, 0),  " + dec + " ) as DTHrs"
          + " from Event E, SubEvent SE, Reasons R"
          + " where E.FLID = " + sAreaID
          + " and E.Eventid = SE.EVENTID"
          + " and E.EVENTTYPE = 'E'"
          + " and E.ReasonID = R.ReasonID"
          + " and lower(R.REASON) = 'available time adjustment'"
          + " and R.Planunplan = " + cFG.CAT_PLANNED
          + " and SE.Sappi_Year = " + iSappiYear
          + " and SE.Sappi_month = " + monthNum
          + " and SE.SAPPI_DAY >= " + dayStart
          + " and SE.SAPPI_DAY <= " + dayEnd
          + " ";

      }


      try
        {
        DataTable dt = cORA.GetDataTable(sql);
        foreach (DataRow item in dt.Rows)
        {
          rv = cmath.getd(item["DTHrs"].ToString());
        }

        if (rv > 0)
          Application.DoEvents();

        if (sqlMonthID > 0 )
          SaveMonths(sqlMonthID, "PLS", rv);

        if ((sqlDayID > 0 ))
          SaveDaily(sqlDayID, "PLS", rv);

        if ((sqlWeekID > 0))
          SaveWeekly(sqlWeekID, "PLS", rv);


      }
      catch (Exception er)
      {
        cFG.LogIt( "SiteData DoPLS: " + er.Message);
        throw er;
      }
      finally
      {
        cORA.CloseDB();
      }


    }//



    private void DoMPPD(int monthNum, int dayStart, int dayEnd, int sqlMonthID, int sqlDayID, int sqlWeekID = 0)
    {
      string sql = "";

      //maintenance process and PIBP
      double kpiValue = 0.0;


      byte dec = 1;           //this was the decimal rounding as at end of april 2023
      if ((monthNum >= 8 && iSappiYear >= 2023) || iSappiYear >= 2024)    //from May 2023
      {
        dec = 2;
      }

      cORA.OpenDB(sSite);

        //planned
      try
      {
        if ((sSite == "SAI" && monthNum >= 5 && iSappiYear >= 2022) || (sSite == "SAI" && iSappiYear >= 2023))      //from Feb 2022
        {
          //Change Request C-2202-0086 : 2022-02-03- Only pull through events >= 5 min from OEE into MEM from February 2022 onwards for all Saiccor Plant in MEM
          sql = " select sum(DTHrs) DTHrs , sum(DTMins) DTMins From  ("
                    + " select SE.EventID, round(nvl(sum(SE.Minutes) / 60, 0), " + dec + ") as DTHrs"
                  + " , round(nvl(sum(SE.Minutes) , 0), 1) as DTMins"
                  + " from Event E, SubEvent SE, Reasons R"
                  + " where E.FLID = " + sAreaID
                  + " and E.Eventid = SE.EVENTID"
                  + " and E.EVENTTYPE = 'E'"
                  + " and E.ReasonID = R.ReasonID"
                  + " and lower(R.REASON) IN ('equipment', 'process')"
                  + " and R.Planunplan = " + cFG.CAT_PLANNED
                  + " and SE.Sappi_Year = " + iSappiYear
                  + " and SE.Sappi_month = " + monthNum
                  + " and SE.SAPPI_DAY >= " + dayStart
                  + " and SE.SAPPI_DAY <= " + dayEnd
                  + " group by SE.EventID"
                  + ") s1 "
             + " where  s1.DTMins >= 5"
             + "";

        }
        else
        {
          sql = " select round(nvl(sum(SE.Minutes) / 60, 0), " + dec + ") as DTHrs"
            + " from Event E, SubEvent SE, Reasons R"
            + " where E.FLID = " + sAreaID
            + " and E.Eventid = SE.EVENTID"
            + " and E.EVENTTYPE = 'E'"
            + " and E.ReasonID = R.ReasonID"
            + " and lower(R.REASON) IN ('equipment', 'process')"
            + " and R.Planunplan = " + cFG.CAT_PLANNED
            + " and SE.Sappi_Year = " + iSappiYear
            + " and SE.Sappi_month = " + monthNum
            + " and SE.SAPPI_DAY >= " + dayStart
            + " and SE.SAPPI_DAY <= " + dayEnd
            + " ";
        }

        DataTable dt = cORA.GetDataTable(sql);
        foreach (DataRow item in dt.Rows)
        {
          kpiValue = cmath.getd(item["DTHrs"].ToString());
        }



        //unplanned
        if ((sSite == "SAI" && monthNum >= 5 && iSappiYear >= 2022) || (sSite == "SAI" && iSappiYear >= 2023))      //from Feb 2022
        {
          //Change Request C-2202-0086 : 2022-02-03- Only pull through events >= 5 min from OEE into MEM from February 2022 onwards for all Saiccor Plant in MEM
          sql = " select sum(DTHrs) DTHrs , sum(DTMins) DTMins From  ("
                     + " select SE.EventID, round(nvl(sum(SE.Minutes) / 60, 0), " + dec + ") as DTHrs"
                    + " , round(nvl(sum(SE.Minutes) , 0), 1) as DTMins"
                    + " from Event E, SubEvent SE, Reasons R"
                    + " where E.FLID = " + sAreaID
                    + " and E.Eventid = SE.EVENTID"
                    + " and E.EVENTTYPE = 'E'"
                    + " and E.ReasonID = R.ReasonID"
                    + " and lower(R.REASON) IN ('pibp', 'equipment','process')"
                    + " and R.Planunplan = " + cFG.CAT_UNPLANNED
                    + " and SE.Sappi_Year = " + iSappiYear
                    + " and SE.Sappi_month = " + monthNum
                    + " and SE.SAPPI_DAY >= " + dayStart
                    + " and SE.SAPPI_DAY <= " + dayEnd
                  + " group by SE.EventID"
                  + ") s1 "
             + " where  s1.DTMins >= 5"
             + "";

        }
        else
        {
          sql = " select round(nvl(sum(SE.Minutes) / 60, 0), " + dec + ") as DTHrs"
            + " from Event E, SubEvent SE, Reasons R"
            + " where E.FLID = " + sAreaID
            + " and E.Eventid = SE.EVENTID"
            + " and E.EVENTTYPE = 'E'"
            + " and E.ReasonID = R.ReasonID"
            + " and lower(R.REASON) IN ('pibp', 'equipment','process')"
            + " and R.Planunplan = " + cFG.CAT_UNPLANNED
            + " and SE.Sappi_Year = " + iSappiYear
            + " and SE.Sappi_month = " + monthNum
            + " and SE.SAPPI_DAY >= " + dayStart
            + " and SE.SAPPI_DAY <= " + dayEnd
            + " ";
        }

        dt = cORA.GetDataTable(sql);
        foreach (DataRow item in dt.Rows)
        {
          kpiValue += cmath.getd(item["DTHrs"].ToString());
        }

        if (kpiValue > 0)
          Application.DoEvents();


        if (sqlMonthID > 0)
          SaveMonths(sqlMonthID, "MPPD", kpiValue);

        if (sqlDayID > 0)
          SaveDaily(sqlDayID, "MPPD", kpiValue);

        if ((sqlWeekID > 0))
          SaveWeekly(sqlWeekID, "MPPD", kpiValue);


      }
      catch (Exception er)
      {
        cFG.LogIt( "SiteData DoMPPD: " + er.Message);
        throw er;
      }
      finally
      {
        cORA.CloseDB();
      }


    }//



    private void DoCMDT(int monthNum, int dayStart, int dayEnd, int sqlMonthID, int sqlDayID, int sqlWeekID = 0)
    {
      //commercial standstills
      string sql = "";

      double rv = 0D;

      byte dec = 1;
      if ((monthNum >= 8 && iSappiYear >= 2023) || iSappiYear >= 2024)    //from May 2023
      {
        dec = 2;
      }

      cORA.OpenDB(sSite);

      if ((sSite == "SAI" && monthNum >= 5 && iSappiYear >= 2022) || (sSite == "SAI" && iSappiYear >= 2023))      //from Feb 2022
      {
          //Change Request C-2202-0086 : 2022-02-03- Only pull through events >= 5 min from OEE into MEM from February 2022 onwards for all Saiccor Plant in MEM
          sql = " select sum(DTHrs) DTHrs , sum(DTMins) DTMins From  ("
                 + " select SE.EventID, round(nvl(sum(SE.Minutes) / 60, 0), " + dec + ") as DTHrs"
                 + " , round(nvl(sum(SE.Minutes) , 0), 1) as DTMins"
                 + " from Event E, SubEvent SE, Reasons R, Categories C"
                 + " where E.FLID = " + sAreaID
                 + " and E.Eventid = SE.EVENTID"
                 + " and E.EVENTTYPE = 'E'"
                 + " and E.ReasonID = R.ReasonID"
                 + " and lower(R.REASON) = 'available time adjustment'"
                 + " and R.Planunplan = " + cFG.CAT_PLANNED
                 + " and E.categoryid = C.CATEGORYID"
                 + " and C.REASONID = R.REASONID"
                 + " and SE.Sappi_Year = " + iSappiYear
                 + " and SE.Sappi_month = " + monthNum
                 + " and SE.SAPPI_DAY >= " + dayStart
                 + " and SE.SAPPI_DAY <= " + dayEnd
                 + " and lower(C.CATEGORY) IN ('commercial shut')"
                 + " group by SE.EventID"
                 + ") s1 "
             + " where  s1.DTMins >= 5"
             + "";

      }
      else
      {

        sql = " select round(nvl(sum(SE.Minutes) / 60, 0), " + dec + ") as DTHrs"
         + " from Event E, SubEvent SE, Reasons R, Categories C"
         + " where E.FLID = " + sAreaID
         + " and E.Eventid = SE.EVENTID"
         + " and E.EVENTTYPE = 'E'"
         + " and E.ReasonID = R.ReasonID"
         + " and lower(R.REASON) = 'available time adjustment'"
         + " and R.Planunplan = " + cFG.CAT_PLANNED
         + " and E.categoryid = C.CATEGORYID"
         + " and C.REASONID = R.REASONID"
         + " and SE.Sappi_Year = " + iSappiYear
         + " and SE.Sappi_month = " + monthNum
         + " and SE.SAPPI_DAY >= " + dayStart
         + " and SE.SAPPI_DAY <= " + dayEnd
         + " and lower(C.CATEGORY) IN ('commercial shut')"
         + " ";
      }

      try
      {
        DataTable dt = cORA.GetDataTable(sql);
        foreach (DataRow item in dt.Rows)
        {
          rv = cmath.getd(item["DTHrs"].ToString());
        }


        if (rv > 0)
          Application.DoEvents();

        if (sqlMonthID > 0)
          SaveMonths(sqlMonthID, "CMDT", rv);

        if (sqlDayID > 0)
          SaveDaily(sqlDayID, "CMDT", rv);

        if (sqlWeekID > 0)
          SaveWeekly(sqlWeekID, "CMDT", rv);

      }
      catch (Exception er)
      {
        cFG.LogIt( "SiteData DoCMDT: " + er.Message);
        throw er;
      }
      finally
      {
        cORA.CloseDB();
      }
    }//




    private void DoPTS(int monthNum, int dayStart, int dayEnd, int sqlMonthID, int sqlDayID, int sqlWeekID = 0)
    {
      
      string sql = "";

      double rv = 0D;
      byte dec = 1;
      if ((monthNum >= 8 && iSappiYear >= 2023) || iSappiYear >= 2024)    //from May 2023
      {
        dec = 2;
      }

      cORA.OpenDB(sSite);

      if ((sSite == "SAI" && monthNum >= 5 && iSappiYear >= 2022) || (sSite == "SAI" && iSappiYear >= 2023))      //from Feb 2022
      {
        //Change Request C-2202-0086 : 2022-02-03- Only pull through events >= 5 min from OEE into MEM from February 2022 onwards for all Saiccor Plant in MEM
        sql = " select sum(DTHrs) DTHrs , sum(DTMins) DTMins From  ("
                + " select SE.EventID, round(nvl(sum(SE.Minutes) / 60, 0), " + dec + ") as DTHrs"
                  + " , round(nvl(sum(SE.Minutes) , 0), 1) as DTMins"
                  + " from Event E, SubEvent SE, Reasons R, Categories C"
                  + " where E.FLID = " + sAreaID
                  + " and E.Eventid = SE.EVENTID"
                  + " and E.EVENTTYPE = 'E'"
                  + " and E.ReasonID = R.ReasonID"
                  + " and lower(R.REASON) = 'available time adjustment'"
                  + " and R.Planunplan = " + cFG.CAT_PLANNED
                  + " and E.categoryid = C.CATEGORYID"
                  + " and C.REASONID = R.REASONID"
                  + " and SE.Sappi_Year = " + iSappiYear
                  + " and SE.Sappi_month = " + monthNum
                  + " and SE.SAPPI_DAY >= " + dayStart
                  + " and SE.SAPPI_DAY <= " + dayEnd
                  + " and lower(C.CATEGORY) IN ('technical standstill','investment standstill')"
                  + " group by SE.EventID"
                  + ") s1 "
             + " where  s1.DTMins >= 5"
             + "";
      }
      else
      {
        sql = " select round(nvl(sum(SE.Minutes) / 60, 0), " + dec + ") as DTHrs"
          + " from Event E, SubEvent SE, Reasons R, Categories C"
          + " where E.FLID = " + sAreaID
          + " and E.Eventid = SE.EVENTID"
          + " and E.EVENTTYPE = 'E'"
          + " and E.ReasonID = R.ReasonID"
          + " and lower(R.REASON) = 'available time adjustment'"
          + " and R.Planunplan = " + cFG.CAT_PLANNED
          + " and E.categoryid = C.CATEGORYID"
          + " and C.REASONID = R.REASONID"
          + " and SE.Sappi_Year = " + iSappiYear
          + " and SE.Sappi_month = " + monthNum
          + " and SE.SAPPI_DAY >= " + dayStart
          + " and SE.SAPPI_DAY <= " + dayEnd
          + " and lower(C.CATEGORY) IN ('technical standstill','investment standstill')"
          + " ";
      }


      try
      {
        DataTable dt = cORA.GetDataTable(sql);
        foreach (DataRow item in dt.Rows)
        {
          rv = cmath.getd(item["DTHrs"].ToString());
        }

        if (rv > 0)
          Application.DoEvents();

        if (sqlMonthID > 0)
          SaveMonths(sqlMonthID, "PTS", rv);

        if (sqlDayID > 0)
          SaveDaily(sqlDayID, "PTS", rv);

        if (sqlWeekID > 0)
          SaveWeekly(sqlWeekID, "PTS", rv);

      }
      catch (Exception er)
      {
        cFG.LogIt( "SiteData DoPTS: " + er.Message);
        throw er;
      }
      finally
      {
        cORA.CloseDB();
      }


    }//



    private void DoPLO(int monthNum, int dayStart, int dayEnd, int sqlMonthID, int sqlDayID, int sqlWeekID = 0)
    {
      //other standstills
      string sql = "";

      byte dec = 1;
      if ((monthNum >= 8 && iSappiYear >= 2023) || iSappiYear >= 2024)    //from May 2023
      {
        dec = 2;
      }

      double rv = 0D;


      cORA.OpenDB(sSite);
      if ((sSite == "SAI" && monthNum >= 5 && iSappiYear >= 2022) || (sSite == "SAI" && iSappiYear >= 2023))      //from Feb 2022
      {
        //Change Request C-2202-0086 : 2022-02-03- Only pull through events >= 5 min from OEE into MEM from February 2022 onwards for all Saiccor Plant in MEM
        sql = " select sum(DTHrs) DTHrs , sum(DTMins) DTMins From  ("
                  + " select SE.EventID, round(nvl(sum(SE.Minutes) / 60, 0), " + dec + ") as DTHrs"
                  + " , round(nvl(sum(SE.Minutes) , 0), 1) as DTMins"
                  + " from Event E, SubEvent SE, Reasons R, Categories C"
                  + " where E.FLID = " + sAreaID
                  + " and E.Eventid = SE.EVENTID"
                  + " and E.EVENTTYPE = 'E'"
                  + " and E.ReasonID = R.ReasonID"
                  + " and lower(R.REASON) = 'available time adjustment'"
                  + " and R.Planunplan = " + cFG.CAT_PLANNED
                  + " and E.categoryid = C.CATEGORYID"
                  + " and C.REASONID = R.REASONID"
                  + " and SE.Sappi_Year = " + iSappiYear
                  + " and SE.Sappi_month = " + monthNum
                  + " and SE.SAPPI_DAY >= " + dayStart
                  + " and SE.SAPPI_DAY <= " + dayEnd
                  + " and lower(C.CATEGORY) IN ('contractural shuts', 'external services' , 'industrial action','unforeseeable events' )"
                  + " group by SE.EventID"
                  + ") s1 "
             + " where  s1.DTMins >= 5"
             + "";

      }
      else
      {
        sql = " select round(nvl(sum(SE.Minutes) / 60, 0), " + dec + ") as DTHrs"
          + " from Event E, SubEvent SE, Reasons R, Categories C"
          + " where E.FLID = " + sAreaID
          + " and E.Eventid = SE.EVENTID"
          + " and E.EVENTTYPE = 'E'"
          + " and E.ReasonID = R.ReasonID"
          + " and lower(R.REASON) = 'available time adjustment'"
          + " and R.Planunplan = " + cFG.CAT_PLANNED
          + " and E.categoryid = C.CATEGORYID"
          + " and C.REASONID = R.REASONID"
          + " and SE.Sappi_Year = " + iSappiYear
          + " and SE.Sappi_month = " + monthNum
          + " and SE.SAPPI_DAY >= " + dayStart
          + " and SE.SAPPI_DAY <= " + dayEnd
          + " and lower(C.CATEGORY) IN ('contractural shuts', 'external services' , 'industrial action','unforeseeable events' )"
          + " ";
      }

      try
      {
        DataTable dt = cORA.GetDataTable(sql);
        foreach (DataRow item in dt.Rows)
        {
          rv = cmath.getd(item["DTHrs"].ToString());
        }

        if (rv > 0)
          Application.DoEvents();

        if (sqlMonthID > 0)
          SaveMonths(sqlMonthID, "PLO", rv);

        if (sqlDayID > 0)
          SaveDaily(sqlDayID, "PLO", rv);

        if (sqlWeekID > 0)
          SaveWeekly(sqlWeekID, "PLO", rv);


      }
      catch (Exception er)
      {
        cFG.LogIt( "SiteData DoPLO: " + er.Message);
        throw er;
      }
      finally
      {
        cORA.CloseDB();
      }


    }//



    private void DoPLMT(int monthNum, int dayStart, int dayEnd, int sqlMonthID, int sqlDayID, int sqlWeekID = 0)
    {
      //Planned maintenance shuts < 24hrs
      string sql = "";

      byte dec = 1;
      if ((monthNum >= 8 && iSappiYear >= 2023) || iSappiYear >= 2024)    //from May 2023
      {
        dec = 2;
      }

      double rv = 0D;

      cORA.OpenDB(sSite);

      if ((sSite == "SAI" && monthNum >= 5 && iSappiYear >= 2022) || (sSite == "SAI" && iSappiYear >= 2023))      //from Feb 2022
      {
        //Change Request C-2202-0086 : 2022-02-03- Only pull through events >= 5 min from OEE into MEM from February 2022 onwards for all Saiccor Plant in MEM
        sql = " select sum(DTHrs) DTHrs , sum(DTMins) DTMins From  ("
                + " select SE.EventID, round(nvl(sum(SE.Minutes) / 60, 0), " + dec + ") as DTHrs"
                  + " , round(nvl(sum(SE.Minutes) , 0), 1) as DTMins"
                  + " from Event E, SubEvent SE, Reasons R"
                  + " where E.FLID = " + sAreaID
                  + " and E.Eventid = SE.EVENTID"
                  + " and E.EVENTTYPE = 'E'"
                  + " and E.ReasonID = R.ReasonID"
                  + " and lower(R.REASON) = 'equipment'"
                  + " and R.Planunplan = " + cFG.CAT_PLANNED
                  + " and SE.Sappi_Year = " + iSappiYear
                  + " and SE.Sappi_month = " + monthNum
                  + " and SE.SAPPI_DAY >= " + dayStart
                  + " and SE.SAPPI_DAY <= " + dayEnd
                  + " group by SE.EventID"
                  + ") s1 "
             + " where  s1.DTMins >= 5"
             + "";

      }
      else
      {
        sql = " select round(nvl(sum(SE.Minutes) / 60, 0), " + dec + ") as DTHrs"
          + " from Event E, SubEvent SE, Reasons R"
          + " where E.FLID = " + sAreaID
          + " and E.Eventid = SE.EVENTID"
          + " and E.EVENTTYPE = 'E'"
          + " and E.ReasonID = R.ReasonID"
          + " and lower(R.REASON) = 'equipment'"
          + " and R.Planunplan = " + cFG.CAT_PLANNED
          + " and SE.Sappi_Year = " + iSappiYear
          + " and SE.Sappi_month = " + monthNum
          + " and SE.SAPPI_DAY >= " + dayStart
          + " and SE.SAPPI_DAY <= " + dayEnd
          + " ";
      }

      try
      {
        DataTable dt = cORA.GetDataTable(sql);
        foreach (DataRow item in dt.Rows)
        {
          rv = cmath.getd(item["DTHrs"].ToString());
        }

        if (rv > 0)
          Application.DoEvents();

        if (sqlMonthID > 0)
          SaveMonths(sqlMonthID, "PLMT", rv);

        if (sqlDayID > 0)
          SaveDaily(sqlDayID, "PLMT", rv);

        if (sqlWeekID > 0)
          SaveWeekly(sqlWeekID, "PLMT", rv);

      }
      catch (Exception er)
      {
        cFG.LogIt( "SiteData DoPLMT: " + er.Message);
        throw er;
      }
      finally
      {
        cORA.CloseDB();
      }


    }//



    private void DoPLPS(int monthNum, int dayStart, int dayEnd, int sqlMonthID, int sqlDayID, int sqlWeekID = 0)
    {
      //Planned production shuts < 24 hrs
      string sql = "";

      byte dec = 1;
      if ((monthNum >= 8 && iSappiYear >= 2023) || iSappiYear >= 2024)    //from May 2023
      {
        dec = 2;
      }

      double rv = 0D;

      cORA.OpenDB(sSite);

      if ((sSite == "SAI" && monthNum >= 5 && iSappiYear >= 2022) || (sSite == "SAI" && iSappiYear >= 2023))      //from Feb 2022
      {
        //Change Request C-2202-0086 : 2022-02-03- Only pull through events >= 5 min from OEE into MEM from February 2022 onwards for all Saiccor Plant in MEM
        sql = " select sum(DTHrs) DTHrs , sum(DTMins) DTMins From  ("
                + " select SE.EventID, round(nvl(sum(SE.Minutes) / 60, 0), " + dec + ") as DTHrs"
                  + " , round(nvl(sum(SE.Minutes) , 0), 1) as DTMins"
                  + " from Event E, SubEvent SE, Reasons R"
                  + " where E.FLID = " + sAreaID
                  + " and E.Eventid = SE.EVENTID"
                  + " and E.EVENTTYPE = 'E'"
                  + " and E.ReasonID = R.ReasonID"
                  + " and lower(R.REASON) = 'process'"
                  + " and R.Planunplan = " + cFG.CAT_PLANNED
                  + " and SE.Sappi_Year = " + iSappiYear
                  + " and SE.Sappi_month = " + monthNum
                  + " and SE.SAPPI_DAY >= " + dayStart
                  + " and SE.SAPPI_DAY <= " + dayEnd
                  + " group by SE.EventID"
                  + ") s1 "
             + " where  s1.DTMins >= 5"
             + "";
      }
      else
      {
        sql = " select round(nvl(sum(SE.Minutes) / 60, 0), " + dec + ") as DTHrs"
          + " from Event E, SubEvent SE, Reasons R"
          + " where E.FLID = " + sAreaID
          + " and E.Eventid = SE.EVENTID"
          + " and E.EVENTTYPE = 'E'"
          + " and E.ReasonID = R.ReasonID"
          + " and lower(R.REASON) = 'process'"
          + " and R.Planunplan = " + cFG.CAT_PLANNED
          + " and SE.Sappi_Year = " + iSappiYear
          + " and SE.Sappi_month = " + monthNum
          + " and SE.SAPPI_DAY >= " + dayStart
          + " and SE.SAPPI_DAY <= " + dayEnd
          + " ";
      }


      try
      {
        DataTable dt = cORA.GetDataTable(sql);
        foreach (DataRow item in dt.Rows)
        {
          rv = cmath.getd(item["DTHrs"].ToString());
        }

        if (rv > 0)
          Application.DoEvents();

        if (sqlMonthID > 0)
          SaveMonths(sqlMonthID, "PLPS", rv);

        if (sqlDayID > 0)
          SaveDaily(sqlDayID, "PLPS", rv);

        if (sqlWeekID > 0)
          SaveWeekly(sqlWeekID, "PLPS", rv);

      }
      catch (Exception er)
      {
        cFG.LogIt( "SiteData DoPLPS: " + er.Message);
        throw er;
      }
      finally
      {
        cORA.CloseDB();
      }

    }//



    private void DoPIBP(int monthNum, int dayStart, int dayEnd, int sqlMonthID, int sqlDayID, int sqlWeekID = 0)
    {
      //PIBP
      string sql = "";

      byte dec = 1;
      if ((monthNum >= 8 && iSappiYear >= 2023) || iSappiYear >= 2024)    //from May 2023
      {
        dec = 2;
      }

      double rv = 0D;

      cORA.OpenDB(sSite);

      if ((sSite == "SAI" && monthNum >= 5 && iSappiYear >= 2022) || (sSite == "SAI" && iSappiYear >= 2023))      //from Feb 2022
      {
        //Change Request C-2202-0086 : 2022-02-03- Only pull through events >= 5 min from OEE into MEM from February 2022 onwards for all Saiccor Plant in MEM
        sql = " select sum(DTHrs) DTHrs , sum(DTMins) DTMins From  ("
              + " select SE.EventID, round(nvl(sum(SE.Minutes) / 60, 0), " + dec + ") as DTHrs"
                + " , round(nvl(sum(SE.Minutes) , 0), 1) as DTMins"
                + " from Event E, SubEvent SE, Reasons R"
                + " where E.FLID = " + sAreaID
                + " and E.Eventid = SE.EVENTID"
                + " and E.EVENTTYPE = 'E'"
                + " and E.ReasonID = R.ReasonID"
                + " and lower(R.REASON) IN ('pibp', 'services')"
                + " and R.Planunplan = " + cFG.CAT_UNPLANNED
                + " and SE.Sappi_Year = " + iSappiYear
                + " and SE.Sappi_month = " + monthNum
                + " and SE.SAPPI_DAY >= " + dayStart
                + " and SE.SAPPI_DAY <= " + dayEnd
                  + " group by SE.EventID"
                  + ") s1 "
             + " where  s1.DTMins >= 5"
             + "";

      }
      else
      {
        sql = " select round(nvl(sum(SE.Minutes) / 60, 0), " + dec + ") as DTHrs"
          + " from Event E, SubEvent SE, Reasons R"
          + " where E.FLID = " + sAreaID
          + " and E.Eventid = SE.EVENTID"
          + " and E.EVENTTYPE = 'E'"
          + " and E.ReasonID = R.ReasonID"
          + " and lower(R.REASON) IN ('pibp', 'services')"
          + " and R.Planunplan = " + cFG.CAT_UNPLANNED
          + " and SE.Sappi_Year = " + iSappiYear
          + " and SE.Sappi_month = " + monthNum
          + " and SE.SAPPI_DAY >= " + dayStart
          + " and SE.SAPPI_DAY <= " + dayEnd
          + " ";
      }

      try
      {
        DataTable dt = cORA.GetDataTable(sql);
        foreach (DataRow item in dt.Rows)
        {
          rv = cmath.getd(item["DTHrs"].ToString());
        }

        if (rv > 0)
          Application.DoEvents();

        if (sqlMonthID > 0)
          SaveMonths(sqlMonthID, "PIBP", rv);

        if (sqlDayID > 0)
          SaveDaily(sqlDayID, "PIBP", rv);

        if (sqlWeekID > 0)
          SaveWeekly(sqlWeekID, "PIBP", rv);

      }
      catch (Exception er)
      {
        cFG.LogIt( "SiteData DoPIBP: " + er.Message);
        throw er;
      }
      finally
      {
        cORA.CloseDB();
      }

    }//



    private void DoUPLE(int monthNum, int dayStart, int dayEnd, int sqlMonthID, int sqlDayID, int sqlWeekID = 0)
    {
      //Maintenance stops
      string sql = "";

      byte dec = 1;
      if ((monthNum >= 8 && iSappiYear >= 2023) || iSappiYear >= 2024)    //from May 2023
      {
        dec = 2;
      }

      double rv = 0D;

      cORA.OpenDB(sSite);

      if ((sSite == "SAI" && monthNum >= 5 && iSappiYear >= 2022) || (sSite == "SAI" && iSappiYear >= 2023))      //from Feb 2022
      {
        //Change Request C-2202-0086 : 2022-02-03- Only pull through events >= 5 min from OEE into MEM from February 2022 onwards for all Saiccor Plant in MEM
        sql = " select sum(DTHrs) DTHrs , sum(DTMins) DTMins From  ("
              + " select SE.EventID, round(nvl(sum(SE.Minutes) / 60, 0), " + dec + ") as DTHrs"
                + " , round(nvl(sum(SE.Minutes) , 0), 1) as DTMins"
                + " from Event E, SubEvent SE, Reasons R"
                + " where E.FLID = " + sAreaID
                + " and E.Eventid = SE.EVENTID"
                + " and E.EVENTTYPE = 'E'"
                + " and E.ReasonID = R.ReasonID"
                + " and lower(R.REASON) = 'equipment'"
                + " and R.Planunplan = " + cFG.CAT_UNPLANNED
                + " and SE.Sappi_Year = " + iSappiYear
                + " and SE.Sappi_month = " + monthNum
                + " and SE.SAPPI_DAY >= " + dayStart
                + " and SE.SAPPI_DAY <= " + dayEnd
                  + " group by SE.EventID"
                  + ") s1 "
             + " where  s1.DTMins >= 5"
              + "";

      }
      else
      {
        sql = " select round(nvl(sum(SE.Minutes) / 60, 0), " + dec + ") as DTHrs"
          + " from Event E, SubEvent SE, Reasons R"
          + " where E.FLID = " + sAreaID
          + " and E.Eventid = SE.EVENTID"
          + " and E.EVENTTYPE = 'E'"
          + " and E.ReasonID = R.ReasonID"
          + " and lower(R.REASON) = 'equipment'"
          + " and R.Planunplan = " + cFG.CAT_UNPLANNED
          + " and SE.Sappi_Year = " + iSappiYear
          + " and SE.Sappi_month = " + monthNum
          + " and SE.SAPPI_DAY >= " + dayStart
          + " and SE.SAPPI_DAY <= " + dayEnd
          + " ";
      }


      try
      {
        DataTable dt = cORA.GetDataTable(sql);
        foreach (DataRow item in dt.Rows)
        {
          rv = cmath.getd(item["DTHrs"].ToString());
        }

        if (rv > 0)
          Application.DoEvents();

        if (sqlMonthID > 0)
          SaveMonths(sqlMonthID, "UPLE", rv);

        if (sqlDayID > 0)
          SaveDaily(sqlDayID, "UPLE", rv);

        if (sqlWeekID > 0)
          SaveWeekly(sqlWeekID, "UPLE", rv);

      }
      catch (Exception er)
      {
        cFG.LogIt( "SiteData DoUPLE: " + er.Message);
        throw er;
      }
      finally
      {
        cORA.CloseDB();
      }

    }//



    private void DoPROC(int monthNum, int dayStart, int dayEnd, int sqlMonthID, int sqlDayID, int sqlWeekID = 0)
    {
      //process stops
      string sql = "";

      byte dec = 1;
      if ((monthNum >= 8 && iSappiYear >= 2023) || iSappiYear >= 2024)    //from May 2023
      {
        dec = 2;
      }

      double rv = 0D;

      cORA.OpenDB(sSite);

      if ((sSite == "SAI" && monthNum >= 5 && iSappiYear >= 2022) || (sSite == "SAI" && iSappiYear >= 2023))      //from Feb 2022
      {
        //Change Request C-2202-0086 : 2022-02-03- Only pull through events >= 5 min from OEE into MEM from February 2022 onwards for all Saiccor Plant in MEM
        sql = " select sum(DTHrs) DTHrs , sum(DTMins) DTMins From  ("
              + " select SE.EventID, round(nvl(sum(SE.Minutes) / 60, 0), " + dec + ") as DTHrs"
                + " , round(nvl(sum(SE.Minutes) , 0), 1) as DTMins"
                + " from Event E, SubEvent SE, Reasons R"
                + " where E.FLID = " + sAreaID
                + " and E.Eventid = SE.EVENTID"
                + " and E.EVENTTYPE = 'E'"
                + " and E.ReasonID = R.ReasonID"
                + " and lower(R.REASON) = 'process'"
                + " and R.Planunplan = " + cFG.CAT_UNPLANNED
                + " and SE.Sappi_Year = " + iSappiYear
                + " and SE.Sappi_month = " + monthNum
                + " and SE.SAPPI_DAY >= " + dayStart
                + " and SE.SAPPI_DAY <= " + dayEnd
                  + " group by SE.EventID"
                  + ") s1 "
             + " where  s1.DTMins >= 5"
            + "";

      }
      else
      {
        sql = " select round(nvl(sum(SE.Minutes) / 60, 0), " + dec + ") as DTHrs"
          + " from Event E, SubEvent SE, Reasons R"
          + " where E.FLID = " + sAreaID
          + " and E.Eventid = SE.EVENTID"
          + " and E.EVENTTYPE = 'E'"
          + " and E.ReasonID = R.ReasonID"
          + " and lower(R.REASON) = 'process'"
          + " and R.Planunplan = " + cFG.CAT_UNPLANNED
          + " and SE.Sappi_Year = " + iSappiYear
          + " and SE.Sappi_month = " + monthNum
          + " and SE.SAPPI_DAY >= " + dayStart
          + " and SE.SAPPI_DAY <= " + dayEnd
          + " ";
      }

      try
      {
        DataTable dt = cORA.GetDataTable(sql);
        foreach (DataRow item in dt.Rows)
        {
          rv = cmath.getd(item["DTHrs"].ToString());
        }

        if (rv > 0)
          Application.DoEvents();

        if (sqlMonthID > 0)
          SaveMonths(sqlMonthID, "PROC", rv);

        if (sqlDayID > 0)
          SaveDaily(sqlDayID, "PROC", rv);

        if (sqlWeekID > 0)
          SaveWeekly(sqlWeekID, "PROC", rv);

      }
      catch (Exception er)
      {
        cFG.LogIt( "SiteData DoPROC: " + er.Message);
        throw er;
      }
      finally
      {
        cORA.CloseDB();
      }

    }//



    private void DoPLAP(int monthNum, int dayStart, int dayEnd, int sqlMonthID, int sqlDayID, int sqlWeekID = 0)
    {
      //'planned shut>24hrs'
      string sql = "";

      byte dec = 1;
      if ((monthNum >= 8 && iSappiYear >= 2023) || iSappiYear >= 2024)    //from May 2023
      {
        dec = 2;
      }

      double rv = 0D;

      cORA.OpenDB(sSite);

      if ((sSite == "SAI" && monthNum >= 5 && iSappiYear >= 2022) || (sSite == "SAI" && iSappiYear >= 2023))      //from Feb 2022
      {
        //Change Request C-2202-0086 : 2022-02-03- Only pull through events >= 5 min from OEE into MEM from February 2022 onwards for all Saiccor Plant in MEM
        sql = " select sum(DTHrs) DTHrs , sum(DTMins) DTMins From  ("
                + " select SE.EventID, round(nvl(sum(SE.Minutes) / 60, 0), " + dec + ") as DTHrs"
                  + " , round(nvl(sum(SE.Minutes) , 0), 1) as DTMins"
                  + " from Event E, SubEvent SE, Reasons R, Categories C"
                  + " where E.FLID = " + sAreaID
                  + " and E.Eventid = SE.EVENTID"
                  + " and E.EVENTTYPE = 'E'"
                  + " and E.ReasonID = R.ReasonID"
                  + " and lower(R.REASON) = 'available time adjustment'"
                  + " and R.Planunplan = " + cFG.CAT_PLANNED
                  + " and E.categoryid = C.CATEGORYID"
                  + " and C.REASONID = R.REASONID"
                  + " and SE.Sappi_Year = " + iSappiYear
                  + " and SE.Sappi_month = " + monthNum
                  + " and SE.SAPPI_DAY >= " + dayStart
                  + " and SE.SAPPI_DAY <= " + dayEnd
                  + " and lower(C.CATEGORY) = 'planned shut>24hrs'"
                  //+ " and lower(C.CATEGORY) IN ('planned shut>24hrs','annual shut')"
                  + " group by SE.EventID"
                  + ") s1 "
             + " where  s1.DTMins >= 5"
              + "";

      }
      else
      {
        sql = " select round(nvl(sum(SE.Minutes) / 60, 0), " + dec + ") as DTHrs"
        + " from Event E, SubEvent SE, Reasons R, Categories C"
        + " where E.FLID = " + sAreaID
        + " and E.Eventid = SE.EVENTID"
        + " and E.EVENTTYPE = 'E'"
        + " and E.ReasonID = R.ReasonID"
        + " and lower(R.REASON) = 'available time adjustment'"
        + " and R.Planunplan = " + cFG.CAT_PLANNED
        + " and E.categoryid = C.CATEGORYID"
        + " and C.REASONID = R.REASONID"
        + " and SE.Sappi_Year = " + iSappiYear
        + " and SE.Sappi_month = " + monthNum
        + " and SE.SAPPI_DAY >= " + dayStart
        + " and SE.SAPPI_DAY <= " + dayEnd
        + " and lower(C.CATEGORY) = 'planned shut>24hrs'"
        //+ " and lower(C.CATEGORY) IN ('planned shut>24hrs','annual shut')"
        + " ";
      }


      try
      {
        DataTable dt = cORA.GetDataTable(sql);
        foreach (DataRow item in dt.Rows)
        {
          rv = cmath.getd(item["DTHrs"].ToString());
        }

        if (rv > 0)
          Application.DoEvents();

        if (sqlMonthID > 0)
          SaveMonths(sqlMonthID, "PLAP", rv);

        if (sqlDayID > 0)
          SaveDaily(sqlDayID, "PLAP", rv);

        if (sqlWeekID > 0)
          SaveWeekly(sqlWeekID, "PLAP", rv);

      }
      catch (Exception er)
      {
        cFG.LogIt("SiteData DoPLAP: " + er.Message);
        throw er;
      }
      finally
      {
        cORA.CloseDB();
      }


    }//



    private void DoTCSS(int monthNum, int dayStart, int dayEnd, int sqlMonthID, int sqlDayID, int sqlWeekID = 0)
    {
      //'technical standstill'
      string sql = "";
      byte dec = 1;
      if ((monthNum >= 8 && iSappiYear >= 2023) || iSappiYear >= 2024)    //from May 2023
      {
        dec = 2;
      }

      double rv = 0D;

      cORA.OpenDB(sSite);

      if ((sSite == "SAI" && monthNum >= 5 && iSappiYear >= 2022) || (sSite == "SAI" && iSappiYear >= 2023))      //from Feb 2022
      {
        //Change Request C-2202-0086 : 2022-02-03- Only pull through events >= 5 min from OEE into MEM from February 2022 onwards for all Saiccor Plant in MEM
        sql = " select sum(DTHrs) DTHrs , sum(DTMins) DTMins From  ("
                + " select SE.EventID, round(nvl(sum(SE.Minutes) / 60, 0), " + dec + ") as DTHrs"
                  + " , round(nvl(sum(SE.Minutes) , 0), 1) as DTMins"
                  + " from Event E, SubEvent SE, Reasons R, Categories C"
                  + " where E.FLID = " + sAreaID
                  + " and E.Eventid = SE.EVENTID"
                  + " and E.EVENTTYPE = 'E'"
                  + " and E.ReasonID = R.ReasonID"
                  + " and lower(R.REASON) = 'available time adjustment'"
                  + " and R.Planunplan = " + cFG.CAT_PLANNED
                  + " and E.categoryid = C.CATEGORYID"
                  + " and C.REASONID = R.REASONID"
                  + " and SE.Sappi_Year = " + iSappiYear
                  + " and SE.Sappi_month = " + monthNum
                  + " and SE.SAPPI_DAY >= " + dayStart
                  + " and SE.SAPPI_DAY <= " + dayEnd
                  + " and lower(C.CATEGORY) = 'technical standstill'"
                  + " group by SE.EventID"
                  + ") s1 "
             + " where  s1.DTMins >= 5"
             + "";
      }
      else
      {
        sql = " select round(nvl(sum(SE.Minutes) / 60, 0), " + dec + ") as DTHrs"
        + " from Event E, SubEvent SE, Reasons R, Categories C"
        + " where E.FLID = " + sAreaID
        + " and E.Eventid = SE.EVENTID"
        + " and E.EVENTTYPE = 'E'"
        + " and E.ReasonID = R.ReasonID"
        + " and lower(R.REASON) = 'available time adjustment'"
        + " and R.Planunplan = " + cFG.CAT_PLANNED
        + " and E.categoryid = C.CATEGORYID"
        + " and C.REASONID = R.REASONID"
        + " and SE.Sappi_Year = " + iSappiYear
        + " and SE.Sappi_month = " + monthNum
        + " and SE.SAPPI_DAY >= " + dayStart
        + " and SE.SAPPI_DAY <= " + dayEnd
        + " and lower(C.CATEGORY) = 'technical standstill'"
        + " ";
      }


      try
      {
        DataTable dt = cORA.GetDataTable(sql);
        foreach (DataRow item in dt.Rows)
        {
          rv = cmath.getd(item["DTHrs"].ToString());
        }

        if (rv > 0)
          Application.DoEvents();

        if (sqlMonthID > 0)
          SaveMonths(sqlMonthID, "TCSS", rv);

        if (sqlDayID > 0)
          SaveDaily(sqlDayID, "TCSS", rv);

        if (sqlWeekID > 0)
          SaveWeekly(sqlWeekID, "TCSS", rv);

      }
      catch (Exception er)
      {
        cFG.LogIt("SiteData DoTCSS: " + er.Message);
        throw er;
      }
      finally
      {
        cORA.CloseDB();
      }


    }//



    private void DoTISS(int monthNum, int dayStart, int dayEnd, int sqlMonthID, int sqlDayID, int sqlWeekID = 0)
    {

      string sql = "";
      byte dec = 1;
      if ((monthNum >= 8 && iSappiYear >= 2023) || iSappiYear >=2024)     //from May 2023
      {
        dec = 2;
      }

      double rv = 0D;

      cORA.OpenDB(sSite);

      if ((sSite == "SAI" && monthNum >= 5 && iSappiYear >= 2022) || (sSite == "SAI" && iSappiYear >= 2023))      //from Feb 2022
      {
        //Change Request C-2202-0086 : 2022-02-03- Only pull through events >= 5 min from OEE into MEM from February 2022 onwards for all Saiccor Plant in MEM
        sql = " select sum(DTHrs) DTHrs , sum(DTMins) DTMins From  ("
            + " select SE.EventID, round(nvl(sum(SE.Minutes) / 60, 0), " + dec + ") as DTHrs"
              + " , round(nvl(sum(SE.Minutes) , 0), 1) as DTMins"
              + " from Event E, SubEvent SE, Reasons R, Categories C"
              + " where E.FLID = " + sAreaID
              + " and E.Eventid = SE.EVENTID"
              + " and E.EVENTTYPE = 'E'"
              + " and E.ReasonID = R.ReasonID"
              + " and lower(R.REASON) = 'available time adjustment'"
              + " and R.Planunplan = " + cFG.CAT_PLANNED
              + " and E.categoryid = C.CATEGORYID"
              + " and C.REASONID = R.REASONID"
              + " and SE.Sappi_Year = " + iSappiYear
              + " and SE.Sappi_month = " + monthNum
              + " and SE.SAPPI_DAY >= " + dayStart
              + " and SE.SAPPI_DAY <= " + dayEnd
              + " and lower(C.CATEGORY) = 'investment standstill'"
              + " group by SE.EventID"
              + ") s1 "
          + " where  s1.DTMins >= 5"
          + "";

      }
      else
      {
        sql = " select round(nvl(sum(SE.Minutes) / 60, 0), " + dec + ") as DTHrs"
        + " from Event E, SubEvent SE, Reasons R, Categories C"
        + " where E.FLID = " + sAreaID
        + " and E.Eventid = SE.EVENTID"
        + " and E.EVENTTYPE = 'E'"
        + " and E.ReasonID = R.ReasonID"
        + " and lower(R.REASON) = 'available time adjustment'"
        + " and R.Planunplan = " + cFG.CAT_PLANNED
        + " and E.categoryid = C.CATEGORYID"
        + " and C.REASONID = R.REASONID"
        + " and SE.Sappi_Year = " + iSappiYear
        + " and SE.Sappi_month = " + monthNum
        + " and SE.SAPPI_DAY >= " + dayStart
        + " and SE.SAPPI_DAY <= " + dayEnd
        + " and lower(C.CATEGORY) = 'investment standstill'"
        + " ";
      }


      try
      {
        DataTable dt = cORA.GetDataTable(sql);
        foreach (DataRow item in dt.Rows)
        {
          rv = cmath.getd(item["DTHrs"].ToString());
        }

        if (rv > 0)
          Application.DoEvents();

        if (sqlMonthID > 0)
          SaveMonths(sqlMonthID, "TISS", rv);

        if (sqlDayID > 0)
          SaveDaily(sqlDayID, "TISS", rv);

        if (sqlWeekID > 0)
          SaveWeekly(sqlWeekID, "TISS", rv);


      }
      catch (Exception er)
      {
        cFG.LogIt("SiteData DoTISS: " + er.Message);
        throw er;
      }
      finally
      {
        cORA.CloseDB();
      }


    }//



    private void SheetBreaks(int sqlMonthID, string pStartDate, string pEndDate, int sqlDayID = 0, int sqlWeekID = 0)
    {

      int iSheetBreaksCount = 0;
      string millKey = "";
      string plantCode = "";
      string sql = "";

      try
      {

        //cORA.OpenDB(sSite);



        ////the count of events with category = 'sheetbreaks'
        //// count Sheetbreaks at Category level as the tag is already used in capturing: 2022-03-11 Sharon / Themba Email
        //sql = "select count(e.eventid) sheetBreakCount"
        //  + " from event e ,  reasons r , categories c"
        //  + " where e.eventtype = 'E'"
        //  + "  and e.flid = " + sAreaID
        //  + "  and trunc(e.datefrom) >= " + cmath.OraDateMon(pStartDate)
        //  + "  and trunc(e.datefrom) <= " + cmath.OraDateMon(pEndDate)
        //  + "  and e.reasonid = r.reasonid"
        //  + "  and e.categoryid = c.categoryid"
        //  + "  and lower(r.reason) = 'process'"
        //  + "  and lower(c.category) = 'sheetbreak'"
        //  + "";



        
        //if (sSite == "NGX" && sAreaID == "139")
        //{
        //  millKey = "1";
        //  plantCode = "PM1";
        //}

        //if (sSite == "NGX" && sAreaID == "25")
        //{
        //  millKey = "1";
        //  plantCode = "PM2";
        //}

        //if (sSite == "STA" && sAreaID == "6")
        //{
        //  millKey = "2";
        //  plantCode = "P1";
        //}

        //if (sSite == "STA" && sAreaID == "12")
        //{
        //  millKey = "2";
        //  plantCode = "ST";
        //}

        //if (sSite == "TUG" && sAreaID == "118")
        //{
        //  millKey = "3";
        //  plantCode = "PM2";
        //}


        if (sSite == "NGX" && sAreaID == "13")
        {
          millKey = "1";
          plantCode = "UT3";
        }

        if (sSite == "SAI" && sAreaID == "4")
        {
          millKey = "4";
          plantCode = "PM3";
        }

        if (sSite == "SAI" && sAreaID == "5")
        {
          millKey = "4";
          plantCode = "PM4";
        }

        if (sSite == "SAI" && sAreaID == "6")
        {
          millKey = "4";
          plantCode = "PM5";
        }
                                
        if (millKey != "")
        {
          //this is for Pulp             
          sql = "Select MillKey, PlantCode, sum(SheetBreaksDaily) SheetBreakCount"
            + " from ZADailyMTDReport"
            + " where   ReportDate >= '" + cmath.getDateStr(pStartDate) + "'"
            + "     and ReportDate <= '" + cmath.getDateStr(pEndDate) + "'"
            + "     and MillKey = " + millKey
            + "     and PlantCode = '" + plantCode + "'"
            + "     group by MillKey,PlantCode"
            + "";

          cSQL.OpenDB("ZAMDW");

          DataTable dt = cSQL.GetDataTable(sql);

          cSQL.CloseDB();

          foreach (DataRow item in dt.Rows)
          {
            iSheetBreaksCount = cmath.geti(item["sheetBreakCount"].ToString());
          }

        }
        else
        {
          // for Paper Machines source data is OEE
          // the count of all events with Division = "Unplanned"  
          sql = "select count(e.eventid) sheetBreakCount"
            + " from event e , Reasons r, PlanUnplan p "
            + " where e.eventtype = 'E'"
            + "  and e.flid = " + sAreaID
            + "  and trunc(e.datefrom) >= " + cmath.OraDateMon(pStartDate)
            + "  and trunc(e.datefrom) <= " + cmath.OraDateMon(pEndDate)
            + "  and e.reasonid = r.reasonid"
            + "  and r.planunplan = p.planunplan"
            + "  and lower(p.description) = 'unplanned'"
            + "";

          cORA.OpenDB(sSite);
          
          DataTable dt = cORA.GetDataTable(sql);
          
          cORA.CloseDB();

          foreach (DataRow item in dt.Rows)
          {
            iSheetBreaksCount = cmath.geti(item["sheetBreakCount"].ToString());
          }

        }



        if (sqlMonthID > 0)
            SaveMonths(sqlMonthID, "SHEETB", iSheetBreaksCount);

          if (sqlDayID > 0)
            SaveDaily(sqlDayID, "SHEETB", iSheetBreaksCount);

          if (sqlWeekID > 0)
            SaveWeekly(sqlWeekID, "SHEETB", iSheetBreaksCount);

      }
      catch (Exception er)
      {
        throw er;
      }
      finally
      {
        cORA.CloseDB();
        cSQL.CloseDB();
      }

    } //



    private void GradeChanges(int sqlMonthID, string pStartDate, string pEndDate, int sqlDayID = 0, int sqlWeekID = 0)
    {
      int dCount = 0;
      string sql = "";

      try
      {

        cORA.OpenDB(sSite);

        sql = "select count(1) gradeChangeCount from"
              + "("
              + "  select lead(gradeid) over(order by eventid) as nextValue, eventid, gradeid"
              + "  from event"
              + "  where flid =  " + sAreaID
              + "  and gradeid > 0"
              + "  and eventtype = 'E'"
              + "  and trunc(datefrom) >=  " + cmath.OraDateMon(pStartDate)
              + "  and trunc(datefrom) <= " + cmath.OraDateMon(pEndDate)
              + "  )  tmp"
              + "  where tmp.nextValue<> tmp.gradeid"
              + "";

        DataTable dt = cORA.GetDataTable(sql);
        foreach (DataRow item in dt.Rows)
        {
          dCount = cmath.geti(item["gradeChangeCount"].ToString());
        }


        if (sqlMonthID > 0)
          SaveMonths(sqlMonthID, "GRDCHG", dCount);

        if (sqlDayID > 0)
          SaveDaily(sqlDayID, "GRDCHG", dCount);

        if (sqlWeekID > 0)
          SaveWeekly(sqlWeekID, "GRDCHG", dCount);

      }
      catch (Exception er)
      {
        throw er;
      }
      finally
      {
        cORA.CloseDB();
      }

    }


    private void DoBudgetProduction(int sqlMonthID, string pStartDate, string pEndDate, int sqlDayID = 0, int sqlWeekID = 0)
    {

      if (sSite == "SAI")
      {
        return;       //saiccor gets the figures from MESQUALITYSYSTEM   PHSSAI, see cKPIJob
      }

      string sDateFrom = cmath.OraDate(pStartDate);
      string sDateTo = cmath.OraDate(pEndDate);

      string sql = "";

      double rv = 0D;

      cORA.OpenDB(sSite);

      sql = "select  sum(bp.bp_value) bp_value from budget_types bt, budget_plan_daily bp "
        + " where bt.bp_type_id = bp.bp_type_id"
        + " and bt.bp_type_code in ('NSPPMD')"
        + " and bp.flid = " + sAreaID
        + " and bp.bp_date >= " + sDateFrom
        + " and bp.bp_date <= " + sDateTo
        + "";

      try
      {
        DataTable dt = cORA.GetDataTable(sql);
        foreach (DataRow item in dt.Rows)
        {
          rv = cmath.getd(item["bp_value"].ToString());
        }

        Application.DoEvents();

        if (sqlMonthID > 0)
          SaveMonths(sqlMonthID, "KPI0163", rv);

        if (sqlDayID > 0)
          SaveDaily(sqlDayID, "KPI0163", rv);

        if (sqlWeekID > 0)
          SaveWeekly(sqlWeekID, "KPI0163", rv);

      }
      catch (Exception er)
      {
        cFG.LogIt("SiteData DoBudgetProduction: " + er.Message);
        throw er;
      }
      finally
      {
        cORA.CloseDB();
      }


    }//



    private void DoSAIBudgetProduction(int sqlMonthID, string pStartDate, string pEndDate, int sqlDayID = 0, int sqlWeekID = 0)
    {

      if (sSite != "SAI")
      {
        return;       
      }

      //1.0.2.17 2023-06-13   C-2306-0137 New KPI for Daily Report KPI0163 production budgets

      string sSQLStartDate = cmath.getDateStr(pStartDate);
      string sSQLEndDate = cmath.getDateStr(pEndDate);

      string sql = "";

      double dTons = 0D;

      int prodLine = 0;      //for saiccor daily, weekly and monthly budget production figure. comes from PHSSAI


      if (sAreaID == "4")
        prodLine = 3;

      if (sAreaID == "5")
        prodLine = 4;

      if (sAreaID == "6")
        prodLine = 5;
   

      try
      {
        sql = "select sum([budgettons]) as budgettons from [mesqualitysystem].[prd].[productionbudget] "
        + " where productionbudgetdate >= cast('" + sSQLStartDate + "' as date)"
        + " and productionbudgetdate <= cast('" + sSQLEndDate + "' as date)"
        + " and ProdLine = " + prodLine;

        cSQL.OpenDB("SAIMES");
        DataTable dt = cSQL.GetDataTable(sql);
        cSQL.CloseDB();

        foreach (DataRow item in dt.Rows)
        {
          dTons = cmath.getd(item["budgettons"].ToString());
        }




        Application.DoEvents();

        if (sqlMonthID > 0)
          SaveMonths(sqlMonthID, "KPI0163", dTons);

        if (sqlDayID > 0)
          SaveDaily(sqlDayID, "KPI0163", dTons);

        if (sqlWeekID > 0)
          SaveWeekly(sqlWeekID, "KPI0163", dTons);

      }
      catch (Exception er)
      {
        cFG.LogIt("SiteData DoSABudgetProduction: " + er.Message);
        throw er;
      }
      finally
      {
        cSQL.CloseDB();
      }


    }//


    private void DoSAIBudgetProduction83(int sqlMonthID, string pStartDate, string pEndDate, int sqlDayID = 0, int sqlWeekID = 0)
    {

      if (sSite != "SAI")
      {
        return;
      }

      string sSQLStartDate = cmath.getDateStr(pStartDate);
      string sSQLEndDate = cmath.getDateStr(pEndDate);

      string sql = "";

      double dTons = 0D;

      int prodLine = 0;      


      if (sAreaID == "4")
        prodLine = 3;

      if (sAreaID == "5")
        prodLine = 4;

      if (sAreaID == "6")
        prodLine = 5;

    

      try
      {
        sql = "select sum([budgettons]) as budgettons from [mesqualitysystem].[prd].[productionbudget] "
        + " where productionbudgetdate >= cast('" + sSQLStartDate + "' as date)"
        + " and productionbudgetdate <= cast('" + sSQLEndDate + "' as date)"
        + " and ProdLine = " + prodLine;

        cSQL.OpenDB("SAIMES");
        DataTable dt = cSQL.GetDataTable(sql);
        cSQL.CloseDB();

        foreach (DataRow item in dt.Rows)
        {
          dTons = cmath.getd(item["budgettons"].ToString());
        }


        Application.DoEvents();

        if (sqlMonthID > 0)
          SaveMonths(sqlMonthID, "KPI0083", dTons);

        if (sqlDayID > 0)
          SaveDaily(sqlDayID, "KPI0083", dTons);

        if (sqlWeekID > 0)
          SaveWeekly(sqlWeekID, "KPI0083", dTons);

      }
      catch (Exception er)
      {
        cFG.LogIt("SiteData DoSABudgetProduction83: " + er.Message);
        throw er;
      }
      finally
      {
        cSQL.CloseDB();
      }


    }//



    private void DoSAI_NSPPM(int sqlMonthID, string pStartDate, string pEndDate, int sqlDayID = 0, int sqlWeekID = 0)
    {


         //2025-02-19 daily and weekly now as a calculation see cKPIJob for NSPPM


      if (sSite != "SAI")
      {
        return;
      }


      double dTons = 0;
      
      string sql = "";

      string sSQLStartDate = cmath.getDateStr(pStartDate);
      string sSQLEndDate = cmath.getDateStr(pEndDate);

      int prodLine = 0;     

      if (sAreaID == "4")
        prodLine = 3;

      if (sAreaID == "5")
        prodLine = 4;

      if (sAreaID == "6")
        prodLine = 5;

      DataTable dt;

      try
      {
        
        cSQL.OpenDB("SAIMES");


        if ((sqlDayID > 0) && (sqlMonthID == 0))
        {
          sql = "select productionbudgetid, budgettons from [mesqualitysystem].[prd].[productionbudget] "
             + " where productionbudgetdate = cast('" + sSQLStartDate + "' as date)"
             + " and ProdLine = " + prodLine;
 
          dt = cSQL.GetDataTable(sql);

          foreach (DataRow item in dt.Rows)
          {
            dTons = cmath.getd(item["budgettons"].ToString());
          }
        }



        if ((sqlWeekID > 0) && (sqlMonthID == 0))
        {
          sql = "select sum([budgettons]) as budgettons from [mesqualitysystem].[prd].[productionbudget] "
             + " where productionbudgetdate >= cast('" + sSQLStartDate + "' as date)"
             + " and productionbudgetdate <= cast('" + sSQLEndDate + "' as date)"
             + " and ProdLine = " + prodLine;


          dt = cSQL.GetDataTable(sql);

          
          foreach (DataRow item in dt.Rows)
          {
            dTons = cmath.getd(item["budgettons"].ToString());
          }

        }

        cSQL.CloseDB();


        if ((sqlDayID == 0) && (sqlMonthID > 0))
        {
          SaveMonths(sqlMonthID, "NSPPM", dTons);
        }

        if ((sqlDayID > 0) && (sqlMonthID == 0))
        {
          SaveDaily(sqlDayID, "NSPPM", dTons);
        }

        if ((sqlWeekID > 0) && (sqlMonthID == 0))
        {
          SaveWeekly(sqlWeekID, "NSPPM", dTons);
        }

      }
      catch (Exception ee)
      {
        cFG.LogIt("SiteData : DoSAI_NSPPM" + ee.Message);
        throw ee;
      }
      finally
      {
        cORA.CloseDB();
        cSQL.CloseDB();
      }

    }//



  }  ///

}///







