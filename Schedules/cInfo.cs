﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schedules
{
  class cInfo
  {

    public string rowSpan { get; set; }
    public string colKPI { get; set; }
    public string colDesc { get; set; }

    public string colUOM { get; set; }
    public double colYTD { get; set; }
    public string colQ1 { get; set; }

    public string colQ2 { get; set; }
    public string colQ3 { get; set; }
    public string colQ4 { get; set; }

    public string colOct { get; set; }
    public string colNov { get; set; }
    public string colDec { get; set; }
    public string colJan { get; set; }
    public string colFeb { get; set; }
    public string colMar { get; set; }
    public string colApr { get; set; }
    public string colMay { get; set; }
    public string colJun { get; set; }
    public string colJul { get; set; }
    public string colAug { get; set; }
    public string colSep { get; set; }

  }
}
