﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Data.SqlClient;
using System.Net;
using System.Net.Mail;
using System.Threading;
using System.Collections;


// cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);

namespace Schedules
{

  public partial class frmMain : Form
  {

    //class used to populate the dropdown list
    class cAreas
    {

      private string _AreaDesc;
      private string _AreaCode;

      public cAreas(string areaDesc, string areaCode)
      {
        _AreaDesc = areaDesc;
        _AreaCode = areaCode;
      }

      public string AreaDesc
      {
        get { return _AreaDesc; }
        set { _AreaDesc = value; }
      }

      public string AreaCode
      {
        get { return _AreaCode; }
        set { _AreaCode = value; }
      }
    };



    bool boCancel = false;

    string args1 = "";
    string paramIDX = "";

    byte NGX = 1;
    byte STA = 2;
    byte TUG = 3;
    byte SAI = 4;
    int MAXEMAILS = 25;


    public frmMain()
    {

        InitializeComponent();

#if !DEBUG
      cmdTest.Visible = false;
      cmdWA.Visible = false;
      cmdFullRun.Visible = false;
      cmdStartRun.Visible = false;
      cmdGradesProdMix.Visible = false;
      cmdZADailyReport.Visible = false;
      cmdUserHR.Visible = false;      
      cmdGlobalReport.Visible = false;      
      cmdUserHR.Visible = false;      
      cmdDailyBudget.Visible = false;
      cmdBudgetProduction.Visible = false;
#endif


         //String S = Environment.CommandLine;
         String[] arguments = Environment.GetCommandLineArgs();
      if (arguments.Length > 1)
      {
          args1 = arguments[1].ToUpper();
        // "ADDMDW" "PREPEMAILS" "RUNALLKPI"  "RUNPREVIOUS"  "RUNPLANT" "ZAMTDDailyReport"

        if (args1 == "RUNPLANT")
        {  
          paramIDX = arguments[2];      //idx
        }
      }


      cFG.LogIt("-------------------------Schedules Started------------------------------\r\n");


      cmdCancel.Visible = false;

      LoadAreas();

      LoadRunProperties();

      timer1.Interval = 50;
      timer1.Start();

      ShowSBar("Ready.....");

    }//



    private void LoadAreas()
    {

      cboAreas.DisplayMember = "AreaDesc";
      cboAreas.ValueMember = "AreaCode";

      cboAreas.Items.Add(new cAreas("Select Item", ""));

      cboAreas.Items.Add(new cAreas("ALL", "ALL ALL"));

      cboAreas.Items.Add(new cAreas("NGX PM1 139", "NGX 139"));
      cboAreas.Items.Add(new cAreas("NGX PM2 25", "NGX 25"));
      cboAreas.Items.Add(new cAreas("NGX UT3 13", "NGX 13"));
      cboAreas.Items.Add(new cAreas("NGX DIG2 5", "NGX 5"));
      cboAreas.Items.Add(new cAreas("NGX FL3 161", "NGX 161"));

      cboAreas.Items.Add(new cAreas("TUG PM 118", "TUG 118"));
      cboAreas.Items.Add(new cAreas("TUG NSSC 126", "TUG 126"));

      cboAreas.Items.Add(new cAreas("STA PM1 6", "STA 6"));
      cboAreas.Items.Add(new cAreas("STA PM2 12", "STA 12"));
      cboAreas.Items.Add(new cAreas("STA DIG 1", "STA 1"));

      cboAreas.Items.Add(new cAreas("SAI C3 4", "SAI 4"));
      cboAreas.Items.Add(new cAreas("SAI C4 5", "SAI 5"));
      cboAreas.Items.Add(new cAreas("SAI C5 6", "SAI 6"));

      cboAreas.Items.Add(new cAreas("SAI MgO1 Dig 2", "SAI 2"));
      cboAreas.Items.Add(new cAreas("SAI MgO2 Dig 1", "SAI 1"));
      cboAreas.Items.Add(new cAreas("SAI MgO3 Dig 65", "SAI 65"));
      cboAreas.Items.Add(new cAreas("SAI Ca Batch 3", "SAI 3"));

      cboAreas.Items.Add(new cAreas("LOM Lomati", "LOM 0"));

      cboAreas.SelectedIndex = 0;


    } //



    private void LoadRunProperties()
    {

      cCal cal = new cCal();

      //defaults
      txtFinYear.Text = "2024";
      txtStartMonth.Text = "1";
      txtPrevDays.Text = "2";

      try
      {

        cSQL.OpenDB("ZAMDW");
        
        cal.GetCal(cmath.getDateStr(cmath.getDateNowStr(),-1));
        
        txtFinYear.Text = cal.iSappiYear.ToString();

        //if (cal.iCurrMonthOfYear > 1)
        //  txtStartMonth.Text = (cal.iCurrMonthOfYear - 1 ).ToString();
        //else
        txtStartMonth.Text = cal.iCurrMonthOfYear.ToString();

        txtPrevDays.Text = "2";

      }
      catch (Exception tt)
      {
        lblMsg.Text = tt.Message;
        cFG.LogIt(tt.Message);
        return;
      }
      finally
      {
        cal = null;
        cSQL.CloseDB();
      }

    } //


    private void ShowSBar(string msg)
    {
      try
      {
        SBar.Items[0].Text = msg;
        SBar.Items[1].Text = DateTime.Now.ToShortDateString();
        SBar.Items[1].Alignment = ToolStripItemAlignment.Right;

        SBar.Refresh();
        Application.DoEvents();
      }
      catch (Exception dd)
      {
        lblMsg.Text = dd.Message;
        cFG.LogIt(dd.Message);
      }

    }//



    private void ToggleButtons(bool boVisible)
    {
      cmdTest.Visible = boVisible;
      cmdWA.Visible = boVisible;
      cmdFullRun.Visible = boVisible;
      cmdStartRun.Visible = boVisible;
      cmdGradesProdMix.Visible = boVisible;
      cmdZADailyReport.Visible = boVisible;
      cmdUserHR.Visible = boVisible;
      cmdGlobalReport.Visible = boVisible;
      cmdCancel.Visible = !boVisible;
      cmdDailyBudget.Visible = boVisible;
      cmdBudgetProduction.Visible = boVisible;
      boCancel = boVisible;

    }//



    private void cmdClose_Click(object sender, EventArgs e)
    {
      Close();
      Application.Exit();
    }


    private void mnuExit_Click(object sender, EventArgs e)
    {
      Close();
      Application.Exit();
    }


    private void cmdCancel_Click(object sender, EventArgs e)
    {
      boCancel = true;
    }


    private void cmdTest_Click(object sender, EventArgs e)
    {

      //RunPlant();

      //PrepEmails();

      //************************************
      //cTest t = new cTest();

      //t.sAreaID = "161";
      //t.DoNGX_FL3(266, "2021-11-08", "2021-11-09", 0, 0);

      //t.sAreaID = "5";
      //t.iSappiYear = 2022;
      //t.DoNGX_DIG2(0, "2021-11-08", "2021-11-09", 0, 0);

      //t = null;
      //************************************


      //DoRunPrevious();

      //cTest t = new cTest();
      //t.TestStoredProc();

    } //

    
    private void timer1_Tick(object sender, EventArgs e)
    {

      timer1.Stop();


      ShowSBar("Ready.....");


      if (args1 == "")    //exit if there is no command on load
      {
        return;
      }


      ToggleButtons(false);

      lblMsg.Text = "";


      ShowSBar("Processing...");

      if (args1 == "RUNALLKPI")
      {


        ToggleButtons(false);

        RunMonthlyData("");
        RunKPIMonthly("");

        RunDailyData(-2);
        RunKPIDaily(-2);

        RunWeeklyData("", 0);
        RunKPIWeekly("", 0);

        RunZAMTDDailyReport();
        
        RunGlobalReport();

        //GradesProductMix();

        DoOEEDailyBudgets();

      }


      if (args1 == "ADDMDW")
      {
        //automated
        ToggleButtons(false);
        AddMDWComments();
      }


      if (args1 == "PREPEMAILS")
      {
        //automated
        ToggleButtons(false);
        PrepEmails();
      }

      if (args1 == "RUNPREVIOUS")
      {
        ToggleButtons(false);
        DoRunPrevious();
      }


      if (args1 == "RUNPLANT")
      {
        RunPlant();   //user initiated monthly run from MEM  UI
      }


      if (args1 == "ZAMTDDailyReport")
      {
        RunZAMTDDailyReport();
      }


      ShowSBar("Job finished");


      cmdCancel.Visible = false;


      Close();

      Application.Exit();


    } // timer


    private void RunPlant()
    {

      ShowSBar("Ready..");
      lblMsg.Text = "";

      cmdCancel.Visible = true;

      ToggleButtons(false);

      Application.DoEvents();

      cJobs j = new cJobs();

      //////paramIDX = "199";     //debug

      j.getJob(paramIDX);

      if (string.IsNullOrEmpty(j.millCode) == false)
      {

        txtFinYear.Text = j.sappiYear.ToString();
        txtStartMonth.Text = j.sappiMonthStart.ToString();

        RunMonthlyData(j.millCode + j.FLID);
       
        if (j.millCode != "LOM")  // no need to run kpimonthly if lomati selected
        {
          RunKPIMonthly(j.millCode + j.FLID);
        }

        //daily must run after monthly
        RunDailyData(-2, j.millCode + j.FLID);

        if (j.millCode != "LOM")    // no need to run kpimonthly if lomati selected
        {
          RunKPIDaily(-2,j.millCode + j.FLID);
        }


        j.jobStatus = cFG.STATUS_COMPELETED;
        j.completedDate = DateTime.Now;

        j.updJob(paramIDX);

      }

    }// 



    private void cmdStartRun_Click(object sender, EventArgs e)
    {

      ShowSBar("Ready..");
      lblMsg.Text = "";

      string sArea = "";
      string sSite = "";

      int iStartMonth = cmath.geti(txtStartMonth.Text);
      int iPrevDaysWeeks = cmath.geti(txtPrevDays.Text);

      cAreas c = (cAreas)cboAreas.SelectedItem;
      string tmp =  c.AreaCode;

      sSite = cmath.SplitStr(ref tmp, ' ');
      sArea = cmath.SplitStr(ref tmp, ' ');
      if (sArea == "")
      {
        ShowSBar("No Area Selected");
        lblMsg.Text = "No Area Selected";
        return;
      }

      if (rbRunMonth.Checked == false && rbRunWeek.Checked == false && rbRunDaily.Checked == false)
      {
        ShowSBar("No run period selected");
        lblMsg.Text = "No run period selected";
        return;
      }

      if (sSite == "ALL")
      {
        sSite = "";
        sArea = "";
      }

      cmdCancel.Visible = true;

      ToggleButtons(false);

      Application.DoEvents();

      if (rbRunMonth.Checked == true)
      {

        RunMonthlyData(sSite + sArea);

        RunKPIMonthly(sSite + sArea);

      }

      if (rbRunWeek.Checked == true)
      {

        RunWeeklyData(sSite + sArea, iPrevDaysWeeks);

        RunKPIWeekly(sSite + sArea, iPrevDaysWeeks);

      }

      if (rbRunDaily.Checked == true)
      {
         //tmpStartDate will not be used when empty
         
         RunDailyData(iPrevDaysWeeks * -1,sSite + sArea,  tmpStartDate.Text);

         RunKPIDaily(iPrevDaysWeeks * -1, sSite + sArea, tmpStartDate.Text);
      }


      Application.DoEvents();

      ShowSBar("Ready..");
      lblMsg.Text = "Finished";

      ToggleButtons(true);

      //Close();
      //Application.Exit();

    }//



    private void cmdWA_Click(object sender, EventArgs e)
    {
      DialogResult rv = MessageBox.Show("Confirm Run WEIGHTED AVERAGES", "Job", MessageBoxButtons.YesNoCancel);
      if (rv == DialogResult.Yes)
      {
        ToggleButtons(false);
        RunWeightedAverageCalcs(2024);
        ToggleButtons(true);
      }
      Close();
      Application.Exit();
    }



    private void cmdZADailyReport_Click(object sender, EventArgs e)
    {

      DialogResult rv = MessageBox.Show("Confirm Run Job ZA Daily Report", "Job", MessageBoxButtons.YesNoCancel);
      if (rv == DialogResult.Yes)
        RunZAMTDDailyReport();
        //RunZAMTDDailyReportLOOP(31);

      Close();
      Application.Exit();

    }//



    private void cmdGlobalReport_Click(object sender, EventArgs e)
    {
      DialogResult rv = MessageBox.Show("Confirm Run Job Global Report", "Job", MessageBoxButtons.YesNoCancel);
      if (rv == DialogResult.Yes)
        RunGlobalReport();

      Close();
      Application.Exit();

    }



    private void cmdBudgetProduction_Click(object sender, EventArgs e)
    {

      DialogResult rv = MessageBox.Show("NOTHING HERE", "Job", MessageBoxButtons.OK);

      //not implemented - see the KPI0163 in cSiteData.cs  2023-06-13   C-2306-0137

      //DialogResult rv = MessageBox.Show("Confirm Run Job BudgetProduction", "Job", MessageBoxButtons.YesNoCancel);
      //if (rv == DialogResult.Yes)
      //  RunBudgetProductionDaily();

      //Close();
      //Application.Exit();

    }



    private void cmdGradesProdMix_Click(object sender, EventArgs e)
    {
      
      //DialogResult rv = MessageBox.Show("Confirm Run GRADES PRODUCT MIX", "Job", MessageBoxButtons.YesNoCancel);

      //if (rv == DialogResult.Yes)
        //GradesProductMix();


      //Close();
      //Application.Exit();

    }



    private void cmdFullRun_Click(object sender, EventArgs e)
    {
      
      cmdCancel.Visible = true;

      cmdFullRun.Enabled = false;
      Application.DoEvents();

      RunMonthlyData("");

      RunKPIMonthly("");

      RunDailyData(-2);

      RunKPIDaily(-2);

      RunWeeklyData("", 0);

      RunKPIWeekly("", 0);

      RunZAMTDDailyReport();

      RunGlobalReport();

      //GradesProductMix();

      DoOEEDailyBudgets();

      cmdFullRun.Enabled = true;

      Close();

      Application.Exit();


//#endif

    }//



    private void DoRunPrevious()
    {

      cFG.LogIt("Running Previous days");

      try
      {
                              
          RunDailyData(-35);
          RunKPIDaily(-35);
                    
          RunWeeklyData("", 5);
          RunKPIWeekly("", 5);
                    
          //RunZAMTDDailyReportLOOP(5);
          
      }
      catch (Exception er)
      {
        lblMsg.Text = er.Message;
        cFG.LogIt(er.Message);
      }
      finally
      {
      }

    } //



    private void cmdDailyBudget_Click(object sender, EventArgs e)
    {

      DoOEEDailyBudgets();

    } //


    private void DoOEEDailyBudgets()
    {

      cOEEDailyProd bt = new cOEEDailyProd();

      try
      {

        cORA.OpenDB("SAI");
        cCal cal = new cCal();
        cal.GetCal(cmath.getDateNowStr(2));     //get the calendar dates n days ahead, to get ready for next month's daily budgets upload
        cORA.CloseDB();

        ShowSBar("STA 6");
        bt.RunJob("STA", "6", cal.iSappiYear, cal.iCurrMonthOfYear);

        ShowSBar("STA 12");
        bt.RunJob("STA", "12", cal.iSappiYear, cal.iCurrMonthOfYear);

        ShowSBar("TUG 118");
        bt.RunJob("TUG", "118", cal.iSappiYear, cal.iCurrMonthOfYear);

        ShowSBar("SAI 4");
        bt.RunJob("SAI", "4", cal.iSappiYear, cal.iCurrMonthOfYear);

        ShowSBar("SAI 5");
        bt.RunJob("SAI", "5", cal.iSappiYear, cal.iCurrMonthOfYear);

        ShowSBar("SAI 6");
        bt.RunJob("SAI", "6", cal.iSappiYear, cal.iCurrMonthOfYear);

        ShowSBar("NGX 25");
        bt.RunJob("NGX", "25", cal.iSappiYear, cal.iCurrMonthOfYear);

        ShowSBar("NGX 13");
        bt.RunJob("NGX", "13", cal.iSappiYear, cal.iCurrMonthOfYear);

        ShowSBar("NGX 139");
        bt.RunJob("NGX", "139", cal.iSappiYear, cal.iCurrMonthOfYear);


        lblMsg.Text = "Finished";
      }
      catch (Exception er)
      {
        cFG.LogIt(er.Message);
        lblMsg.Text = er.Message;
      }
      finally
      {
        bt = null;
        cORA.CloseDB();
        ShowSBar("Finished");
        cFG.LogIt("Completed daily budgets");
        cFG.LogIt("--------------------------------------------------------------------------------");

      }



    }
    
    
    //-----------------------------------------------------------------------------------------------


    /// <summary>
    /// run 1st
    /// </summary>
    private void RunMonthlyData(string whichOne)
    {

      //fetch data from PTS production and OEE downtime

      int iRunYear = cmath.geti(txtFinYear.Text);                   

      int iStartMonth = cmath.geti(txtStartMonth.Text);            


      cSiteData monthlyData = new cSiteData();

      try
      {

        cFG.LogIt( "Begin monthly site data " + iRunYear.ToString());

        if (boCancel) goto getout;


        if ((whichOne == "") || (whichOne == "TUG118"))
        {
          ShowSBar("TUG 118 " + iRunYear.ToString());
          monthlyData.RunMonthly("TUG", "118", iRunYear, iStartMonth);
          cFG.LogIt("Tugela 118 finished\r\n");
        }

        if (boCancel) goto getout;

        if ((whichOne == "") || (whichOne == "STA6"))
        {
          ShowSBar("STA 6 " + iRunYear.ToString());
          monthlyData.RunMonthly("STA", "6", iRunYear, iStartMonth);
          cFG.LogIt("Stanger 6 finished\r\n");
        }

        if (boCancel) goto getout;

        if ((whichOne == "") || (whichOne == "STA12"))
        {
          ShowSBar("STA 12 " + iRunYear.ToString());
          monthlyData.RunMonthly("STA", "12", iRunYear, iStartMonth);
          cFG.LogIt("Stanger 12 finished\r\n");
        }        

        if (boCancel) goto getout;


        if ((whichOne == "") || (whichOne == "SAI4"))
        {
          ShowSBar("SAI 4 " + iRunYear.ToString());
          monthlyData.RunMonthly("SAI", "4", iRunYear, iStartMonth);
          cFG.LogIt("Saiccor 4 finished\r\n");
        }

        if (boCancel) goto getout;

        if ((whichOne == "") || (whichOne == "SAI5"))
        {
          ShowSBar("SAI 5 " + iRunYear.ToString());
          monthlyData.RunMonthly("SAI", "5", iRunYear, iStartMonth);
          cFG.LogIt("Saiccor 5 finished\r\n");
        }

        if (boCancel) goto getout;

        if ((whichOne == "") || (whichOne == "SAI6"))
        {
          ShowSBar("SAI 6 " + iRunYear.ToString());
          monthlyData.RunMonthly("SAI", "6", iRunYear, iStartMonth);
          cFG.LogIt("Saiccor 6 finished\r\n");
        }

        if (boCancel) goto getout;

        if ((whichOne == "") || (whichOne == "NGX13"))
        {
          ShowSBar("NGX 13 " + iRunYear.ToString());
          monthlyData.RunMonthly("NGX", "13", iRunYear, iStartMonth);
          cFG.LogIt("Ngodwana 13 finished\r\n");
        }


        if (boCancel) goto getout;

        if ((whichOne == "") || (whichOne == "NGX139"))
        {
          ShowSBar("NGX 139 " + iRunYear.ToString());
          monthlyData.RunMonthly("NGX", "139", iRunYear, iStartMonth);
          cFG.LogIt("Ngodwana 139 finished\r\n");
        }

        if ((whichOne == "") || (whichOne == "NGX25"))
        {
          ShowSBar("NGX 25 " + iRunYear.ToString());
          monthlyData.RunMonthly("NGX", "25", iRunYear, iStartMonth);
          cFG.LogIt("Ngodwana 25 finished\r\n");
        }

        //digesters--------------------------------


        if (boCancel) goto getout;

        if ((whichOne == "") || (whichOne == "NGX5"))
        {
          ShowSBar("NGX 5 " + iRunYear.ToString());
          monthlyData.RunMonthly("NGX", "5", iRunYear, iStartMonth);
          cFG.LogIt("Ngodwana 5 finished\r\n");
        }


        if (boCancel) goto getout;

        if ((whichOne == "") || (whichOne == "NGX161"))
        {
          ShowSBar("NGX 161 " + iRunYear.ToString());
          monthlyData.RunMonthly("NGX", "161", iRunYear, iStartMonth);
          cFG.LogIt("Ngodwana 161 finished\r\n");
        }

        if (boCancel) goto getout;

        if ((whichOne == "") || (whichOne == "SAI3"))
        {
          ShowSBar("SAI 3 " + iRunYear.ToString());
          monthlyData.RunMonthly("SAI", "3", iRunYear, iStartMonth);
          cFG.LogIt("SAI 3 finished\r\n");
        }

        if (boCancel) goto getout;

        if ((whichOne == "") || (whichOne == "SAI2"))
        {
          ShowSBar("SAI 2 " + iRunYear.ToString());
          monthlyData.RunMonthly("SAI", "2", iRunYear, iStartMonth);
          cFG.LogIt("SAI 2 finished\r\n");
        }

        if (boCancel) goto getout;

        if ((whichOne == "") || (whichOne == "SAI1"))
        {
          ShowSBar("SAI 1 " + iRunYear.ToString());
          monthlyData.RunMonthly("SAI", "1", iRunYear, iStartMonth);
          cFG.LogIt("SAI 1 finished\r\n");
        }

        if (boCancel) goto getout;

        if ((whichOne == "") || (whichOne == "SAI65"))
        {
          ShowSBar("SAI 65 " + iRunYear.ToString());
          monthlyData.RunMonthly("SAI", "65", iRunYear, iStartMonth);
          cFG.LogIt("SAI 65 finished\r\n");
        }

        if (boCancel) goto getout;

        if ((whichOne == "") || (whichOne == "STA1"))
        {
          ShowSBar("STA 1 " + iRunYear.ToString());
          monthlyData.RunMonthly("STA", "1", iRunYear, iStartMonth);
          cFG.LogIt("STA 1 finished\r\n");
        }

        if (boCancel) goto getout;

        if ((whichOne == "") || (whichOne == "TUG126"))
        {
          ShowSBar("TUG 126 " + iRunYear.ToString());
          monthlyData.RunMonthly("TUG", "126", iRunYear, iStartMonth);
          cFG.LogIt("TUG 126 finished\r\n");
        }


        if ((whichOne == "") || (whichOne == "LOM0"))
        {
          ShowSBar("Lomati " + iRunYear.ToString());
          //monthlyData.RunLomati(iRunYear, iStartMonth);
          cFG.LogIt("Lomati finished\r\n");
        }


      getout:
        cFG.LogIt( "End monthly site data\r\n");

      }
      catch (Exception er)
      {
        lblMsg.Text = er.Message;
        cFG.LogIt( er.Message);
      }
      finally
      {
        monthlyData = null;
      }

      cFG.LogIt( "--------------------------------------\r\n");

    }//



    /// <summary>
    /// run 2nd
    /// </summary>
    private void RunKPIMonthly(string whichOne)
    {

      // zero to run this financial year
      int iSappiYear = cmath.geti(txtFinYear.Text);

      // false processes all months;   true processes current month
      int iStartMonth = cmath.geti(txtStartMonth.Text);


      cKPIJob kpi = new cKPIJob();
      

      cFG.LogIt( "Begin kpi monthly " + iSappiYear.ToString());

      try
      {

        if (boCancel) goto getout;

        if ((whichOne == "") || (whichOne == "TUG118"))
        {
          ShowSBar("TUG 118 " + iSappiYear.ToString());
          kpi.RunMonthly("TUG", "118", iStartMonth, iSappiYear);
          cFG.LogIt("Tugela 118 finished\r\n");
        }


        if (boCancel) goto getout;

        if ((whichOne == "") || (whichOne == "STA6"))
        {
          ShowSBar("STA 6 " + iSappiYear.ToString());
          kpi.RunMonthly("STA", "6", iStartMonth, iSappiYear);
          cFG.LogIt("Stanger 6 finished\r\n");
        }


        if (boCancel) goto getout;

        if ((whichOne == "") || (whichOne == "STA12"))
        {
          ShowSBar("STA 12 " + iSappiYear.ToString());
          kpi.RunMonthly("STA", "12", iStartMonth, iSappiYear);
          cFG.LogIt("Stanger 12 finished\r\n");
        }

        if (boCancel) goto getout;

        if ((whichOne == "") || (whichOne == "NGX139"))
        {
          ShowSBar("NGX 139 " + iSappiYear.ToString());
          kpi.RunMonthly("NGX", "139", iStartMonth, iSappiYear);
          cFG.LogIt("Ngodwana 139 finished\r\n");
        }

        if (boCancel) goto getout;

        if ((whichOne == "") || (whichOne == "NGX25"))
        {
          ShowSBar("NGX 25 " + iSappiYear.ToString());
          kpi.RunMonthly("NGX", "25", iStartMonth, iSappiYear);
          cFG.LogIt("Ngodwana 25 finished\r\n");
        }

        if (boCancel) goto getout;

        if ((whichOne == "") || (whichOne == "NGX13"))
        {
          ShowSBar("NGX 13 " + iSappiYear.ToString());
          kpi.RunMonthly("NGX", "13", iStartMonth, iSappiYear);
          cFG.LogIt("Ngodwana 13 finished\r\n");
        }

        if (boCancel) goto getout;

        if ((whichOne == "") || (whichOne == "SAI4"))
        {
          ShowSBar("SAI 4 " + iSappiYear.ToString());
          kpi.RunMonthly("SAI", "4", iStartMonth, iSappiYear);
          cFG.LogIt("Saiccor 4 finished\r\n");
        }


        if (boCancel) goto getout;

        if ((whichOne == "") || (whichOne == "SAI5"))
        {
          ShowSBar("SAI 5 " + iSappiYear.ToString());
          kpi.RunMonthly("SAI", "5", iStartMonth, iSappiYear);
          cFG.LogIt("Saiccor 5 finished\r\n");
        }

        if (boCancel) goto getout;

        if ((whichOne == "") || (whichOne == "SAI6"))
        {
          ShowSBar("SAI 6 " + iSappiYear.ToString());
          kpi.RunMonthly("SAI", "6", iStartMonth, iSappiYear);
          cFG.LogIt("Saiccor 6 finished\r\n");
        }



        //--------------DIGESTERS------------------------------------------------------


        if (boCancel) goto getout;

        if ((whichOne == "") || (whichOne == "NGX5"))
        {
          ShowSBar("NGX 5 " + iSappiYear.ToString());
          kpi.RunMonthly("NGX", "5", iStartMonth, iSappiYear);
          cFG.LogIt("Ngodwana 5 finished\r\n");
        }


        if (boCancel) goto getout;

        if ((whichOne == "") || (whichOne == "NGX161"))
        {
          ShowSBar("NGX 161 " + iSappiYear.ToString());
          kpi.RunMonthly("NGX", "161", iStartMonth, iSappiYear);
          cFG.LogIt("Ngodwana 161 finished\r\n");
        }

        if (boCancel) goto getout;

        if ((whichOne == "") || (whichOne == "SAI3"))
        {
          ShowSBar("SAI 3 " + iSappiYear.ToString());
          kpi.RunMonthly("SAI", "3", iStartMonth, iSappiYear);
          cFG.LogIt("SAI 3 finished\r\n");
        }

        if (boCancel) goto getout;

        if ((whichOne == "") || (whichOne == "SAI2"))
        {
          ShowSBar("SAI 2 " + iSappiYear.ToString());
          kpi.RunMonthly("SAI", "2", iStartMonth, iSappiYear);
          cFG.LogIt("SAI 2 finished\r\n");
        }

        if (boCancel) goto getout;

        if ((whichOne == "") || (whichOne == "SAI1"))
        {
          ShowSBar("SAI 1 " + iSappiYear.ToString());
          kpi.RunMonthly("SAI", "1", iStartMonth, iSappiYear);
          cFG.LogIt("SAI 1 finished\r\n");
        }

        if (boCancel) goto getout;

        if ((whichOne == "") || (whichOne == "SAI65"))
        {
          ShowSBar("SAI 65 " + iSappiYear.ToString());
          kpi.RunMonthly("SAI", "65", iStartMonth, iSappiYear);
          cFG.LogIt("SAI 65 finished\r\n");
        }


        if (boCancel) goto getout;

        if ((whichOne == "") || (whichOne == "STA1"))
        {
          ShowSBar("STA 1 " + iSappiYear.ToString());
          kpi.RunMonthly("STA", "1", iStartMonth, iSappiYear);
          cFG.LogIt("STA 1 finished\r\n");
        }

        if (boCancel) goto getout;

        if ((whichOne == "") || (whichOne == "TUG126"))
        {
          ShowSBar("TUG 126 " + iSappiYear.ToString());
          kpi.RunMonthly("TUG", "126", iStartMonth, iSappiYear);
          cFG.LogIt("TUG 126 finished\r\n");
        }

        if (whichOne == "LOM0")
          goto getout;

        if (boCancel) goto getout;

        //---------------------------------------------------------------------
        cFG.LogIt("Begin RunWeightedAverageCalcs " + iSappiYear.ToString());
        //---------------------------------------------------------------------

        if ((whichOne == "") || (whichOne.Contains("NGX")))
        {
          ShowSBar("NGX " + iSappiYear.ToString());
          kpi.DoWeightedAve(iSappiYear, "NGX","PPP", "139", "25", "","");
          cFG.LogIt("NGX finished\r\n");
        }

        if (boCancel) goto getout;

        if ((whichOne == "") || (whichOne.Contains("STA")))
        {
          ShowSBar("STA " + iSappiYear.ToString());
          kpi.DoWeightedAve(iSappiYear, "STA", "PPP", "6", "12", "", "");
          cFG.LogIt("STA finished\r\n");
        }

        if (boCancel) goto getout;

        if ((whichOne == "") || (whichOne.Contains("SAI")))
        {
          ShowSBar("SAI " + iSappiYear.ToString());
          kpi.DoWeightedAve(iSappiYear, "SAI", "DWP", "4", "5", "6", "");
          cFG.LogIt("SAI finished\r\n");
        }
        
        if (boCancel) goto getout;


        if ((whichOne == "") || (whichOne.Contains("SAI")))
        {
          ShowSBar("SAI DIG" + iSappiYear.ToString());
          kpi.DoWeightedAve(iSappiYear, "SAI", "DIGB", "3", "2", "1", "65");
          cFG.LogIt("SAI DIG finished\r\n");
        }


        kpi.DoWeightedAveRegionalPaper(iSappiYear);
        kpi.DoWeightedAveRegionalPulp(iSappiYear);
        kpi.DoWeightedAveRegionalDigBatch(iSappiYear);
        kpi.DoWeightedAveRegionalDigCont(iSappiYear);


       if (whichOne == "")
          kpi.SaveKPILastRun("MONTH");

        getout:
        cFG.LogIt( "End Monthly kpi \r\n");
        ShowSBar("Finished Monthly KPI");

      }
      catch (Exception er)
      {
        lblMsg.Text = er.Message;
        cFG.LogIt( er.Message);
      }
      finally
      {
        cORA.CloseDB();
      }
      
      kpi = null;
      cFG.LogIt( "--------------------------------------\r\n");

    }//


    

    /// <summary>
    /// run 3rd
    /// </summary>
    private void RunDailyData(int numDays, string whichOne="", string startDate = "")
    {


      //fetch data from PTS production and OEE downtime

      cSiteData dailyData = new cSiteData();

      cFG.LogIt( "Begin daily site data ");

      try
      {

        DateTime dBegin = Convert.ToDateTime(DateTime.Now.AddDays(numDays));
        
        DateTime dEnd = Convert.ToDateTime(DateTime.Now.AddDays(-1));

        if (string.IsNullOrEmpty(startDate ) == false)
        {
          dBegin = Convert.ToDateTime(startDate);
        }

        //dBegin = Convert.ToDateTime("2023-10-19");
        //dEnd = Convert.ToDateTime("2023-10-19");

        string runDate = "";

        while (dBegin <= dEnd)
        {

          Thread.Sleep(3000);

          runDate = cmath.getDateStr(dBegin);

          cFG.LogIt( "Run Date " + runDate);

          if (boCancel) goto getout;

          if ((whichOne == "") || (whichOne == "TUG118"))
          {
            ShowSBar("TUG 118 " + runDate);
            dailyData.RunDaily("TUG", "118", runDate);
            cFG.LogIt("Tugela 118 finished\r\n");
          }

          if (boCancel) goto getout;

          if ((whichOne == "") || (whichOne == "STA6"))
          {
            ShowSBar("STA 6 " + runDate);
            dailyData.RunDaily("STA", "6", runDate);
            cFG.LogIt("Stanger 6 finished\r\n");
          }

          if (boCancel) goto getout;

          if ((whichOne == "") || (whichOne == "STA12"))
          {
            ShowSBar("STA 12 " + runDate);
            dailyData.RunDaily("STA", "12", runDate);
            cFG.LogIt("Stanger 12 finished\r\n");
          }

          if (boCancel) goto getout;

          if ((whichOne == "") || (whichOne == "SAI4"))
          {
            ShowSBar("SAI 4 " + runDate);
            dailyData.RunDaily("SAI", "4", runDate);
            cFG.LogIt("Saiccor 4 finished\r\n");
          }

          if (boCancel) goto getout;

          if ((whichOne == "") || (whichOne == "SAI5"))
          {
            ShowSBar("SAI 5 " + runDate);
            dailyData.RunDaily("SAI", "5", runDate);
            cFG.LogIt("Saiccor 5 finished\r\n");
          }

          if (boCancel) goto getout;

          if ((whichOne == "") || (whichOne == "SAI6"))
          {
            ShowSBar("SAI 6 " + runDate);
            dailyData.RunDaily("SAI", "6", runDate);
            cFG.LogIt("Saiccor 6 finished\r\n");
          }


          if (boCancel) goto getout;

          if ((whichOne == "") || (whichOne == "NGX13"))
          {
            ShowSBar("NGX 13 " + runDate);
            dailyData.RunDaily("NGX", "13", runDate);
            cFG.LogIt("Ngodwana 13 finished\r\n");
          }

          if (boCancel) goto getout;

          if ((whichOne == "") || (whichOne == "NGX139"))
          {
            ShowSBar("NGX 139 " + runDate);
            dailyData.RunDaily("NGX", "139", runDate); ;
            cFG.LogIt("Ngodwana 139 finished\r\n");
          }

          if (boCancel) goto getout;

          if ((whichOne == "") || (whichOne == "NGX25"))
          {
            ShowSBar("NGX 25 " + runDate);
            dailyData.RunDaily("NGX", "25", runDate);
            cFG.LogIt("Ngodwana 25 finished \r\n");
          }


          //--------------DIGESTERS------------------------------------------------------


          if (boCancel) goto getout;

          if ((whichOne == "") || (whichOne == "NGX5"))
          {
            ShowSBar("NGX 5 " + runDate);
            dailyData.RunDaily("NGX", "5", runDate);
            cFG.LogIt("Ngodwana 5 finished\r\n");
          }


          if (boCancel) goto getout;

          if ((whichOne == "") || (whichOne == "NGX161"))
          {
            ShowSBar("NGX 161 " + runDate);
            dailyData.RunDaily("NGX", "161", runDate);
            cFG.LogIt("Ngodwana 161 finished\r\n");
          }

          if (boCancel) goto getout;

          if ((whichOne == "") || (whichOne == "SAI3"))
          {
            ShowSBar("SAI 3 " + runDate);
            dailyData.RunDaily("SAI", "3", runDate);
            cFG.LogIt("SAI 3 finished\r\n");
          }

          if (boCancel) goto getout;

          if ((whichOne == "") || (whichOne == "SAI2"))
          {
            ShowSBar("SAI 2 " + runDate);
            dailyData.RunDaily("SAI", "2", runDate);
            cFG.LogIt("SAI 2 finished\r\n");
          }

          if (boCancel) goto getout;

          if ((whichOne == "") || (whichOne == "SAI1"))
          {
            ShowSBar("SAI 1 " + runDate);
            dailyData.RunDaily("SAI", "1", runDate);
            cFG.LogIt("SAI 1 finished\r\n");
          }

          if ((whichOne == "") || (whichOne == "SAI65"))
          {
            ShowSBar("SAI 65 " + runDate);
            dailyData.RunDaily("SAI", "65", runDate);
            cFG.LogIt("SAI 65 finished\r\n");
          }

          if (boCancel) goto getout;

          if ((whichOne == "") || (whichOne == "STA1"))
          {
            ShowSBar("STA 1 " + runDate);
            dailyData.RunDaily("STA", "1", runDate);
            cFG.LogIt("STA 1 finished\r\n");
          }

          if (boCancel) goto getout;

          if ((whichOne == "") || (whichOne == "TUG126"))
          {
            ShowSBar("TUG 126 " + runDate);
            dailyData.RunDaily("TUG", "126", runDate);
            cFG.LogIt("TUG 126 finished\r\n");
          }


          dBegin = dBegin.AddDays(1);

        } //while

        getout:
        cFG.LogIt( "End daily data\r\n");
      }
      catch (Exception er)
      {
        lblMsg.Text = er.Message;
        cFG.LogIt( er.Message);
      }
      finally
      {
        dailyData = null;
      }

      cFG.LogIt( "--------------------------------------\r\n");


    }//

    

    /// <summary>
    /// run 4th
    /// </summary>
    private void RunKPIDaily(int numDays, string whichOne = "", string startDate = "")
    {

      cFG.LogIt( "Begin kpi daily run");

      cKPIJob kpi = new cKPIJob();


      try
      {
        DateTime dBegin = Convert.ToDateTime(DateTime.Now.AddDays(numDays));

        
        DateTime dEnd = Convert.ToDateTime(DateTime.Now.AddDays(-1));

        if (string.IsNullOrEmpty(startDate) == false)
        {
            dBegin = Convert.ToDateTime(startDate);
        }

        //dBegin = Convert.ToDateTime("2023-10-19");
        //dEnd = Convert.ToDateTime("2023-10-19");

        string runDate = "";

        while (dBegin <= dEnd)
        {

          Thread.Sleep(3000);

          if (boCancel) goto getout;

          runDate = cmath.getDateStr(dBegin);

          cFG.LogIt( "RunDate " + runDate);

          if (boCancel) goto getout;


          if ((whichOne == "") || (whichOne == "TUG118"))
          {
            ShowSBar("TUG 118 " + runDate);
            kpi.RunDaily("TUG", "118", runDate);
            cFG.LogIt("Tugela 118 finished\r\n");
          }

          if (boCancel) goto getout;

          if ((whichOne == "") || (whichOne == "STA6"))
          {
            ShowSBar("STA 6 " + runDate);
            kpi.RunDaily("STA", "6", runDate);
            cFG.LogIt("Stanger 6 finished\r\n");
          }

          if (boCancel) goto getout;

          if ((whichOne == "") || (whichOne == "STA12"))
          {
            ShowSBar("STA 12 " + runDate);
            kpi.RunDaily("STA", "12", runDate);
            cFG.LogIt("Stanger 12 finished\r\n");
          }

          if (boCancel) goto getout;


          if ((whichOne == "") || (whichOne == "NGX139"))
          {
            ShowSBar("NGX 139 " + runDate);
            kpi.RunDaily("NGX", "139", runDate);
            cFG.LogIt("Ngodwana 139 finished\r\n");
          }


          if (boCancel) goto getout;

          if ((whichOne == "") || (whichOne == "NGX25"))
          {
            ShowSBar("NGX 25 " + runDate);
            kpi.RunDaily("NGX", "25", runDate);
            cFG.LogIt("Ngodwana 25 finished\r\n");
          }


          if (boCancel) goto getout;

          if ((whichOne == "") || (whichOne == "NGX13"))
          {
            ShowSBar("NGX 13 " + runDate);
            kpi.RunDaily("NGX", "13", runDate);
            cFG.LogIt("Ngodwana 13 finished\r\n");
          }

          if (boCancel) goto getout;

          if ((whichOne == "") || (whichOne == "SAI4"))
          {
            ShowSBar("SAI 4 " + runDate);
            kpi.RunDaily("SAI", "4", runDate);
            cFG.LogIt("Saiccor 4 finished\r\n");
          }

          if (boCancel) goto getout;

          if ((whichOne == "") || (whichOne == "SAI5"))
          {
            ShowSBar("SAI 5 " + runDate);
            kpi.RunDaily("SAI", "5", runDate);
            cFG.LogIt("Saiccor 5 finished\r\n");
          }

          if (boCancel) goto getout;

          if ((whichOne == "") || (whichOne == "SAI6"))
          {
            ShowSBar("SAI 6 " + runDate);
            kpi.RunDaily("SAI", "6", runDate);
            cFG.LogIt("Saiccor 6 finished \r\n");
          }

          //--------------DIGESTERS------------------------------------------------------

          if (boCancel) goto getout;

          if ((whichOne == "") || (whichOne == "NGX5"))
          {
            ShowSBar("NGX 5 " + runDate);
            kpi.RunDaily("NGX", "5", runDate);
            cFG.LogIt("Ngodwana 5 finished\r\n");
          }


          if (boCancel) goto getout;

          if ((whichOne == "") || (whichOne == "NGX161"))
          {
            ShowSBar("NGX 161 " + runDate);
            kpi.RunDaily("NGX", "161", runDate);
            cFG.LogIt("Ngodwana 161 finished\r\n");
          }

          if (boCancel) goto getout;

          if ((whichOne == "") || (whichOne == "SAI3"))
          {
            ShowSBar("SAI 3 " + runDate);
            kpi.RunDaily("SAI", "3", runDate);
            cFG.LogIt("SAI 3 finished\r\n");
          }

          if (boCancel) goto getout;

          if ((whichOne == "") || (whichOne == "SAI2"))
          {
            ShowSBar("SAI 2 " + runDate);
            kpi.RunDaily("SAI", "2", runDate);
            cFG.LogIt("SAI 2 finished\r\n");
          }

          if (boCancel) goto getout;

          if ((whichOne == "") || (whichOne == "SAI1"))
          {
            ShowSBar("SAI 1 " + runDate);
            kpi.RunDaily("SAI", "1", runDate);
            cFG.LogIt("SAI 1 finished\r\n");
          }

          if ((whichOne == "") || (whichOne == "SAI65"))
          {
            ShowSBar("SAI 65 " + runDate);
            kpi.RunDaily("SAI", "65", runDate);
            cFG.LogIt("SAI 65 finished\r\n");
          }


          if (boCancel) goto getout;

          if ((whichOne == "") || (whichOne == "STA1"))
          {
            ShowSBar("STA 1 " + runDate);
            kpi.RunDaily("STA", "1", runDate);
            cFG.LogIt("STA 1 finished\r\n");
          }

          if (boCancel) goto getout;

          if ((whichOne == "") || (whichOne == "TUG126"))
          {
            ShowSBar("TUG 126 " + runDate);
            kpi.RunDaily("TUG", "126", runDate);
            cFG.LogIt("TUG 126 finished\r\n");
          }


          dBegin = dBegin.AddDays(1);

        }//loop days


        kpi.SaveKPILastRun("DAY");

        getout:
        cFG.LogIt( "End kpi daily \r\n");

      }
      catch (Exception er)
      {
        lblMsg.Text = er.Message;
        cFG.LogIt( er.Message);
      }
      finally
      {
        cORA.CloseDB();
        cSQL.CloseDB();
      }

      kpi = null;
      //za = null;

      cFG.LogIt( "-----------------------------------------------\r\n");

    }//



    /// <summary>
    /// run 5th
    /// </summary>
    private void RunWeeklyData(string whichOne, int pWeeksBack)
    {
      
      //fetch data from PTS production and OEE downtime

      int iSappiYear = 0;
      int iWeekNum = 0;
      int currWeek = 0;

      cCal cal = new cCal();

      try
      {
        cal.GetCal();
        iSappiYear = cal.iSappiYear;
        currWeek = cal.iCurrWeekOfYear;
      }
      catch (Exception er)
      {
        lblMsg.Text = er.Message;
        cFG.LogIt(er.Message);
      }
      finally
      {
        cal = null;
      }

      iWeekNum = currWeek - pWeeksBack;

      if (iWeekNum <= 0)
        iWeekNum = 1;


      cSiteData weeklyData = new cSiteData();

      try
      {

        cFG.LogIt("Begin weekly site data ");


        while (iWeekNum <= currWeek)
        {

          if ((whichOne == "") || (whichOne == "TUG118"))
          {
            ShowSBar("TUG 118 " + iWeekNum.ToString());
            weeklyData.RunWeekly("TUG", "118", iSappiYear, iWeekNum);
            cFG.LogIt("Tugela 118 finished\r\n");
          }


          if (boCancel) break;


          if ((whichOne == "") || (whichOne == "STA6"))
          {

            ShowSBar("STA 6 " + iWeekNum.ToString());
            weeklyData.RunWeekly("STA", "6", iSappiYear, iWeekNum);
            cFG.LogIt("Stanger 6 finished\r\n");
          }


          if (boCancel) break;


          if ((whichOne == "") || (whichOne == "STA12"))
          {
            ShowSBar("STA 12 " + iWeekNum.ToString());
            weeklyData.RunWeekly("STA", "12" , iSappiYear, iWeekNum);
            cFG.LogIt("Stanger 12 finished\r\n");
          }


          if (boCancel) break;

          if ((whichOne == "") || (whichOne == "SAI4"))
          {
            ShowSBar("SAI 4 " + iWeekNum.ToString());
            weeklyData.RunWeekly("SAI", "4" , iSappiYear, iWeekNum);
            cFG.LogIt("Saiccor 4 finished\r\n");
          }


          if (boCancel) break;

          if ((whichOne == "") || (whichOne == "SAI5"))
          {
            ShowSBar("SAI 5 "  + iWeekNum.ToString());
            weeklyData.RunWeekly("SAI", "5" , iSappiYear, iWeekNum);
            cFG.LogIt("Saiccor 5 finished\r\n");
          }


          if (boCancel) break;

          if ((whichOne == "") || (whichOne == "SAI6"))
          {
            ShowSBar("SAI 6 " + iWeekNum.ToString());
            weeklyData.RunWeekly("SAI", "6",  iSappiYear, iWeekNum);
            cFG.LogIt("Saiccor 6 finished\r\n");
          }


          if (boCancel) break;

          if ((whichOne == "") || (whichOne == "NGX13"))
          {
            ShowSBar("NGX 13 " + iWeekNum.ToString());
            weeklyData.RunWeekly("NGX", "13", iSappiYear, iWeekNum);
            cFG.LogIt("Ngodwana 13 finished\r\n");
          }


          if (boCancel) break;

          if ((whichOne == "") || (whichOne == "NGX139"))
          {
            ShowSBar("NGX 139 " + iWeekNum.ToString());
            weeklyData.RunWeekly("NGX", "139", iSappiYear, iWeekNum);
            cFG.LogIt("Ngodwana 139 finished\r\n");
          }


          if (boCancel) break;

          if ((whichOne == "") || (whichOne == "NGX25"))
          {
            ShowSBar("NGX 25 " + iWeekNum.ToString());
            weeklyData.RunWeekly("NGX", "25", iSappiYear, iWeekNum);
            cFG.LogIt("Ngodwana 25 finished\r\n");
          }

          //--------------DIGESTERS------------------------------------------------------


          if (boCancel) break;

          if ((whichOne == "") || (whichOne == "NGX5"))
          {
            ShowSBar("NGX 5 " + iSappiYear.ToString());
            weeklyData.RunWeekly("NGX", "5", iSappiYear, iWeekNum);
            cFG.LogIt("Ngodwana 5 finished\r\n");
          }


          if (boCancel) break;

          if ((whichOne == "") || (whichOne == "NGX161"))
          {
            ShowSBar("NGX 161 " + iSappiYear.ToString());
            weeklyData.RunWeekly("NGX", "161", iSappiYear, iWeekNum);
            cFG.LogIt("Ngodwana 161 finished\r\n");
          }

          if (boCancel) break;

          if ((whichOne == "") || (whichOne == "SAI3"))
          {
            ShowSBar("SAI 3 " + iSappiYear.ToString());
            weeklyData.RunWeekly("SAI", "3", iSappiYear, iWeekNum);
            cFG.LogIt("SAI 3 finished\r\n");
          }

          if (boCancel) break;

          if ((whichOne == "") || (whichOne == "SAI2"))
          {
            ShowSBar("SAI 2 " + iSappiYear.ToString());
            weeklyData.RunWeekly("SAI", "2", iSappiYear, iWeekNum);
            cFG.LogIt("SAI 2 finished\r\n");
          }

          if (boCancel) break;

          if ((whichOne == "") || (whichOne == "SAI1"))
          {
            ShowSBar("SAI 1 " + iSappiYear.ToString());
            weeklyData.RunWeekly("SAI", "1", iSappiYear, iWeekNum);
            cFG.LogIt("SAI 1 finished\r\n");
          }

          if (boCancel) break;

          if ((whichOne == "") || (whichOne == "SAI65"))
          {
            ShowSBar("SAI 65 " + iSappiYear.ToString());
            weeklyData.RunWeekly("SAI", "65", iSappiYear, iWeekNum);
            cFG.LogIt("SAI 65 finished\r\n");
          }

          if (boCancel) break;

          if ((whichOne == "") || (whichOne == "STA1"))
          {
            ShowSBar("STA 1 " + iSappiYear.ToString());
            weeklyData.RunWeekly("STA", "1", iSappiYear, iWeekNum);
            cFG.LogIt("STA 1 finished\r\n");
          }

          if (boCancel) break;

          if ((whichOne == "") || (whichOne == "TUG126"))
          {
            ShowSBar("TUG 126 " + iSappiYear.ToString());
            weeklyData.RunWeekly("TUG", "126", iSappiYear, iWeekNum);
            cFG.LogIt("TUG 126 finished\r\n");
          }



          iWeekNum++;
        }


        cFG.LogIt("End weekly site data\r\n");

      }
      catch (Exception er)
      {
        lblMsg.Text = er.Message;
        cFG.LogIt(er.Message);
      }
      finally
      {
        weeklyData = null;
      }

      cFG.LogIt("--------------------------------------\r\n");

    }//



    /// <summary>
    /// run 6th
    /// </summary>
    private void RunKPIWeekly(string whichOne, int pWeeksBack)
    {

      cFG.LogIt("Begin kpi weekly run");

      int iSappiYear = 0;
      int iWeekNum = 0;
      int currWeek = 0;

      cCal cal = new cCal();

      try
      {
        cal.GetCal();
        iSappiYear = cal.iSappiYear;
        currWeek = cal.iCurrWeekOfYear;
      }
      catch (Exception er)
      {
        lblMsg.Text = er.Message;
        cFG.LogIt(er.Message);
      }
      finally
      {
        cal = null;
      }

      iWeekNum = currWeek - pWeeksBack;

      if (iWeekNum <= 0)
        iWeekNum = 1;

     

      cKPIJob kpi = new cKPIJob();

  

      try
      {
        while (iWeekNum <= currWeek)
        {

        if (iWeekNum % 3 == 0)
        {
             Application.DoEvents();
        }


          if ((whichOne == "") || (whichOne == "TUG118"))
          {
            ShowSBar("TUG 118 ");
            kpi.RunWeekly("TUG", "118", iSappiYear, iWeekNum);
            cFG.LogIt("Tugela 118 finished\r\n");
          }

          if (boCancel) break;

          if ((whichOne == "") || (whichOne == "STA6"))
          {
            ShowSBar("STA 6 ");
            kpi.RunWeekly("STA", "6", iSappiYear, iWeekNum);
            cFG.LogIt("Stanger 6 finished\r\n");
          }


          if (boCancel) break;

          if ((whichOne == "") || (whichOne == "STA12"))
          {
            ShowSBar("STA 12 ");
            kpi.RunWeekly("STA", "12", iSappiYear, iWeekNum);
            cFG.LogIt("Stanger 12 finished\r\n");
          }

          if (boCancel) break;

          if ((whichOne == "") || (whichOne == "NGX139"))
          {
            ShowSBar("NGX 139 ");
            kpi.RunWeekly("NGX", "139", iSappiYear, iWeekNum);
            cFG.LogIt("Ngodwana 139 finished\r\n");
          }

          if (boCancel) break;

          if ((whichOne == "") || (whichOne == "NGX25"))
          {
            ShowSBar("NGX 25 ");
            kpi.RunWeekly("NGX", "25", iSappiYear, iWeekNum);
            cFG.LogIt("Ngodwana 25 finished\r\n");
          }

          if (boCancel) break;

          if ((whichOne == "") || (whichOne == "NGX13"))
          {
            ShowSBar("NGX 13 ");
            kpi.RunWeekly("NGX", "13", iSappiYear, iWeekNum);
            cFG.LogIt("Ngodwana 13 finished\r\n");
          }

          if (boCancel) break;

          if ((whichOne == "") || (whichOne == "SAI4"))
          {
            ShowSBar("SAI 4 ");
            kpi.RunWeekly("SAI", "4", iSappiYear, iWeekNum);
            cFG.LogIt("Saiccor 4 finished\r\n");
          }

          if (boCancel) break;

          if ((whichOne == "") || (whichOne == "SAI5"))
          {
            ShowSBar("SAI 5 ");
            kpi.RunWeekly("SAI", "5", iSappiYear, iWeekNum);
            cFG.LogIt("Saiccor 5 finished\r\n");
          }

          if (boCancel) break;

          if ((whichOne == "") || (whichOne == "SAI6"))
          {
            ShowSBar("SAI 6 ");
            kpi.RunWeekly("SAI", "6", iSappiYear, iWeekNum);
            cFG.LogIt("Saiccor 6 finished \r\n");
          }


          //--------------DIGESTERS------------------------------------------------------


          if (boCancel) break;

          if ((whichOne == "") || (whichOne == "NGX5"))
          {
            ShowSBar("NGX 5 " + iSappiYear.ToString());
            kpi.RunWeekly("NGX", "5", iSappiYear, iWeekNum);
            cFG.LogIt("Ngodwana 5 finished\r\n");
          }


          if (boCancel) break;

          if ((whichOne == "") || (whichOne == "NGX161"))
          {
            ShowSBar("NGX 161 " + iSappiYear.ToString());
            kpi.RunWeekly("NGX", "161", iSappiYear, iWeekNum);
            cFG.LogIt("Ngodwana 161 finished\r\n");
          }

          if (boCancel) break;

          if ((whichOne == "") || (whichOne == "SAI3"))
          {
            ShowSBar("SAI 3 " + iSappiYear.ToString());
            kpi.RunWeekly("SAI", "3", iSappiYear, iWeekNum);
            cFG.LogIt("SAI 3 finished\r\n");
          }

          if (boCancel) break;

          if ((whichOne == "") || (whichOne == "SAI2"))
          {
            ShowSBar("SAI 2 " + iSappiYear.ToString());
            kpi.RunWeekly("SAI", "2", iSappiYear, iWeekNum);
            cFG.LogIt("SAI 2 finished\r\n");
          }

          if (boCancel) break;

          if ((whichOne == "") || (whichOne == "SAI1"))
          {
            ShowSBar("SAI 1 " + iSappiYear.ToString());
            kpi.RunWeekly("SAI", "1", iSappiYear, iWeekNum);
            cFG.LogIt("SAI 1 finished\r\n");
          }

          if (boCancel) break;

          if ((whichOne == "") || (whichOne == "SAI65"))
          {
            ShowSBar("SAI 65 " + iSappiYear.ToString());
            kpi.RunWeekly("SAI", "65", iSappiYear, iWeekNum);
            cFG.LogIt("SAI 65 finished\r\n");
          }


          if (boCancel) break;

          if ((whichOne == "") || (whichOne == "STA1"))
          {
            ShowSBar("STA 1 " + iSappiYear.ToString());
            kpi.RunWeekly("STA", "1", iSappiYear, iWeekNum);
            cFG.LogIt("STA 1 finished\r\n");
          }

          if (boCancel) break;

          if ((whichOne == "") || (whichOne == "TUG126"))
          {
            ShowSBar("TUG 126 " + iSappiYear.ToString());
            kpi.RunWeekly("TUG", "126", iSappiYear, iWeekNum);
            cFG.LogIt("TUG 126 finished\r\n");
          }

          iWeekNum++;

      }  //for


        cFG.LogIt("End kpi weekly \r\n");

      }
      catch (Exception er)
      {
        lblMsg.Text = er.Message;
        cFG.LogIt(er.Message);
      }
      finally
      {
        cORA.CloseDB();
        cSQL.CloseDB();
      }

      kpi = null;
      //za = null;

      cFG.LogIt("-----------------------------------------------\r\n");

    }//


    private void RunWeightedAverageCalcs(int pSappiYear)
    {
        
      //manual run

      cKPIJob kpi = new cKPIJob();


      cFG.LogIt("Begin RunWeightedAverageCalcs " + pSappiYear.ToString());

      try
      {

        if (boCancel) goto getout;

        ShowSBar("SAI " + pSappiYear.ToString());
        kpi.DoWeightedAve(pSappiYear, "SAI", "DWP", "4", "5", "6", "");
        cFG.LogIt("SAI finished\r\n");

        if (boCancel) goto getout;

        ShowSBar("NGX " + pSappiYear.ToString());
        kpi.DoWeightedAve(pSappiYear, "NGX", "PPP", "139", "25","", "");
        cFG.LogIt("NGX finished\r\n");

        if (boCancel) goto getout;

        ShowSBar("STA " + pSappiYear.ToString());
        kpi.DoWeightedAve(pSappiYear, "STA", "PPP", "6", "12", "", "");
        cFG.LogIt("STA finished\r\n");

        if (boCancel) goto getout;


        ShowSBar("SAI DIG " + pSappiYear.ToString());
        kpi.DoWeightedAve(pSappiYear, "SAI", "DIGB", "3", "2", "1", "65");
        cFG.LogIt("SAI DIG finished\r\n");

        //ShowSBar("TUG " + pSappiYear.ToString());
        //kpi.DoWeightedAve(pSappiYear, "TUG", "118", "", "");
        //cFG.LogIt("TUG finished\r\n");

        if (boCancel) goto getout;

        kpi.DoWeightedAveRegionalPaper(pSappiYear);
        kpi.DoWeightedAveRegionalPulp(pSappiYear);
        kpi.DoWeightedAveRegionalDigBatch(pSappiYear);
        kpi.DoWeightedAveRegionalDigCont(pSappiYear);


      getout:
        cFG.LogIt("End RunWeightedAverageCalcs \r\n");
        ShowSBar("Finished RunWeightedAverageCalcs");

      }
      catch (Exception er)
      {
        lblMsg.Text = er.Message;
        cFG.LogIt(er.Message);
      }
      finally
      {
        cORA.CloseDB();
        kpi = null;
      }
     
      cFG.LogIt("--------------------------------------\r\n");

    }//


    private void RunZAMTDDailyReport()
    {
        

      lblMsg.Text = "Begin";

      cZADailyMTDReport za = new cZADailyMTDReport();
      try
      {
        ShowSBar("Run cZADailyMTDReport");
        cFG.LogIt("Run cZADailyMTDReport " + cmath.getDateNowStr(-1));

        za.RunJob(cmath.getDateNowStr(-1));
        //za.RunJob("2023-01-23");

        cFG.LogIt("Finished cZADailyMTDReport ");
        ShowSBar("Finished cZADailyMTDReport");

        lblMsg.Text = "End";

      }
      catch (Exception er)
      {
        lblMsg.Text = er.Message;
        cFG.LogIt("cZADailyMTDReport " + er.Message);
        ShowSBar(lblMsg.Text);
      }
      finally
      {
        cORA.CloseDB();
        cSQL.CloseDB();
        za = null;
      }

    }//


    private void RunGlobalReport()
    {

      lblMsg.Text = "Begin";
      
      string runDate = "";

      cCal cal = new cCal();
      
      //get previous day
      cal.GetCal(cmath.getDateNowStr(-1));

      
      //get previous month's last date  to retrieve last month's start and end dates
      runDate = cmath.getDateStr(cal.sMonthStartDate, -1);    

      cal.GetCal(runDate);        // last month start and end dates

      //cal.GetCal("2024-12-31");        // run  a different month

      runDate = "";

      DateTime dStart = Convert.ToDateTime(cal.sMonthStartDate);
      DateTime dEnd = DateTime.Now.AddDays(-1);


      cGlobalReport g = new cGlobalReport();

      try
      {

        ShowSBar("Run Global Report");
        cFG.LogIt("Run Global Report " + cmath.getDateNowStr(-1));

        ////g.RunJob("2023-10-29");      // test one day only


        while (dStart <= dEnd)
        {
          runDate = cmath.getDateStr(dStart);
          g.RunJob(runDate);
          dStart = dStart.AddDays(1);
        }


        cFG.LogIt("Finished Global Report ");
        ShowSBar("Finished Global Report");

        lblMsg.Text = "End";

      }
      catch (Exception er)
      {
        lblMsg.Text = er.Message;
        cFG.LogIt("Global Report " + er.Message);
        ShowSBar(lblMsg.Text);
      }
      finally
      {
        cSQL.CloseDB();
        g = null;
      }

    }//




    private void RunZAMTDDailyReportLOOP(int numDays)
    {


      cZADailyMTDReport za = new cZADailyMTDReport();

      lblMsg.Text = "Begin";

      try
      {

        for (int i = numDays; i > 0; i--)
        {
          if (i % 5 == 0)
            Application.DoEvents();

          cFG.LogIt("------------------------------------------------------");
          cFG.LogIt("Days back " + i.ToString());
          lblMsg.Text = cmath.getDateNowStr(-i);
          za.RunJob(cmath.getDateNowStr(-i));
        }

        lblMsg.Text = "End";

      }
      catch (Exception er)
      {
        lblMsg.Text = er.Message;
        cFG.LogIt("cZADailyMTDReportLOOP " + er.Message);
        ShowSBar(lblMsg.Text);
      }
      finally
      {
        cORA.CloseDB();
        cSQL.CloseDB();
        za = null;
      }

    }//



    private void GradesProductMix()
    {


      lblMsg.Text = "Begin";


      int iSappiYear = cmath.geti(txtFinYear.Text);             //0 = current year
        
      int iSappiMonth = cmath.geti(txtStartMonth.Text);         //0 = current month


      cGradesProductMix gr = new cGradesProductMix();
      try
      {
        ShowSBar("Run GradesProductMix");
        cFG.LogIt("Run GradesProductMix ");

        if (boCancel) goto getout;

        ShowSBar("STA");
        cFG.LogIt("Begin STA");
        gr.RunJob("STA", iSappiYear, iSappiMonth);
        cFG.LogIt("Finished STA");

        if (boCancel) goto getout;

        ShowSBar("NGX");
        cFG.LogIt("Begin NGX");
        gr.RunJob("NGX", iSappiYear, iSappiMonth);
        cFG.LogIt("Finished NGX");


        ShowSBar("Saiccor");
        cFG.LogIt("Begin Saiccor");
        gr.RunJob("SAI", iSappiYear, iSappiMonth);
        cFG.LogIt("Finished Saiccor");

        if (boCancel) goto getout;

        ShowSBar("TUG");
        cFG.LogIt("Begin TUG");
        gr.RunJob("TUG", iSappiYear, iSappiMonth);
        cFG.LogIt("Finished TUG");


        getout:

        cFG.LogIt("Finished GradesProductMix ");
        ShowSBar("Finished GradesProductMix");

        lblMsg.Text = "End";

      }
      catch (Exception er)
      {
        lblMsg.Text = er.Message;
        cFG.LogIt("GradesProductMix " + er.Message);
        ShowSBar(lblMsg.Text);
      }
      finally
      {
        cORA.CloseDB();
        cSQL.CloseDB();
        gr = null;
      }

    }//



    #region ----OEE Comments ADD TO ZAMDW--------------
    private void AddMDWComments()
    {

      //upload comments to ZAMDW from OEE sites
      cFG.LogIt( "----------------------UPDATE ZAMDW COMMENTS-----------------------------");

      DoComments(SAI, "SAI", "4", "PM3");       
      DoComments(SAI, "SAI", "5", "PM4");
      DoComments(SAI, "SAI", "6", "PM5");
      cFG.LogIt( "Finished Saiccor");

      if (boCancel)       return;

      DoComments(TUG, "TUG", "118", "PM2");
      cFG.LogIt( "Finished Tugela");

      if (boCancel) return;

      DoComments(STA, "STA", "6", "P1");
      DoComments(STA, "STA", "12", "ST");
      cFG.LogIt( "Finished Stanger");

      if (boCancel) return;

      DoComments(NGX, "NGX", "139", "PM1");
      DoComments(NGX, "NGX", "25", "PM2");
      DoComments(NGX, "NGX", "13", "UT3");

      cFG.LogIt( "Finished Ngodwana");

    }//


    private void DoComments(byte pMillKey, string pDataBase, string pFLID, string pPlantCode)
    {

      string sComment = "";
      string sql = "";

      try
      {

        sql = "select e.eventid, e.flid, f.functionaldesc, e.datefrom, pup.description, r.reason, c.category, e.downtimedesc"
         + "    , to_char(round(e.dateTO - e.datefrom, 3) * 24, '9999990.00')  AS Hrs "
         + " From event e , functionalareas f, planunplan pup, reasons r, categories c"
         + " where   "
         + "    e.flid = f.flid"
         //+ "    and e.managementreport = 1 "
         + "    and((e.dateTO - e.datefrom) * 24) >= 2"
         + "    and e.flid = " + pFLID
         //+ "    and trunc(e.datefrom) = trunc(sysdate - 1)"
         + "    and trunc(sysdate - 1) between trunc(e.datefrom) and trunc(e.dateTo)"
         + "    and e.reasonid = r.reasonid"
         + "    and r.planunplan = pup.planunplan"
         + "    and (pup.planunplan = 1 or pup.planunplan = 0)"
         + "    and e.categoryid = c.categoryid"
         + "    order by e.datefrom "
         + "";


        cSQL.OpenDB("ZAMDW");

        cORA.OpenDB(pDataBase);
        DataTable dt = cORA.GetDataTable(sql);

        cFG.LogIt( "Records found : " + dt.Rows.Count);

        foreach (DataRow item in dt.Rows)
        {

          //string sLog = item["eventid"].ToString()
          //        + " " + item["Hrs"].ToString()
          //        + " " + item["functionaldesc"].ToString()
          //        + " " + item["datefrom"].ToString()
          //        + " " + item["description"].ToString()
          //        + " " + item["reason"].ToString()
          //        + " " + item["category"].ToString()
          //        + " " + item["downtimedesc"].ToString();


          //Console.WriteLine(sLog);

          //cFG.LogIt(sLog);

          //if ((item["category"].ToString().ToLower() == "runrate on budget")
          //      || (item["category"].ToString().ToLower() == "runrate above budget"))
          //{
          //  continue; //do not process these
          //}


          sComment = "(" + item["Hrs"].ToString().Trim() + "Hrs) " +  item["downtimedesc"].ToString().Replace("'", "''");

          sql = "Select MillKey, PlantCode, EventDate from ProdComments ";
          sql += " where MillKey = " + pMillKey;
          sql += " and PlantCode = '" + pPlantCode + "'";
          sql += " and EventDate = '" + cmath.getDateTimeStr(item["datefrom"].ToString()) + "'";
          DataTable dtMDW = cSQL.GetDataTable(sql);
          if (dtMDW.Rows.Count == 0)
          {
            sql = "INSERT INTO ProdComments (MillKey, PlantCode, EventDate, EventID, Comment)";
            sql += " VALUES(";
            sql += pMillKey;
            sql += ",'" + pPlantCode + "'";
            sql += ",'" + cmath.getDateTimeStr(item["datefrom"].ToString()) + "'";
            sql += ",'" + item["EventID"].ToString() + "'";
            sql += ",'" + sComment + "'";
            sql += ")";
            cSQL.ExecuteQuery(sql);
          }
          else
          {
            sql = "UPDATE ProdComments Set Comment = '" + sComment + "'";
            sql += ", EventID = " + item["EventID"].ToString();
            sql += ", LastUpdated = '" + cmath.getDateTimeNowStr() + "'";
            sql += " Where MillKey = " + pMillKey;
            sql += " And PlantCode = '" + pPlantCode + "'";
            sql += " And EventDate = '" + cmath.getDateTimeStr(item["datefrom"].ToString()) + "'";
            cSQL.ExecuteQuery(sql);
          }


        } //foreach


      }
      catch (Exception tt)
      {
        cFG.LogIt( pPlantCode + " " + tt.Message);
        cFG.LogIt( sql);
      }
      finally
      {
        cORA.CloseDB();
        cSQL.CloseDB();
      }


    }//



    #endregion



    
    #region ---Prepare email for OEE Comments-----------------------

    private void PrepEmails()
    {
      bool boFound = false;
      int ndx = 0;
      string theMessage = "";
      string sendBCC = "denys.dearruda @sappi.com";
      string[] sendTo = new string[MAXEMAILS];
      for (int i = 0; i < MAXEMAILS; i++)
      {
        sendTo[i] = "";
      }

      ndx = 0;
      
      sendTo[ndx++] = "Themba.Mabuza@sappi.com";
      sendTo[ndx++] = "Kugan.Moodley@sappi.com";
      sendTo[ndx++] = "Sharon.Bunting@sappi.com";
      sendTo[ndx++] = "Gopalan.Naidoo@sappi.com";
      sendTo[ndx++] = "Bhim.Kissoon@sappi.com";
      sendTo[ndx++] = "Quintin.Edwards@sappi.com";
      sendTo[ndx++] = "Sizwe.Ntshalintshali@sappi.com";
      sendTo[ndx++] = "Siyanda.Nxele@sappi.com";
      sendTo[ndx++] = "Sinothi.Phoswa@sappi.com";
      sendTo[ndx++] = "Sabiha.Bacaus@sappi.com";
      sendTo[ndx++] = "Gerda.Roberts@sappi.com";
      sendTo[ndx++] = "Krish.Naidu@sappi.com";
      sendTo[ndx++] = "Chris.Francis@sappi.com";
      sendTo[ndx++] = "Reagan.Naidoo@sappi.com";
      sendTo[ndx++] = "poobie.govender@sappi.com";
      sendTo[ndx++] = "jan.cillie@sappi.com";
      sendTo[ndx++] = "Kitesh.Sookdawe@sappi.com";
      sendTo[ndx++] = "Trevor.Harrison@sappi.com";
      sendTo[ndx++] = "naresh.tiribeni@sappi.com";      //2022-07-26
      sendTo[ndx++] = "Zamakhosi.Mthethwa@sappi.com";      //2024-10-03
      sendTo[ndx++] = "Sakhile.Zungu@sappi.com";            //2024-11-20
         


         ////debug------------
         //for (int i = 0; i < MAXEMAILS; i++)
         //{
         //  sendTo[i] = "";
         //}

         //ndx = 0;
         //sendTo[ndx++] = "denys.dearruda@sappi.com";
         //sendBCC = "";
         ////------------------


         theMessage = "The following Planned and Unplanned Downtime Events => 2 hrs from the OEE system will appear in the global daily detailed report.";

      theMessage += "<br /><br />";
      theMessage += "<table  style=\"margin:auto; width:98%; border-width:thin; border-style:solid;border-color:Gray;font-family:Calibri;font-size:small;border-collapse:collapse;\">";
      theMessage += "<tr style=\"background-color:AliceBlue;\">";
      theMessage += "<td style=\"width:8%;border-style:solid;border-color:Silver; border-width:thin\">" + "Mill" + "</td>";
      theMessage += "<td style=\"width:8%;border-style:solid;border-color:Silver; border-width:thin\">" + "EventID" + "</td>";
      theMessage += "<td style=\"width:10%;border-style:solid;border-color:Silver; border-width:thin\">" + "EventDate" + "</td>";
      //theMessage += "<td style=\"width:5%;border-style:solid;border-color:Silver; border-width:thin\">" + "Area" + "</td>";
      theMessage += "<td style=\"width:10%;border-style:solid;border-color:Silver; border-width:thin\">" + "AreaDesc" + "</td>";
      theMessage += "<td style=\"width:49%;border-style:solid;border-color:Silver; border-width:thin\">" + "Comments" + "</td>";
      theMessage += "</tr>";

      try
      {
        cFG.LogIt("----------------------PREPARE EMAILS-----------------------------");

        PrepEmailDailyCommentsMDW(NGX, "NGX", "139", "PM1", ref theMessage, ref boFound);
        PrepEmailDailyCommentsMDW(NGX, "NGX", "25", "PM2", ref theMessage, ref boFound);
        PrepEmailDailyCommentsMDW(NGX, "NGX", "13", "UT3", ref theMessage, ref boFound);
        cFG.LogIt("Finished Ngodwana");

        if (boCancel) return;


        PrepEmailDailyCommentsMDW(SAI, "SAI", "4", "PM3", ref theMessage, ref boFound);
        PrepEmailDailyCommentsMDW(SAI, "SAI", "5", "PM4", ref theMessage, ref boFound);
        PrepEmailDailyCommentsMDW(SAI, "SAI", "6", "PM5", ref theMessage, ref boFound);
        cFG.LogIt("Finished Saiccor");

        if (boCancel) return;

        PrepEmailDailyCommentsMDW(TUG, "TUG", "118", "PM2", ref theMessage, ref boFound);
        cFG.LogIt("Finished Tugela");

        if (boCancel) return;

        PrepEmailDailyCommentsMDW(STA, "STA", "6", "P1", ref theMessage, ref boFound);
        PrepEmailDailyCommentsMDW(STA, "STA", "12", "ST", ref theMessage, ref boFound);
        cFG.LogIt("Finished Stanger");

        if (boCancel) return;

        theMessage += "</table>";

        if (boFound)
          SendEmail(sendTo, "", sendBCC, "Global Report Items", theMessage);

      }
      catch (Exception tt)
      {
        cFG.LogIt(tt.Message);
        lblMsg.Text = tt.Message;
      }

    }//



    private void PrepEmailDailyCommentsMDW(byte pMillKey, string pDataBase, string pFLID, string pPlantCode, ref string theMessage, ref bool boFound)
    {

      string sql = "";

      StringBuilder sb = new StringBuilder();

      try
      {

        sql = "select e.eventid, e.flid, f.functionaldesc, f.functionalArea, e.datefrom, pup.description, r.reason, c.category, e.downtimedesc"
         + " , to_char(round(e.dateTO - e.datefrom, 3) * 24, '9999990.00')  AS Hrs  "
           + " From   event e , functionalareas f, planunplan pup, reasons r, categories c"
           + " where   "
           + "    e.flid = f.flid"
           //+ "    and e.managementreport = 1 "
           + "    and((e.dateTO - e.datefrom) * 24) >= 2"
           + "    and e.flid = " + pFLID
           //+ "    and trunc(e.datefrom) = trunc(sysdate - 1)"
           + "    and trunc(sysdate - 1) between trunc(e.datefrom) and trunc(e.dateTo)"
           + "    and e.reasonid = r.reasonid"
           + "    and r.planunplan = pup.planunplan"
           + "    and (pup.planunplan = 1 or pup.planunplan = 0)"     
           + "    and e.categoryid = c.categoryid"
           + "    order by e.datefrom "
           + "";


        cORA.OpenDB(pDataBase);
        DataTable dt = cORA.GetDataTable(sql);

        cFG.LogIt( "Records found : " + dt.Rows.Count);


        foreach (DataRow item in dt.Rows)
        {

          //string sLog = item["eventid"].ToString()
          //        + " " + item["Hrs"].ToString()
          //        + " " + item["functionaldesc"].ToString()
          //        + " " + item["datefrom"].ToString()
          //        + " " + item["description"].ToString()
          //        + " " + item["reason"].ToString()
          //        + " " + item["category"].ToString()
          //        + " " + item["downtimedesc"].ToString();


          //if ((item["category"].ToString().ToLower() == "runrate on budget")
          //      || (item["category"].ToString().ToLower() == "runrate above budget")
          //      || (item["category"].ToString().ToLower() == "runrate below budget"))
          //{
          //  continue; //do not process these
          //}


          boFound = true;
          sb.AppendLine("<tr>");
          sb.AppendLine("<td style=\"width:8%;border-style:solid;border-color:Silver; border-width:thin\">" + pDataBase + "</td>");
          sb.AppendLine("<td style=\"width:8%;border-style:solid;border-color:Silver; border-width:thin\">" + item["EventID"].ToString() + "</td>");
          sb.AppendLine("<td style=\"width:10%;border-style:solid;border-color:Silver; border-width:thin\">" + item["datefrom"].ToString() + "</td>");
          //sb.AppendLine("<td style=\"width:5%;border-style:solid;border-color:Silver; border-width:thin\">" + item["functionalArea"].ToString() + "</td>");
          sb.AppendLine("<td style=\"width:10%;border-style:solid;border-color:Silver; border-width:thin\">" + item["functionaldesc"].ToString() + "</td>");
          sb.AppendLine("<td style=\"width:49%;border-style:solid;border-color:Silver; border-width:thin\">"  
                      + "(" + item["Hrs"].ToString().Trim()  +  "Hrs) " + item["downtimedesc"].ToString() + "</td>");
          sb.AppendLine("</tr>");

          
        } //foreach

        theMessage += sb.ToString();

      }
      catch (Exception tt)
      {
        cFG.LogIt( pPlantCode + " " + tt.Message);
      }
      finally
      {
        cORA.CloseDB();
      }

    }//



    private void SendEmail(string[] sSendTo, string sSendCC, string sSendBCC, string sSubject, string sMessage)
    {

      /*
      //http://  blogs.msdn.com/b/mariya/archive/2006/06/15/633007.aspx
       * https: //msdn.microsoft.com/en-us/library/system.net.mail.smtpclient.aspx
      */


      SmtpClient client = new SmtpClient("smtp.za.sappi.com");

      //client.EnableSsl = true;

      //MailAddress from = new MailAddress("server@sappi.com");

      //MailAddress to = new MailAddress();

      MailMessage mailMsg = new MailMessage();

      mailMsg.From = new MailAddress("server@sappi.com");

      for (int i = 0; i < sSendTo.Length; i++)
      {
        if (sSendTo[i] != "")
          mailMsg.To.Add(new MailAddress(sSendTo[i]));
      }


      if (sSendCC != "")
        mailMsg.CC.Add(new MailAddress(sSendCC));

      if (sSendBCC != "")
        mailMsg.Bcc.Add(sSendBCC);

      mailMsg.Priority = MailPriority.Normal;
      mailMsg.SubjectEncoding = System.Text.Encoding.UTF8;
      mailMsg.BodyEncoding = System.Text.Encoding.UTF8;
      mailMsg.IsBodyHtml = true;
      //message.Body = sMessage;    // see below

      mailMsg.Subject = sSubject;

      client.UseDefaultCredentials = true;

      mailMsg.Body = sMessage;

      try
      {
        client.Send(mailMsg);
      }

      catch (Exception ex)
      {
        cFG.LogIt( "Error sending Email : " + ex.Message);
        lblMsg.Text = ex.Message;
      }
    }



    #endregion



    #region ---Update OEE sites with the latest HR Info-----------------------

    private void cmdUserHR_Click(object sender, EventArgs e)
    {

         //cFG.LogIt("Updating OEE userHR  tables from HRFeed");

         //cFG.LogIt("Processing OEE Lomati ");
         //UpdOEELomatiHRInfo();
         //cFG.LogIt("Finished  OEE Lomati ");



         //cFG.LogIt("Processing STA");
         //UpdOEEHRInfo("STA");
         //cFG.LogIt("Finished STA");

         //cFG.LogIt("Processing TUG");
         //UpdOEEHRInfo("TUG");
         //cFG.LogIt("Finished TUG");

         //cFG.LogIt("Processing SAI");
         //UpdOEEHRInfo("SAI");
         //cFG.LogIt("Finished SAI");

         //cFG.LogIt("Processing NGX");
         //UpdOEEHRInfo("NGX");
         //cFG.LogIt("Finished NGX");

      }


    private void UpdOEEHRInfo(string site)
    {

      int rv = 0;
      string sql = "";
      int cnt = 0;
      string empNo = "";
      string lastName = "";
      string firstName = "";
      string emailAddr = "";
      string userName = "";

      try
      {
        
        cORA.OpenDB(site);

        cSQL.OpenDB("HRFeed");

        sql = "select country,UserName, EmpNo, SName, FName, LName, Email, employeestatus from hrinfo ";
        sql += " where Country = 'South Africa'";
        sql += " and UserName != ''";
        sql += " and EmployeeStatus != 'Withdrawn'";
        //sql += "";

        DataTable dt = cSQL.GetDataTable(sql);
        foreach (DataRow item in dt.Rows)
        {
          userName = item["UserName"].ToString().ToUpper();
          empNo = item["EmpNo"].ToString();
          lastName = item["LName"].ToString().Replace("'","''");
          firstName = item["FName"].ToString().Replace("'", "''");
          emailAddr = item["Email"].ToString().Replace("'", "''");

          if (cnt++ % 5 == 0)
          {
            lblMsg.Text = userName;
            Application.DoEvents();
          }
            

          sql = "Update USERSHR set userName = '" + userName + "'"
              + ", FirstName =  '" + firstName + "'"
              + ", LastName =  '" + lastName + "'"
              + ", Email =  '" + emailAddr + "'"
              + ", Active =  '" + 1 + "'"
              + ", LastUpdate =  " + cmath.OraDateTime(cmath.getDateTimeNowStr())
              + " where EmpNo = " + empNo;

          rv = cORA.ExecQuery(sql);
          if (rv <= 0)
          {
            sql = "Insert Into USERSHR (username, empno, firstname, lastname, email, active) Values("
              + " '" + userName + "'"
              + ", " + empNo
              + ",'" + firstName + "'"
              + ",'" + lastName + "'"
              + ",'" + emailAddr + "'"
              + ", 1"
              + ")";

            rv = cORA.ExecQuery(sql);

          }

        } //foreach
        
        lblMsg.Text = "Finished";

      }//try
      catch (Exception er)
      {
        cFG.LogIt(er.Message);
        cFG.LogIt(sql);
        lblMsg.Text = er.Message;
      }
      finally
      {
        cSQL.CloseDB();
        cORA.CloseDB();
      }

    }//



    private void UpdOEELomatiHRInfo()
    {

      int rv = 0;
      string sql = "";
      int cnt = 0;
      string empNo = "";
      string lastName = "";
      string firstName = "";
      string emailAddr = "";
      string userName = "";

      try
      {


        cSQL.OpenDB("HRFeed");

        sql = "select country,UserName, EmpNo, SName, FName, LName, Email, employeestatus from hrinfo ";
        sql += " where Country = 'South Africa'";
        sql += " and UserName != ''";
        sql += " and EmployeeStatus != 'Withdrawn'";
        //sql += "";

        DataTable dt = cSQL.GetDataTable(sql);

        cSQL.CloseDB();



        cSQL.OpenDB("OEELOMATI");

        sql = "Update [UserHR] set active = 0";
        rv = cSQL.ExecuteQuery(sql);

        foreach (DataRow item in dt.Rows)
        {
          userName = item["UserName"].ToString().ToUpper();
          empNo = item["EmpNo"].ToString();
          lastName = item["LName"].ToString().Replace("'", "''");
          firstName = item["FName"].ToString().Replace("'", "''");
          emailAddr = item["Email"].ToString().Replace("'", "''");

          if (cnt++ % 5 == 0)
          {
            lblMsg.Text = userName;
            Application.DoEvents();
          }


          sql = "Update [UserHR] set userName = '" + userName + "'"
              + ", FirstName =  '" + firstName + "'"
              + ", LastName =  '" + lastName + "'"
              + ", Email =  '" + emailAddr + "'"
              + ", Active =  '" + 1 + "'"
              + ", LastUpdate =  '" + cmath.getDateTimeNowStr() + "'"
              + " where EmpNo = " + empNo;

          rv = cSQL.ExecuteQuery(sql);

          if (rv <= 0)
          {
            sql = "Insert Into [UserHR] (username, empno, firstname, lastname, email, active) Values("
              + " '" + userName + "'"
              + ", " + empNo
              + ",'" + firstName + "'"
              + ",'" + lastName + "'"
              + ",'" + emailAddr + "'"
              + ", 1"
              + ")";

            rv = cSQL.ExecuteQuery(sql);

          }

        } //foreach

        lblMsg.Text = "Finished";

      }//try
      catch (Exception er)
      {
        cFG.LogIt(er.Message);
        cFG.LogIt(sql);
        lblMsg.Text = er.Message;
      }
      finally
      {
        cSQL.CloseDB();
      }

    }//


    #endregion





  }//

}///




