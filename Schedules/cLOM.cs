﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;



namespace Schedules
{

  //class for Lomati data


  class cLOM
  {

    //cSQL   must be open from the caller

    //Lomati OEE ShiftMinutes
    public double GetShiftHoursMin(int pFLID, string pFromDate, string pToDate)
    {
      // <returns>return the total hours or minutes for a period</returns>

      double rv = 0.0D;

      pFromDate = cmath.getDateStr(pFromDate);
      pToDate = cmath.getDateStr(pToDate);

      string sql = "Select FLID, (SUM(shiftHours)) HRS, (SUM(ShiftHours) * 60) MINS";
      sql += " From Shifts where FLID =  " + pFLID;
      sql += " and DayOfWeek >= '" + pFromDate + "'";
      sql += " and DayOfWeek <= '" + pToDate + "'";
      sql += " and shiftnum in (1,2,3)";
      sql += " GROUP BY FLID";

      try
      {
        DataTable dt = cSQL.GetDataTable(sql);
        foreach (DataRow item in dt.Rows)
        {
          rv = cmath.getd(item["MINS"].ToString());
        }
        return rv;
      }
      catch (Exception ee)
      {
        throw ee;
      }

    } //--



    public double GetShiftBreakHoursMin(int pFLID, string pFromDate, string pToDate)
    {
      //SUM  shift hours for tea and lunch break

      double rv = 0.0D;

      pFromDate = cmath.getDateStr(pFromDate);
      pToDate = cmath.getDateStr(pToDate);

      string sql = "Select FLID, (SUM(shiftHours)) HRS, (SUM(ShiftHours) * 60) MINS";
      sql += " From Shifts where FLID =  " + pFLID;
      sql += " and DayOfWeek >= '" + pFromDate + "'";
      sql += " and DayOfWeek <= '" + pToDate + "'";
      sql += " and shiftnum in (4,5,6,7)";
      sql += " GROUP BY FLID";

      try
      {
        DataTable dt = cSQL.GetDataTable(sql);
        foreach (DataRow item in dt.Rows)
        {
          rv = cmath.getd(item["MINS"].ToString());
        }
        return rv;
      }
      catch (Exception ee)
      {
        throw ee;
      }

    } //--


    //lomati events
    public DataTable GetEventsGrid(int pFlid, string pDateFrom, string pDateTo)
    {
      // using a temporary table to store the hrs and minutes and then join with Events Table
      //EVENT TYPE 'E'  OR 'EO' AND QUALITY 'Q'

      DataTable dt = null;

      pDateFrom = cmath.getDateStr(pDateFrom);
      pDateTo = cmath.getDateStr(pDateTo);

      string sql = "declare @temptable table(	 EventID int,	 Hrs decimal(7, 2),	 Min decimal(10))";

      sql += " INSERT INTO @temptable(EventID, Hrs, Min)";
      sql += " Select E.EventID";
      sql += ", Datediff(\"mi\", dbo.GreatestDate(E.DateFrom, '" + pDateFrom + "'), dbo.LeastDate(E.DateTo,'" + pDateTo + "')) / 60.00 as Hrs";
      sql += ", Datediff(\"mi\", dbo.GreatestDate(E.DateFrom, '" + pDateFrom + "'), dbo.LeastDate(E.DateTo,'" + pDateTo + "')) as Min";
      sql += " FROM Events E ";
      sql += " Left Join SappiFiscalCalendar.dbo.tbl_months MM on E.DateFrom between MM.month_start and MM.Month_End ";
      sql += " Left Join SappiFiscalCalendar.dbo.tbl_Years YY on YY.Year_id = MM.Year_id";
      sql += " Left Join Grades G on E.GradeID = G.GradeID";
      sql += " Left Join Reasons R on E.ReasonID = R.ReasonID";
      sql += "  Where(E.EventType = 'E'  or E.EventType = 'EO'  or E.EventType = 'Q' ) ";

      if (pFlid > 0)
        sql += "  AND E.FLID = " + pFlid;

      sql += " And( E.DateFrom Between '" + pDateFrom + "'  And '" + pDateTo + "'";
      sql += "   OR E.DateTo Between   '" + pDateFrom + "'  And '" + pDateTo + "'  ";
      sql += " 	 OR(E.DateFrom < '" + pDateFrom + "'   And E.DateTo >  '" + pDateTo + "')";
      sql += " ) ";


      sql += " Select E.EventID, E.Flid,  E.DateFrom, E.DateTo";
      sql += ", Format(T.Hrs, '######0.00') as Hrs, Format(T.Min, '#########0') as Min";
      sql += ", isnull(E.RunRate, 0) RunRate, PB.RunRate RunRateBudget, PB.RunRateFactor";

      sql += ", P.Description PlanUnplanDesc, P.PlanUnplan";
      sql += ", isnull(R.Reason, '') Reason,  isnull(C.Category, '') Category ,  isnull(SC.SubCat, '') SubCat,  isnull(K.Keyword, '') Keyword";
      sql += ", isnull(E.Quality, 0) Quality,  isnull(E.DowntimeDesc, '') DowntimeDesc, substring(isnull(E.DowntimeDesc,''),1,60) DownTimeDesc60";
      sql += ", isnull(EQ.EquipCode, 0) EquipCode, isnull(EQ.EquipDesc, '') EquipDesc, isnull(DCS.DCSTag, '') DCSTag, isnull(E.RCAStatus, '') RCAStatus";
      sql += ", isnull(E.RCACoordinator, '') RCACoordinator, isnull(E.EventFlag, '') EventFlag , E.UserLogon, isnull(E.SAPNote, '') SAPNote";
      sql += ", isnull(E.AutoEvent, 0) AutoEvent, E.EventType, E.UserLogonClose ";
      sql += ", MM.month_start, MM.Month_End, MM.Month_Of_Year, YY.year,  PB.SappiMonth, PB.SappiYear, FA.FunctionalArea";

      sql += " FROM @temptable T ";
      sql += " Left Join Events E on T.EventID = E.EventID";
      sql += " Left Join Grades G on E.GradeID = G.GradeID";
      sql += " Left Join FunctionalAreas FA on E.FLID = FA.FLID";
      sql += " Left Join Reasons R on E.ReasonID = R.ReasonID";
      sql += " Left Join PlanUnPlan P on R.PlanUnplan = P.Planunplan";
      sql += " Left Join Categories C on E.CategoryID = C.CategoryID ";
      sql += " Left Join SubCategories SC on E.SubCatID = SC.SubCatID ";
      sql += " Left Join Keywords K on E.KeywordID = K.KeywordID ";
      sql += " Left Join Equipment EQ on E.EquipID = EQ.EquipID ";
      sql += " Left Join DCSTags DCS on E.DCSTagID = DCS.DCSTagID ";
      sql += " Left Join SappiFiscalCalendar.dbo.tbl_months MM on E.DateFrom between MM.month_start and MM.Month_End ";
      sql += " Left Join SappiFiscalCalendar.dbo.tbl_Years YY on YY.Year_id = MM.Year_id";
      sql += " Left Join ProdBudgets PB on  E.Flid = PB.FLID And isNull(E.GradeID,0) = isNull(PB.GradeID,0) And PB.SappiYear = YY.Year and PB.SappiMonth = MM.Month_Of_Year";

      sql += " ORDER by E.DateTo";

      try
      {
        dt = cSQL.GetDataTable(sql);
      }
      catch (Exception err)
      {
        throw err;
      }

      return dt;

    }////////


    public double PlannedDT(ref DataTable dtEvents)
    {
      //Available time =	shift Hours - ([PLANNED] [Available Time Adjustment])

      double dAvailTimeAdj = 0D;

      //get available time adjustment from categories
      foreach (DataRow row in dtEvents.Rows)
      {

        if (row["DateFrom"].ToString() == "")     //skip empty rows
          continue;

        if (row["EventType"].ToString().ToUpper() == "Q")
          continue;


        if (row["Min"] != null)
        {
          if (row["PlanUnplan"].ToString() == "1"           //planned
                    && ((row["EventType"].ToString() == "E") || (row["EventType"].ToString() == "EO"))
                    && row["Reason"].ToString().ToLower() == "available time adjustment"
                    )
          {

            dAvailTimeAdj += cmath.getd(row["Min"].ToString());
          }
        } //min



      }//foreach


      return dAvailTimeAdj;


    } //


    public double PlanUnpDT(ref DataTable dtEvents)
    {

      //NPT = Available time - [category planned and unplanned items]

      double tmp = 0D;

      foreach (DataRow row in dtEvents.Rows)
      {

        if (row["DateFrom"] == null)
          continue;

        if (row["DateFrom"].ToString() == "")     //skip empty rows
          continue;

        if (row["EventType"].ToString().ToUpper() == "Q")
          continue;


        if (row["Min"] != null)
        {
          //planned
          if (row["PlanUnplan"].ToString() == "1"       // planned
                    && ((row["EventType"].ToString() == "E") || (row["EventType"].ToString() == "EO")))
          {
            //equipment and planned maintenance
            if ((row["Reason"].ToString().ToLower() == "equipment" && row["Category"].ToString().ToLower() == "planned maintenance"))
              tmp += cmath.getd(row["Min"].ToString());

            //process and planned schedules
            if ((row["Reason"].ToString().ToLower() == "process") && (row["Category"].ToString().ToLower() == "planned schedules"))
              tmp += cmath.getd(row["Min"].ToString());

          }

          // unplanned        
          if (row["PlanUnplan"].ToString() == "3"         // unplanned
                    && ((row["EventType"].ToString() == "E") || (row["EventType"].ToString() == "EO")))
          {
            //process or equipment or sercies or pibp
            if ((row["Reason"].ToString().ToLower() == "process")
                || (row["Reason"].ToString().ToLower() == "equipment")
                || (row["Reason"].ToString().ToLower() == "services")
                || (row["Reason"].ToString().ToLower() == "pibp")
              )

              tmp += cmath.getd(row["Min"].ToString());
          }

        } //min



      }//foreach


      return tmp;

    } //


    public double CalcRunRatePerc(ref DataTable dtEvents)
    {

      double dRR = 0D;
      double dRRBgt = 0D;
      double dRRFactor = 0D;
      double dRRLog = 0D;
      double tmp = 0D;



      foreach (DataRow row in dtEvents.Rows)
      {
        if (row["DateFrom"] == null)
          continue;

        if (row["DateFrom"].ToString() == "")     //skip empty rows
          continue;

        if (row["EventType"].ToString().ToUpper() == "Q")       //skip quality
          continue;

        dRRBgt = 0;
        dRRFactor = 0;

        if (row["RunRate"] != null)
        {
          tmp = cmath.getd(row["RunRate"].ToString());          //add up the runrate

          if (tmp > 0)
          {
            dRR += tmp;

            if (row["RunRateBudget"] != null)
              dRRBgt = cmath.getd(row["RunRateBudget"].ToString());

            if (row["RunRateFactor"] != null)
              dRRFactor = cmath.getd(row["RunRateFactor"].ToString());

            dRRLog += cmath.div(dRRBgt, dRRFactor);

            //clsConfig.Log("Event id, " + row["Eventid"].ToString() + ", RunRate, " + row["RunRate"].ToString() + ",  RR Log, " + math.div(dRRBgt, dRRFactor).ToString());

          }
        }

      }//foreach

      tmp = 0;

      if (dRR > 0)
      {
        tmp = cmath.div(dRR, dRRLog) * 100;
        //clsConfig.Log(",Total RunRate, " + dRR.ToString() + ", Total Log , " + dRRLog + ", Percentage , " + tmp.ToString());
      }

      return tmp;

    }//


  }///
}///
