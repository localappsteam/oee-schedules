﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Oracle.ManagedDataAccess.Client;


namespace Schedules
{

// auto load the daily production figure in OEE if not yet done so for the current month

  class cOEEDailyProd
  {

    string sReportDate = "";
    string sMonthStartDate = "";
    string sMonthEndDate = "";
    int iDaysInMonth = 0;
    bool boValuesFound = false;
    double dDefaultValue = 0;
    int iBudgetTypeID = 0;


    cCal cal = new cCal();



    public void RunJob(string siteID, string areaID, int sappiYear, int sappiMonth)
    {

      string sql = "";

      cFG.LogIt("Load Daily Budgets " + sappiYear + " " + sappiMonth + " " + siteID + " " + areaID);

      cal = cal ?? new cCal();

      cORA.OpenDB(siteID);

      cSQL.OpenDB("ZAMDW");

      cal.GetMonthMSSQL(sappiYear, sappiMonth, ref sMonthStartDate, ref sMonthEndDate);

      cal.GetCal(cmath.getDateStr(sMonthStartDate));
      iDaysInMonth = cal.iDaysInMonth;

      DateTime startDate = Convert.ToDateTime(sMonthStartDate);
      DateTime endDate = Convert.ToDateTime(sMonthEndDate);
      DataTable dt;

      try
      {

        dDefaultValue = 0;

        //GETTHE DEFAULT daily value from the monthly budgets  
        dDefaultValue = GetMonthBudget(sappiYear, sappiMonth, areaID);

        iBudgetTypeID = GetBudgetPlanID("NSPPMD");

        if (dDefaultValue == 0)
        {
          cFG.LogIt("No default value found from Monthly Budgets " + sappiYear + " " + sappiMonth + " " + siteID + " " + areaID);
        }

        if (iBudgetTypeID == 0)
        {
          cFG.LogIt("BudgetTypeID not found in Monthly Budgets " + sappiYear + " " + sappiMonth + " " + siteID + " " + areaID);
        }

        if (dDefaultValue > 0 && iBudgetTypeID > 0)
        {

          while (startDate <= endDate)
          {

            sql = "select id, bp_date, bp_value, sappi_month, sappi_year from budget_plan_daily"
              + " where  bp_date = " + cmath.OraDate(startDate)
              + " and flid =  " + areaID
              + "";

            dt = cORA.GetDataTable(sql);

            if (dt.Rows.Count == 0 || string.IsNullOrEmpty(dt.Rows[0]["bp_value"].ToString()) == true)
            {
              //insert
              sql = "insert into budget_plan_daily (flid, bp_type_id, bp_date, bp_value, sappi_year, sappi_month, userlogin)"
                           + " Values (" + areaID
                            + "," + iBudgetTypeID
                           + "," + cmath.OraDate(startDate)
                           + "," + dDefaultValue
                           + "," + sappiYear
                           + "," + sappiMonth
                           + ",'MemETL'"
                           + ")";

              cORA.ExecQuery(sql);

              cFG.LogIt(sql);

            }

            startDate = startDate.AddDays(1);


          } //while     

        }//defaultValue != 0

      }
      catch (Exception er)
      {
        cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
        throw new Exception(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
      }
      finally
      {
        cORA.CloseDB();
        cal = null;
      }

    }//




    public int GetBudgetPlanID(string typeCode)
    {
      string sql = "";
      int rv = 0;
      try
      {
        sql = " select bt.bp_type_code, bt.bp_type_id, bt.bp_type_desc"
        + " from BUDGET_TYPES bt"
        + " where   bt.bp_type_code = '" + typeCode + "'"
        + "";

        DataTable dt = cORA.GetDataTable(sql);
        foreach (DataRow item in dt.Rows)
        {
          rv = cmath.geti(item["bp_type_id"].ToString());
        }
        return rv;
      }
      finally
      {
        //
      }

    } //


    private double GetMonthBudget(int pYear, int pMonth, string siteID)
    {


      double rv = 0;
      double dBGP = 0;
      double dBPSY = 0;
  

      try
      {
        string sql = " select bp.millid, bp.flid, bt.bp_type_code, bp.bp_type_id, bp.plan_bp_id, bp.sappi_year, bp.sappi_month"
            + ", bp.bp_value, bt.bp_type_desc from BUDGET_TYPES bt , BUDGET_PLAN bp"
            + " where bt.bp_type_id = bp.bp_type_id"
            + " and bp.sappi_year = " + pYear
            + " and bp.sappi_month = " + pMonth
            + " and bt.bp_type_code in ('BGP','BPSY')"
            + " and bp.flid = " + siteID
            + "";

        DataTable dt = cORA.GetDataTable(sql);

        foreach (DataRow item in dt.Rows)
        {
          if (item["bp_type_code"].ToString() == "BGP")
            dBGP = cmath.getd(item["bp_value"].ToString());

          if (item["bp_type_code"].ToString() == "BPSY")
            dBPSY = cmath.getd(item["bp_value"].ToString());
        }



        rv = (dBGP * dBPSY) / 100;

        rv = Math.Round(cmath.div(rv, iDaysInMonth), 5);

        return rv;

      }
      catch (Exception er)
      {
        throw er;
      }
      finally
      {

      }

    }//


    private void updbudget_plan_daily(int id, string flid, string bp_type_id, double bp_value,  string sappi_month, string sappi_year)
    {
      string sql = "update budget_plan_daily set ";
      sql += ", bp_type_id = '" + bp_type_id + "'";
      sql += ", bp_value = " + bp_value;
      sql += ", active = 1";
      sql += ", sappi_month = " + sappi_month;
      sql += ", sappi_year = " + sappi_year;
      sql += ", userlogin = '" + "MemETL" + "'";
      sql += " where id = " + id;

      cORA.ExecQuery(sql);

    } // end updbudget_plan_daily




    private void addbudget_plan_daily(string flid, string bp_type_id, string bp_date, double bp_value, string sappi_month, string sappi_year)
    {
      string sql = "insert into budget_plan_daily(";
      sql += "flid";
      sql += ", bp_type_id";
      sql += ", bp_date";
      sql += ", bp_value";
      sql += ", active";
      sql += ", sappi_month";
      sql += ", sappi_year";
      sql += ", userlogin";
      sql += ")";
      sql += " values (";
      sql += flid;
      sql += "," + bp_type_id;
      sql += ",'" + bp_date + "'";
      sql += "," + bp_value;
      sql += ",1";
      sql += ",'" + sappi_month + "'";
      sql += ",'" + sappi_year + "'";
      sql += ",'" + "MemETL" + "'";
      sql += ")";

      cORA.ExecQuery(sql);

    } //


  }///
}///
