﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Windows.Forms;
using System.Timers;



namespace Schedules
{
  class cKPIJob
  {

    cCal cal = new cCal();

    //parameters from index html
    int iSappiYear = 0;
    string sSite = "";
    string sAreaID = "";

    bool isDigester = false;
  
    int currDay = 0;            //daily run
    int currMonth = 0;          // this month
    int iSqlMonthID = 0;        // month ID for SQL server
    int iSqlDayID = 0;          // day id for SQL server
    int iSqlWeekID = 0;        // week id for SQL server
    int numOfDaysMonth = 0;
    int numDaysWeek = 0;
    int quarterNum = 0;

    bool boRunMonth = false;
    bool boRunWeek = false;
    bool boRunDaily = false;
    int iRunMonth = 0;              //run from this month number
    string sDailyStartDate = "";    //used for NSPPM  atm
    string sDailyEndDate = "";      //used for NSPPM  atm
   
    int col = 0;


    //temp totals
    double t1 = 0D;
    double t2 = 0D;
    double t3 = 0D;
    double t4 = 0D;
    double t5 = 0D;
    double t6 = 0D;
    double t7 = 0D;

    //totals quarter
    double Q1 = 0.0;
    double Q2 = 0.0;
    double Q3 = 0.0;
    double Q4 = 0.0;

    //temp storage for months data from the budgets and KPI query
    DataTable dtMonths = null;


    private const int ROUND1 = 1;
    private const int ROUND5 = 5;

    //total report rows
    private const byte MAX_ROWS = 224;      //dont forget to increase MAX_ROWS when adding KPI's

    //total cols
    private const byte MAX_COLS = 17;


    //array format
    //|0   |1  |2  |3  |4  |5   |6  |7  |8  |9  |10 |11 |12 |13|14 |15 |16 |
    //|ytd |q1 |q2 |q3 |q4 | m1 |m2 |m3 |m4 |m5 |m6 |m7 |m8 |m9 |m10|m11|m12|


    double[,] arr = new double[MAX_ROWS, MAX_COLS];


    //figures for the month used in the daily calculation
    double monthCMDT = 0D;      
    double monthPTS = 0D;
    double monthPLO = 0D;
    double monthBPTP = 0D;

    double monthBPTS = 0D;
    double monthBPTS1 = 0D;
    double monthBPCS = 0D;
    double monthBPOS = 0D;

    //double monthNSPPM = 0D;

    double monthBGP = 0D;
    double monthWAGRR = 0D;

    //-------------

    // OCT starts at column 5
    private const byte FROM_YTD = 0;
    private const byte FROM_Q1 = 1;
    private const byte TO_Q4 = 4;
    private const byte FROM_MONTH = 5;


    //data rows
    private const byte zTextBudgetInfo = 0;         //row 0 nothing here
    private const byte zSappiHours = 1;
    private const byte zBP_MGPR = 2;                 
    private const byte zBP_PURM = 3;                 
    private const byte zBP_WAGRR = 4;                
    private const byte zBP_BGP = 5;                  
    private const byte zBP_NSPPM = 6;                
    private const byte zBP_NRFTP = 7;                
    private const byte zBP_AMBQR = 8;                 
    private const byte zBP_POR = 9;                   
    private const byte zBP_BNPP = 10;                
    private const byte zBP_BQR = 11;                  
    private const byte zBP_BPSY = 12;                 
    private const byte zBP_BPRFT = 13;                
    private const byte zBP_BPMS = 14;                 
    private const byte zBP_BPOME = 15;                
    private const byte zBP_BOE = 16;                  
    private const byte zBP_TEEP = 17;                 


    private const byte zTextProductMix = 18;          //
    private const byte zKPI_ABGP = 19;                
    private const byte zKPI_20 = 20;                 
    private const byte zKPI_21 = 21;                  
    private const byte zKPI_22 = 22;                 

    private const byte zTextAvailability = 23;        //
    private const byte zKPI_CAL24 = 24;              
    private const byte zKPI_PLS = 25;                 
    private const byte zKPI_26 = 26;                  
    private const byte zKPI_MPPD = 27;                      
    private const byte zKPI_28 = 28;                  
    private const byte zKPI_29 = 29;                  

    private const byte zKPI_Empty = 30;              //
    private const byte zKPI_GP = 31;                  
    private const byte zKPI_DKL = 32;                
    private const byte zKPI_33 = 33;                 
    private const byte zKPI_TBS = 34;                 
    private const byte zKPI_35 = 35;                  
    private const byte zKPI_36 = 36;                  
    private const byte zKPI_37 = 37;                  
    private const byte zKPI_38 = 38;                 
    private const byte zKPI_PPAG = 39;                
    private const byte zKPI_40 = 40;                  
    private const byte zKPI_RFT = 41;                
    private const byte zKPI_42 = 42;                  
    private const byte zKPI_43 = 43;                  

    private const byte zTextSpeed = 44;               
    private const byte zKPI_45 = 45;                  
    private const byte zKPI_46 = 46;                  
    private const byte zKPI_47 = 47;                 

    private const byte zTextEfficiencies = 48;        
    private const byte zKPI_49 = 49;                  
    private const byte zKPI_50 = 50;                  
    private const byte zKPI_51 = 51;                  
    private const byte zKPI_52 = 52;                  
    private const byte zKPI_53 = 53;                  
    private const byte zKPI_54 = 54;                  
    private const byte zKPI_55 = 55;                 
    private const byte zKPI_56 = 56;                 
    private const byte zKPI_57 = 57;                  
    private const byte zKPI_58 = 58;                  

    private const byte zTextEffAct2Budget = 59;        
    private const byte zKPI_60 = 60;                   
    private const byte zKPI_61 = 61;                 
    private const byte zKPI_62 = 62;                 
    private const byte zKPI_63 = 63;                
    private const byte zKPI_64 = 64;               
    private const byte zKPI_65 = 65;               
    private const byte zKPI_66 = 66;              
    private const byte zKPI_67 = 67;              

    private const byte zTextCrossCheckAnalysis = 68;     
    private const byte zKPI_69 = 69;                 
    private const byte zKPI_70 = 70;                  
    private const byte zKPI_71 = 71;                  
    private const byte zKPI_72 = 72;                  
    private const byte zKPI_73 = 73;                
    private const byte zKPI_74 = 74;               
    private const byte zKPI_75 = 75;                 

    private const byte zTextBudgetProdSumm = 76;     
    private const byte zKPI_77 = 77;                 
    private const byte zKPI_78 = 78;                
    private const byte zKPI_79 = 79;                
    private const byte zKPI_80 = 80;                  
    private const byte zKPI_81 = 81;                  
    private const byte zKPI_82 = 82;                 
    private const byte zKPI_83 = 83;                
    private const byte zKPI_84 = 84;                
    private const byte zKPI_85 = 85;                 
    private const byte zKPI_86 = 86;                 
    private const byte zKPI_87 = 87;                
    private const byte zKPI_88 = 88;                

   
    private const byte zKPI_161 = 89;                  
    private const byte zKPI_90 = 90;               
    private const byte zKPI_91 = 91;                 
    private const byte zKPI_92 = 92;                  
    private const byte zKPI_93 = 93;                  
    private const byte zKPI_94 = 94;               
    private const byte zKPI_95 = 95;               
    private const byte zKPI_96 = 96;                 
    private const byte zKPI_97 = 97;                 
    private const byte zKPI_98 = 98;                  
    private const byte zKPI_99 = 99;                 
    private const byte zKPI_100 = 100;              
    private const byte zKPI_101 = 101;             

   
    private const byte zKPI_162 = 102;                
    private const byte zKPI_103 = 103;              
    private const byte zKPI_104 = 104;              
    private const byte zKPI_105 = 105;                
    private const byte zKPI_106 = 106;               
    private const byte zKPI_107 = 107;            
    private const byte zKPI_108 = 108;               
    private const byte zKPI_109 = 109;                 
    private const byte zKPI_110 = 110;               
    private const byte zKPI_111 = 111;              
    private const byte zKPI_112 = 112;             
    private const byte zKPI_113 = 113;              
    private const byte zKPI_114 = 114;           
    private const byte zBP_BPOH = 115;            

    private const byte zKPI_116 = 116;            
    private const byte zKPI_117 = 117;               
    private const byte zKPI_118 = 118;             
    private const byte zKPI_119 = 119;              
    private const byte zBP_BPGH = 120;          

    //AVAILABILITY
    private const byte zKPI_CMDT = 121;                
    private const byte zKPI_PTS = 122;              
    private const byte zKPI_PLO = 123;             
    private const byte zKPI_PLMT = 124;                
    private const byte zKPI_PLPS = 125;              
    private const byte zKPI_PIBP = 126;             
    private const byte zKPI_UPLE = 127;             
    private const byte zKPI_PROC = 128;            

    //ACTUAL PRODUCTION SUMMARY
    private const byte zKPI_120 = 129;               
    private const byte zKPI_121 = 130;              
    private const byte zKPI_122 = 131;            
    private const byte zKPI_123 = 132;             
    private const byte zKPI_124 = 133;              
    private const byte zKPI_125 = 134;            
    private const byte zKPI_126 = 135;           
    private const byte zKPI_127 = 136;           
    private const byte zKPI_128 = 137;            
    private const byte zKPI_129 = 138;            

    private const byte zBP_BPCS = 139;            
    private const byte zBP_BPOS = 140;            
    private const byte zBP_BPTS = 141;            

    private const byte zKPI_130 = 142;             
    private const byte zKPI_131 = 143;                
    private const byte zKPI_132 = 144;             

    private const byte zKPI_133 = 145;              
    private const byte zKPI_134 = 146;            
    private const byte zKPI_135 = 147;          

    private const byte zBP_BPNPT = 148;           


    
    private const byte zBP_BPRFT2 = 149;                //default to 100%  in the DoInt() 

    //there are hidden from the main grid               //I-2102-2915 MEM new KPI's in preparation for forthcoming reports
    private const byte zKPI_150 = 150;
    private const byte zKPI_151 = 151;
    private const byte zKPI_152 = 152;
    private const byte zKPI_153 = 153;
    private const byte zKPI_154 = 154;
    private const byte zKPI_155 = 155;
    private const byte zKPI_156 = 156;
    private const byte zKPI_157 = 157;
    private const byte zKPI_158 = 158;
    private const byte zKPI_159 = 159;
    //---------------------------------------------------

    private const byte zKPI_136 = 160;                 //China B Grade Saiccor only)

    private const byte zBP_BMS = 161;                  
    private const byte zKPI_PLAP = 162;               
    private const byte zKPI_TCSS = 163;               
    private const byte zKPI_TISS = 164;               

    //hidden
    private const byte zKPI_137 = 165;                 //calculations for the Mafube report
    private const byte zKPI_138 = 166;                 //
    private const byte zKPI_139 = 167;                 //
    private const byte zKPI_140 = 168;                 //
    private const byte zKPI_141 = 169;                 //
    private const byte zKPI_142 = 170;                 //
    private const byte zKPI_143 = 171;                 //
    private const byte zKPI_144 = 172;                 //
    private const byte zKPI_MISDM = 173;               //

    //Weighted avarage calcs for CEO Report (Danie),  Barry to create chart
    private const byte zKPI_WABOEE = 174;                
    private const byte zKPI_WAOEE = 175;              
    private const byte zKPI_WABOME = 176;              
    private const byte zKPI_WAOME = 177;               

    private const byte zBP_BPMMM = 178;                 
    private const byte zBP_BPPPP = 179;                  
    private const byte zKPI_145 = 180;                  
    private const byte zKPI_146 = 181;                 
    private const byte zKPI_147 = 182;                  
    private const byte zKPI_148 = 183;                 

    private const byte zKPI_149 = 184;                 
    private const byte zKPI_160 = 185;             

    //MAFUBE REPORT KPI'S
    private const byte zKPI_MSC1 = 186;              
    private const byte zKPI_MPDPOS = 187;             
    private const byte zKPI_MNSP = 188;
    private const byte zKPI_MNSPP = 189;
    private const byte zKPI_MPMJ = 190;
    private const byte zKPI_MOJEDL = 191;
    private const byte zKPI_MOJMDP = 192;
    private const byte zKPI_MOJMPD = 193;
    private const byte zKPI_MFJO = 194;
    private const byte zKPI_MFJOED = 195;
    private const byte zKPI_MFJOEDL = 196;
    private const byte zKPI_MPDP = 197;
    private const byte zKPI_MNOJ = 198;
    private const byte zKPI_MJSD = 199;
    private const byte zKPI_MJCS = 200;
    private const byte zKPI_MFT = 201;
    private const byte zKPI_MFAMAN = 202;
    private const byte zKPI_MFAMAR = 203;

    private const byte zBP_NSPD = 204;               

    private const byte zBP_BPTS1 = 205;
    private const byte zBP_BPTP = 206;

    private const byte zBP_BPMST = 207;
    private const byte zBP_BPPPS = 208;
    private const byte zBP_BPIBP = 209;
    private const byte zBP_BPMDB = 210;
    private const byte zBP_BPPST = 211;

    private const byte zBP_BPCBI = 212;        
    private const byte zBP_BPGFY = 213;
    private const byte zBP_BPGDC = 214;
    private const byte zBP_BPCDUMP = 215;

    private const byte zKPI_ADCAT = 216;         
    private const byte zKPI_AGDC = 217;
    private const byte zKPI_AGFY = 218;
    private const byte zKPI_GFYC = 219;
    private const byte zKPI_ADQPP = 220;

    private const byte zKPI_SHEETB = 221;
    private const byte zKPI_GRDCHG = 222;

    private const byte zKPI_163 = 223;      


    //next kpi number is KPI_164             



    #region --------------------PERIODS to PROCESS---------------------------------------

    //private void DoMonthly(int pSappiYear)
    public void RunMonthly (string pSite, string pAreaID, int pStartMonth, int pSappiYear = 0)
    {
      string sStartDate = "";
      string sEndDate = "";
      int currYear = 0;

      try
      {
        boRunMonth = true;
        boRunWeek = false;
        boRunDaily = false;

        sSite = pSite;
        sAreaID = pAreaID;
        iRunMonth = pStartMonth;
        iSappiYear = pSappiYear;

        isDigester = ((sSite == "NGX" && sAreaID == "161")
             || (sSite == "NGX" && sAreaID == "5")
             || (sSite == "SAI" && sAreaID == "3")
             || (sSite == "SAI" && sAreaID == "2")
             || (sSite == "SAI" && sAreaID == "1")
             || (sSite == "SAI" && sAreaID == "65")
             || (sSite == "STA" && sAreaID == "1")
             || (sSite == "TUG" && sAreaID == "126")
                );


        cal.GetCal(cmath.getDateNowStr(-1));
        currYear = cal.iSappiYear;
        currMonth = cal.iCurrMonthOfYear;
        sStartDate = cal.sMonthStartDate;
        sEndDate  = cal.sMonthEndDate;
        quarterNum = cal.iQuarterOfYear;


        cFG.LogIt("KPI Monthly Process : " + iSappiYear.ToString() + "  From Month# " + pStartMonth.ToString() + " To " + currMonth);

        //if running for previous years
        if (iSappiYear < currYear)
        {
          currMonth = 12;             //if last year then the current month is 12 
        }

        //used in SaveData() update per month starting at the first month
        iSqlMonthID = cal.GetFirstMonthID(iSappiYear);

        if (pStartMonth < currMonth)
        {
          cSQL.OpenDB("ZAMDW");
          cal. GetMonthMSSQL(pSappiYear, iRunMonth, ref sStartDate, ref sEndDate);
          cSQL.CloseDB();
        }

        cal = null;

        sDailyStartDate = cmath.getDateStr(sStartDate); 
        
        if (pStartMonth == currMonth)
          sDailyEndDate = cmath.getDateEODStr(cmath.getDateStr(DateTime.Now.ToString(), -1));       //up to yesterday
        else
          sDailyEndDate = cmath.getDateStr(sEndDate);

        if (iSqlMonthID == 0)
        {
          cFG.LogIt("Run Kpi Monthly Month ID is 0 ");
          return;
        }

        DoProcess("");
        
        DoProcess_YTD_Q();

      }
      catch (Exception er)
      {
        cFG.LogIt("Run Kpi Monthly " + er.Message);
      }
      finally
      {
        cORA.CloseDB();
        cSQL.CloseDB();
      }

    }//



    public void RunDaily(string pSite, string pAreaID, string pRunDate)
    {
      string sStartDate = "";
      string sEndDate = "";

      try
      {
        boRunDaily = true;
        boRunMonth = false;
        boRunWeek = false;

        sSite = pSite;
        sAreaID = pAreaID;

        isDigester = ((sSite == "NGX" && sAreaID == "161")
             || (sSite == "NGX" && sAreaID == "5")
             || (sSite == "SAI" && sAreaID == "3")
             || (sSite == "SAI" && sAreaID == "2")
             || (sSite == "SAI" && sAreaID == "1")
             || (sSite == "SAI" && sAreaID == "65")
             || (sSite == "STA" && sAreaID == "1")
             || (sSite == "TUG" && sAreaID == "126")
             );


        cal = cal ?? new cCal();


        cal.GetCal(pRunDate);

        numOfDaysMonth = cal.iDaysInMonth;      
        iSappiYear = cal.iSappiYear;
        currMonth = cal.iCurrMonthOfYear;
        sStartDate = cmath.getDateStr( cal.sDayStart);
        sEndDate = cmath.getDateStr(cal.sDayEnd);
        currDay = cal.iDayOfYear;

        iSqlDayID = cal.iDayID;

        iSqlMonthID = cal.iCurrMonthID;


        cFG.LogIt("Process Daily: Month# " + currMonth + " Date Start : " + sStartDate + " , Date End " + sEndDate
              + " , currDay " + currDay);

        cal = null;

        sDailyStartDate = cmath.getDateStr(sStartDate);       
        sDailyEndDate = cmath.getDateStr(sEndDate);           

        iRunMonth = currMonth;

        DoProcess("");

      }
      catch (Exception er)
      {
        cFG.LogIt("Run Kpi Daily " + er.Message);
      }
      finally
      {
        cORA.CloseDB();
        cSQL.CloseDB();
      }

    } //


    public void RunWeekly(string pSite, string pAreaID, int pSappiYear, int pWeekNum)
    {
      string sStartDate = "";
      string sEndDate = "";

      int currWeek = 0;

      try
      {
        boRunWeek = true;
        boRunDaily = false;
        boRunMonth = false;

        sSite = pSite;
        sAreaID = pAreaID;
        currWeek = pWeekNum;
        iSappiYear = pSappiYear;

        isDigester = ((sSite == "NGX" && sAreaID == "161")
             || (sSite == "NGX" && sAreaID == "5")
             || (sSite == "SAI" && sAreaID == "3")
             || (sSite == "SAI" && sAreaID == "2")
             || (sSite == "SAI" && sAreaID == "1")
             || (sSite == "SAI" && sAreaID == "65")
             || (sSite == "STA" && sAreaID == "1")
             || (sSite == "TUG" && sAreaID == "126")
                );

        cal = cal ?? new cCal();

      

        
        cal.GetWeekMSSQL(iSappiYear, currWeek, ref currMonth, ref sStartDate, ref sEndDate, ref numDaysWeek);

        
        cal.GetCal(cmath.getDateStr(sStartDate));
        numOfDaysMonth = cal.iDaysInMonth;

        cal.GetCal();
        int thisWeek = cal.iCurrWeekOfYear;


        if (currWeek == thisWeek)
          sEndDate = cmath.getDateEODStr(cmath.getDateStr(DateTime.Now.ToString(), -1));       //hrs up to yesterday

        


        iSqlWeekID = cal.GetSappiWeekID(iSappiYear, currWeek);


        cFG.LogIt("Process Weekly: Week# " + currWeek + " Date Start : " + sStartDate + " , Date End " + sEndDate);

        cal = null;

        sDailyStartDate = cmath.getDateStr(sStartDate);       //this is for NSPPM  2023-04-20
        sDailyEndDate = cmath.getDateStr(sEndDate);           //this is for NSPPM  2023-04-20
        
        iRunMonth = currMonth;

        DoProcess(sEndDate);

      }
      catch (Exception er)
      {
        cFG.LogIt("Run Kpi Weekly " + er.Message);
      }
      finally
      {
        cORA.CloseDB();
        cSQL.CloseDB();
      }


    } //

    #endregion


    #region LOAD DATA---------------------------------------------------------

    private void LoadBudgets()
    {
      int iMonth = 1;
      double dValue = 0D;
      string sql = "";
      int ndx = 0;

      cORA.OpenDB(sSite);

      try
      {
        if (boRunMonth == true)
        {

          sql = " select bp.sappi_month, bp.sappi_Year , bp.flid, bp_type_code, bt.bp_type_desc, bt.bp_uom, bp.bp_value"
            + " from BUDGET_TYPES bt, BUDGET_PLAN bp"
            + " where bt.bp_type_id = bp.bp_type_id"
            + " and bp.flid = " + sAreaID
            + " and bp.sappi_year = " + iSappiYear
            + "  order by bp.sappi_month"
            + "";


          dtMonths = cORA.GetDataTable(sql);

          foreach (DataRow row in dtMonths.Rows)
          {
            iMonth = cmath.geti(row["Sappi_Month"].ToString());
            //array format
            //|0   |1  |2  |3  |4  |5   |6  |7  |8  |9  |10 |11 |12 |13|14 |15 |16 |
            //|ytd |q1 |q2 |q3 |q4 | m1 |m2 |m3 |m4 |m5 |m6 |m7 |m8 |m9 |m10|m11|m12|

            ndx = (iMonth - 1) + FROM_MONTH;          //start from the month column

            dValue = cmath.getd(row["bp_value"].ToString());

            if (iMonth == 12)
            {
              string tmp = dValue.ToString();
            }

            if (iMonth <= currMonth)
            {

              if (row["bp_type_code"].ToString() == "CT")
              {
                arr[zSappiHours, ndx] = Math.Round(dValue, ROUND1);

              }

              if (row["bp_type_code"].ToString() == "MGPR")
              {
                arr[zBP_MGPR, ndx] = dValue;

              }

              if (row["bp_type_code"].ToString() == "PURM")
              {
                arr[zBP_PURM, ndx] = dValue;

              }

              if (row["bp_type_code"].ToString() == "WAGRR")
              {
                arr[zBP_WAGRR, ndx] = dValue;

              }


              if (row["bp_type_code"].ToString() == "BGP")
              {
                arr[zBP_BGP, ndx] = dValue;

              }

              if (row["bp_type_code"].ToString() == "ABGP")
              {
                arr[zKPI_ABGP, ndx] = dValue;

              }

              if (row["bp_type_code"].ToString() == "NSPPM")
              {
                arr[zBP_NSPPM, ndx] = dValue;

              }

              if (row["bp_type_code"].ToString() == "NRFTP")
              {
                arr[zBP_NRFTP, ndx] = dValue;

              }

              if (row["bp_type_code"].ToString() == "AMBQR")
              {
                arr[zBP_AMBQR, ndx] = dValue;

              }

              if (row["bp_type_code"].ToString() == "POR")
              {
                arr[zBP_POR, ndx] = dValue;

              }

              if (row["bp_type_code"].ToString() == "BNPP")
              {
                arr[zBP_BNPP, ndx] = dValue;

              }

              if (row["bp_type_code"].ToString() == "BQR")
              {
                arr[zBP_BQR, ndx] = dValue;

              }

              if (row["bp_type_code"].ToString() == "BPSY")
              {
                arr[zBP_BPSY, ndx] = dValue;

              }

              if (row["bp_type_code"].ToString() == "BPRFT")
              {
                arr[zBP_BPRFT, ndx] = dValue;

              }

              if (row["bp_type_code"].ToString() == "BPMS")
              {
                arr[zBP_BPMS, ndx] = dValue;

              }

              if (row["bp_type_code"].ToString() == "BPOME")
              {
                arr[zBP_BPOME, ndx] = dValue;

              }

              if (row["bp_type_code"].ToString() == "BOE")
              {
                arr[zBP_BOE, ndx] = dValue;

              }

              if (row["bp_type_code"].ToString() == "TEEP")
              {
                arr[zBP_TEEP, ndx] = dValue;

              }

              if (row["bp_type_code"].ToString() == "BPOH")
              {
                arr[zBP_BPOH, ndx] = dValue;
              }

              if (row["bp_type_code"].ToString() == "BPGH")
              {
                arr[zBP_BPGH, ndx] = dValue;
              }

              if (row["bp_type_code"].ToString() == "BPCS")
              {
                arr[zBP_BPCS, ndx] = dValue;
              }

              if (row["bp_type_code"].ToString() == "BPOS")
              {
                arr[zBP_BPOS, ndx] = dValue;
              }

              if (row["bp_type_code"].ToString() == "BMS")
              {
                arr[zBP_BMS, ndx] = dValue;
              }
              if (row["bp_type_code"].ToString() == "BPMMM")
              {
                arr[zBP_BPMMM, ndx] = dValue;
              }

              if (row["bp_type_code"].ToString() == "BPPPP")
              {
                arr[zBP_BPPPP, ndx] = dValue;
              }

              if (row["bp_type_code"].ToString() == "BPTS1")
              {
                arr[zBP_BPTS1, ndx] = dValue;
              }

              if (row["bp_type_code"].ToString() == "BPTP")
              {
                arr[zBP_BPTP, ndx] = dValue;
              }

              if (row["bp_type_code"].ToString() == "BPMST")
              {
                arr[zBP_BPMST, ndx] = dValue;
              }

              if (row["bp_type_code"].ToString() == "BPPPS")
              {
                arr[zBP_BPPPS, ndx] = dValue;
              }
              if (row["bp_type_code"].ToString() == "BPIBP")
              {
                arr[zBP_BPIBP, ndx] = dValue;
              }
              if (row["bp_type_code"].ToString() == "BPMDB")
              {
                arr[zBP_BPMDB, ndx] = dValue;
              }
              
              if (row["bp_type_code"].ToString() == "BPPST")
              {
                arr[zBP_BPPST, ndx] = dValue;
              }

              if (row["bp_type_code"].ToString() == "BPCBI")
              {
                arr[zBP_BPCBI, ndx] = dValue;
              }
              if (row["bp_type_code"].ToString() == "BPGFY")
              {
                arr[zBP_BPGFY, ndx] = dValue;
              }
              if (row["bp_type_code"].ToString() == "BPGDC")
              {
                arr[zBP_BPGDC, ndx] = dValue;
              }
              if (row["bp_type_code"].ToString() == "CDUMP")
              {
                arr[zBP_BPCDUMP, ndx] = dValue;
              }

            }



          } //foreach


        } // boRunMonth true    



        if (boRunDaily == true)
        {

          //runmonth budgets

          sql = " select bp.sappi_month, bp_type_code, bt.bp_type_desc, bt.bp_uom, bp.bp_value"
            + " from BUDGET_TYPES bt, BUDGET_PLAN bp"
            + " where bt.bp_type_id = bp.bp_type_id"
            + " and bp.flid = " + sAreaID
            + " and bp.sappi_month = " + iRunMonth      //currMonth
            + " and bp.sappi_year = " + iSappiYear
            + "";


          dtMonths = cORA.GetDataTable(sql);

          foreach (DataRow row in dtMonths.Rows)
          {


            ndx = FROM_MONTH;          //work the daily figures in the FROM_MONTH Column

            dValue = cmath.getd(row["bp_value"].ToString());

            //Console.WriteLine(row["bp_type_code"].ToString() + " " + dValue.ToString());

            if (row["bp_type_code"].ToString() == "CT")
            {
              arr[zSappiHours, ndx] = dValue / numOfDaysMonth ;
              //double t = (dValue / numOfDaysMonth );
            }

            if (row["bp_type_code"].ToString() == "MGPR")
            {
              arr[zBP_MGPR, ndx] = dValue;
            }

            if (row["bp_type_code"].ToString() == "PURM")
            {
              arr[zBP_PURM, ndx] = dValue;
            }

            if (row["bp_type_code"].ToString() == "WAGRR")
            {
              arr[zBP_WAGRR, ndx] = dValue;
            }


            if (row["bp_type_code"].ToString() == "BGP")
            {
              arr[zBP_BGP, ndx] = dValue;
            }

            if (row["bp_type_code"].ToString() == "ABGP")
            {
              arr[zKPI_ABGP, ndx] = dValue;
            }

            if (row["bp_type_code"].ToString() == "NSPPM")
            {
              arr[zBP_NSPPM, ndx] = dValue;
            }

            if (row["bp_type_code"].ToString() == "NRFTP")
            {
              arr[zBP_NRFTP, ndx] = dValue;
            }

            if (row["bp_type_code"].ToString() == "AMBQR")
            {
              arr[zBP_AMBQR, ndx] = dValue;
            }

            if (row["bp_type_code"].ToString() == "POR")
            {
              arr[zBP_POR, ndx] = dValue;
            }

            if (row["bp_type_code"].ToString() == "BNPP")
            {
              arr[zBP_BNPP, ndx] = dValue;
            }

            if (row["bp_type_code"].ToString() == "BQR")
            {
              arr[zBP_BQR, ndx] = dValue;
            }

            if (row["bp_type_code"].ToString() == "BPSY")
            {
              arr[zBP_BPSY, ndx] = dValue;
            }

            if (row["bp_type_code"].ToString() == "BPRFT")
            {
              arr[zBP_BPRFT, ndx] = dValue;
            }

            if (row["bp_type_code"].ToString() == "BPMS")
            {
              arr[zBP_BPMS, ndx] = dValue;
            }

            if (row["bp_type_code"].ToString() == "BPOME")
            {
              arr[zBP_BPOME, ndx] = dValue;
            }

            if (row["bp_type_code"].ToString() == "BOE")
            {
              arr[zBP_BOE, ndx] = dValue;
            }

            if (row["bp_type_code"].ToString() == "TEEP")
            {
              arr[zBP_TEEP, ndx] = dValue;
            }

            if (row["bp_type_code"].ToString() == "BPOH")
            {
              arr[zBP_BPOH, ndx] = dValue;
            }

            if (row["bp_type_code"].ToString() == "BPGH")
            {
              arr[zBP_BPGH, ndx] = dValue;
            }

            if (row["bp_type_code"].ToString() == "BPCS")
            {
              arr[zBP_BPCS, ndx] = dValue;
            }

            if (row["bp_type_code"].ToString() == "BPOS")
            {
              arr[zBP_BPOS, ndx] = dValue;
            }

            if (row["bp_type_code"].ToString() == "BMS")
            {
              arr[zBP_BMS, ndx] = dValue;
            }

            if (row["bp_type_code"].ToString() == "BPMMM")
            {
              arr[zBP_BPMMM, ndx] = dValue;
            }

            if (row["bp_type_code"].ToString() == "BPPPP")
            {
              arr[zBP_BPPPP, ndx] = dValue;
            }

            if (row["bp_type_code"].ToString() == "BPTS1")
            {
              arr[zBP_BPTS1, ndx] = dValue;
            }

            if (row["bp_type_code"].ToString() == "BPTP")
            {
              arr[zBP_BPTP, ndx] = dValue;
            }

            if (row["bp_type_code"].ToString() == "BPMST")
            {
              arr[zBP_BPMST, ndx] = dValue;
            }

            if (row["bp_type_code"].ToString() == "BPPPS")
            {
              arr[zBP_BPPPS, ndx] = dValue;
            }
            if (row["bp_type_code"].ToString() == "BPIBP")
            {
              arr[zBP_BPIBP, ndx] = dValue;
            }
            if (row["bp_type_code"].ToString() == "BPMDB")
            {
              arr[zBP_BPMDB, ndx] = dValue;
            }
            if (row["bp_type_code"].ToString() == "BPPST")
            {
              arr[zBP_BPPST, ndx] = dValue;
            }
            if (row["bp_type_code"].ToString() == "BPCBI")
            {
              arr[zBP_BPCBI, ndx] = dValue;
            }
            if (row["bp_type_code"].ToString() == "BPGFY")
            {
              arr[zBP_BPGFY, ndx] = dValue;
            }
            if (row["bp_type_code"].ToString() == "BPGDC")
            {
              arr[zBP_BPGDC, ndx] = dValue;
            }
            if (row["bp_type_code"].ToString() == "CDUMP")
            {
              arr[zBP_BPCDUMP, ndx] = dValue;
            }


          } //foreach

        } // boRunDaily



        if (boRunWeek == true)
        {
          //runmonth budgets for weekly run

          sql = " select bp.sappi_month, bp_type_code, bt.bp_type_desc, bt.bp_uom, bp.bp_value"
            + " from BUDGET_TYPES bt, BUDGET_PLAN bp"
            + " where bt.bp_type_id = bp.bp_type_id"
            + " and bp.flid = " + sAreaID
            + " and bp.sappi_month = " + iRunMonth    // currMonth
            + " and bp.sappi_year = " + iSappiYear
            + "";


          dtMonths = cORA.GetDataTable(sql);

          foreach (DataRow row in dtMonths.Rows)
          {


            ndx = FROM_MONTH;          //work the daily figures in the FROM_MONTH Column

            dValue = cmath.getd(row["bp_value"].ToString());

            //Console.WriteLine(row["bp_type_code"].ToString() + " "  + dValue.ToString()) ;

            if (row["bp_type_code"].ToString() == "CT")
            {
              arr[zSappiHours, ndx] = (dValue  / numOfDaysMonth * numDaysWeek);      //week hrs
              //double t = (dValue / numOfDaysMonth * numDaysWeek);
            }

            if (row["bp_type_code"].ToString() == "MGPR")
            {
              arr[zBP_MGPR, ndx] = dValue;
            }

            if (row["bp_type_code"].ToString() == "PURM")
            {
              arr[zBP_PURM, ndx] = dValue;
            }

            if (row["bp_type_code"].ToString() == "WAGRR")
            {
              arr[zBP_WAGRR, ndx] = dValue;
            }


            if (row["bp_type_code"].ToString() == "BGP")
            {
              arr[zBP_BGP, ndx] = dValue;
            }

            if (row["bp_type_code"].ToString() == "ABGP")
            {
              arr[zKPI_ABGP, ndx] = dValue;
            }

            if (row["bp_type_code"].ToString() == "NSPPM")
            {
              arr[zBP_NSPPM, ndx] = dValue;
            }

            if (row["bp_type_code"].ToString() == "NRFTP")
            {
              arr[zBP_NRFTP, ndx] = dValue;
            }

            if (row["bp_type_code"].ToString() == "AMBQR")
            {
              arr[zBP_AMBQR, ndx] = dValue;
            }

            if (row["bp_type_code"].ToString() == "POR")
            {
              arr[zBP_POR, ndx] = dValue;
            }

            if (row["bp_type_code"].ToString() == "BNPP")
            {
              arr[zBP_BNPP, ndx] = dValue;
            }

            if (row["bp_type_code"].ToString() == "BQR")
            {
              arr[zBP_BQR, ndx] = dValue;
            }

            if (row["bp_type_code"].ToString() == "BPSY")
            {
              arr[zBP_BPSY, ndx] = dValue;
            }

            if (row["bp_type_code"].ToString() == "BPRFT")
            {
              arr[zBP_BPRFT, ndx] = dValue;
            }

            if (row["bp_type_code"].ToString() == "BPMS")
            {
              arr[zBP_BPMS, ndx] = dValue;
            }

            if (row["bp_type_code"].ToString() == "BPOME")
            {
              arr[zBP_BPOME, ndx] = dValue;
            }

            if (row["bp_type_code"].ToString() == "BOE")
            {
              arr[zBP_BOE, ndx] = dValue;
            }

            if (row["bp_type_code"].ToString() == "TEEP")
            {
              arr[zBP_TEEP, ndx] = dValue;
            }

            if (row["bp_type_code"].ToString() == "BPOH")
            {
              arr[zBP_BPOH, ndx] = dValue;
            }

            if (row["bp_type_code"].ToString() == "BPGH")
            {
              arr[zBP_BPGH, ndx] = dValue;
            }

            if (row["bp_type_code"].ToString() == "BPCS")
            {
              arr[zBP_BPCS, ndx] = dValue;
            }

            if (row["bp_type_code"].ToString() == "BPOS")
            {
              arr[zBP_BPOS, ndx] = dValue;
            }

            if (row["bp_type_code"].ToString() == "BMS")
            {
              arr[zBP_BMS, ndx] = dValue;
            }

            if (row["bp_type_code"].ToString() == "BPMMM")
            {
              arr[zBP_BPMMM, ndx] = dValue;
            }

            if (row["bp_type_code"].ToString() == "BPPPP")
            {
              arr[zBP_BPPPP, ndx] = dValue;
            }

            if (row["bp_type_code"].ToString() == "BPTS1")
            {
              arr[zBP_BPTS1, ndx] = dValue;
            }

            if (row["bp_type_code"].ToString() == "BPTP")
            {
              arr[zBP_BPTP, ndx] = dValue;
            }

            if (row["bp_type_code"].ToString() == "BPMST")
            {
              arr[zBP_BPMST, ndx] = dValue;
            }

            if (row["bp_type_code"].ToString() == "BPPPS")
            {
              arr[zBP_BPPPS, ndx] = dValue;
            }
            if (row["bp_type_code"].ToString() == "BPIBP")
            {
              arr[zBP_BPIBP, ndx] = dValue;
            }
            if (row["bp_type_code"].ToString() == "BPMDB")
            {
              arr[zBP_BPMDB, ndx] = dValue;
            }
            if (row["bp_type_code"].ToString() == "BPPST")
            {
              arr[zBP_BPPST, ndx] = dValue;
            }
            if (row["bp_type_code"].ToString() == "BPCBI")
            {
              arr[zBP_BPCBI, ndx] = dValue;
            }
            if (row["bp_type_code"].ToString() == "BPGFY")
            {
              arr[zBP_BPGFY, ndx] = dValue;
            }
            if (row["bp_type_code"].ToString() == "BPGDC")
            {
              arr[zBP_BPGDC, ndx] = dValue;
            }
            if (row["bp_type_code"].ToString() == "CDUMP")
            {
              arr[zBP_BPCDUMP, ndx] = dValue;
            }

          } //foreach

        } // boRunWeekly


      }
      catch (Exception er)
      {
        cFG.LogIt("LoadBudgets KPIJob " + er.Message);
        throw new Exception("Load Budgets: " + er.Message);
      }
      finally
      {
        cORA.CloseDB();
      }

    } //


    private void LoadKPI()
    {
        //load inputs from PTS/PHS

      string sql = "";
      int iMonth = 0;
      double dValue = 0D;
      string sCode = "";
      int ndx = 0;

      cSQL.OpenDB("ZAMDW");


      try
      {

        if (boRunMonth == true)
        {
          sql = " select km.kpimonth_id,  km.mill_code,  km.kpi_code, kd.KpiDesc, km.sappimonth_id, sm.month_of_year"
            + "         ,kd.Uom, smn.name_short, sy.year, km.kpi_value, km.lastupdated"
            + " from   kpimonthly km"
            + "       , kpidefinition kd  "
            + "       , sappifiscalcalendar.dbo.tbl_months sm  "
            + "       , sappifiscalcalendar.dbo.tbl_month_names smn"
            + "       , sappifiscalcalendar.dbo.tbl_years sy"
            + " where"
            + "      km.KPI_CODE = kd.KpiCode"
            + "      and km.sappimonth_id = sm.month_id"
            + "      and sm.month_name_id = smn.month_name_id"
            + "      and sm.year_id = sy.year_id"
            + "      and sy.year = " + iSappiYear
            + "      and km.mill_code = '" + sSite + "'"
            + "      and km.flid = " + sAreaID
            + "";


          dtMonths = cSQL.GetDataTable(sql);

          foreach (DataRow row in dtMonths.Rows)
          {
            iMonth = cmath.geti(row["month_of_year"].ToString());
            dValue = cmath.getd(row["kpi_value"].ToString());
            sCode = row["kpi_code"].ToString();
            //array format
            //|0   |1  |2  |3  |4  |5   |6  |7  |8  |9  |10 |11 |12 |13|14 |15 |16 |
            //|ytd |q1 |q2 |q3 |q4 | m1 |m2 |m3 |m4 |m5 |m6 |m7 |m8 |m9 |m10|m11|m12|

            ndx = (iMonth - 1) + FROM_MONTH;          //start from the month column

            if (iMonth >= currMonth)
            {
              Application.DoEvents();
            }

            if (iMonth <= currMonth)
            {

              if (sCode == "GP")
              {
                arr[zKPI_GP, ndx] = dValue;
              }

              if (sCode == "DKL")
              {
                arr[zKPI_DKL, ndx] = dValue;
              }

              if (sCode == "TBS")
              {
                arr[zKPI_TBS, ndx] = dValue;
              }
              if (sCode == "PPAG")
              {
                arr[zKPI_PPAG, ndx] = dValue;
              }

              if (sCode == "RFT")
              {
                arr[zKPI_RFT, ndx] = dValue;
              }

              if (sCode == "PLS")
              {
                arr[zKPI_PLS, ndx] = dValue;
              }

              if (sCode == "MPPD")
              {
                arr[zKPI_MPPD, ndx] = dValue;
              }

              if (sCode == "CMDT")
              {
                arr[zKPI_CMDT, ndx] = dValue;
              }

              if (sCode == "PTS")
              {
                arr[zKPI_PTS, ndx] = dValue;
              }

              if (sCode == "PLO")
              {
                arr[zKPI_PLO, ndx] = dValue;
              }

              if (sCode == "PLMT")
              {
                arr[zKPI_PLMT, ndx] = dValue; ;
              }

              if (sCode == "PLPS")
              {
                arr[zKPI_PLPS, ndx] = dValue;
              }

              if (sCode == "PIBP")
              {
                arr[zKPI_PIBP, ndx] = dValue;
              }

              if (sCode == "UPLE")
              {
                arr[zKPI_UPLE, ndx] = dValue;
              }

              if (sCode == "PROC")
              {
                arr[zKPI_PROC, ndx] = dValue;
              }

              if (sCode == "KPI0136")
              {
                arr[zKPI_136, ndx] = dValue;
              }

              if (sCode == "PLAP")
              {
                arr[zKPI_PLAP, ndx] = dValue;
              }

              if (sCode == "TCSS")
              {
                arr[zKPI_TCSS, ndx] = dValue;
              }

              if (sCode == "TISS")
              {
                arr[zKPI_TISS, ndx] = dValue;
              }

              if (sCode == "MSC1")
              {
                arr[zKPI_MSC1, ndx] = dValue;
              }

              if (sCode == "MPDPOS")
              {
                arr[zKPI_MPDPOS, ndx] = dValue;
              }

              if (sCode == "AGDC")
              {
                arr[zKPI_AGDC, ndx] = dValue;
              }

              if (sCode == "ADQPP")
              {
                arr[zKPI_ADQPP, ndx] = dValue;
              }

              if (sCode == "SHEETB")
              {
                arr[zKPI_SHEETB, ndx] = dValue;
              }

              if (sCode == "GRDCHG")
              {
                arr[zKPI_GRDCHG, ndx] = dValue;
              }

              if (sCode == "KPI0163")
              {
                arr[zKPI_163, ndx] = dValue;
              }
              if (sCode == "KPI0083")
              {
                arr[zKPI_83, ndx] = dValue;
              }
            } //iMonth

          } //for
        } //borunMonth = true



        if (boRunDaily == true)
        {

          sql = " SELECT km.MillCode , km.FLID, km.KPICode, km.SappiDayID, km.KPIValue "
            + " FROM KPIDaily km, kpidefinition kd"
            + " where  km.KPICODE = kd.KpiCode"
            + " and km.MILLCODE = '" + sSite + "'"
            + " and km.FLID = " + sAreaID
            + " and km.SappiDayID = " + iSqlDayID
            + "";

          dtMonths = cSQL.GetDataTable(sql);

          foreach (DataRow row in dtMonths.Rows)
          {

            dValue = cmath.getd(row["KPIValue"].ToString());
            sCode = row["KPICode"].ToString();

            ndx = FROM_MONTH;          //use the FROM_MONTH column


            string tmp = dValue.ToString();

              if (sCode == "GP")
              {
                arr[zKPI_GP, ndx] = dValue;
              }

              if (sCode == "DKL")
              {
                arr[zKPI_DKL, ndx] = dValue;
              }

              if (sCode == "TBS")
              {
                arr[zKPI_TBS, ndx] = dValue;
              }


              if (sCode == "PPAG")
              {
                arr[zKPI_PPAG, ndx] = dValue;
              }

              if (sCode == "RFT")
              {
                arr[zKPI_RFT, ndx] = dValue;
              }

              if (sCode == "PLS")
              {
                arr[zKPI_PLS, ndx] = dValue;
              }

              if (sCode == "MPPD")
              {
                arr[zKPI_MPPD, ndx] = dValue;
              }

              if (sCode == "CMDT")
              {
                arr[zKPI_CMDT, ndx] = dValue;
              }

              if (sCode == "PTS")
              {
                arr[zKPI_PTS, ndx] = dValue;
              }

              if (sCode == "PLO")
              {
                arr[zKPI_PLO, ndx] = dValue;
              }

              if (sCode == "PLMT")
              {
                arr[zKPI_PLMT, ndx] = dValue; ;
              }

              if (sCode == "PLPS")
              {
                arr[zKPI_PLPS, ndx] = dValue;
              }

              if (sCode == "PIBP")
              {
                arr[zKPI_PIBP, ndx] = dValue;
              }

              if (sCode == "UPLE")
              {
                arr[zKPI_UPLE, ndx] = dValue;
              }

              if (sCode == "PROC")
              {
                arr[zKPI_PROC, ndx] = dValue;
              }

              if (sCode == "KPI0136")
              {
                arr[zKPI_136, ndx] = dValue;
              }

              if (sCode == "PLAP")
              {
                arr[zKPI_PLAP, ndx] = dValue;
              }

              if (sCode == "TCSS")
              {
                arr[zKPI_TCSS, ndx] = dValue;
              }

              if (sCode == "TISS")
              {
                arr[zKPI_TISS, ndx] = dValue;
              }

              if (sCode == "MSC1")
              {
                arr[zKPI_MSC1, ndx] = dValue;
              }

              if (sCode == "MPDPOS")
              {
                arr[zKPI_MPDPOS, ndx] = dValue;
              }

              if (sCode == "AGDC")
              {
                arr[zKPI_AGDC, ndx] = dValue;
              }

              if (sCode == "ADQPP")
              {
                arr[zKPI_ADQPP, ndx] = dValue;
              }

              if (sCode == "KPI0163")
              {
                arr[zKPI_163, ndx] = dValue;
              }

             if (sCode == "NSPPM")
              {
                 arr[zBP_NSPPM, ndx] = dValue;
              }
            if (sCode == "KPI0083")
            {
              arr[zKPI_83, ndx] = dValue;
            }
          } //for

        } //borunDaily



        if (boRunWeek == true)
        {

          sql = " SELECT km.MillCode , km.FLID, km.KPICode, km.SappiWeekID, km.KPIValue "
            + " FROM KPIWeekly km, kpidefinition kd"
            + " where  km.KPICODE = kd.KpiCode"
            + " and km.MILLCODE = '" + sSite + "'"
            + " and km.FLID = " + sAreaID
            + " and km.SappiWeekID = " + iSqlWeekID
            + "";

          dtMonths = cSQL.GetDataTable(sql);

          foreach (DataRow row in dtMonths.Rows)
          {

            dValue = cmath.getd(row["KPIValue"].ToString());
            sCode = row["KPICode"].ToString();

            ndx = FROM_MONTH;          //use the FROM_MONTH column


            //Console.WriteLine(sCode + " " +  dValue.ToString());


            if (sCode == "GP")
            {
              arr[zKPI_GP, ndx] = dValue;
            }

            if (sCode == "DKL")
            {
              arr[zKPI_DKL, ndx] = dValue;
            }

            if (sCode == "TBS")
            {
              arr[zKPI_TBS, ndx] = dValue;
            }


            if (sCode == "PPAG")
            {
              arr[zKPI_PPAG, ndx] = dValue;
            }

            if (sCode == "RFT")
            {
              arr[zKPI_RFT, ndx] = dValue;
            }

            if (sCode == "PLS")
            {
              arr[zKPI_PLS, ndx] = dValue;
            }

            if (sCode == "MPPD")
            {
              arr[zKPI_MPPD, ndx] = dValue;
            }

            if (sCode == "CMDT")
            {
              arr[zKPI_CMDT, ndx] = dValue;
            }

            if (sCode == "PTS")
            {
              arr[zKPI_PTS, ndx] = dValue;
            }

            if (sCode == "PLO")
            {
              arr[zKPI_PLO, ndx] = dValue;
            }

            if (sCode == "PLMT")
            {
              arr[zKPI_PLMT, ndx] = dValue; ;
            }

            if (sCode == "PLPS")
            {
              arr[zKPI_PLPS, ndx] = dValue;
            }

            if (sCode == "PIBP")
            {
              arr[zKPI_PIBP, ndx] = dValue;
            }

            if (sCode == "UPLE")
            {
              arr[zKPI_UPLE, ndx] = dValue;
            }

            if (sCode == "PROC")
            {
              arr[zKPI_PROC, ndx] = dValue;
            }

            if (sCode == "KPI0136")
            {
              arr[zKPI_136, ndx] = dValue;
            }

            if (sCode == "PLAP")
            {
              arr[zKPI_PLAP, ndx] = dValue;
            }

            if (sCode == "TCSS")
            {
              arr[zKPI_TCSS, ndx] = dValue;
            }

            if (sCode == "TISS")
            {
              arr[zKPI_TISS, ndx] = dValue;
            }

            if (sCode == "MSC1")
            {
              arr[zKPI_MSC1, ndx] = dValue;
            }

            if (sCode == "MPDPOS")
            {
              arr[zKPI_MPDPOS, ndx] = dValue;
            }
            if (sCode == "AGDC")
            {
              arr[zKPI_AGDC, ndx] = dValue;
            }
            if (sCode == "ADQPP")
            {
              arr[zKPI_ADQPP, ndx] = dValue;
            }

            if (sCode == "KPI0163")
            {
              arr[zKPI_163, ndx] = dValue;
            }

            if (sCode == "NSPPM")
            {
              arr[zBP_NSPPM, ndx] = dValue;
            }
            if (sCode == "KPI0083")
            {
              arr[zKPI_83, ndx] = dValue;
            }
          } //for

        } //borunWeek


      }
      catch (Exception er)
      {
        cFG.LogIt("LoadKPI KPIJob " + er.Message);
        throw new Exception("Load KPI: " + er.Message);
      }
      finally
      {
        cSQL.CloseDB();
      }

    } //


    private void LoadKPIMonth()
    {
      //load single month value

      string sql = "";
      int iMonth = 0;
      double dValue = 0D;
      string sCode = "";

      cSQL.OpenDB("ZAMDW");


      try
      {

        sql = " select km.kpimonth_id,  km.mill_code,  km.kpi_code, kd.KpiDesc, km.sappimonth_id, sm.month_of_year"
          + "         ,kd.Uom, smn.name_short, sy.year, km.kpi_value, km.lastupdated"
          + " from   kpimonthly km"
          + "       , kpidefinition kd  "
          + "       , sappifiscalcalendar.dbo.tbl_months sm  "
          + "       , sappifiscalcalendar.dbo.tbl_month_names smn"
          + "       , sappifiscalcalendar.dbo.tbl_years sy"
          + " where"
          + "      km.KPI_CODE = kd.KpiCode"
          + "      and km.sappimonth_id = sm.month_id"
          + "      and sm.month_name_id = smn.month_name_id"
          + "      and sm.year_id = sy.year_id"
          + "      and sy.year = " + iSappiYear
          + "      and km.mill_code = '" + sSite + "'"
          + "      and km.flid = " + sAreaID
          + " 	   and sm.month_of_year = " + iRunMonth   //currMonth 2023-06-13
          + "";


        dtMonths = cSQL.GetDataTable(sql);

        foreach (DataRow row in dtMonths.Rows)
        {
          iMonth = cmath.geti(row["month_of_year"].ToString());
          dValue = cmath.getd(row["kpi_value"].ToString());
          sCode = row["kpi_code"].ToString();

          if (sCode == "CMDT")
          {
            monthCMDT = dValue;
          }

          if (sCode == "PTS")
          {
            monthPTS = dValue;
          }

          if (sCode == "PLO")
          {
            monthPLO = dValue;
          }

          if (sCode == "BPTS")
          {
            monthBPTS = dValue;
          }

            if (sCode == "BPTS1")
            {
               monthBPTS1 = dValue;
            }

            if (sCode == "BPTP")
            {
               monthBPTP = dValue;
            }

          if (sCode == "BPCS")
          {
            monthBPCS = dValue;
          }

          if (sCode == "BPOS")
          {
            monthBPOS = dValue;
          }

          //if (sCode == "NSPPM")
          //{
          //  monthNSPPM = dValue;
          //}

          if (sCode == "WAGRR")
          {
            monthWAGRR = dValue;
          }
          if (sCode == "BGP")
          {
            monthBGP = dValue;
          }

        } //for


      }
      catch (Exception er)
      {
        cFG.LogIt("LoadKPIMonth KPIJob " + er.Message);
        throw new Exception("LoadKPI Month: " + er.Message);
      }
      finally
      {
        cSQL.CloseDB();
      }

    } //

    #endregion


    #region -----------------------------PROCESS MAIN THREAD---------------------------------------


    private void DoInit()
    {

      for (int i = 0; i < MAX_ROWS; i++)
      {
        for (int k = 0; k < MAX_COLS; k++)
        {
          arr[i, k] = 0D;
        }
      }

      //Showarr(zKPI_162);

      //default  zBP_BPRFT2  to 100%  C-2012-0419 as per Eric
      for (int k = 0; k < MAX_COLS; k++)
      {
        arr[zBP_BPRFT2, k] = 100.00;
      }

    }//


    private void Showarr(int rowNum)
    {

      Console.WriteLine(rowNum.ToString());

      for (int k = 0; k < MAX_COLS; k++)
      {
          Console.Write(arr[rowNum, k].ToString() + " ");
      }
        
        Console.WriteLine();

    }//


    private bool DoProcess(string sWeekDate)
    {

      bool rv = true;
      try
      {

        DoInit();

        //budgets------------------------------------------------------
        LoadBudgets();
        LoadKPI();
        LoadKPIMonth();

        Application.DoEvents();

        SaveData(zSappiHours, "CT");
        SumYTD_QTR(zSappiHours, ROUND1);
        SaveQuarter(zSappiHours, "CT");


        DoCalendarTime(sWeekDate);
        SumYTD_QTR(zKPI_CAL24,ROUND1);
        SaveQuarter(zKPI_CAL24, "CTT");


        SaveData(zBP_MGPR, "MGPR" );
        CalcByHrs_YTD_QTR(zBP_MGPR, ROUND5);        
        SaveQuarter(zBP_MGPR, "MGPR");


        SaveData(zBP_PURM, "PURM");
        CalcByHrs_YTD_QTR(zBP_PURM,  ROUND5);        
        SaveQuarter(zBP_PURM, "PURM");


        if (boRunMonth == false)    //daily/weekly  then run this before POR
        {

          DoBPTS_Daily();

          //SaveData(zKPI_CMDT, "BPCS");  //Save the CMDT   data to BPCS
          //arr[zBP_BPCS, FROM_MONTH] = arr[zKPI_CMDT, FROM_MONTH];    //assign CMDT to BPCS and save later
          DoBPCSDaily();

            //SaveData(zKPI_PLO, "BPOS");                          //Save the PLO   data to BPOS and save later
            //arr[zBP_BPOS, FROM_MONTH] = arr[zKPI_PLO, FROM_MONTH];
            DoBPOSDaily();

          //SaveData(zKPI_PLAP, "BPTS1");                          //Save the PLAP data to BPTS1 and save all later
          //arr[zBP_BPTS1, FROM_MONTH] = arr[zKPI_PLAP, FROM_MONTH];
          DoBPTS1Daily();

          //SaveData(zKPI_PTS, "BPTP");                          //Save the PTS data to BPTP and save all later
          //arr[zBP_BPTP, FROM_MONTH] = arr[zKPI_PTS, FROM_MONTH];
          DoBPTPDaily();

        }


        if (boRunMonth == true)
        { 
          SaveData(zBP_POR, "POR");     //use the month data
        }
        else
          DoPOR();      //daily/weekly calc

        DoBPOH();             //do this before the POR Quarter calculation 2023-09-07

        //CalcByHrs_YTD_QTR(zBP_POR, ROUND5);       2023-09-07
        DoPOR_QCalc();
        SaveQuarter(zBP_POR, "POR");

        ///////////
        //monthly only
        SaveData(zBP_BPTS1, "BPTS1");               
        SumYTD_QTR(zBP_BPTS1, ROUND5);             
        SaveQuarter(zBP_BPTS1, "BPTS1");
                    


        SaveData(zBP_BPTP, "BPTP");
        SumYTD_QTR(zBP_BPTP, ROUND5);                
        SaveQuarter(zBP_BPTP, "BPTP");


        ///////////       2021-10-06
        if (boRunMonth == false)    //daily/weekly run 
        {
          //t7 = arr[zSappiHours, FROM_MONTH];
          //t7 = arr[zBP_BPMST, FROM_MONTH];
          arr[zBP_BPMST, FROM_MONTH] = cmath.div(arr[zBP_BPMST, FROM_MONTH] , arr[zSappiHours, FROM_MONTH]);
          arr[zBP_BPPPS, FROM_MONTH] = cmath.div(arr[zBP_BPPPS, FROM_MONTH] , arr[zSappiHours, FROM_MONTH]);
          arr[zBP_BPIBP, FROM_MONTH] = cmath.div(arr[zBP_BPIBP, FROM_MONTH] , arr[zSappiHours, FROM_MONTH]);
          arr[zBP_BPMDB, FROM_MONTH] = cmath.div(arr[zBP_BPMDB, FROM_MONTH] , arr[zSappiHours, FROM_MONTH]);
          arr[zBP_BPPST, FROM_MONTH] = cmath.div(arr[zBP_BPPST, FROM_MONTH] , arr[zSappiHours, FROM_MONTH]);
        }

        SaveData(zBP_BPMST, "BPMST");               //2021-09-18
        SumYTD_QTR(zBP_BPMST, ROUND5);
        SaveQuarter(zBP_BPMST, "BPMST");

        SaveData(zBP_BPPPS, "BPPPS");               //2021-09-18
        SumYTD_QTR(zBP_BPPPS, ROUND5);
        SaveQuarter(zBP_BPPPS, "BPPPS");

        SaveData(zBP_BPIBP, "BPIBP");               //2021-09-18
        SumYTD_QTR(zBP_BPIBP, ROUND5);
        SaveQuarter(zBP_BPIBP, "BPIBP");

        SaveData(zBP_BPMDB, "BPMDB");               //2021-09-18
        SumYTD_QTR(zBP_BPMDB, ROUND5);
        SaveQuarter(zBP_BPMDB, "BPMDB");

        SaveData(zBP_BPPST, "BPPST");               //2021-09-18
        SumYTD_QTR(zBP_BPPST, ROUND5);
        SaveQuarter(zBP_BPPST, "BPPST");


        SaveData(zBP_AMBQR, "AMBQR");
        CalcByHrs_YTD_QTR(zBP_AMBQR, ROUND5);        
        SaveQuarter(zBP_AMBQR, "AMBQR");

        DoBPNPT();

        SaveData(zBP_BNPP, "BNPP");
        DoBNPP_QCalc();
        SaveQuarter(zBP_BNPP, "BNPP");


        SaveData(zBP_BMS, "BMS");                     
        CalcByHrs_YTD_QTR(zBP_BMS, ROUND5);
        SaveQuarter(zBP_BMS, "BMS");

        SaveData(zBP_BPMMM, "BPMMM");                  
        CalcByHrs_YTD_QTR(zBP_BPMMM, ROUND5);
        SaveQuarter(zBP_BPMMM, "BPMMM");

        SaveData(zBP_BPPPP, "BPPPP");                 
        CalcByHrs_YTD_QTR(zBP_BPPPP, ROUND5);
        SaveQuarter(zBP_BPPPP, "BPPPP");

        if (boRunDaily || boRunWeek)
          DoWAGRR();        


        DoBGP();                        
        SumYTD_QTR(zBP_BGP, ROUND5);
        SaveQuarter(zBP_BGP, "BGP");

        DoWAGRR();


        DoABGP();                                 // after BGP
        SumYTD_QTR(zKPI_ABGP, ROUND5);
        SaveQuarter(zKPI_ABGP, "ABGP");


        SaveData(zBP_BQR, "BQR");
        CalcByHrs_YTD_QTR(zBP_BQR, ROUND5);  
        SaveQuarter(zBP_BQR, "BQR");

        SaveData(zBP_BPSY, "BPSY");
        CalcByHrs_YTD_QTR(zBP_BPSY, ROUND5);       
        SaveQuarter(zBP_BPSY, "BPSY");

        DoNSPPM();                                   //after BGP and BPSY   


        SaveData(zBP_BPRFT, "BPRFT");
        CalcByHrs_YTD_QTR(zBP_BPRFT, ROUND5);      
        SaveQuarter(zBP_BPRFT, "BPRFT");

        SaveData(zBP_BPRFT2, "BPRFT2");
        CalcByHrs_YTD_QTR(zBP_BPRFT2, ROUND5);       
        SaveQuarter(zBP_BPRFT2, "BPRFT2");

        DoNRFTP();                                    // after BGP and BQR

        SaveData(zBP_BPMS, "BPMS");
        CalcByHrs_YTD_QTR(zBP_BPMS, ROUND5);        
        SaveQuarter(zBP_BPMS, "BPMS");

        //2021-11-02
        SaveData(zBP_BPCBI, "BPCBI");
        CalcByHrs_YTD_QTR(zBP_BPCBI, ROUND5);
        SaveQuarter(zBP_BPCBI, "BPCBI");

        SaveData(zBP_BPGFY, "BPGFY");
        CalcByHrs_YTD_QTR(zBP_BPGFY, ROUND5);
        SaveQuarter(zBP_BPGFY, "BPGFY");

        //2021-11-02
        SaveData(zBP_BPGDC, "BPGDC");
        CalcByHrs_YTD_QTR(zBP_BPGDC, ROUND5);
        SaveQuarter(zBP_BPGDC, "BPGDC");

        //2021-11-02
        SaveData(zBP_BPCDUMP, "CDUMP");
        SumYTD_QTR(zBP_BPCDUMP, ROUND5);
        SaveQuarter(zBP_BPCDUMP, "CDUMP");


        DoBPOME();

        DoBOE();

        DoTEEP();

        //DoBPOH();         //2023-09-07  move to before POR for the POR quarters calc

        //DoBPNPT();          // moved to before BNPP for the quarters calc  2023-09-07

        DoNSPD();


        SaveData(zBP_BPCS, "BPCS");
        SumYTD_QTR(zBP_BPCS, ROUND5);         //bp commercial standstill
        SaveQuarter(zBP_BPCS, "BPCS");

        SaveData(zBP_BPOS, "BPOS");
        SumYTD_QTR(zBP_BPOS, ROUND5);         //bp commercial standstill
        SaveQuarter(zBP_BPOS, "BPOS");

        if (boRunMonth)
          DoBPTS();          //bp technical standstill,  see below if a daily run is performed


        //KPIs------------------------------------------------------

        Application.DoEvents();

        SumYTD_QTR(zKPI_CMDT, ROUND1);
        SaveQuarter(zKPI_CMDT, "CMDT");

        Application.DoEvents();

        SumYTD_QTR(zKPI_PTS, ROUND1);
        SaveQuarter(zKPI_PTS, "PTS");

        SumYTD_QTR(zKPI_PLAP, ROUND1);    //2021-02-19
        SaveQuarter(zKPI_PLAP, "PLAP");

        SumYTD_QTR(zKPI_TCSS, ROUND1);    //2021-02-22
        SaveQuarter(zKPI_TCSS, "TCSS");

        SumYTD_QTR(zKPI_TISS, ROUND1);    //2021-02-22
        SaveQuarter(zKPI_TISS, "TISS");

        Application.DoEvents();

        SumYTD_QTR(zKPI_PLO, ROUND1);
        SaveQuarter(zKPI_PLO, "PLO");

        Application.DoEvents();

        DoRow26();                //Operating time

        DoRow20();                //after KPI0026

        DoRow21();       

        DoRow22();       

        SumYTD_QTR(zKPI_PLMT, ROUND1);
        SaveQuarter(zKPI_PLMT, "PLMT");

        Application.DoEvents();


        SumYTD_QTR(zKPI_PLPS, ROUND1);
        SaveQuarter(zKPI_PLPS, "PLPS");

        Application.DoEvents();

        SumYTD_QTR(zKPI_PIBP, ROUND1);
        SaveQuarter(zKPI_PIBP, "PIBP");

        Application.DoEvents();
        
        SumYTD_QTR(zKPI_UPLE, ROUND1);
        SaveQuarter(zKPI_UPLE, "UPLE");

        Application.DoEvents();

        SumYTD_QTR(zKPI_PROC, ROUND1);
        SaveQuarter(zKPI_PROC, "PROC");

        Application.DoEvents();

        DoRow28();                //Net Productive time

        DoRow29();                //availability

        Application.DoEvents();

        SumYTD_QTR(zKPI_PLS, ROUND1);     //the old availability 
        SaveQuarter(zKPI_PLS, "PLS");

        SumYTD_QTR(zKPI_MPPD, ROUND1);    // the old availalility
        SaveQuarter(zKPI_MPPD, "MPPD");

        Application.DoEvents();

        SumYTD_QTR(zKPI_GP, ROUND5);
        SaveQuarter(zKPI_GP, "GP");

        SumYTD_QTR(zKPI_DKL, ROUND5);
        SaveQuarter(zKPI_DKL, "DKL");

        DoRow33();

        Application.DoEvents();

        SumYTD_QTR(zKPI_TBS, ROUND5);
        SaveQuarter(zKPI_TBS, "TBS");

        SumYTD_QTR(zKPI_136, ROUND5);         //2021-02-16 china b grade saiccor
        SaveQuarter(zKPI_136, "KPI0136");



        SumYTD_QTR(zKPI_AGDC, ROUND5);            //2021-11-02
        SaveQuarter(zKPI_AGDC, "AGDC");

        CalcByHrs_YTD_QTR(zKPI_ADQPP, ROUND5);    //2021-11-02
        SaveQuarter(zKPI_ADQPP, "ADQPP");

        DoADCAT();                                //2021-11-02

        DoAGFY();                                 //2021-11-02

        DoGYFC();                                 //2021-11-02         

        if (isDigester == true)                    //2021-11-02       
        {
          DoTBS();
        }



        DoRow35();

        DoRow36();

        Application.DoEvents();

        DoRow37();

        DoRow38();

        SumYTD_QTR(zKPI_PPAG, ROUND5);
        SaveQuarter(zKPI_PPAG, "PPAG");

        if (isDigester == true)
        {
          DoPPAG();                               //2021-11-02       
        }


        DoRow40();

        Application.DoEvents();

        SumYTD_QTR(zKPI_RFT, ROUND5);
        SaveQuarter(zKPI_RFT, "RFT");

        if (isDigester == true)
        {
          DoRFT();
        }


        Application.DoEvents();

        DoRow42();
        DoRow43();                
        DoRow46();

        Application.DoEvents();

        DoRow49();
        DoRow50();
        DoRow51();

        Application.DoEvents();

        DoRow52();
        DoRow53();
        DoRow56();

        Application.DoEvents();

        DoRow60();
        DoRow61();
        DoRow62();

        Application.DoEvents();

        DoRow63();
        DoRow65();
        DoRow66();

        Application.DoEvents();

        DoRow64();        //do this after 65 and 66

        //DoRow57();        //do after 66

        //DoRow58();        // after 57

        Application.DoEvents();

        DoRow57();         

        DoRow47();         

        DoRow58();

        DoRow45();          //after 46 and 47

        //if (sSite == "NGX" && sAreaID == "139")
        //{
        //  DoRow47_QCalc();   // because ytd is a circular reference  do the ytd after KPI0045 and KPU0046  //(KPI0046 / KPI0045)*100
        //  SaveQuarter(zKPI_47, "KPI0047");
        //}

        //if (sSite == "NGX" && (sAreaID == "161" || sAreaID == "139"))     //C-2210-0105
        //{
        //  DoRows57_QCalc();   // because ytd is a circular reference (KPI 0029 * KPI 0043 * KPI 0047), do the ytd after KPI0047
        //  SaveQuarter(zKPI_57, "KPI0057");
        //}


        DoRow54();
            
        Application.DoEvents();

        DoRow55();
        DoRow67();
        DoRow69();

        Application.DoEvents();

        DoRow70();
        DoRow71();
        DoRow72();

        Application.DoEvents();

        DoRow73();
        DoRow74();
        DoRow75();

        Application.DoEvents();

        DoRow77();
        DoRow78();
        DoRow79();

        Application.DoEvents();

        DoRow80();
        DoRow81();
        DoRow82();

        Application.DoEvents();

        DoRow83();
        DoRow163();   //2023-06-13

        DoRow84();
        DoRow85();

        Application.DoEvents();

        DoRow86();
        DoRow87();
        DoRow88();

        Application.DoEvents();

        DoRow90();
        DoRow91();
        DoRow92();

        Application.DoEvents();

        DoRow93();
        DoRow94();
        DoRow95();

        Application.DoEvents();

        DoRow96();
        DoRow97();
        DoRow98();


        Application.DoEvents();
        DoRow99();
        DoRow100();
        DoRow101();

        Application.DoEvents();
        DoRow103();
        DoRow104();
        DoRow105();

        Application.DoEvents();
        DoRow106();
        DoRow107();
        DoRow108();

        Application.DoEvents();
        DoRow109();
        DoRow110();
        DoRow111();

        Application.DoEvents();
        DoRow112();
        DoRow113();
        DoRow114();

        Application.DoEvents();
        DoRow116();
        DoRow117();
        DoRow118();
        DoRow119();


        Application.DoEvents();

        DoKPI_120();
        DoKPI_121();
        DoKPI_122();

        DoKPI_123();
        DoKPI_124();
        DoKPI_125();

        DoKPI_126();
        DoKPI_127();
        DoKPI_128();
        DoKPI_129();

        DoKPI_130();
        DoKPI_131();
        DoKPI_132();
        DoKPI_133();

        DoKPI_134();
        DoKPI_135();

        //2021-02-05 new KPIs  hidden
        DoKPI_153();
        DoKPI_155();
        DoKPI_156();

        DoKPI_150();
        DoKPI_151();
        DoKPI_152();

        DoKPI_154();
        DoKPI_157();
        DoKPI_158();
        DoKPI_159();

        //mafube
        DoKPI_137();
        DoKPI_139();
        DoKPI_140();

        DoKPI_138();          // uses 0140
        DoKPI_141();
        DoKPI_142();
        DoKPI_143();
        DoKPI_144();

        DoKPI_145();          //after 81 and BPMMM and BPPPP
        DoKPI_146();
        DoKPI_147();
        DoKPI_148();

        DoKPI_149();      //2021-03-04
        DoKPI_160();      //2021-03-04

        DoKPI_161();      //2021-11-14
        DoKPI_162();      //2021-11-14

        //FOR MAFUBE REPORT     //2021-03-08
        //DoKPI_MSC1();         //initialise inputs only 2021-03-10
        //DoKPI_MPDPOS();       //initialise inputs only 2021-03-10

        //FOR MAFUBE REPORT     //2021-03-08
        DoKPI_MISDM();
        DoKPI_MNSP();
        DoKPI_MNSPP();
        DoKPI_MPMJ();

        DoKPI_MOJEDL();
        DoKPI_MOJMPD();
        DoKPI_MOJMDP();
        DoKPI_MFJO();

        DoKPI_MFJOED();
        DoKPI_MFJOEDL();
        DoKPI_MPDP();
        DoKPI_MNOJ();
        DoKPI_MJSD();

        DoKPI_MJCS();
        DoKPI_MFT();
        DoKPI_MFT();
        DoKPI_MFAMAN();
        DoKPI_MFAMAR();

       
        Application.DoEvents();

      }
      catch (Exception er)
      {
        rv = false;
        cFG.LogIt( "Do Process KPIJob " + er.Message);
        throw new Exception(er.Message);
      }

      return rv;

    } //


    private bool DoProcess_YTD_Q()
    {

      //called from the RunMonthly()

      //process the quarters and ytd for each month of the year

      //preconditions---------------------
      //the array contains all the monthly data necessary to recalculate the ytd and quarter

      bool rv = true;
      int sappiMonthID = 0;

      try
      {

        //descend the months columns
        for (int monthNum = currMonth; monthNum > 0; monthNum--)
        {
          //begin from the current month


          sappiMonthID = cal.GetSappiMonthID(iSappiYear, monthNum);

          cFG.LogIt("Process YTD_Q Area " + sAreaID + " Month " + monthNum );



          SumYTD_QTR(zSappiHours, ROUND1);
          SaveQM(zSappiHours, "CT", sappiMonthID);

          SumYTD_QTR(zKPI_CAL24, ROUND1);
          SaveQM(zKPI_CAL24, "CTT", sappiMonthID);

          CalcByHrs_YTD_QTR(zBP_MGPR, ROUND5);
          SaveQM(zBP_MGPR, "MGPR", sappiMonthID);

          Application.DoEvents();

          CalcByHrs_YTD_QTR(zBP_PURM, ROUND5);
          SaveQM(zBP_PURM, "PURM", sappiMonthID);

          SumYTD_QTR(zBP_BPOH, ROUND5);             //moved from after POR
          SaveQM(zBP_BPOH, "BPOH", sappiMonthID);

          //CalcByHrs_YTD_QTR(zBP_POR, ROUND5);  2023-09-07
          DoPOR_QCalc();  
          SaveQM(zBP_POR, "POR", sappiMonthID);

          SumYTD_QTR(zBP_BGP, ROUND5);
          SaveQM(zBP_BGP, "BGP", sappiMonthID);

          Application.DoEvents();

          SumYTD_QTR(zKPI_ABGP, ROUND5);
          SaveQM(zKPI_ABGP, "ABGP", sappiMonthID);

          CalcByHrs_YTD_QTR(zBP_AMBQR, ROUND5);
          SaveQM(zBP_AMBQR, "AMBQR", sappiMonthID);

          SumYTD_QTR(zBP_BPNPT, ROUND5);                      //moved from below
          SaveQM(zBP_BPNPT, "BPNPT", sappiMonthID);

          //CalcByHrs_YTD_QTR(zBP_BNPP, ROUND5);  2023-09-07
          DoBNPP_QCalc(); 
          SaveQM(zBP_BNPP, "BNPP", sappiMonthID);

          //CalcByHrs_YTD_QTR(zBP_WAGRR, ROUND5); 2023-09-07
          DoWAGRR_QCalc();
          SaveQM(zBP_WAGRR, "WAGRR", sappiMonthID);

          Application.DoEvents();

          
          SumYTD_QTR(zBP_BPTS1, ROUND5);
          SaveQM(zBP_BPTS1, "BPTS1", sappiMonthID);

          SumYTD_QTR(zBP_BPTP, ROUND5);
          SaveQM(zBP_BPTP, "BPTP", sappiMonthID);

          SumYTD_QTR(zBP_BPMST, ROUND5);
          SaveQM(zBP_BPMST, "BPMST", sappiMonthID);

          SumYTD_QTR(zBP_BPPPS, ROUND5);
          SaveQM(zBP_BPPPS, "BPPPS", sappiMonthID);

          SumYTD_QTR(zBP_BPIBP, ROUND5);
          SaveQM(zBP_BPIBP, "BPIBP", sappiMonthID);

          SumYTD_QTR(zBP_BPMDB, ROUND5);
          SaveQM(zBP_BPMDB, "BPMDB", sappiMonthID);

          SumYTD_QTR(zBP_BPPST, ROUND5);
          SaveQM(zBP_BPPST, "BPPST", sappiMonthID);

          Application.DoEvents();

          CalcByHrs_YTD_QTR(zBP_BQR, ROUND5);
          SaveQM(zBP_BQR, "BQR", sappiMonthID);

          CalcByHrs_YTD_QTR(zBP_BPSY, ROUND5);
          SaveQM(zBP_BPSY, "BPSY", sappiMonthID);

          SumYTD_QTR(zBP_NSPPM, ROUND5);
          SaveQM(zBP_NSPPM, "NSPPM", sappiMonthID);

          CalcByHrs_YTD_QTR(zBP_BPRFT, ROUND5);
          SaveQM(zBP_BPRFT, "BPRFT", sappiMonthID);

          Application.DoEvents();

          CalcByHrs_YTD_QTR(zBP_BPRFT2, ROUND5);
          SaveQM(zBP_BPRFT2, "BPRFT2", sappiMonthID);

          SumYTD_QTR(zBP_NRFTP, ROUND5);
          SaveQM(zBP_NRFTP, "NRFTP", sappiMonthID);

          CalcByHrs_YTD_QTR(zBP_BPMS, ROUND5);
          SaveQM(zBP_BPMS, "BPMS", sappiMonthID);

          //CalcByHrs_YTD_QTR(zBP_BPOME, ROUND5);  2023-09-07
          DoBPOME_QCalc();
          SaveQM(zBP_BPOME, "BPOME", sappiMonthID);

          Application.DoEvents();

          //CalcByHrs_YTD_QTR(zBP_BOE, ROUND5);   2023-09-07
          DoBOE_QCalc();
          SaveQM(zBP_BOE, "BOE", sappiMonthID);

          //CalcByHrs_YTD_QTR(zBP_TEEP, ROUND5);  2023-09-07
          DoTEEP_QCalc();
          SaveQM(zBP_TEEP, "TEEP", sappiMonthID);

          //SumYTD_QTR(zBP_BPOH, ROUND5);             //moved to before POR  2023-09-07
          //SaveQM(zBP_BPOH, "BPOH", sappiMonthID);

          //SumYTD_QTR(zBP_BPNPT, ROUND5);              //moved to before BNPP
          //SaveQM(zBP_BPNPT, "BPNPT", sappiMonthID);

          Application.DoEvents();

          SumYTD_QTR(zBP_BPCS, ROUND5);
          SaveQM(zBP_BPCS, "BPCS", sappiMonthID);

          SumYTD_QTR(zBP_BPOS, ROUND5);
          SaveQM(zBP_BPOS, "BPOS", sappiMonthID);

          SumYTD_QTR(zBP_BPTS, ROUND5);
          SaveQM(zBP_BPTS, "BPTS", sappiMonthID);

          //KPIs------------------------------------------------------
          Application.DoEvents();

          SumYTD_QTR(zKPI_26, ROUND5);                    //moved from below
          SaveQM(zKPI_26, "KPI0026", sappiMonthID);

          //CalcByHrs_YTD_QTR_NOZero(zKPI_20, ROUND5);  2023-09-07
          DoRow20_QCalc();
          SaveQM(zKPI_20, "KPI0020", sappiMonthID);

          //CalcByHrs_YTD_QTR(zKPI_21, ROUND5);     2023-09-07
          DoRow21_QCalc();
          SaveQM(zKPI_21, "KPI0021", sappiMonthID);

          //CalcByHrs_YTD_QTR(zKPI_22, ROUND5); 2023-09-07
          DoRow22_QCalc();
          SaveQM(zKPI_22, "KPI0022", sappiMonthID);

          Application.DoEvents();

          SumYTD_QTR(zKPI_CMDT, ROUND1);
          SaveQM(zKPI_CMDT, "CMDT", sappiMonthID);


          SumYTD_QTR(zKPI_PTS, ROUND1);
          SaveQM(zKPI_PTS, "PTS", sappiMonthID);

          SumYTD_QTR(zKPI_PLAP, ROUND1);
          SaveQM(zKPI_PLAP, "PLAP", sappiMonthID);

          SumYTD_QTR(zKPI_TCSS, ROUND1);
          SaveQM(zKPI_TCSS, "TCSS", sappiMonthID);

          SumYTD_QTR(zKPI_TISS, ROUND1);
          SaveQM(zKPI_TISS, "TISS", sappiMonthID);

          SumYTD_QTR(zKPI_PLO, ROUND1);
          SaveQM(zKPI_PLO, "PLO", sappiMonthID);

          Application.DoEvents();

          //SumYTD_QTR(zKPI_26, ROUND5);                    //moved
          //SaveQM(zKPI_26, "KPI0026", sappiMonthID);


          SumYTD_QTR(zKPI_PLMT, ROUND1);
          SaveQM(zKPI_PLMT, "PLMT", sappiMonthID);


          SumYTD_QTR(zKPI_PLPS, ROUND1);
          SaveQM(zKPI_PLPS, "PLPS", sappiMonthID);

          Application.DoEvents();

          SumYTD_QTR(zKPI_PIBP, ROUND1);
          SaveQM(zKPI_PIBP, "PIBP", sappiMonthID);

          SumYTD_QTR(zKPI_UPLE, ROUND1);
          SaveQM(zKPI_UPLE, "UPLE", sappiMonthID);


          SumYTD_QTR(zKPI_PROC, ROUND1);
          SaveQM(zKPI_PROC, "PROC", sappiMonthID);

          Application.DoEvents();

          SumYTD_QTR(zKPI_28, ROUND5);
          SaveQM(zKPI_28, "KPI0028", sappiMonthID);

         
          DoRow29_Qcalc();
          SaveQM(zKPI_29, "KPI0029", sappiMonthID);

          Application.DoEvents();

          SumYTD_QTR(zKPI_PLS, ROUND1);     //the old availability 
          SaveQM(zKPI_PLS, "PLS", sappiMonthID);

          SumYTD_QTR(zKPI_MPPD, ROUND1);    // the old availalility
          SaveQM(zKPI_MPPD, "MPPD", sappiMonthID);

          Application.DoEvents();

          SumYTD_QTR(zKPI_GP, ROUND5);
          SaveQM(zKPI_GP, "GP", sappiMonthID);

          SumYTD_QTR(zKPI_DKL, ROUND5);
          SaveQM(zKPI_DKL, "DKL", sappiMonthID);

          SumYTD_QTR(zKPI_33, ROUND5);
          SaveQM(zKPI_33, "KPI0033", sappiMonthID);

          Application.DoEvents();

          SumYTD_QTR(zKPI_TBS, ROUND5);
          SaveQM(zKPI_TBS, "TBS", sappiMonthID);

          SumYTD_QTR(zKPI_136, ROUND5);  
          SaveQM(zKPI_136, "KPI0136", sappiMonthID);

          SumYTD_QTR(zKPI_35, ROUND5);
          SaveQM(zKPI_35, "KPI0035", sappiMonthID);

          
          DoRow38_Qcalc();
          SaveQM(zKPI_38, "KPI0038", sappiMonthID);

          //CalcByHrs_YTD_QTR(zKPI_36, ROUND5);   2023-09-07
          DoRow36_QCalc();
          SaveQM(zKPI_36, "KPI0036", sappiMonthID);

          Application.DoEvents();

          //CalcByHrs_YTD_QTR(zKPI_37, ROUND5); 2023-09-07
          DoRow37_QCalc();
          SaveQM(zKPI_37, "KPI0037", sappiMonthID);

          SumYTD_QTR(zKPI_PPAG, ROUND5);
          SaveQM(zKPI_PPAG, "PPAG", sappiMonthID);

          DoRow40_Qcalc();
          SaveQM(zKPI_40, "KPI0040", sappiMonthID);


          Application.DoEvents();

          SumYTD_QTR(zKPI_RFT, ROUND5);
          SaveQM(zKPI_RFT, "RFT", sappiMonthID);


         
          DoRow42_QCalc();    
          SaveQM(zKPI_42, "KPI0042", sappiMonthID);

          Application.DoEvents();


       
          DoRow43_QCalc();          //2022-07-19    after KPI0038, KPI0040, KPI0042
          SaveQM(zKPI_43, "KPI0043", sappiMonthID);

          //CalcByHrs_YTD_QTR(zKPI_46, ROUND5);   2023-09-07
          DoRow46_QCalc();
          SaveQM(zKPI_46, "KPI0046", sappiMonthID);

          Application.DoEvents();

          //CalcByHrs_YTD_QTR(zKPI_49, ROUND5); 2023-09-07
          DoRow49_QCalc();
          SaveQM(zKPI_49, "KPI0049", sappiMonthID);


          //CalcByHrs_YTD_QTR(zKPI_50, ROUND5);  // 2023-09-07
          DoRow50_Qcalc();
          SaveQM(zKPI_50, "KPI0050", sappiMonthID);

          //CalcByHrs_YTD_QTR(zKPI_51, ROUND5);
          DoRow51_QCalc();
          SaveQM(zKPI_51, "KPI0051", sappiMonthID);

          Application.DoEvents();

        
          DoRows52_QCalc();         //2022-07-19    
          SaveQM(zKPI_52, "KPI0052", sappiMonthID);

        
          DoRows53_QCalc();           //2022-07-19  
          SaveQM(zKPI_53, "KPI0053", sappiMonthID);

          
          DoRows56_QCalc();
          SaveQM(zKPI_56, "KPI0056", sappiMonthID);

          Application.DoEvents();

          //CalcByHrs_YTD_QTR(zKPI_60, ROUND5);       //2023-09-07
          DoRow60_QCalc();
          SaveQM(zKPI_60, "KPI0060", sappiMonthID);

          //CalcByHrs_YTD_QTR(zKPI_61, ROUND5);   //2023-09-07
          DoRow61_QCalc();
          SaveQM(zKPI_61, "KPI0061", sappiMonthID);

          //CalcByHrs_YTD_QTR(zKPI_62, ROUND5);   2023-09-07
          DoRow62_QCalc();
          SaveQM(zKPI_62, "KPI0062", sappiMonthID);

          Application.DoEvents();

          //CalcByHrs_YTD_QTR(zKPI_63, ROUND5);
          DoRow63_QCalc();
          SaveQM(zKPI_63, "KPI0063", sappiMonthID);

          //CalcByHrs_YTD_QTR(zKPI_65, ROUND5);
          DoRow65_QCalc();
          SaveQM(zKPI_65, "KPI0065", sappiMonthID);

          //CalcByHrs_YTD_QTR(zKPI_66, ROUND5);
          DoRow66_QCalc();
          SaveQM(zKPI_66, "KPI0066", sappiMonthID);

          Application.DoEvents();

          //CalcByHrs_YTD_QTR(zKPI_64, ROUND5);
          DoRow64_QCalc();
          SaveQM(zKPI_64, "KPI0064", sappiMonthID);


          Application.DoEvents();

          //CalcByHrs_YTD_QTR(zKPI_57, ROUND5);     
          DoRows57_QCalc();
          SaveQM(zKPI_57, "KPI0057", sappiMonthID);


          DoRow47_QCalc();         // 2023-09-07
          SaveQM(zKPI_47, "KPI0047", sappiMonthID);


          //CalcByHrs_YTD_QTR(zKPI_58, ROUND5);     //2023-09-07
          DoRow58_QCalc();
          SaveQM(zKPI_58, "KPI0058", sappiMonthID);

          //CalcByHrs_YTD_QTR(zKPI_45, ROUND5);   2023-09-07
          DoRow45_QCalc();
          SaveQM(zKPI_45, "KPI0045", sappiMonthID);


          DoRows54_QCalc();       
          SaveQM(zKPI_54, "KPI0054", sappiMonthID);

          Application.DoEvents();

          SumYTD_QTR(zKPI_55, ROUND5);
          SaveQM(zKPI_55, "KPI0055", sappiMonthID);


          //CalcByHrs_YTD_QTR(zKPI_67, ROUND5);
          DoRow67_QCalc();
          SaveQM(zKPI_67, "KPI0067", sappiMonthID);


          //CalcByHrs_YTD_QTR(zKPI_69, ROUND5);
          DoRow69_QCalc();
          SaveQM(zKPI_69, "KPI0069", sappiMonthID);


          Application.DoEvents();

          //CalcByHrs_YTD_QTR(zKPI_70, ROUND5);
          DoRow70_QCalc();
          SaveQM(zKPI_70, "KPI0070", sappiMonthID);

          //CalcByHrs_YTD_QTR(zKPI_71, ROUND5);
          DoRow71_QCalc();
          SaveQM(zKPI_71, "KPI0071", sappiMonthID);

          //CalcByHrs_YTD_QTR(zKPI_72, ROUND5);
          DoRow72_QCalc();
          SaveQM(zKPI_72, "KPI0072", sappiMonthID);

          Application.DoEvents();

          //CalcByHrs_YTD_QTR(zKPI_73, ROUND5);
          DoRow73_QCalc();
          SaveQM(zKPI_73, "KPI0073", sappiMonthID);

          //CalcByHrs_YTD_QTR(zKPI_74, ROUND5);
          DoRow74_QCalc();
          SaveQM(zKPI_74, "KPI0074", sappiMonthID);

          //CalcByHrs_YTD_QTR(zKPI_75, ROUND5);
          DoRow75_QCalc();
          SaveQM(zKPI_75, "KPI0075", sappiMonthID);

          Application.DoEvents();

          SumYTD_QTR(zKPI_77, ROUND5);
          SaveQM(zKPI_77, "KPI0077", sappiMonthID);

          SumYTD_QTR(zKPI_78, ROUND5);
          SaveQM(zKPI_78, "KPI0078", sappiMonthID);

          SumYTD_QTR(zKPI_79, ROUND5);
          SaveQM(zKPI_79, "KPI0079", sappiMonthID);

          Application.DoEvents();

          SumYTD_QTR(zKPI_80, ROUND5);
          SaveQM(zKPI_80, "KPI0080", sappiMonthID);

          SumYTD_QTR(zKPI_81, ROUND5);
          SaveQM(zKPI_81, "KPI0081", sappiMonthID);

          SumYTD_QTR(zKPI_82, ROUND5);
          SaveQM(zKPI_82, "KPI0082", sappiMonthID);

          Application.DoEvents();

          SumYTD_QTR(zKPI_83, ROUND5);
          SaveQM(zKPI_83, "KPI0083", sappiMonthID);

          SumYTD_QTR(zKPI_163, ROUND5);
          SaveQM(zKPI_163, "KPI0163", sappiMonthID);    //2023-06-13
                            
          SumYTD_QTR(zKPI_84, ROUND5);
          SaveQM(zKPI_84, "KPI0084", sappiMonthID);

          SumYTD_QTR(zKPI_85, ROUND5);
          SaveQM(zKPI_85, "KPI0085", sappiMonthID);

          Application.DoEvents();

          SumYTD_QTR(zKPI_86, ROUND5);
          SaveQM(zKPI_86, "KPI0086", sappiMonthID);

          SumYTD_QTR(zKPI_87, ROUND5);
          SaveQM(zKPI_87, "KPI0087", sappiMonthID);

          SumYTD_QTR(zKPI_88, ROUND5);
          SaveQM(zKPI_88, "KPI0088", sappiMonthID);

          Application.DoEvents();

          SumYTD_QTR(zKPI_90, ROUND5);
          SaveQM(zKPI_90, "KPI0090", sappiMonthID);

          SumYTD_QTR(zKPI_91, ROUND5);
          SaveQM(zKPI_91, "KPI0091", sappiMonthID);

          SumYTD_QTR(zKPI_92, ROUND5);
          SaveQM(zKPI_92, "KPI0092", sappiMonthID);


          Application.DoEvents();

          SumYTD_QTR(zKPI_93, ROUND5);
          SaveQM(zKPI_93, "KPI0093", sappiMonthID);

          SumYTD_QTR(zKPI_94, ROUND5);
          SaveQM(zKPI_94, "KPI0094", sappiMonthID);

          SumYTD_QTR(zKPI_95, ROUND5);
          SaveQM(zKPI_95, "KPI0095", sappiMonthID);

          Application.DoEvents();

          SumYTD_QTR(zKPI_96, ROUND5);
          SaveQM(zKPI_96, "KPI0096", sappiMonthID);

          SumYTD_QTR(zKPI_97, ROUND5);
          SaveQM(zKPI_97, "KPI0097", sappiMonthID);

          SumYTD_QTR(zKPI_98, ROUND5);
          SaveQM(zKPI_98, "KPI0098", sappiMonthID);


          Application.DoEvents();

          SumYTD_QTR(zKPI_99, ROUND5);
          SaveQM(zKPI_99, "KPI0099", sappiMonthID);

          SumYTD_QTR(zKPI_100, ROUND5);
          SaveQM(zKPI_100, "KPI0100", sappiMonthID);

          SumYTD_QTR(zKPI_101, ROUND5);
          SaveQM(zKPI_101, "KPI0101", sappiMonthID);


          Application.DoEvents();

          SumYTD_QTR(zKPI_103, ROUND5);
          SaveQM(zKPI_103, "KPI0103", sappiMonthID);

          SumYTD_QTR(zKPI_104, ROUND5);
          SaveQM(zKPI_104, "KPI0104", sappiMonthID);

          SumYTD_QTR(zKPI_105, ROUND5);
          SaveQM(zKPI_105, "KPI0105", sappiMonthID);

          Application.DoEvents();

          SumYTD_QTR(zKPI_106, ROUND5);
          SaveQM(zKPI_106, "KPI0106", sappiMonthID);

          SumYTD_QTR(zKPI_107, ROUND5);
          SaveQM(zKPI_107, "KPI0107", sappiMonthID);

          SumYTD_QTR(zKPI_108, ROUND5);
          SaveQM(zKPI_108, "KPI0108", sappiMonthID);

          Application.DoEvents();

          SumYTD_QTR(zKPI_109, ROUND5);
          SaveQM(zKPI_109, "KPI0109", sappiMonthID);

          SumYTD_QTR(zKPI_110, ROUND5);
          SaveQM(zKPI_110, "KPI0110", sappiMonthID);

          SumYTD_QTR(zKPI_111, ROUND5);
          SaveQM(zKPI_111, "KPI0111", sappiMonthID);

          Application.DoEvents();

          SumYTD_QTR(zKPI_112, ROUND5);
          SaveQM(zKPI_112, "KPI0112", sappiMonthID);

          SumYTD_QTR(zKPI_113, ROUND5);
          SaveQM(zKPI_113, "KPI0113", sappiMonthID);

          SumYTD_QTR(zKPI_114, ROUND5);
          SaveQM(zKPI_114, "KPI0114", sappiMonthID);

          Application.DoEvents();

          //CalcByHrs_YTD_QTR(zKPI_116, ROUND5);
          DoRow116_QCalc();
          SaveQM(zKPI_116, "KPI0116", sappiMonthID);

          //CalcByHrs_YTD_QTR(zKPI_117, ROUND5);
          DoRow117_QCalc();
          SaveQM(zKPI_117, "KPI0117", sappiMonthID);

          //CalcByHrs_YTD_QTR(zKPI_118, ROUND5);
          DoRow118_QCalc();
          SaveQM(zKPI_118, "KPI0118", sappiMonthID);

          //CalcByHrs_YTD_QTR(zKPI_119, ROUND5);
          DoRow119_QCalc();
          SaveQM(zKPI_119, "KPI0119", sappiMonthID);

          Application.DoEvents();

          SumYTD_QTR(zKPI_120, ROUND5);
          SaveQM(zKPI_120, "KPI0120", sappiMonthID);

          SumYTD_QTR(zKPI_121, ROUND5);
          SaveQM(zKPI_121, "KPI0121", sappiMonthID);

          SumYTD_QTR(zKPI_122, ROUND5);
          SaveQM(zKPI_122, "KPI0122", sappiMonthID);

          SumYTD_QTR(zKPI_123, ROUND5);
          SaveQM(zKPI_123, "KPI0123", sappiMonthID);

          SumYTD_QTR(zKPI_124, ROUND5);
          SaveQM(zKPI_124, "KPI0124", sappiMonthID);

          SumYTD_QTR(zKPI_125, ROUND5);
          SaveQM(zKPI_125, "KPI0125", sappiMonthID);

          SumYTD_QTR(zKPI_126, ROUND5);
          SaveQM(zKPI_126, "KPI0126", sappiMonthID);

          SumYTD_QTR(zKPI_127, ROUND5);
          SaveQM(zKPI_127, "KPI0127", sappiMonthID);

          SumYTD_QTR(zKPI_128, ROUND5);
          SaveQM(zKPI_128, "KPI0128", sappiMonthID);

          SumYTD_QTR(zKPI_129, ROUND5);
          SaveQM(zKPI_129, "KPI0129", sappiMonthID);

          SumYTD_QTR(zKPI_130, ROUND5);
          SaveQM(zKPI_130, "KPI0130", sappiMonthID);

          SumYTD_QTR(zKPI_131, ROUND5);
          SaveQM(zKPI_131, "KPI0131", sappiMonthID);

          SumYTD_QTR(zKPI_132, ROUND5);
          SaveQM(zKPI_132, "KPI0132", sappiMonthID);

          SumYTD_QTR(zKPI_133, ROUND5);
          SaveQM(zKPI_133, "KPI0133", sappiMonthID);

          SumYTD_QTR(zKPI_134, ROUND5);
          SaveQM(zKPI_134, "KPI0134", sappiMonthID);

          SumYTD_QTR(zKPI_135, ROUND5);
          SaveQM(zKPI_135, "KPI0135", sappiMonthID);


         
          DoRow153_Qcalc();
          SaveQM(zKPI_153, "KPI0153", sappiMonthID);

        
          DoRow155_Qcalc();
          SaveQM(zKPI_155, "KPI0155", sappiMonthID);

       
          DoRow156_Qcalc();
          SaveQM(zKPI_156, "KPI0156", sappiMonthID);

         
          DoRow150_Qcalc();
          SaveQM(zKPI_150, "KPI0150", sappiMonthID);

        
          DoRow151_Qcalc();                    
          SaveQM(zKPI_151, "KPI0151", sappiMonthID);

       
          DoRow152_Qcalc();                   
          SaveQM(zKPI_152, "KPI0152", sappiMonthID);

      
          DoRow154_Qcalc();                   
          SaveQM(zKPI_154, "KPI0154", sappiMonthID);

       
          DoRow157_Qcalc();                  
          SaveQM(zKPI_157, "KPI0157", sappiMonthID);


       
          DoRow158_Qcalc();
          SaveQM(zKPI_158, "KPI0158", sappiMonthID);

       
          DoRow159_Qcalc();
          SaveQM(zKPI_159, "KPI0159", sappiMonthID);

          //mafube stuff
          SumYTD_QTR(zKPI_137, ROUND5);
          SaveQM(zKPI_137, "KPI0137", sappiMonthID);

          SumYTD_QTR(zKPI_138, ROUND5);
          SaveQM(zKPI_138, "KPI0138", sappiMonthID);

          SumYTD_QTR(zKPI_139, ROUND5);
          SaveQM(zKPI_139, "KPI0139", sappiMonthID);

          SumYTD_QTR(zKPI_140, ROUND5);
          SaveQM(zKPI_140, "KPI0140", sappiMonthID);

          SumYTD_QTR(zKPI_141, ROUND5);
          SaveQM(zKPI_141, "KPI0141", sappiMonthID);

          SumYTD_QTR(zKPI_142, ROUND5);
          SaveQM(zKPI_142, "KPI0142", sappiMonthID);

          SumYTD_QTR(zKPI_143, ROUND5);
          SaveQM(zKPI_143, "KPI0143", sappiMonthID);

          SumYTD_QTR(zKPI_144, ROUND5);
          SaveQM(zKPI_144, "KPI0144", sappiMonthID);

          CalcByHrs_YTD_QTR(zBP_BMS, ROUND5);
          SaveQM(zBP_BMS, "BMS", sappiMonthID);

          CalcByHrs_YTD_QTR(zBP_BPMMM, ROUND5);       
          SaveQM(zBP_BPMMM, "BPMMM", sappiMonthID);

          CalcByHrs_YTD_QTR(zBP_BPPPP, ROUND5);        
          SaveQM(zBP_BPPPP, "BPPPP", sappiMonthID);

          SumYTD_QTR(zKPI_145, ROUND5);         
          SaveQM(zKPI_145, "KPI0145", sappiMonthID);

          SumYTD_QTR(zKPI_146, ROUND5);         
          SaveQM(zKPI_146, "KPI0146", sappiMonthID);

          //CalcByHrs_YTD_QTR(zKPI_147, ROUND5);
          DoRow147_QCalc();
          SaveQM(zKPI_147, "KPI0147", sappiMonthID);

          //CalcByHrs_YTD_QTR(zKPI_148, ROUND5);
          DoRow148_QCalc();
          SaveQM(zKPI_148, "KPI0148", sappiMonthID);

          SumYTD_QTR(zKPI_149, ROUND5);         
          SaveQM(zKPI_149, "KPI0149", sappiMonthID);

          SumYTD_QTR(zKPI_160, ROUND5);         
          SaveQM(zKPI_160, "KPI0160", sappiMonthID);


          SumYTD_QTR(zKPI_161, ROUND5);         
          SaveQM(zKPI_161, "KPI0161", sappiMonthID);

          //CalcByHrs_YTD_QTR(zKPI_162, ROUND5);
          DoRow162_QCalc();
          SaveQM(zKPI_162, "KPI0162", sappiMonthID);

          SumYTD_QTR(zKPI_MSC1, ROUND5);            
          SaveQM(zKPI_MSC1, "MSC1", sappiMonthID);

          SumYTD_QTR(zKPI_MPDPOS, ROUND5);          
          SaveQM(zKPI_MPDPOS, "MPDPOS", sappiMonthID);

          SumYTD_QTR(zKPI_MISDM, ROUND5);             
          SaveQM(zKPI_MISDM, "MISDM", sappiMonthID);


          SumYTD_QTR(zKPI_MNSP, ROUND5);                
          SaveQM(zKPI_MNSP, "MNSP", sappiMonthID);

          SumYTD_QTR(zKPI_MNSPP, ROUND5);                 
          SaveQM(zKPI_MNSPP, "MNSPP", sappiMonthID);

          SumYTD_QTR(zKPI_MPMJ, ROUND5);                 
          SaveQM(zKPI_MPMJ, "MPMJ", sappiMonthID);

          SumYTD_QTR(zKPI_MOJEDL, ROUND5);                  
          SaveQM(zKPI_MOJEDL, "MOJEDL", sappiMonthID);

          SumYTD_QTR(zKPI_MOJMPD, ROUND5);               
          SaveQM(zKPI_MOJMPD, "MOJMPD", sappiMonthID);

          SumYTD_QTR(zKPI_MOJMDP, ROUND5);                 
          SaveQM(zKPI_MOJMDP, "MOJMDP", sappiMonthID);

          SumYTD_QTR(zKPI_MFJO, ROUND5);                    
          SaveQM(zKPI_MFJO, "MFJO", sappiMonthID);

          SumYTD_QTR(zKPI_MFJOED, ROUND5);                 
          SaveQM(zKPI_MFJOED, "MFJOED", sappiMonthID);

          SumYTD_QTR(zKPI_MFJOEDL, ROUND5);                
          SaveQM(zKPI_MFJOEDL, "MFJOEDL", sappiMonthID);

          SumYTD_QTR(zKPI_MPDP, ROUND5);                  
          SaveQM(zKPI_MPDP, "MPDP", sappiMonthID);

          SumYTD_QTR(zKPI_MNOJ, ROUND5);                   
          SaveQM(zKPI_MNOJ, "MNOJ", sappiMonthID);

          SumYTD_QTR(zKPI_MJSD, ROUND5);                   
          SaveQM(zKPI_MJSD, "MJSD", sappiMonthID);

          SumYTD_QTR(zKPI_MJCS, ROUND5);                    
          SaveQM(zKPI_MJCS, "MJCS", sappiMonthID);

          SumYTD_QTR(zKPI_MFT, ROUND5);                  
          SaveQM(zKPI_MFT, "MFT", sappiMonthID);

          SumYTD_QTR(zKPI_MFAMAN, ROUND5);                   
          SaveQM(zKPI_MFAMAN, "MFAMAN", sappiMonthID);

          SumYTD_QTR(zKPI_MFAMAR, ROUND5);                  
          SaveQM(zKPI_MFAMAR, "MFAMAR", sappiMonthID);

          CalcByHrs_YTD_QTR(zBP_BPCBI, ROUND5);                  
          SaveQM(zBP_BPCBI, "BPCBI", sappiMonthID);

          CalcByHrs_YTD_QTR(zBP_BPGFY, ROUND5);                     
          SaveQM(zBP_BPGFY, "BPGFY", sappiMonthID);

          CalcByHrs_YTD_QTR(zBP_BPGDC, ROUND5);                   
          SaveQM(zBP_BPGDC, "BPGDC", sappiMonthID);
          
          SumYTD_QTR(zBP_BPCDUMP, ROUND5);                       
          SaveQM(zBP_BPCDUMP, "CDUMP", sappiMonthID);

          CalcByHrs_YTD_QTR(zKPI_ADQPP, ROUND5);                 
          SaveQM(zKPI_ADQPP, "ADQPP", sappiMonthID);

          DoADCAT_Qcalc();                                              
          SaveQM(zKPI_ADCAT, "ADCAT", sappiMonthID);

          SumYTD_QTR(zKPI_AGDC, ROUND5);                           
          SaveQM(zKPI_AGDC, "AGDC", sappiMonthID);

          SumYTD_QTR(zKPI_AGFY, ROUND5);                        
          SaveQM(zKPI_AGFY, "AGFY", sappiMonthID);

          DoGFYC_Qcalc();                                             
          SaveQM(zKPI_GFYC, "GFYC", sappiMonthID);

          SumYTD_QTR(zKPI_SHEETB, ROUND5);
          SaveQM(zKPI_SHEETB, "SHEETB", sappiMonthID);

          SumYTD_QTR(zKPI_GRDCHG, ROUND5);
          SaveQM(zKPI_GRDCHG, "GRDCHG", sappiMonthID);

          //descend the months by assigning zero to the last month
          //to calculate the Q_YTD for the previous month : zero the current month
          for (int i = 0; i < MAX_ROWS; i++)  //by row
          {
            arr[i, FROM_MONTH + monthNum - 1] = 0D;    
          }

          Application.DoEvents();

        } //for 

      }
      catch (Exception er)
      {
        rv = false;
        cFG.LogIt("Do Process YTD_Q KPIJob " + er.Message);
        throw new Exception(er.Message);
      }

      return rv;

    } //






    #endregion  




    #region PROCESS KPI'S--------------------------------------------------------

    private void DoBPTS1Daily()
    {

         //aportion the month value to the last (n) days

         int daysToSplit = (int)(Math.Round(monthBPTS1,2) / 24);


         string monthStartDate = "";
         string monthEndDate = "";

         double mtdValue = 0;       //month to date
         double dayValue = 0;       //result

         double downtimeValue = arr[zKPI_PLAP, FROM_MONTH];  //downtime value for the day

         double monthBudgetValue = monthBPTS1;

         cSQL.OpenDB("ZAMDW");

         int dayOfMonth = cal.GetDayOfMonth(sDailyStartDate);


         cal.GetMonthMSSQL(iSappiYear,currMonth,ref monthStartDate, ref monthEndDate);


         //get mtd value of daily bpts1
         string sql = "select d.MillCode, d.FLID, d.kpicode, sum(d.KPIValue) KPIValue  from KPIDaily d"
          + ", kpidefinition kd "
          + ", DimPlant p"
          + ", DimMill m"
          + ", sappifiscalcalendar.dbo.tbl_days sd"
          + ", sappifiscalcalendar.dbo.tbl_months sm"
          + ", sappifiscalcalendar.dbo.tbl_month_names smn"
          + ", sappifiscalcalendar.dbo.tbl_years sy"
          + " where d.KPICODE = kd.KpiCode"
          + " and   d.flid = p.flid"
          + " and   d.millcode = m.SiteAbbrev"
          + " and   m.millkey = p.millkey"
          + " and  d.sappidayid = sd.day_id"
          + " and   sd.month_id = sm.month_id"
          + " and sm.month_name_id = smn.month_name_id"
          + " and   sm.year_id = sy.year_id"
          + " and sy.year = " + iSappiYear
          + " and   sm.month_of_year = " + currMonth
          + " and d.MILLCODE = '" + sSite + "'"
          + " and   d.FLID = " + sAreaID
          + " and   d.kpicode  = 'BPTS1'"
          + " and sd.day_start < '" + sDailyStartDate + "'"
          + " group by d.MillCode, d.FLID, d.KpiCode"
          + "";

         DataTable dt = cSQL.GetDataTable(sql);
         foreach (DataRow item in dt.Rows)
         {
            mtdValue = cmath.getd(item["KPIValue"].ToString());
         }

         int daysLeft = (numOfDaysMonth - dayOfMonth+1);

         if (mtdValue < monthBudgetValue)
         {
            if (daysLeft > 1)
            {
               dayValue = downtimeValue;
               if (downtimeValue == 0)
               {
                  if (daysLeft <= daysToSplit)
                  {
                     double remainingValue = (monthBudgetValue - mtdValue);
                     if (remainingValue > 24)
                        dayValue = 24;
                     else
                        dayValue = remainingValue;
                  }
               }
                  
            }
            else
            {
               dayValue = (monthBudgetValue - mtdValue);
            }
         }

         
         
         arr[zBP_BPTS1, FROM_MONTH] = dayValue;


         SaveData(zBP_BPTS1, "BPTS1");
         SumYTD_QTR(zBP_BPTS1, ROUND5);
         SaveQuarter(zBP_BPTS1, "BPTS1");

         cSQL.CloseDB();


      }





      private void DoBPCSDaily()
      {

         //monthBPCS contains the month value / calendar hours
         int daysToSplit = (int)(Math.Round(monthBPCS, 2) / 24);

         string monthStartDate = "";
         string monthEndDate = "";

         double mtdValue = 0;       //month to date
         double dayValue = 0;       //result

         double downtimeValue = arr[zKPI_CMDT, FROM_MONTH];  //downtime value for the day

         double monthBudgetValue = monthBPCS;

         cSQL.OpenDB("ZAMDW");

         int dayOfMonth = cal.GetDayOfMonth(sDailyStartDate);


         cal.GetMonthMSSQL(iSappiYear, currMonth, ref monthStartDate, ref monthEndDate);


         //get mtd value of daily bpts1
         string sql = "select d.MillCode, d.FLID, d.kpicode, sum(d.KPIValue) KPIValue  from KPIDaily d"
          + ", kpidefinition kd "
          + ", DimPlant p"
          + ", DimMill m"
          + ", sappifiscalcalendar.dbo.tbl_days sd"
          + ", sappifiscalcalendar.dbo.tbl_months sm"
          + ", sappifiscalcalendar.dbo.tbl_month_names smn"
          + ", sappifiscalcalendar.dbo.tbl_years sy"
          + " where d.KPICODE = kd.KpiCode"
          + " and   d.flid = p.flid"
          + " and   d.millcode = m.SiteAbbrev"
          + " and   m.millkey = p.millkey"
          + " and  d.sappidayid = sd.day_id"
          + " and   sd.month_id = sm.month_id"
          + " and sm.month_name_id = smn.month_name_id"
          + " and   sm.year_id = sy.year_id"
          + " and sy.year = " + iSappiYear
          + " and   sm.month_of_year = " + currMonth
          + " and d.MILLCODE = '" + sSite + "'"
          + " and   d.FLID = " + sAreaID
          + " and   d.kpicode  = 'BPCS'"
          + " and sd.day_start < '" + sDailyStartDate + "'"
          + " group by d.MillCode, d.FLID, d.KpiCode"
          + "";

         DataTable dt = cSQL.GetDataTable(sql);
         foreach (DataRow item in dt.Rows)
         {
            mtdValue = cmath.getd(item["KPIValue"].ToString());
         }

         int daysLeft = (numOfDaysMonth - dayOfMonth + 1);

         if (mtdValue < monthBudgetValue)
         {
            if (daysLeft > 1)
            {
               dayValue = downtimeValue;
               if (downtimeValue == 0)
               {
                  if (daysLeft <= daysToSplit)
                  {
                     double remainingValue = (monthBudgetValue - mtdValue);
                     if (remainingValue > 24)
                        dayValue = 24;
                     else
                        dayValue = remainingValue;
                  }
               }

            }
            else
            {
               dayValue = (monthBudgetValue - mtdValue);
            }
         }



         arr[zBP_BPCS, FROM_MONTH] = dayValue;


         SaveData(zBP_BPCS, "BPCS");
         SumYTD_QTR(zBP_BPCS, ROUND5);
         SaveQuarter(zBP_BPCS, "BPCS");

         cSQL.CloseDB();


      } //



      private void DoBPTPDaily()
      {

         int daysToSplit = (int)(Math.Round(monthBPTP, 2) / 24);

         string monthStartDate = "";
         string monthEndDate = "";

         double mtdValue = 0;       //month to date
         double dayValue = 0;       //result

         double downtimeValue = arr[zKPI_PTS, FROM_MONTH];  //downtime value for the day

         double monthBudgetValue = monthBPTP;

         cSQL.OpenDB("ZAMDW");

         int dayOfMonth = cal.GetDayOfMonth(sDailyStartDate);


         cal.GetMonthMSSQL(iSappiYear, currMonth, ref monthStartDate, ref monthEndDate);


         //get mtd value of daily bpts1
         string sql = "select d.MillCode, d.FLID, d.kpicode, sum(d.KPIValue) KPIValue  from KPIDaily d"
          + ", kpidefinition kd "
          + ", DimPlant p"
          + ", DimMill m"
          + ", sappifiscalcalendar.dbo.tbl_days sd"
          + ", sappifiscalcalendar.dbo.tbl_months sm"
          + ", sappifiscalcalendar.dbo.tbl_month_names smn"
          + ", sappifiscalcalendar.dbo.tbl_years sy"
          + " where d.KPICODE = kd.KpiCode"
          + " and   d.flid = p.flid"
          + " and   d.millcode = m.SiteAbbrev"
          + " and   m.millkey = p.millkey"
          + " and  d.sappidayid = sd.day_id"
          + " and   sd.month_id = sm.month_id"
          + " and sm.month_name_id = smn.month_name_id"
          + " and   sm.year_id = sy.year_id"
          + " and sy.year = " + iSappiYear
          + " and   sm.month_of_year = " + currMonth
          + " and d.MILLCODE = '" + sSite + "'"
          + " and   d.FLID = " + sAreaID
          + " and   d.kpicode  = 'BPTP'"
          + " and sd.day_start < '" + sDailyStartDate + "'"
          + " group by d.MillCode, d.FLID, d.KpiCode"
          + "";

         DataTable dt = cSQL.GetDataTable(sql);
         foreach (DataRow item in dt.Rows)
         {
            mtdValue = cmath.getd(item["KPIValue"].ToString());
         }

         int daysLeft = (numOfDaysMonth - dayOfMonth + 1);

         if (mtdValue < monthBudgetValue)
         {
            if (daysLeft > 1)
            {
               dayValue = downtimeValue;
               if (downtimeValue == 0)
               {
                  if (daysLeft <= daysToSplit)
                  {
                     double remainingValue = (monthBudgetValue - mtdValue);
                     if (remainingValue > 24)
                        dayValue = 24;
                     else
                        dayValue = remainingValue;
                  }
               }

            }
            else
            {
               dayValue = (monthBudgetValue - mtdValue);
            }
         }


         arr[zBP_BPTP, FROM_MONTH] = dayValue;


         SaveData(zBP_BPTP, "BPTP");
         SumYTD_QTR(zBP_BPTP, ROUND5);
         SaveQuarter(zBP_BPTP, "BPTP");

         cSQL.CloseDB();


      }


      private void DoBPOSDaily()
      {

         int daysToSplit = (int)(Math.Round(monthBPOS, 2) / 24);

         string monthStartDate = "";
         string monthEndDate = "";

         double mtdValue = 0;       //month to date
         double dayValue = 0;       //result

         double downtimeValue = arr[zKPI_PLO, FROM_MONTH];  //downtime value for the day

         double monthBudgetValue = monthBPOS;


         cSQL.OpenDB("ZAMDW");

         int dayOfMonth = cal.GetDayOfMonth(sDailyStartDate);


         cal.GetMonthMSSQL(iSappiYear, currMonth, ref monthStartDate, ref monthEndDate);


         //get mtd value of daily bpts1
         string sql = "select d.MillCode, d.FLID, d.kpicode, sum(d.KPIValue) KPIValue  from KPIDaily d"
          + ", kpidefinition kd "
          + ", DimPlant p"
          + ", DimMill m"
          + ", sappifiscalcalendar.dbo.tbl_days sd"
          + ", sappifiscalcalendar.dbo.tbl_months sm"
          + ", sappifiscalcalendar.dbo.tbl_month_names smn"
          + ", sappifiscalcalendar.dbo.tbl_years sy"
          + " where d.KPICODE = kd.KpiCode"
          + " and   d.flid = p.flid"
          + " and   d.millcode = m.SiteAbbrev"
          + " and   m.millkey = p.millkey"
          + " and  d.sappidayid = sd.day_id"
          + " and   sd.month_id = sm.month_id"
          + " and sm.month_name_id = smn.month_name_id"
          + " and   sm.year_id = sy.year_id"
          + " and sy.year = " + iSappiYear
          + " and   sm.month_of_year = " + currMonth
          + " and d.MILLCODE = '" + sSite + "'"
          + " and   d.FLID = " + sAreaID
          + " and   d.kpicode  = 'BPOS'"
          + " and sd.day_start < '" + sDailyStartDate + "'"
          + " group by d.MillCode, d.FLID, d.KpiCode"
          + "";

         DataTable dt = cSQL.GetDataTable(sql);
         foreach (DataRow item in dt.Rows)
         {
            mtdValue = cmath.getd(item["KPIValue"].ToString());
         }

         int daysLeft = (numOfDaysMonth - dayOfMonth + 1);

         if (mtdValue < monthBudgetValue)
         {
            if (daysLeft > 1)
            {
               dayValue = downtimeValue;
               if (downtimeValue == 0)
               {
                  if (daysLeft <= daysToSplit)
                  {
                     double remainingValue = (monthBudgetValue - mtdValue);
                     if (remainingValue > 24)
                        dayValue = 24;
                     else
                        dayValue = remainingValue;
                  }
               }

            }
            else
            {
               dayValue = (monthBudgetValue - mtdValue);
            }
         }


         arr[zBP_BPOS, FROM_MONTH] = dayValue;


         SaveData(zBP_BPOS, "BPOS");
         SumYTD_QTR(zBP_BPOS, ROUND5);
         SaveQuarter(zBP_BPOS, "BPOS");

         cSQL.CloseDB();


      }







      private void DoPOR()
    {
      //POR = (CT - (BPCS + BPOS + BPTS1 + BPTP) / CT) * 100
      // if running daily or weekly
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zSappiHours, col];
        t2 = arr[zBP_BPCS, col];
        t3 = arr[zBP_BPOS, col];
        t4 = arr[zBP_BPTS1, col];
        t5 = arr[zBP_BPTP, col];
        //t7 = ((t1 - (t2 + t3 + t4 + t5)) / t1) * 100;
        t7 = cmath.div((t1 - (t2 + t3 + t4 + t5)) , t1) * 100;
        arr[zBP_POR, col] = Math.Round(t7, ROUND5);
      }

      SaveData(zBP_POR, "POR");

    }//


    private void DoPOR_QCalc()
    {
      //2023-09-07

      for (col = FROM_Q1; col <= TO_Q4; col++)
      {
        t1 = arr[zSappiHours, col];
        t2 = arr[zBP_BPOH, col];
        t7 = cmath.div(t2, t1) * 100;
        arr[zBP_POR, col] = Math.Round(t7, ROUND5);
      }

      //ytd 
      t1 = arr[zSappiHours, FROM_YTD];
      t2 = arr[zBP_BPOH, FROM_YTD];
      t7 = cmath.div(t2, t1) * 100;
      arr[zBP_POR, FROM_YTD] = Math.Round(t7, ROUND5);

    }//


    private void DoBNPP_QCalc()
    {
      //2023-09-07

      for (col = FROM_Q1; col <= TO_Q4; col++)
      {
        t1 = arr[zBP_BPNPT, col];
        t2 = arr[zBP_BPOH, col];
        t7 = cmath.div(t1, t2) * 100;
        arr[zBP_BNPP, col] = Math.Round(t7, ROUND5);
      }

      //ytd
      t1 = arr[zBP_BPNPT, FROM_YTD];
      t2 = arr[zBP_BPOH, FROM_YTD];
      t7 = cmath.div(t1, t2) * 100;
      arr[zBP_BNPP, FROM_YTD] = Math.Round(t7, ROUND5);

    }//


    private void DoBGP()
    {

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        if (boRunMonth)
          t5 = arr[zBP_BGP, col];
        else
        {
          t1 = arr[zBP_WAGRR, col];
          t2 = arr[zBP_POR, col];
          t3 = arr[zBP_BNPP, col];
          t4 = arr[zSappiHours, col];
          t5 = (t1 * t4 * t2 * t3) / 10000;
        }

        arr[zBP_BGP, col] = Math.Round(t5, ROUND5);

      }

      SaveData(zBP_BGP, "BGP");


    }//


    private void DoABGP()
    {

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        if (boRunMonth)
          t5 = arr[zKPI_ABGP, col];
        else
        {
          t5 = arr[zBP_BGP, col];      
        }

        arr[zKPI_ABGP, col] = Math.Round(t5, ROUND5);

      }

      SaveData(zKPI_ABGP, "ABGP");


    }//


    private void DoWAGRR()
    {
     
      int ndx = 1;  //loop months

      if (boRunMonth)
      {
              for (col = FROM_MONTH; col < MAX_COLS; col++)
              {
                t1 = arr[zBP_BGP, col];
                t2 = arr[zSappiHours, col];
                t3 = arr[zBP_POR, col];
                t4 = arr[zBP_BNPP, col];
                t2 = (t2 * t3 * t4) / 10000;
                t1 = Math.Round(cmath.div(t1, t2), ROUND5);

                arr[zBP_WAGRR, col] = t1;

                //if 0 then use BMS
                if ((t1 == 0) && (ndx <= currMonth))
                {     //t1 here will be zero when bgp or por is zero, then use BMS, but dont go beyond the current month
                  if (boRunMonth)
                  {
                    arr[zBP_WAGRR, col] = arr[zBP_BMS, col];
                  }
                  else
                  {
                    if (ndx == 0)     //use the first month for daily
                    {
                      t7 = arr[zBP_BMS, FROM_MONTH];     //debug
                      arr[zBP_WAGRR, FROM_MONTH] = arr[zBP_BMS, FROM_MONTH];
                    }
                  }
                }

                ndx++;

              }   //
      }     //month


      //2021-02-23  changes to the daily calculcation
      if (boRunMonth == false)
      {     
            
        for (col = FROM_MONTH; col < MAX_COLS; col++)
            {
              t1 = arr[zBP_BMS, col];
              if (monthBGP == 0)
                arr[zBP_WAGRR, col] = t1;
              else
                arr[zBP_WAGRR, col] = monthWAGRR;
            }

      }     //daily


      SaveData(zBP_WAGRR, "WAGRR");

      DoWAGRR_QCalc();
      
      SaveQuarter(zBP_WAGRR, "WAGRR");

    }     //

    private void DoWAGRR_QCalc()
    {
      //2023-09-07
      int monthNdx = 1;

      for (col = FROM_Q1; col <= TO_Q4; col++)
      {
        t1 = arr[zBP_BGP, col];
        t2 = arr[zSappiHours, col];
        t3 = arr[zBP_POR, col];
        t4 = arr[zBP_BNPP, col];
        t2 = (t2 * t3 * t4) / 10000;
        t1 = Math.Round(cmath.div(t1, t2), ROUND5);

        arr[zBP_WAGRR, col] = t1;

        if ((t1 == 0) && (monthNdx <= quarterNum))
        {     //t1 here will be zero when bgp or por is zero, then use BMS        
            arr[zBP_WAGRR, col] = arr[zBP_BMS, col];
        }
        monthNdx++;
      }


      //ytd
      t1 = arr[zBP_BGP, FROM_YTD];
      t2 = arr[zSappiHours, FROM_YTD];
      t3 = arr[zBP_POR, FROM_YTD];
      t4 = arr[zBP_BNPP, FROM_YTD];
      t2 = (t2 * t3 * t4) / 10000;
      t1 = Math.Round(cmath.div(t1, t2), ROUND5);
      arr[zBP_WAGRR, FROM_YTD] = t1;
      if (t1 == 0)
      {     //t1 here will be zero when bgp or por is zero, then use BMS        
        arr[zBP_WAGRR, FROM_YTD] = arr[zBP_BMS, FROM_YTD];
      }

    }//



    private void DoNSPPM()
    {

         //Showarr(zBP_NSPPM);
        

         for (col = FROM_MONTH; col < MAX_COLS; col++)
         {
            t1 = arr[zBP_BGP, col];
            t4 = arr[zBP_BPSY, col];
            t7 = Math.Round((t1 * t4) / 100, ROUND5);
            arr[zBP_NSPPM, col] = t7;
         }


         //2025-02-19  
         //if ((sSite == "SAI" && (boRunMonth == true)) || sSite != "SAI")
         //{
         //  for (col = FROM_MONTH; col < MAX_COLS; col++)
         //  {
         //    t1 = arr[zBP_BGP, col];
         //    t4 = arr[zBP_BPSY, col];
         //    t7 = Math.Round((t1 * t4) / 100, ROUND5);
         //    arr[zBP_NSPPM, col] = t7;
         //  }
         //}


         SaveData(zBP_NSPPM, "NSPPM");

         SumYTD_QTR(zBP_NSPPM, ROUND5);

         SaveQuarter(zBP_NSPPM, "NSPPM");

    }//


    private void DoNRFTP()
    {

      if (boRunMonth)
      {
        for (col = FROM_MONTH; col < MAX_COLS; col++)
        {
          t1 = arr[zBP_BGP, col];
          t2 = arr[zBP_BQR, col];
          t3 = arr[zBP_BPRFT2, col];        //2020-12-14 prev = zBP_BPRFT
          t7 = Math.Round((t1 * t2 * t3) / 10000, ROUND5);
          arr[zBP_NRFTP, col] = t7;
        }
      }   //month
      else
      {
        for (col = FROM_MONTH; col < MAX_COLS; col++)
        {     //2021-02-23
          t1 = arr[zBP_BGP, col];
          t2 = arr[zBP_BQR, col];
          t7 = Math.Round((t1 * t2) / 100, ROUND5);
          arr[zBP_NRFTP, col] = t7;
        }
      }   //day / week

      SaveData(zBP_NRFTP, "NRFTP");

      SumYTD_QTR(zBP_NRFTP, ROUND5);
        

      SaveQuarter(zBP_NRFTP, "NRFTP");

    }//


    private void DoBPOME()
    {

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zBP_BNPP,col];
        t2 = arr[zBP_BQR,col];
        t1 = Math.Round((t1 * t2) / 100, ROUND5);
        arr[zBP_BPOME,col] = t1;
      }

      SaveData(zBP_BPOME, "BPOME");

      //CalcByHrs_YTD_QTR(zBP_BPOME, ROUND5);
      DoBPOME_QCalc();


      SaveQuarter(zBP_BPOME, "BPOME");

    }//

    private void DoBPOME_QCalc()
    {
      for (col = FROM_Q1; col <= TO_Q4; col++)
      {
        t1 = arr[zBP_BNPP, col];
        t2 = arr[zBP_BQR, col];
        t7 = Math.Round((t1 * t2) / 100, ROUND5);
        arr[zBP_BPOME, col] = t7;
      }

      //ytd
      t1 = arr[zBP_BNPP, FROM_YTD];
      t2 = arr[zBP_BQR, FROM_YTD];
      t7 = Math.Round((t1 * t2) / 100, ROUND5);
      arr[zBP_BPOME, FROM_YTD] = t7;

    } //


    private void DoBOE()
    {
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zBP_BPOME,col];
        t2 = arr[zBP_BPMS,col];
        t1 = Math.Round((t1 * t2) / 100, ROUND5);
        arr[zBP_BOE, col] = t1;
      }

      SaveData(zBP_BOE, "BOE");


      //CalcByHrs_YTD_QTR(zBP_BOE, ROUND5);
      DoBOE_QCalc();

      SaveQuarter(zBP_BOE, "BOE");
      
    }//


    private void DoBOE_QCalc(){

      //quarterly
      for (col = FROM_Q1; col <= TO_Q4; col++)
      {
        t1 = arr[zBP_BPOME, col];
        t2 = arr[zBP_BPMS, col];
        t1 = Math.Round((t1 * t2) / 100, ROUND5);
        arr[zBP_BOE, col] = t1;
      }

      //ytd
      t1 = arr[zBP_BPOME, FROM_YTD];
      t2 = arr[zBP_BPMS, FROM_YTD];
      t1 = Math.Round((t1 * t2) / 100, ROUND5);
      arr[zBP_BOE, FROM_YTD] = t1;

    }


    private void DoTEEP()
    {
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zBP_POR,col];
        t2 = arr[zBP_BOE,col];
        t3 = arr[zBP_AMBQR,col];
        t1 = Math.Round((t1 * t2 * t3) / 10000, ROUND5);
        arr[zBP_TEEP,col] = t1;
      }

      SaveData(zBP_TEEP, "TEEP");

      //CalcByHrs_YTD_QTR(zBP_TEEP, ROUND5);
      DoTEEP_QCalc();

      SaveQuarter(zBP_TEEP, "TEEP");

    }//

    private void DoTEEP_QCalc(){

      //quarterly
      for (col = FROM_Q1; col <= TO_Q4; col++)
      {
        t1 = arr[zBP_POR, col];
        t2 = arr[zBP_BOE, col];
        t3 = arr[zBP_AMBQR, col];
        t1 = Math.Round((t1 * t2 * t3) / 10000, ROUND5);
        arr[zBP_TEEP, col] = t1;
      }

      //ytd
      t1 = arr[zBP_POR, FROM_YTD];
      t2 = arr[zBP_BOE, FROM_YTD];
      t3 = arr[zBP_AMBQR, FROM_YTD];
      t1 = Math.Round((t1 * t2 * t3) / 10000, ROUND5);
      arr[zBP_TEEP, FROM_YTD] = t1;
    }


    private void DoBPOH()
    {
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zSappiHours,col];
        t2 = arr[zBP_POR,col];
        t1 = Math.Round((t1 * t2) / 100, ROUND5);
        arr[zBP_BPOH,col] = t1;
      }

      SaveData(zBP_BPOH, "BPOH");

      SumYTD_QTR(zBP_BPOH, ROUND5);
       
      SaveQuarter(zBP_BPOH, "BPOH");

    }//


    private void DoBPNPT()
    {
      //net productive time hrs
      //BPOH x BNPP / 100

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zBP_BPOH, col];
        t2 = arr[zBP_BNPP, col];
        t1 = Math.Round((t1 * t2) / 100, ROUND5);
        arr[zBP_BPNPT, col] = t1;
      }

      SaveData(zBP_BPNPT, "BPNPT");

  
      SumYTD_QTR(zBP_BPNPT, ROUND5);
        

      SaveQuarter(zBP_BPNPT, "BPNPT");

    }//


    private void DoNSPD()
    {
      //BP NSP/ day(during Operating Days)
      //NSPPM / (CT  / 24 ) / (POR / 100)

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zBP_NSPPM, col];
        t2 = arr[zSappiHours, col] / 24;
        t3 = arr[zBP_POR, col] / 100;

        t7 = 0;
        if ((t2 > 0) && (t3 > 0))
          t7 = (t1 / t2 / t3);

        arr[zBP_NSPD, col] = t7;
      }

      SaveData(zBP_NSPD, "NSPD");

        //quarterly
        for (col = FROM_Q1; col <= TO_Q4; col++)
        {
            t1 = arr[zBP_NSPPM, col];
            t2 = arr[zSappiHours, col] / 24;
            t3 = arr[zBP_POR, col] / 100;

            t7 = 0;
            if ((t2 > 0) && (t3 > 0))
              t7 = (t1 / t2 / t3);

            arr[zBP_NSPD, col] = t7;
      }

      for (col = FROM_YTD; col <= FROM_YTD; col++)
        {
          t1 = arr[zBP_NSPPM, col];
          t2 = arr[zSappiHours, col] / 24 ;
          t3 = arr[zBP_POR, col] / 100;

          t7 = 0;
          if ((t2 > 0) && (t3 > 0))
            t7 = (t1 / t2 / t3);

          arr[zBP_NSPD, col] = t7;
        }


      SaveQuarter(zBP_NSPD, "NSPD");

    }//


    private void DoBPTS()
    {
     

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zSappiHours, col];
        t2 = arr[zBP_BPCS, col];
        t3 = arr[zBP_BPOS, col];
        t4 = (arr[zBP_POR, col] * arr[zSappiHours, col]) / 100;
        t1 = (t1 - t2 - t3 - t4);
        arr[zBP_BPTS, col] = t1;
      }

      SaveData(zBP_BPTS, "BPTS");


      SumYTD_QTR(zBP_BPTS, ROUND5);
        

      SaveQuarter(zBP_BPTS, "BPTS");

    }//


    private void DoBPTS_Daily()
    {
      double rv = 0;
      //F= monthly data from xls
      //G=daily data from xls
      //=IF(((F12+F13+F14)-(F28+F29+F30))>0,(IF((F12+F13+F14)-(F30+F29+F28)<24,(IF(((F12+F13+F14)-(F28+F29+F30))>(G28+G29+G30),(G28+G29+G30),(F12+F13+F14-(F28+F29+F30)))),IF((G28+G29+G30)<24,(G28+G29+G30),24))),0)
      //=if(((zBP_BPTS + zBP_BPCS + zBP_BPOS) - (monthCMDT + monthPTS + monthPLO)) > 0, (if((zBP_BPTS + zBP_BPCS + zBP_BPOS) - (monthPLO + monthPTS + monthCMDT) < 24, (if(((zBP_BPTS + zBP_BPCS + zBP_BPOS) - (monthCMDT + monthPTS + monthPLO)) > (monthCMDT + monthPTS + monthPLO), (monthCMDT + monthPTS + monthPLO), (zBP_BPTS + zBP_BPCS + zBP_BPOS - (monthCMDT + monthPTS + monthPLO)))), if((monthCMDT + monthPTS + monthPLO) < 24, (monthCMDT + monthPTS + monthPLO), 24))), 0)
      //if (((BPTS + BPCS + BPOS) - (monthCMDT + monthPTS + monthPLO)) > 0, (if ((BPTS + BPCS + BPOS) - (monthPLO + monthPTS + monthCMDT) < 24, (if (((BPTS + BPCS + BPOS) - (monthCMDT + monthPTS + monthPLO)) > (monthCMDT + monthPTS + monthPLO), (monthCMDT + monthPTS + monthPLO), (zBP_BPTS + zBP_BPCS + zBP_BPOS - (monthCMDT + monthPTS + monthPLO)))), if ((monthCMDT + monthPTS + monthPLO) < 24, (monthCMDT + monthPTS + monthPLO), 24))), 0)
      t2 = arr[zKPI_CAL24,FROM_MONTH];      //calender time

      //the monthly values
      double A = monthBPTS + monthBPCS + monthBPOS;
      //monthly values
      double B = monthCMDT + monthPTS + monthPLO;
      //daily values
      double C = arr[zKPI_CMDT, FROM_MONTH] + arr[zKPI_PTS, FROM_MONTH] + arr[zKPI_PLO, FROM_MONTH];

      
      if ((A - B) > 0)
        if ((A - B) < t2)
          if ((A - B) > C)
            rv = C;
          else
          {
            rv = A - B;
          }
        else
          if (C < t2)
          rv = C;
        else
          rv = t2;
      else
        rv = 0;

      arr[zBP_BPTS, FROM_MONTH] = rv;
      SaveData(zBP_BPTS,"BPTS");

    }//


    private void DoRow20()
    {
      //2021-02-24  more changes
      //= IFERROR(  G32  / (G4 * G15 * G20 ) * 10000,   G172 + (G32  /   G42  )  )
      //  IFERROR(  ABGP / (CT * POR * BNPP) * 10000,   BMS +  ( ABGP / KPI0026) )

      int monthNdx = 1;

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_ABGP,col];
        t2 = arr[zSappiHours,col];
        t3 = arr[zBP_POR,col];
        t4 = arr[zBP_BNPP,col];
        t5 = arr[zBP_BMS, col];
        t6 = arr[zKPI_26, col];
        t7 = (cmath.div(t1, (t2 * t3 * t4)) * 10000);
        if ((t7 == 0) && (monthNdx <= currMonth))
        {
          t7 = (t5 + cmath.div(t1, t6) );
        }
        arr[zKPI_20, col] = Math.Round(t7, ROUND5);
        monthNdx++;
      }

      SaveData(zKPI_20, "KPI0020");

      //CalcByHrs_YTD_QTR_NOZero(zKPI_20, ROUND5);
      DoRow20_QCalc();

      SaveQuarter(zKPI_20, "KPI0020");

    }//

    private void DoRow20_QCalc(){

      int monthNdx = 1;
      //quarterly
      for (col = FROM_Q1; col <= TO_Q4; col++)
      {
        t1 = arr[zKPI_ABGP, col];
        t2 = arr[zSappiHours, col];
        t3 = arr[zBP_POR, col];
        t4 = arr[zBP_BNPP, col];
        t5 = arr[zBP_BMS, col];
        t6 = arr[zKPI_26, col];
        t7 = (cmath.div(t1, (t2 * t3 * t4)) * 10000);
        if ((t7 == 0) && (monthNdx <= quarterNum))
        {
          t7 = (t5 + cmath.div(t1, t6));
        }
        arr[zKPI_20, col] = Math.Round(t7, ROUND5);
        monthNdx++;
      }

      //ytd
      for (col = FROM_YTD; col <= FROM_YTD; col++)
      {
        t1 = arr[zKPI_ABGP, col];
        t2 = arr[zSappiHours, col];
        t3 = arr[zBP_POR, col];
        t4 = arr[zBP_BNPP, col];
        t5 = arr[zBP_BMS, col];
        t6 = arr[zKPI_26, col];
        t7 = (cmath.div(t1, (t2 * t3 * t4)) * 10000);
        if (t7 == 0)
        {
          t7 = (t5 + cmath.div(t1, t6));
        }
        arr[zKPI_20, col] = Math.Round(t7, ROUND5);
      }
    }


    private void DoRow21()
    {
      
      int monthNdx = 1;

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_ABGP, col];
        t2 = arr[zBP_BGP, col];
        t3 = arr[zKPI_20, col];
        t4 = arr[zBP_WAGRR, col];
        t7 = cmath.div(t1, t2) * 100;
        if ((t7 == 0) && (monthNdx <= currMonth))
        {
          t7 = cmath.div(t3, t4) * 100;
        }
        arr[zKPI_21, col] = Math.Round(t7, ROUND5);
        monthNdx++;
      }

      SaveData(zKPI_21, "KPI0021");

      //CalcByHrs_YTD_QTR(zKPI_21, ROUND5);
      DoRow21_QCalc();

      SaveQuarter(zKPI_21, "KPI0021");

    }//

    private void DoRow21_QCalc(){

      int monthNdx = 1;
      //quarterly
      for (col = FROM_Q1; col <= TO_Q4; col++)
      {
        t1 = arr[zKPI_ABGP, col];
        t2 = arr[zBP_BGP, col];
        t3 = arr[zKPI_20, col];
        t4 = arr[zBP_WAGRR, col];
        t7 = cmath.div(t1, t2) * 100;
        if ((t7 == 0) && (monthNdx <= quarterNum))
        {
          t7 = cmath.div(t3, t4) * 100;
        }
        arr[zKPI_21, col] = Math.Round(t7, ROUND5);
        monthNdx++;
      }

      for (col = FROM_YTD; col <= FROM_YTD; col++)
      {
        t1 = arr[zKPI_ABGP, col];
        t2 = arr[zBP_BGP, col];
        t3 = arr[zKPI_20, col];
        t4 = arr[zBP_WAGRR, col];
        t7 = cmath.div(t1, t2) * 100;
        if (t7 == 0)
        {
          t7 = cmath.div(t3, t4) * 100;
        }
        arr[zKPI_21, col] = Math.Round(t7, ROUND5);
      }

    }

    private void DoRow22()
    {
      //monthly
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        arr[zKPI_22,col] = arr[zKPI_21,col];
      }

      SaveData(zKPI_22, "KPI0022");

      //CalcByHrs_YTD_QTR(zKPI_22, ROUND5); 2023-09-07
      DoRow22_QCalc();

      SaveQuarter(zKPI_22, "KPI0022");

    }//

    private void DoRow22_QCalc(){

      //quarterly
      for (col = FROM_Q1; col <= TO_Q4; col++)
      {
        arr[zKPI_22, col] = arr[zKPI_21, col];
      }

      //ytd
      arr[zKPI_22, FROM_YTD] = arr[zKPI_21, FROM_YTD];

    }


    private void DoRow26()
    {
      //(CTT - CMDT - PTS - PLO - PLAP)

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_CAL24,col];
        t2 = arr[zKPI_CMDT,col];
        t3 = arr[zKPI_PTS,col];
        t4 = arr[zKPI_PLO,col];
        t5 = arr[zKPI_PLAP, col];
        t7 = (t1 - t2 - t3 - t4 - t5);
        arr[zKPI_26, col] = Math.Round(t7, ROUND1);
      }

      

      SaveData(zKPI_26, "KPI0026");

      SumYTD_QTR(zKPI_26, ROUND5);

      SaveQuarter(zKPI_26, "KPI0026");

    }//


    private void DoRow28()
    {
      //Net Productive time
      //monthly
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_26, col];
        t2 = arr[zKPI_PLMT, col];
        t3 = arr[zKPI_PLPS, col];
        t4 = arr[zKPI_PIBP, col];
        t5 = arr[zKPI_UPLE, col];
        t6 = arr[zKPI_PROC, col];
        t7 = (t1 - t2 - t3 - t4 - t5 - t6);
        arr[zKPI_28, col] = Math.Round(t7, ROUND1);
      }

      SaveData(zKPI_28, "KPI0028");

   
      SumYTD_QTR(zKPI_28, ROUND5);
        

      SaveQuarter(zKPI_28, "KPI0028");


    }//



    private void DoRow29()
    {
      //monthly

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_28, col];
        t2 = arr[zKPI_26, col];
        arr[zKPI_29, col] = Math.Round((cmath.div(t1, t2) * 100), ROUND5);
      }

      SaveData(zKPI_29, "KPI0029");

      
      DoRow29_Qcalc();

      SaveQuarter(zKPI_29, "KPI0029");

    }//


    private void DoRow29_Qcalc()
    {

        //quarterly
        for (col = FROM_Q1; col <= TO_Q4; col++)
        {
          t1 = arr[zKPI_28, col];
          t2 = arr[zKPI_26, col];
          arr[zKPI_29, col] = Math.Round((cmath.div(t1, t2) * 100), ROUND5);
        }

        //ytd
        t1 = arr[zKPI_28, FROM_YTD];
        t2 = arr[zKPI_26, FROM_YTD];
        arr[zKPI_29, FROM_YTD] = Math.Round((cmath.div(t1, t2) * 100), ROUND5);

    }//


    private void DoRow33()
    {
      //
      //monthly
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_GP, col];
        t2 = arr[zKPI_DKL, col];
        t3 = Math.Round((t1 - t2), ROUND5);
        arr[zKPI_33, col] = t3;
      }

      SaveData(zKPI_33, "KPI0033");

      SumYTD_QTR(zKPI_33, ROUND5);
       
      SaveQuarter(zKPI_33, "KPI0033");


    }//


    private void DoRow35()
    {
      //
      //monthly
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_33, col];
        t2 = arr[zKPI_TBS, col];
        arr[zKPI_35, col] = Math.Round((t1 - t2), ROUND5);
      }

      SaveData(zKPI_35, "KPI0035");

     
      SumYTD_QTR(zKPI_35, ROUND5);
       

      SaveQuarter(zKPI_35, "KPI0035");


    }//


    private void DoRow36()
    {
      //
      //monthly
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_GP, col];
        t2 = arr[zKPI_33, col];
        arr[zKPI_36, col] = Math.Round((cmath.div(t1 - t2, t1) * 100), ROUND5);
      }

      SaveData(zKPI_36, "KPI0036");

      
      //CalcByHrs_YTD_QTR(zKPI_36, ROUND5);
      DoRow36_QCalc();

      SaveQuarter(zKPI_36, "KPI0036");


    }//

    private void DoRow36_QCalc(){

      //quarterly
      for (col = FROM_Q1; col <= TO_Q4; col++)
      {
        t1 = arr[zKPI_GP, col];
        t2 = arr[zKPI_33, col];
        arr[zKPI_36, col] = Math.Round((cmath.div(t1 - t2, t1) * 100), ROUND5);
      }

      //ytd
      t1 = arr[zKPI_GP, FROM_YTD];
      t2 = arr[zKPI_33, FROM_YTD];
      arr[zKPI_36, FROM_YTD] = Math.Round((cmath.div(t1 - t2, t1) * 100), ROUND5);
    }


    private void DoRow37()
    {
      //
      //monthly
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_33, col];
        t2 = arr[zKPI_35, col];
        arr[zKPI_37, col] = Math.Round((cmath.div(t1 - t2, t1) * 100), ROUND5);
      }

      SaveData(zKPI_37, "KPI0037");


      //CalcByHrs_YTD_QTR(zKPI_37, ROUND5);
      DoRow37_QCalc();

      SaveQuarter(zKPI_37, "KPI0037");


    }//

    private void DoRow37_QCalc()
    {
      //quarterly
      for (col = FROM_Q1; col <= TO_Q4; col++)
      {
        t1 = arr[zKPI_33, col];
        t2 = arr[zKPI_35, col];
        arr[zKPI_37, col] = Math.Round((cmath.div(t1 - t2, t1) * 100), ROUND5);
      }

      //ytd
      t1 = arr[zKPI_33, FROM_YTD];
      t2 = arr[zKPI_35, FROM_YTD];
      arr[zKPI_37, FROM_YTD] = Math.Round((cmath.div(t1 - t2, t1) * 100), ROUND5);

    }

      private void DoRow38()
    {
      //
      
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_35, col];
        t2 = arr[zKPI_GP, col];
        arr[zKPI_38, col] = Math.Round((cmath.div(t1, t2) * 100), ROUND5);
      }

      SaveData(zKPI_38, "KPI0038");

      
      DoRow38_Qcalc();
       

      SaveQuarter(zKPI_38, "KPI0038");

    }//


    private void DoRow38_Qcalc()
    {
      //quarterly
      for (col = FROM_Q1; col <= TO_Q4; col++)
      {
        t1 = arr[zKPI_35, col];
        t2 = arr[zKPI_GP, col];
        arr[zKPI_38, col] = Math.Round((cmath.div(t1, t2) * 100), ROUND5);
      }

      //ytd
      t1 = arr[zKPI_35, FROM_YTD];
      t2 = arr[zKPI_GP, FROM_YTD];
      arr[zKPI_38, FROM_YTD] = Math.Round((cmath.div(t1, t2) * 100), ROUND5);


    }//


    private void DoRow40()
    {
      //
      //monthly
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_PPAG, col];
        t2 = arr[zKPI_35, col];
        arr[zKPI_40, col] = Math.Round((cmath.div(t1, t2) * 100), ROUND5);
      }

      SaveData(zKPI_40, "KPI0040");

      DoRow40_Qcalc();

      SaveQuarter(zKPI_40, "KPI0040");

    }//


    private void DoRow40_Qcalc()
    {
        //quarterly
        for (col = FROM_Q1; col <= TO_Q4; col++)
        {
          t1 = arr[zKPI_PPAG, col];
          t2 = arr[zKPI_35, col];
          arr[zKPI_40, col] = Math.Round((cmath.div(t1, t2) * 100), ROUND5);
        }

      //ytd
      for (col = FROM_YTD; col <= FROM_YTD; col++)
      {
        t1 = arr[zKPI_PPAG, col];
        t2 = arr[zKPI_35, col];
        arr[zKPI_40, col] = Math.Round((cmath.div(t1, t2) * 100), ROUND5);
      }
    }//


    private void DoADCAT()
    {
      //((CT / 24 ) * BPCBI) - (((PLAP+CMDT+PTS+PLO)/24) * BPCBI)

      //monthly
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zSappiHours, col];
        t2 = arr[zBP_BPCBI, col];
        t3 = arr[zKPI_PLAP, col];
        t4 = arr[zKPI_CMDT, col];
        t5 = arr[zKPI_PTS, col];
        t6 = arr[zKPI_PLO, col];

        t7 = ( (t1 / 24) * t2) - ( ( (t3 + t4 + t5 + t6) / 24 ) * t2);

        arr[zKPI_ADCAT, col] = Math.Round(t7, ROUND5);
      }

      SaveData(zKPI_ADCAT, "ADCAT");

     
      DoADCAT_Qcalc();

      SaveQuarter(zKPI_ADCAT, "ADCAT");

    }


    private void DoADCAT_Qcalc()
    {
      //quarterly
      for (col = FROM_Q1; col <= TO_Q4; col++)
      {
        t1 = arr[zSappiHours, col];
        t2 = arr[zBP_BPCBI, col];
        t3 = arr[zKPI_PLAP, col];
        t4 = arr[zKPI_CMDT, col];
        t5 = arr[zKPI_PTS, col];
        t6 = arr[zKPI_PLO, col];

        t7 = ((t1 / 24) * t2) - (((t3 + t4 + t5 + t6) / 24) * t2);

        arr[zKPI_ADCAT, col] = Math.Round(t7, ROUND5);
      }

      //ytd
      for (col = FROM_YTD; col <= FROM_YTD; col++)
      {
        t1 = arr[zSappiHours, col];
        t2 = arr[zBP_BPCBI, col];
        t3 = arr[zKPI_PLAP, col];
        t4 = arr[zKPI_CMDT, col];
        t5 = arr[zKPI_PTS, col];
        t6 = arr[zKPI_PLO, col];

        t7 = ((t1 / 24) * t2) - (((t3 + t4 + t5 + t6) / 24) * t2);

        arr[zKPI_ADCAT, col] = Math.Round(t7, ROUND5);
      }

    }//


    private void DoAGFY()
    {
      //(GP / AGDC)

      //monthly
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_GP, col];
        t2 = arr[zKPI_AGDC, col];

        t7 = cmath.div(t1, t2);

        arr[zKPI_AGFY, col] = Math.Round(t7, ROUND5);
      }


      SaveData(zKPI_AGFY, "AGFY");

      SumYTD_QTR(zKPI_AGFY, ROUND5);

      SaveQuarter(zKPI_AGFY, "AGFY");

    } //


    private void DoGYFC()
    {
      //(GP / (AGFY / BPGFY)) - GP
      // result must be > 0


      //monthly
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_GP, col];
        t2 = arr[zKPI_AGFY, col];
        t3 = arr[zBP_BPGFY, col];

        t7 = cmath.div(t1, cmath.div(t2,t3)) - t1;
        if (t7 < 0)
          t7 = 0;

        arr[zKPI_GFYC, col] = Math.Round(t7, ROUND5);
      }


      SaveData(zKPI_GFYC, "GFYC");

     
      DoGFYC_Qcalc();

      SaveQuarter(zKPI_GFYC, "GFYC");

    } //



    private void DoGFYC_Qcalc()
    {
      //quarterly
      for (col = FROM_Q1; col <= TO_Q4; col++)
      {
        t1 = arr[zKPI_GP, col];
        t2 = arr[zKPI_AGFY, col];
        t3 = arr[zBP_BPGFY, col];

        t7 = cmath.div(t1, cmath.div(t2, t3)) - t1;
        if (t7 < 0)
          t7 = 0;

        arr[zKPI_GFYC, col] = Math.Round(t7, ROUND5);
      }

      //ytd
      for (col = FROM_YTD; col <= FROM_YTD; col++)
      {
        t1 = arr[zKPI_GP, col];
        t2 = arr[zKPI_AGFY, col];
        t3 = arr[zBP_BPGFY, col];

        t7 = cmath.div(t1, cmath.div(t2, t3)) - t1;
        if (t7 < 0)
          t7 = 0;

        arr[zKPI_GFYC, col] = Math.Round(t7, ROUND5);
      }

    }//


    private void DoTBS()
    {
      //CDUMP * (AGFY + GFYC)   2021-11-02

      //changed to CDUMP +  GFYC    2021-11-10

      //monthly
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zBP_BPCDUMP, col];
        t2 = arr[zKPI_GFYC, col];

        t7 = t1 + t2; 

        arr[zKPI_TBS, col] = Math.Round(t7, ROUND5);
      }

      SaveData(zKPI_TBS, "TBS");

      SumYTD_QTR(zKPI_TBS, ROUND5);

      SaveQuarter(zKPI_TBS, "TBS");

    } //


    private void DoPPAG()
    {
      //(GP - TBS) * ADQPP / 100

      //monthly
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_GP, col];
        t2 = arr[zKPI_TBS, col];
        t3 = arr[zKPI_ADQPP, col];

        t7 = (t1 - t2) * t3 / 100;

        arr[zKPI_PPAG, col] = Math.Round(t7, ROUND5);
      }

      SaveData(zKPI_PPAG, "PPAG");

      SumYTD_QTR(zKPI_PPAG, ROUND5);

      SaveQuarter(zKPI_PPAG, "PPAG");

    } //


    private void DoRFT()
    {
      // = PPAG

      //monthly
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {

        t7 = arr[zKPI_PPAG, col];

        arr[zKPI_RFT, col] = Math.Round(t7, ROUND5);
      }

      SaveData(zKPI_RFT, "RFT");

      SumYTD_QTR(zKPI_RFT, ROUND5);

      SaveQuarter(zKPI_RFT, "RFT");

    } //


    private void DoRow42()
    {
      //
      //monthly
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_RFT, col];
        t2 = arr[zKPI_PPAG, col];
        arr[zKPI_42, col] = Math.Round((cmath.div(t1, t2) * 100), ROUND5);
      }

      SaveData(zKPI_42, "KPI0042");

      DoRow42_QCalc();
       
      SaveQuarter(zKPI_42, "KPI0042");

    }//


    private void DoRow42_QCalc()
    {
      for (col = FROM_Q1; col <= TO_Q4; col++)
      {
        t1 = arr[zKPI_RFT, col];
        t2 = arr[zKPI_PPAG, col];
        arr[zKPI_42, col] = Math.Round((cmath.div(t1, t2) * 100), ROUND5);
      }

      //ytd
      t1 = arr[zKPI_RFT, FROM_YTD];
      t2 = arr[zKPI_PPAG, FROM_YTD];
      arr[zKPI_42, FROM_YTD] = Math.Round((cmath.div(t1, t2) * 100), ROUND5);

    }//


    private void DoRow43()
    {

      if ( sSite == "NGX" && (sAreaID == "161" || sAreaID == "5") )
      {
        for (col = FROM_MONTH; col < MAX_COLS; col++)
        {
          t1 = arr[zKPI_ADQPP, col];

          arr[zKPI_43, col] = t1;
        }
      }
      else
      {
        for (col = FROM_MONTH; col < MAX_COLS; col++)
        {
          t1 = arr[zKPI_38, col];
          t2 = arr[zKPI_40, col];
          t3 = arr[zKPI_42, col];
          arr[zKPI_43, col] = Math.Round(cmath.div(((t1 * t2 * t3) / 100), t3), ROUND5);
        }

      }

      SaveData(zKPI_43, "KPI0043");

      DoRow43_QCalc(); 
    
      SaveQuarter(zKPI_43, "KPI0043");

    }//


    private void DoRow43_QCalc()
    {

        //quarterly
        for (col = FROM_Q1; col <= TO_Q4; col++)
        {
          t1 = arr[zKPI_38, col];
          t2 = arr[zKPI_40, col];
          t3 = arr[zKPI_42, col];
          arr[zKPI_43, col] = Math.Round(cmath.div(((t1 * t2 * t3) / 100), t3), ROUND5);
        }

        //ytd
        t1 = arr[zKPI_38, FROM_YTD];
        t2 = arr[zKPI_40, FROM_YTD];
        t3 = arr[zKPI_42, FROM_YTD];
        arr[zKPI_43, FROM_YTD] = Math.Round(cmath.div(((t1 * t2 * t3) / 100), t3), ROUND5);

    }//


    private void DoRow45()
    {
      //
      //monthly
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_46, col];
        t2 = arr[zKPI_47, col];
        t3 = cmath.div(t1, t2) * 100;
        arr[zKPI_45,col] = Math.Round(t3, ROUND5);
      }

      SaveData(zKPI_45, "KPI0045");


      //CalcByHrs_YTD_QTR(zKPI_45, ROUND5);
      DoRow45_QCalc();

      SaveQuarter(zKPI_45, "KPI0045");

    }//

    private void DoRow45_QCalc(){

      //quarterly
      for (col = FROM_Q1; col <= TO_Q4; col++)
      {
        t1 = arr[zKPI_46, col];
        t2 = arr[zKPI_47, col];
        t3 = cmath.div(t1, t2) * 100;
        arr[zKPI_45, col] = Math.Round(t3, ROUND5);
      }

      //ytd
      t1 = arr[zKPI_46, FROM_YTD];
      t2 = arr[zKPI_47, FROM_YTD];
      t3 = cmath.div(t1, t2) * 100;
      arr[zKPI_45, FROM_YTD] = Math.Round(t3, ROUND5);
    }


    private void DoRow46()
    {
      //
      //monthly
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_GP, col];
        t2 = arr[zKPI_28, col];
        arr[zKPI_46,col] = Math.Round(cmath.div(t1, t2), ROUND5);
      }

      SaveData(zKPI_46, "KPI0046");

      //CalcByHrs_YTD_QTR(zKPI_46, ROUND5);
      DoRow46_QCalc();

      SaveQuarter(zKPI_46, "KPI0046");

    }   //


    private void DoRow46_QCalc(){
      //quarterly
      for (col = FROM_Q1; col <= TO_Q4; col++)
      {
        t1 = arr[zKPI_GP, col];
        t2 = arr[zKPI_28, col];
        arr[zKPI_46, col] = Math.Round(cmath.div(t1, t2), ROUND5);
      }

      //ytd
      t1 = arr[zKPI_GP, FROM_YTD];
      t2 = arr[zKPI_28, FROM_YTD];
      arr[zKPI_46, FROM_YTD] = Math.Round(cmath.div(t1, t2), ROUND5);

    }


    private void DoRow47()
    {
      //2021-02-24
      //= IFERROR((G81 / G80)       * (G81 / G81)       * 100 * G10 / G10, IFERROR(100 * G69     / G33     , 0))
      //= IFERROR((KPI0057/KPI0056) * (KPI0057/KPI0057) * 100 * BGP / BGP, IFERROR(100 * KPI0046 / KPI0020 , 0))

      int monthNdx = 1;

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_57, col];
        t2 = arr[zKPI_56, col];
        t3 = arr[zBP_BGP, col];
        t4 = arr[zKPI_46, col];
        t5 = arr[zKPI_20, col];
        t7 = (cmath.div(t1, t2) * cmath.div(t1, t1) * 100 *  cmath.div(t3, t3));
        if ((t7 <= 0) && (monthNdx <= currMonth))
        {
          t7 =  100 * cmath.div(t4, t5);
        }
        
        arr[zKPI_47, col] = Math.Round(t7, ROUND5);
        monthNdx++;
      }

      SaveData(zKPI_47, "KPI0047");

      DoRow47_QCalc();

      SaveQuarter(zKPI_47, "KPI0047");

    } //



    private void DoRow47_QCalc()
    {
      int monthNdx = 1;

      //if (sSite == "NGX" && sAreaID == "139")
      //{
      //  for (col = FROM_Q1; col <= TO_Q4; col++)
      //  {
      //    t1 = arr[zKPI_46, col];
      //    t2 = arr[zKPI_45, col];
      //    t7 = cmath.div(t1, t2) * 100;
      //    arr[zKPI_47, col] = Math.Round(t7, ROUND5);
      //  }

      //  //ytd
      //  t1 = arr[zKPI_46, FROM_YTD];
      //  t2 = arr[zKPI_45, FROM_YTD];
      //  t7 = cmath.div(t1, t2) * 100;
      //  arr[zKPI_47, FROM_YTD] = Math.Round(t7, ROUND5);
      //}
      //else{
        for (col = FROM_Q1; col <= TO_Q4; col++)
        {
          t1 = arr[zKPI_57, col];
          t2 = arr[zKPI_56, col];
          t3 = arr[zBP_BGP, col];
          t4 = arr[zKPI_46, col];
          t5 = arr[zKPI_20, col];
          t7 = (cmath.div(t1, t2) * cmath.div(t1, t1) * 100 * cmath.div(t3, t3));
          if ((t7 == 0) && (monthNdx <= quarterNum))
          {
            t7 = 100 * cmath.div(t4, t5);
          }
          arr[zKPI_47, col] = Math.Round(t7, ROUND5);
          monthNdx++;
        }

        //ytd
        for (col = FROM_YTD; col <= FROM_YTD; col++)
        {
          t1 = arr[zKPI_57, col];
          t2 = arr[zKPI_56, col];
          t3 = arr[zBP_BGP, col];
          t4 = arr[zKPI_46, col];
          t5 = arr[zKPI_20, col];
          t7 = (cmath.div(t1, t2) * cmath.div(t1, t1) * 100 * cmath.div(t3, t3));
          if (t7 == 0)
          {
            t7 = 100 * cmath.div(t4, t5);
          }
          arr[zKPI_47, col] = Math.Round(t7, ROUND5);
          monthNdx++;
        }
      //}

    } //


    private void DoRow49()
    {
      //
      //monthly
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        arr[zKPI_49,col] = arr[zBP_WAGRR,col];
      }

      SaveData(zKPI_49, "KPI0049");



      //CalcByHrs_YTD_QTR(zKPI_49, ROUND5);   2023-09-07
      DoRow49_QCalc();

      SaveQuarter(zKPI_49, "KPI0049");

    }   //

    private void DoRow49_QCalc(){

        //quarterly
        for (col = FROM_Q1; col <= TO_Q4; col++)
        {
          arr[zKPI_49, col] = arr[zBP_WAGRR, col];
        }

        //ytd
        arr[zKPI_49, FROM_YTD] = arr[zBP_WAGRR, FROM_YTD];
      
    }


    private void DoRow50()
    {
      //
      //monthly
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        arr[zKPI_50,col] = arr[zKPI_22,col];
      }

      SaveData(zKPI_50, "KPI0050");

      //CalcByHrs_YTD_QTR(zKPI_50, ROUND5);  2023-09-07
      DoRow50_Qcalc();

      SaveQuarter(zKPI_50, "KPI0050");

    }   //


    private void DoRow50_Qcalc(){

      //quarterly
      for (col = FROM_Q1; col <= TO_Q4; col++)
      {
        arr[zKPI_50, col] = arr[zKPI_22, col];
      }

      //ytd
      arr[zKPI_50, FROM_YTD] = arr[zKPI_22, FROM_YTD];
   
      
    }//



    private void DoRow51()
    {
      //
      //monthly
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_26, col];
        t2 = arr[zKPI_CAL24, col];
        arr[zKPI_51,col] = Math.Round((cmath.div(t1, t2) * 100), ROUND5);
      }

      SaveData(zKPI_51, "KPI0051");


      //CalcByHrs_YTD_QTR(zKPI_51, ROUND5);
      DoRow51_QCalc();

      SaveQuarter(zKPI_51, "KPI0051");


    }   //

     private void DoRow51_QCalc()
      {
  
        //quarterly
        for (col = FROM_Q1; col <= TO_Q4; col++)
        {
          t1 = arr[zKPI_26, col];
          t2 = arr[zKPI_CAL24, col];
          arr[zKPI_51, col] = Math.Round((cmath.div(t1, t2) * 100), ROUND5);
        }

        //ytd
        t1 = arr[zKPI_26, FROM_YTD];
        t2 = arr[zKPI_CAL24, FROM_YTD];
        arr[zKPI_51, FROM_YTD] = Math.Round((cmath.div(t1, t2) * 100), ROUND5);
    }

    private void DoRow52()
  {
      //
      //monthly
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        arr[zKPI_52,col] = arr[zKPI_29,col];
      }

      SaveData(zKPI_52, "KPI0052");

      DoRows52_QCalc();
       
      SaveQuarter(zKPI_52, "KPI0052");

    }   //


    private void DoRows52_QCalc()
    {
      //quarterly
      for (col = FROM_Q1; col <= TO_Q4; col++)
      {
        arr[zKPI_52, col] = arr[zKPI_29, col];
      }

      //ytd
      arr[zKPI_52, FROM_YTD] = arr[zKPI_29, FROM_YTD];

    }//


    private void DoRow53()
    {
                  
      //monthly
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        arr[zKPI_53, col] = arr[zKPI_43, col];
      }

      SaveData(zKPI_53, "KPI0053");

      DoRows53_QCalc();
     
      SaveQuarter(zKPI_53, "KPI0053");

    }  //


    private void DoRows53_QCalc()
    {
      //quarterly
      for (col = FROM_Q1; col <= TO_Q4; col++)
      {
        arr[zKPI_53, col] = arr[zKPI_43, col];     
      }

      //ytd
      arr[zKPI_53, FROM_YTD] = arr[zKPI_43, FROM_YTD];    

   }


    private void DoRow54()
    {
      //
      //monthly
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        arr[zKPI_54,col] = arr[zKPI_47,col];
      }

      SaveData(zKPI_54, "KPI0054");

  
      DoRows54_QCalc();

      SaveQuarter(zKPI_54, "KPI0054");

    }   //

    private void DoRows54_QCalc()
    {
      //quarterly
      for (col = FROM_Q1; col <= TO_Q4; col++)
      {
        arr[zKPI_54, col] = arr[zKPI_47, col];
      }

      //ytd
      arr[zKPI_54, FROM_YTD] = arr[zKPI_47, FROM_YTD];
    }//



    private void DoRow55()
    {
     
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t3 = arr[zKPI_CAL24, col];
        t3 *= arr[zKPI_49, col];
        t3 *= arr[zKPI_50, col] / 100;
        t3 *= arr[zKPI_51,col] / 100;
        t3 *= arr[zKPI_52,col] / 100;
        t3 *= arr[zKPI_53,col] / 100;
        t3 *= arr[zKPI_54,col] / 100;
        arr[zKPI_55,col] = Math.Round(t3, ROUND5);
      }

      SaveData(zKPI_55, "KPI0055");

      SumYTD_QTR(zKPI_55, ROUND5);
     
      SaveQuarter(zKPI_55, "KPI0055");

    }   //


    private void DoRow56()
    {
      //
      //monthly
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_52, col];
        t2 = arr[zKPI_53, col];
        arr[zKPI_56,col] = Math.Round((t1 * t2) / 100, ROUND5);
      }

      SaveData(zKPI_56, "KPI0056");

        DoRows56_QCalc(); 
       
      SaveQuarter(zKPI_56, "KPI0056");


    }   //


    private void DoRows56_QCalc()
    {
      //quarterly
      for (col = FROM_Q1; col <= TO_Q4; col++)
      {
        t1 = arr[zKPI_52, col];
        t2 = arr[zKPI_53, col];
        arr[zKPI_56, col] = Math.Round((t1 * t2) / 100, ROUND5);
      }

      //ytd
      t1 = arr[zKPI_52, FROM_YTD];
      t2 = arr[zKPI_53, FROM_YTD];
      arr[zKPI_56, FROM_YTD] = Math.Round((t1 * t2) / 100, ROUND5);
    }


    private void DoRow57()
    {
     
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_66, col];
        t2 = arr[zBP_BOE,col];
        t3 = (t1 * t2) / 100;
        arr[zKPI_57,col] = Math.Round(t3, ROUND5);
      }

      SaveData(zKPI_57, "KPI0057");

      DoRows57_QCalc();     //2023-09-07

        //if (sSite == "NGX" && (sAreaID == "161" || sAreaID == "139"))     //C-2210-0105
        //{
        //  DoRows57_QCalc();
        //}
        //else
        //{
        //  CalcByHrs_YTD_QTR(zKPI_57, ROUND5);
        //}


      SaveQuarter(zKPI_57, "KPI0057");

    }   //


    private void DoRows57_QCalc()
    {

      //if (sSite == "NGX" && (sAreaID == "161" || sAreaID == "139"))     //C-2210-0105
      //{
      //  for (col = FROM_Q1; col <= TO_Q4; col++)
      //  {
      //    t1 = arr[zKPI_29, col];
      //    t2 = arr[zKPI_43, col];
      //    t3 = arr[zKPI_47, col];
      //    t7 = (t1 * t2 * t3) / 10000;
      //    arr[zKPI_57, col] = Math.Round(t7, ROUND5);
      //  }

      //  //ytd
      //  t1 = arr[zKPI_29, FROM_YTD];
      //  t2 = arr[zKPI_43, FROM_YTD];
      //  t3 = arr[zKPI_47, FROM_YTD];
      //  t7 = (t1 * t2 * t3) / 10000;
      //  arr[zKPI_57, FROM_YTD] = Math.Round(t7, ROUND5);
      //}
      //else
      //{

        for (col = FROM_Q1; col <= TO_Q4; col++)
        {
          t1 = arr[zKPI_66, col];
          t2 = arr[zBP_BOE, col];
          t3 = (t1 * t2) / 100;
          arr[zKPI_57, col] = Math.Round(t3, ROUND5);
        }

        //ytd
        t1 = arr[zKPI_66, FROM_YTD];
        t2 = arr[zBP_BOE, FROM_YTD];
        t3 = (t1 * t2) / 100;
        arr[zKPI_57, FROM_YTD] = Math.Round(t3, ROUND5);

      //}

    } //



    private void DoRow58()
    {

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_50,col];
        t2 = arr[zKPI_51,col];
        t3 = arr[zKPI_57,col];
        t4 = ((t1 * t2 * t3) / 10000);
        arr[zKPI_58,col] = Math.Round(t4, ROUND5);
      }

      SaveData(zKPI_58, "KPI0058");


      //CalcByHrs_YTD_QTR(zKPI_58, ROUND5); 2023-09-07
      DoRow58_QCalc();

      SaveQuarter(zKPI_58, "KPI0058");

    }   //

    private void DoRow58_QCalc(){

      //quarterly
      for (col = FROM_Q1; col <= TO_Q4; col++)
      {
        t1 = arr[zKPI_50, col];
        t2 = arr[zKPI_51, col];
        t3 = arr[zKPI_57, col];
        t4 = ((t1 * t2 * t3) / 10000);
        arr[zKPI_58, col] = Math.Round(t4, ROUND5);
      }

      //ytd
      t1 = arr[zKPI_50, FROM_YTD];
      t2 = arr[zKPI_51, FROM_YTD];
      t3 = arr[zKPI_57, FROM_YTD];
      t4 = ((t1 * t2 * t3) / 10000);
      arr[zKPI_58, FROM_YTD] = Math.Round(t4, ROUND5);
    }

    private void DoRow60()
    {

    
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_22,col];
        t2 = arr[zBP_AMBQR,col];
        arr[zKPI_60,col] = Math.Round((cmath.div(t1, t2) * 100), ROUND5);
      }

      SaveData(zKPI_60, "KPI0060");

      //CalcByHrs_YTD_QTR(zKPI_60, ROUND5);   2023-09-07
      DoRow60_QCalc();

      SaveQuarter(zKPI_60, "KPI0060");

    }   //

    private void DoRow60_QCalc(){

      //quarterly
      for (col = FROM_Q1; col <= TO_Q4; col++)
      {
        t1 = arr[zKPI_22, col];
        t2 = arr[zBP_AMBQR, col];
        arr[zKPI_60, col] = Math.Round((cmath.div(t1, t2) * 100), ROUND5);
      }

      //ytd
      t1 = arr[zKPI_22, FROM_YTD];
      t2 = arr[zBP_AMBQR, FROM_YTD];
      arr[zKPI_60, FROM_YTD] = Math.Round((cmath.div(t1, t2) * 100), ROUND5);

    }//


    private void DoRow61()
    {

      //monthly
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_51,col];
        t2 = arr[zBP_POR,col];
        arr[zKPI_61,col] = Math.Round((cmath.div(t1, t2) * 100), ROUND5);
      }

      SaveData(zKPI_61, "KPI0061");

      //CalcByHrs_YTD_QTR(zKPI_61, ROUND5);     2023-09-07
      DoRow61_QCalc();

      SaveQuarter(zKPI_61, "KPI0061");

    }   //


    private void DoRow61_QCalc(){
      //quarterly
      for (col = FROM_Q1; col <= TO_Q4; col++)
      {
        t1 = arr[zKPI_51, col];
        t2 = arr[zBP_POR, col];
        arr[zKPI_61, col] = Math.Round((cmath.div(t1, t2) * 100), ROUND5);
      }

      //ytd
      t1 = arr[zKPI_51, FROM_YTD];
      t2 = arr[zBP_POR, FROM_YTD];
      arr[zKPI_61, FROM_YTD] = Math.Round((cmath.div(t1, t2) * 100), ROUND5);

    }


    private void DoRow62()
    {

      //monthly
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_52,col];
        t2 = arr[zBP_BNPP,col];
        arr[zKPI_62,col] = Math.Round((cmath.div(t1, t2) * 100), ROUND5);
      }

      SaveData(zKPI_62, "KPI0062");


      //CalcByHrs_YTD_QTR(zKPI_62, ROUND5);   2023-09-07
      DoRow62_QCalc();

      SaveQuarter(zKPI_62, "KPI0062");

    }   //


    private void DoRow62_QCalc(){
      //quarterly
      for (col = FROM_Q1; col <= TO_Q4; col++)
      {
        t1 = arr[zKPI_52, col];
        t2 = arr[zBP_BNPP, col];
        arr[zKPI_62, col] = Math.Round((cmath.div(t1, t2) * 100), ROUND5);
      }

      //ytd
      t1 = arr[zKPI_52, FROM_YTD];
      t2 = arr[zBP_BNPP, FROM_YTD];
      arr[zKPI_62, FROM_YTD] = Math.Round((cmath.div(t1, t2) * 100), ROUND5);
    }

    private void DoRow63()
    {

      //monthly
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_43,col];
        t2 = arr[zBP_BQR,col];
        arr[zKPI_63,col] = Math.Round((cmath.div(t1, t2) * 100), ROUND5);
      }

      SaveData(zKPI_63, "KPI0063");



      //CalcByHrs_YTD_QTR(zKPI_63, ROUND5);
      DoRow63_QCalc();

      SaveQuarter(zKPI_63, "KPI0063");

    }   //


    private void DoRow63_QCalc(){

      //quarterly
      for (col = FROM_Q1; col <= TO_Q4; col++)
      {
        t1 = arr[zKPI_43, col];
        t2 = arr[zBP_BQR, col];
        arr[zKPI_63, col] = Math.Round((cmath.div(t1, t2) * 100), ROUND5);
      }

      //ytd
      t1 = arr[zKPI_43, FROM_YTD];
      t2 = arr[zBP_BQR, FROM_YTD];
      arr[zKPI_63, FROM_YTD] = Math.Round((cmath.div(t1, t2) * 100), ROUND5);

    }

    private void DoRow64()
    {

      //monthly
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_66,col];
        t2 = arr[zKPI_65,col];
        arr[zKPI_64,col] = Math.Round((cmath.div(t1, t2) * 100), ROUND5);
      }

      SaveData(zKPI_64, "KPI0064");


      //CalcByHrs_YTD_QTR(zKPI_64, ROUND5);
      DoRow64_QCalc();

      SaveQuarter(zKPI_64, "KPI0064");

    }   //


    private void DoRow64_QCalc()
    {

      //quarterly
      for (col = FROM_Q1; col <= TO_Q4; col++)
      {
        t1 = arr[zKPI_66, col];
        t2 = arr[zKPI_65, col];
        arr[zKPI_64, col] = Math.Round((cmath.div(t1, t2) * 100), ROUND5);
      }

      //ytd
      t1 = arr[zKPI_66, FROM_YTD];
      t2 = arr[zKPI_65, FROM_YTD];
      arr[zKPI_64, FROM_YTD] = Math.Round((cmath.div(t1, t2) * 100), ROUND5);
    }


      private void DoRow65()
    {

      //monthly
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_56,col];
        t2 = arr[zBP_BPOME,col];
        arr[zKPI_65,col] = Math.Round((cmath.div(t1, t2) * 100), ROUND5);
      }

      SaveData(zKPI_65, "KPI0065");

      //CalcByHrs_YTD_QTR(zKPI_65, ROUND5);
      DoRow65_QCalc();


      SaveQuarter(zKPI_65, "KPI0065");

    }   //


    private void DoRow65_QCalc()
    {
      //quarterly
      for (col = FROM_Q1; col <= TO_Q4; col++)
      {
        t1 = arr[zKPI_56, col];
        t2 = arr[zBP_BPOME, col];
        arr[zKPI_65, col] = Math.Round((cmath.div(t1, t2) * 100), ROUND5);
      }

      //ytd
      t1 = arr[zKPI_56, FROM_YTD];
      t2 = arr[zBP_BPOME, FROM_YTD];
      arr[zKPI_65, FROM_YTD] = Math.Round((cmath.div(t1, t2) * 100), ROUND5);

    }


    private void DoRow66()
    {
      //2021-02-24
      //= IFERROR((G62 / (G8 * G22 * G25 * G38) * 1000000) / (G85 * G84 / 10000)         * (G85     / G85)     * (G10 / G10), (G89     * (G69     / (G8    + (G32 / G42)))))
      //= IFERROR((RFT/(WAGRR*BQR*BPRFT2 * CTT) * 1000000) / (KPI0061 * KPI0060 / 10000) * (KPI0061 / KPI0061) * (BGP / BGP), (KPI0065 * (KPI0046 / (WAGRR + (ABGP / KPI0026 )))))

      //latest
      //IFERROR((G60 / (G8 * G22 * G25 * G38 * G14 * G15 * G20) * 1000000000000) / (G85 * G84 / 10000) * (G85 / G85) * (G10 / G10), (G89 * (G69 / (G8 + (G32 / G42)))))
      //       (PPAG / (WAGRR*BQR*BPRFT2*CTT * AMBQR* POR * BNPP ) * 1000000000000) / (KPI0061 * KPI0060 / 10000) * (KPI0061 / KPI0061) * (BGP / BGP), (KPI0065 * (KPI0046 / (WAGRR + (ABGP / KPI0026 )))))

      int monthNdx = 1;
      double t8 = 0D;
      double t9 = 0D;
      double t10 = 0D;
      double t11 = 0D;
      double t12 = 0D;
      double t13 = 0D;
      double t14 = 0D;
      double t15 = 0D;
      double tot = 0D;

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_PPAG, col];
        t2 = arr[zBP_WAGRR,col];
        t3 = arr[zBP_BQR,col];
        t4 = arr[zBP_BPRFT2,col];
        t5 = arr[zKPI_CAL24, col];
        t6 = arr[zKPI_61, col];
        t7 = arr[zKPI_60, col];
        t8 = arr[zBP_BGP, col];
        t9 = arr[zKPI_65, col];
        t10 = arr[zKPI_46, col];
        t11 = arr[zKPI_ABGP, col];
        t12 = arr[zKPI_26, col];

        t13 = arr[zBP_AMBQR, col];
        t14 = arr[zBP_POR, col];
        t15 = arr[zBP_BNPP, col];

        tot = cmath.div(t1, (t2 * t3 * t4 * t5 * t13 * t14 * t15)) * 1000000000000;
        tot = cmath.div(tot , (t6 * t7 / 10000)) * cmath.div(t6 , t6) * cmath.div(t8 , t8);

        if ((tot == 0) && (monthNdx <= currMonth))
        {   //the error part of the iferror()
          //  (KPI0065 * (KPI0046 / (WAGRR + (ABGP / KPI0026))
          tot = (t9 * cmath.div(t10 ,  (t2    + cmath.div(t11 , t12) ) ) );
        }
        
        arr[zKPI_66, col] = Math.Round(tot, ROUND5);

        monthNdx++;
      }

      SaveData(zKPI_66, "KPI0066");


      //CalcByHrs_YTD_QTR(zKPI_66, ROUND5);
      DoRow66_QCalc();

      SaveQuarter(zKPI_66, "KPI0066");

    }   //


    private void DoRow66_QCalc(){
      

      int monthNdx = 1;
      double t8 = 0D;
      double t9 = 0D;
      double t10 = 0D;
      double t11 = 0D;
      double t12 = 0D;
      double t13 = 0D;
      double t14 = 0D;
      double t15 = 0D;
      double tot = 0D;

      for (col = FROM_Q1; col <= TO_Q4; col++)
      {

        t1 = arr[zKPI_PPAG, col];
        t2 = arr[zBP_WAGRR, col];
        t3 = arr[zBP_BQR, col];
        t4 = arr[zBP_BPRFT2, col];
        t5 = arr[zKPI_CAL24, col];
        t6 = arr[zKPI_61, col];
        t7 = arr[zKPI_60, col];
        t8 = arr[zBP_BGP, col];
        t9 = arr[zKPI_65, col];
        t10 = arr[zKPI_46, col];
        t11 = arr[zKPI_ABGP, col];
        t12 = arr[zKPI_26, col];

        t13 = arr[zBP_AMBQR, col];
        t14 = arr[zBP_POR, col];
        t15 = arr[zBP_BNPP, col];

        tot = cmath.div(t1, (t2 * t3 * t4 * t5 * t13 * t14 * t15)) * 1000000000000;
        tot = cmath.div(tot, (t6 * t7 / 10000)) * cmath.div(t6, t6) * cmath.div(t8, t8);

        if ((tot == 0) && (monthNdx <= quarterNum))
        {   //the error part of the iferror()
          tot = (t9 * cmath.div(t10, (t2 + cmath.div(t11, t12))));
        }

        arr[zKPI_66, col] = Math.Round(tot, ROUND5);

        monthNdx++;

      } //for


      for (col = FROM_YTD; col <= FROM_YTD; col++)
      {
        t1 = arr[zKPI_PPAG, col];
        t2 = arr[zBP_WAGRR, col];
        t3 = arr[zBP_BQR, col];
        t4 = arr[zBP_BPRFT2, col];
        t5 = arr[zKPI_CAL24, col];
        t6 = arr[zKPI_61, col];
        t7 = arr[zKPI_60, col];
        t8 = arr[zBP_BGP, col];
        t9 = arr[zKPI_65, col];
        t10 = arr[zKPI_46, col];
        t11 = arr[zKPI_ABGP, col];
        t12 = arr[zKPI_26, col];

        t13 = arr[zBP_AMBQR, col];
        t14 = arr[zBP_POR, col];
        t15 = arr[zBP_BNPP, col];

        tot = cmath.div(t1, (t2 * t3 * t4 * t5 * t13 * t14 * t15)) * 1000000000000;
        tot = cmath.div(tot, (t6 * t7 / 10000)) * cmath.div(t6, t6) * cmath.div(t8, t8);

        if (tot == 0)
        {   //the error part of the iferror()
          tot = (t9 * cmath.div(t10, (t2 + cmath.div(t11, t12))));
        }

        arr[zKPI_66, col] = Math.Round(tot, ROUND5);

      } //for
    }


    private void DoRow67()
    {

      //monthly
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_58, col];
        t2 = arr[zBP_TEEP,col];
        arr[zKPI_67,col] = Math.Round((cmath.div(t1, t2) * 100), ROUND5);
      }

      SaveData(zKPI_67, "KPI0067");


      //CalcByHrs_YTD_QTR(zKPI_67, ROUND5);
      DoRow67_QCalc();


      SaveQuarter(zKPI_67, "KPI0067");

    }   //


    private void DoRow67_QCalc(){

      //quarterly
      for (col = FROM_Q1; col <= TO_Q4; col++)
      {
        t1 = arr[zKPI_58, col];
        t2 = arr[zBP_TEEP, col];
        arr[zKPI_67, col] = Math.Round((cmath.div(t1, t2) * 100), ROUND5);
      }

      //ytd
      t1 = arr[zKPI_58, FROM_YTD];
      t2 = arr[zBP_TEEP, FROM_YTD];
      arr[zKPI_67, FROM_YTD] = Math.Round((cmath.div(t1, t2) * 100), ROUND5);
    }


    private void DoRow69()
    {

      //monthly
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        arr[zKPI_69,col] = arr[zKPI_60,col];
      }

      SaveData(zKPI_69, "KPI0069");

      //CalcByHrs_YTD_QTR(zKPI_69, ROUND5);
      DoRow69_QCalc();

      SaveQuarter(zKPI_69, "KPI0069");

    }   //


    private void DoRow69_QCalc(){
      //quarterly
      for (col = FROM_Q1; col <= TO_Q4; col++)
      {
        arr[zKPI_69, col] = arr[zKPI_60, col];
      }

      //ytd
      arr[zKPI_69, FROM_YTD] = arr[zKPI_60, FROM_YTD];
    }


    private void DoRow70()
    {

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        arr[zKPI_70, col] = arr[zKPI_61, col];
      }

      SaveData(zKPI_70, "KPI0070");


      //CalcByHrs_YTD_QTR(zKPI_70, ROUND5);
      DoRow70_QCalc();


      SaveQuarter(zKPI_70, "KPI0070");

    }   //

    private void DoRow70_QCalc(){

      //quarterly
      for (col = FROM_Q1; col <= TO_Q4; col++)
      {
        arr[zKPI_70, col] = arr[zKPI_61, col];
      }

      //ytd
      arr[zKPI_70, FROM_YTD] = arr[zKPI_61, FROM_YTD];

    }

    private void DoRow71()
    {

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        arr[zKPI_71,col] = arr[zKPI_66,col];
      }

      SaveData(zKPI_71, "KPI0071");


      //CalcByHrs_YTD_QTR(zKPI_71, ROUND5);
      DoRow71_QCalc();

      SaveQuarter(zKPI_71, "KPI0071");


    }   //

    private void DoRow71_QCalc(){
      //quarterly
      for (col = FROM_Q1; col <= TO_Q4; col++)
      {
        arr[zKPI_71, col] = arr[zKPI_66, col];
      }

      //ytd
      arr[zKPI_71, FROM_YTD] = arr[zKPI_66, FROM_YTD];
    }

    private void DoRow72()
    {

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_69,col];
        t2 = arr[zKPI_70,col];
        t3 = arr[zKPI_71,col];
        t4 = ((t1 * t2 * t3) / 10000);
        arr[zKPI_72,col] = Math.Round(t4, ROUND5);
      }

      SaveData(zKPI_72, "KPI0072");


      //CalcByHrs_YTD_QTR(zKPI_72, ROUND5);
      DoRow72_QCalc();

      SaveQuarter(zKPI_72, "KPI0072");

    }   //

    private void DoRow72_QCalc(){

      //quarterly
      for (col = FROM_Q1; col <= TO_Q4; col++)
      {
        t1 = arr[zKPI_69, col];
        t2 = arr[zKPI_70, col];
        t3 = arr[zKPI_71, col];
        t4 = ((t1 * t2 * t3) / 10000);
        arr[zKPI_72, col] = Math.Round(t4, ROUND5);
      }


      //ytd
      for (col = FROM_YTD; col <= FROM_YTD; col++)
      {
        t1 = arr[zKPI_69, col];
        t2 = arr[zKPI_70, col];
        t3 = arr[zKPI_71, col];
        t4 = ((t1 * t2 * t3) / 10000);
        arr[zKPI_72, col] = Math.Round(t4, ROUND5);
      }

    }//

    private void DoRow73()
    {
      

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        
        t1 = arr[zKPI_RFT, col];
        t2 = arr[zBP_NRFTP, col];
        t3 = arr[zKPI_PPAG, col];
        t4 = arr[zBP_NSPPM, col];
        t5 = cmath.div(t2, t1) * cmath.div(t1, t2) * cmath.div(t3, t4) * 100;
        arr[zKPI_73, col] = Math.Round(t5, ROUND5);

      }

      SaveData(zKPI_73, "KPI0073");

      //CalcByHrs_YTD_QTR(zKPI_73, ROUND5);
      DoRow73_QCalc();

      SaveQuarter(zKPI_73, "KPI0073");

    }   //

    private void DoRow73_QCalc(){
      //quarterly
      for (col = FROM_Q1; col <= TO_Q4; col++)
      {

        t1 = arr[zKPI_RFT, col];
        t2 = arr[zBP_NRFTP, col];
        t3 = arr[zKPI_PPAG, col];
        t4 = arr[zBP_NSPPM, col];
        t5 = cmath.div(t2, t1) * cmath.div(t1, t2) * cmath.div(t3, t4) * 100;
        arr[zKPI_73, col] = Math.Round(t5, ROUND5);

      }


      //ytd
      for (col = FROM_YTD; col <= FROM_YTD; col++)
      {


        t1 = arr[zKPI_RFT, col];
        t2 = arr[zBP_NRFTP, col];
        t3 = arr[zKPI_PPAG, col];
        t4 = arr[zBP_NSPPM, col];
        t5 = cmath.div(t2, t1) * cmath.div(t1, t2) * cmath.div(t3, t4) * 100;
        arr[zKPI_73, col] = Math.Round(t5, ROUND5);

      }
    }

    private void DoRow74()
    {
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_73,col];
        t2 = arr[zKPI_72,col];
        t4 = (t1 - t2);
        arr[zKPI_74,col] = Math.Round(t4, ROUND5);
      }

      SaveData(zKPI_74, "KPI0074");

      //CalcByHrs_YTD_QTR(zKPI_74, ROUND5);
      DoRow74_QCalc();

      SaveQuarter(zKPI_74, "KPI0074");

    }   //


    private void DoRow74_QCalc(){

      //quarterly
      for (col = FROM_Q1; col <= TO_Q4; col++)
      {
        t1 = arr[zKPI_73, col];
        t2 = arr[zKPI_72, col];
        t4 = (t1 - t2);
        arr[zKPI_74, col] = Math.Round(t4, ROUND5);
      }

      //ytd
      for (col = FROM_YTD; col <= FROM_YTD; col++)
      {
        t1 = arr[zKPI_73, col];
        t2 = arr[zKPI_72, col];
        t4 = (t1 - t2);
        arr[zKPI_74, col] = Math.Round(t4, ROUND5);
      }

    }


    private void DoRow75()
    {
      //KPI0035 * 100 / (WAGGR*POR*AMBQR*BNPP*BPSY*CTT)   //2021-02-27

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_35,col];
        t2 = arr[zBP_WAGRR,col];
        t3 = arr[zBP_POR, col];
        t4 = arr[zBP_AMBQR, col];
        t5 = arr[zBP_BNPP, col];
        t6 = arr[zBP_BPSY, col];
        t7 = arr[zKPI_CAL24, col];

        t7 = (t2 * (t3/100) * (t4/100) * (t5/100) * (t6/100) * t7);
        t7 = cmath.div(t1 * 100, t7);

        arr[zKPI_75,col] = Math.Round(t7, ROUND5);
      }

      SaveData(zKPI_75, "KPI0075");


      //CalcByHrs_YTD_QTR(zKPI_75, ROUND5);
      DoRow75_QCalc();

      SaveQuarter(zKPI_75, "KPI0075");

    }   //

    private void DoRow75_QCalc()
    {

          //quarterly
          for (col = FROM_Q1; col <= TO_Q4; col++)
          {
            t1 = arr[zKPI_35, col];
            t2 = arr[zBP_WAGRR, col];
            t3 = arr[zBP_POR, col];
            t4 = arr[zBP_AMBQR, col];
            t5 = arr[zBP_BNPP, col];
            t6 = arr[zBP_BPSY, col];
            t7 = arr[zKPI_CAL24, col];

            t7 = (t2 * (t3 / 100) * (t4 / 100) * (t5 / 100) * (t6 / 100) * t7);
            t7 = cmath.div(t1 * 100, t7);

            arr[zKPI_75, col] = Math.Round(t7, ROUND5);
          }


          //ytd
          for (col = FROM_YTD; col <= FROM_YTD; col++)
          {
            t1 = arr[zKPI_35, col];
            t2 = arr[zBP_WAGRR, col];
            t3 = arr[zBP_POR, col];
            t4 = arr[zBP_AMBQR, col];
            t5 = arr[zBP_BNPP, col];
            t6 = arr[zBP_BPSY, col];
            t7 = arr[zKPI_CAL24, col];

            t7 = (t2 * (t3 / 100) * (t4 / 100) * (t5 / 100) * (t6 / 100) * t7);
            t7 = cmath.div(t1 * 100, t7);

            arr[zKPI_75, col] = Math.Round(t7, ROUND5);
          }
    }


    private void DoRow77()
    {

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zBP_WAGRR,col];
        t2 = arr[zKPI_CAL24,col];

        arr[zKPI_77,col] = Math.Round((t1 * t2), ROUND5);
      }

      SaveData(zKPI_77, "KPI0077");

      SumYTD_QTR(zKPI_77, ROUND5);

      SaveQuarter(zKPI_77, "KPI0077");

    }   //


    private void DoRow78()
    {

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zBP_AMBQR,col];
        t1 = 1 - (t1 / 100);
        t2 = arr[zKPI_77,col];
        //arr[zKPI_78,col] = Math.Round((t1 * t2), ROUND5);
        arr[zKPI_78, col] = Math.Round((t1 * t2 * -1), ROUND5);   //2021-01-27
      }

      SaveData(zKPI_78, "KPI0078");

     
      SumYTD_QTR(zKPI_78, ROUND5);

      SaveQuarter(zKPI_78, "KPI0078");

    }   //



    private void DoRow79()
    {
      //(KPI0077 + KPI0078) * (1 - POR / 100)
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zBP_POR,col];
        t1 = 1 - (t1 / 100);
        t2 = arr[zKPI_77,col];
        t3 = arr[zKPI_78,col];
        //t4 = (t2 - t3) * t1;
        t4 = (t2 + t3) * t1;
        //arr[zKPI_79,col] = Math.Round(t4, ROUND5);
        arr[zKPI_79, col] = Math.Round(t4 * -1, ROUND5);    //2021-01-27
      }

      SaveData(zKPI_79, "KPI0079");

      
      SumYTD_QTR(zKPI_79, ROUND5);

      SaveQuarter(zKPI_79, "KPI0079");

    }   //



    private void DoRow80()
    {

      //(KPI0077 + KPI0078 + KPI0079) * (1 - BNPP / 100)

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zBP_BNPP,col];
        t1 = 1 - (t1 / 100);
        t2 = arr[zKPI_77,col];
        t3 = arr[zKPI_78,col];
        t4 = arr[zKPI_79,col];
        //t4 = (t2 - t3 - t4) * t1;
        t4 = (t2 + t3 + t4) * t1;
        //arr[zKPI_80,col] = Math.Round(t4, ROUND5);
        arr[zKPI_80, col] = Math.Round(t4 * -1, ROUND5);    //2021-01-27
      }


      SaveData(zKPI_80, "KPI0080");

    
      SumYTD_QTR(zKPI_80, ROUND5);

      SaveQuarter(zKPI_80, "KPI0080");

    }   //



    private void DoRow81()
    {
      //(KPI0077 + KPI0078 + KPI0079 + KPI0080) * (1 - BPSY / 100)

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zBP_BPSY,col];
        t1 = 1 - (t1 / 100);
        t2 = arr[zKPI_77,col];
        t3 = arr[zKPI_78,col];
        t4 = arr[zKPI_79,col];
        t5 = arr[zKPI_80,col];
        t4 = (t2 + t3 + t4 + t5) * t1;
        //arr[zKPI_81,col] = Math.Round(t4, ROUND5);
        arr[zKPI_81, col] = Math.Round(t4 * -1, ROUND5);    //2021-01-27
      }

      SaveData(zKPI_81, "KPI0081");

      SumYTD_QTR(zKPI_81, ROUND5);

      SaveQuarter(zKPI_81, "KPI0081");

    }   //



    private void DoRow82()
    {
      //(KPI0077 + KPI0078 + KPI0079 + KPI0080 + KPI0081) * (1 - BPMS / 100)

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zBP_BPMS,col];         // was BPSY as per sanjay
        t1 = 1 - (t1 / 100);
        t2 = arr[zKPI_77,col];
        t3 = arr[zKPI_78,col];
        t4 = arr[zKPI_79,col];
        t5 = arr[zKPI_80,col];
        t6 = arr[zKPI_81,col];
        //t4 = (t2 - t3 - t4 - t5 - t6) * t1;
        t4 = (t2 + t3 + t4 + t5 + t6) * t1;
        //arr[zKPI_82,col] = Math.Round(t4, ROUND5);
        arr[zKPI_82, col] = Math.Round(t4 * -1, ROUND5);    //2021-01-27
      }

      SaveData(zKPI_82, "KPI0082");

      SumYTD_QTR(zKPI_82, ROUND5);

      SaveQuarter(zKPI_82, "KPI0082");


    }   //



    private void DoRow83()
    {

      //int prodLine = 0;     //for saiccor weekly and monthly budget production figure.  2022-04-20   C-2304-0326
      //                      // 2022-05-30   C-2305-0629

      //if (sAreaID == "4")
      //  prodLine = 3;

      //if (sAreaID == "5")
      //  prodLine = 4;

      //if (sAreaID == "6")
      //  prodLine = 5;

      if ((sSite == "SAI" && (boRunDaily == true)) || sSite != "SAI")
      {
        for (col = FROM_MONTH; col < MAX_COLS; col++)
        {
          t1 = arr[zKPI_77, col];
          t2 = arr[zKPI_78, col];
          t3 = arr[zKPI_79, col];
          t4 = arr[zKPI_80, col];
          t5 = arr[zKPI_81, col];
          t6 = arr[zKPI_82, col];
          //t1 = t1 - (t2 + t3 + t4 + t5 + t6);
          t1 = t1 + (t2 + t3 + t4 + t5 + t6);     //2021-01-27
          arr[zKPI_83, col] = Math.Round(t1, ROUND5);
        }
      }

      //if (sSite == "SAI" && boRunWeek == true)
      //{
      //  string sql = "select sum([budgettons]) as budgettons from [mesqualitysystem].[prd].[productionbudget] "
      //     + " where productionbudgetdate >= cast('" + sDailyStartDate + "' as date)"
      //     + " and productionbudgetdate <= cast('" + sDailyEndDate + "' as date)"
      //     + " and ProdLine = " + prodLine;

      //  cSQL.OpenDB("SAIMES");
      //  DataTable dt = cSQL.GetDataTable(sql);
      //  cSQL.CloseDB();
      //  t1 = 0;
      //  foreach (DataRow item in dt.Rows)
      //  {
      //    t1 = cmath.getd(item["budgettons"].ToString());
      //  }

      //  arr[zKPI_83, FROM_MONTH] = t1;

      //} //weekly


      //if (sSite == "SAI" && boRunMonth == true)
      //{
      //  string sql = "select sum([budgettons]) as budgettons from [mesqualitysystem].[prd].[productionbudget] "
      //     + " where productionbudgetdate >= cast('" + sDailyStartDate + "' as date)"
      //     + " and productionbudgetdate <= cast('" + sDailyEndDate + "' as date)"
      //     + " and ProdLine = " + prodLine;

      //  cSQL.OpenDB("SAIMES");
      //  DataTable dt = cSQL.GetDataTable(sql);
      //  cSQL.CloseDB();
      //  t1 = 0;
      //  foreach (DataRow item in dt.Rows)
      //  {
      //    t1 = cmath.getd(item["budgettons"].ToString());
      //  }

      //  arr[zKPI_83, FROM_MONTH + iRunMonth - 1] = t1;      //because array starts at 0

      //} //monthly


      SaveData(zKPI_83, "KPI0083");

      SumYTD_QTR(zKPI_83, ROUND5);

      SaveQuarter(zKPI_83, "KPI0083");

    }   //



    private void DoRow163()
    {

      // int prodLine = 0;     //for saiccor daily, weekly and monthly budget production figure. comes from PHSSAI


      //if (sAreaID == "4")
      //  prodLine = 3;

      //if (sAreaID == "5")
      //  prodLine = 4;

      //if (sAreaID == "6")
      //  prodLine = 5;

      ////update kpi0163 with saiccor figures from phssai
      //if (sSite == "SAI" && (boRunWeek == true || boRunDaily == true) )
      //{
      //  string sql = "select sum([budgettons]) as budgettons from [mesqualitysystem].[prd].[productionbudget] "
      //     + " where productionbudgetdate >= cast('" + sDailyStartDate + "' as date)"
      //     + " and productionbudgetdate <= cast('" + sDailyEndDate + "' as date)"
      //     + " and ProdLine = " + prodLine;

      //  cSQL.OpenDB("SAIMES");
      //  DataTable dt = cSQL.GetDataTable(sql);
      //  cSQL.CloseDB();
      //  t1 = 0;
      //  foreach (DataRow item in dt.Rows)
      //  {
      //    t1 = cmath.getd(item["budgettons"].ToString());
      //  }

      //  arr[zKPI_163, FROM_MONTH] = t1;

      //  SaveData(zKPI_163, "KPI0163");

      //} //weekly


      ////update kpi0163 with saiccor figures from phssai
      //if (sSite == "SAI" && boRunMonth == true)
      //{
      //  string sql = "select sum([budgettons]) as budgettons from [mesqualitysystem].[prd].[productionbudget] "
      //     + " where productionbudgetdate >= cast('" + sDailyStartDate + "' as date)"
      //     + " and productionbudgetdate <= cast('" + sDailyEndDate + "' as date)"
      //     + " and ProdLine = " + prodLine;

      //  cSQL.OpenDB("SAIMES");
      //  DataTable dt = cSQL.GetDataTable(sql);
      //  cSQL.CloseDB();
      //  t1 = 0;
      //  foreach (DataRow item in dt.Rows)
      //  {
      //    t1 = cmath.getd(item["budgettons"].ToString());
      //  }

      //  arr[zKPI_163, FROM_MONTH + iRunMonth - 1] = t1;

      //  SaveData(zKPI_163, "KPI0163");

      //} //monthly


      SumYTD_QTR(zKPI_163, ROUND5);

      SaveQuarter(zKPI_163, "KPI0163");
      
      
    }   //





    private void DoRow84()
    {

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_83,col];
        t2 = arr[zBP_BQR,col] / 100.00;
        t3 = arr[zBP_BPSY,col] / 100.00;
        t4 = (1 - cmath.div(t2, t3));
        t1 = t1 * t4;
        //arr[zKPI_84,col] = Math.Round(t1, ROUND5);
        arr[zKPI_84, col] = Math.Round(t1 * -1, ROUND5);    //2021-01-27
      }

      SaveData(zKPI_84, "KPI0084");

      SumYTD_QTR(zKPI_84, ROUND5);

      SaveQuarter(zKPI_84, "KPI0084");

    }   //



    private void DoRow85()
    {
      //(KPI0083 + KPI0084) * (1 - BPRFT2 / 100)

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_83,col];
        t2 = arr[zKPI_84,col];
        t3 = arr[zBP_BPRFT2,col];              //2020-12-14 ex zBP_BPRFT
        t4 = (1 - (t3 / 100));
        //t1 = (t1 - t2) * t4;
        t1 = (t1 + t2) * t4;
        //arr[zKPI_85, col] = Math.Round(t1, ROUND5);
        arr[zKPI_85, col] = Math.Round(t1 * -1, ROUND5);    //2021-01-27
      }

      SaveData(zKPI_85, "KPI0085");

    
      SumYTD_QTR(zKPI_85, ROUND5);

      SaveQuarter(zKPI_85, "KPI0085");

    }   //



    private void DoRow86()
    {

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_80,col];
        t2 = arr[zKPI_81,col];
        t3 = arr[zKPI_84,col];
        t4 = arr[zKPI_85,col];
        t1 = (t1 + t2 + t3 + t4);
        arr[zKPI_86,col] = Math.Round(t1, ROUND5);
      }

      SaveData(zKPI_86, "KPI0086");

     
      SumYTD_QTR(zKPI_86, ROUND5);


      SaveQuarter(zKPI_86, "KPI0086");

    }   //



    private void DoRow87()
    {

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_82,col];
        t2 = arr[zKPI_86,col];
        t1 = (t1 + t2);
        arr[zKPI_87,col] = Math.Round(t1, ROUND5);
      }

      SaveData(zKPI_87, "KPI0087");

    
      SumYTD_QTR(zKPI_87, ROUND5);

      SaveQuarter(zKPI_87, "KPI0087");

    }   //



    private void DoRow88()
    {

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_78,col];
        t2 = arr[zKPI_79,col];
        t3 = arr[zKPI_80,col];
        t4 = arr[zKPI_81,col];
        //t5 = arr[zKPI_82,col];          // as per sanjay
        t6 = arr[zKPI_84,col];
        t7 = arr[zKPI_85,col];
        t1 = (t1 + t2 + t3 + t4 +  t6 + t7);
        arr[zKPI_88,col] = Math.Round(t1, ROUND5);
      }

      SaveData(zKPI_88, "KPI0088");

  
      SumYTD_QTR(zKPI_88, ROUND5);

      SaveQuarter(zKPI_88, "KPI0088");

    }   //



    private void DoRow90()
    {

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zBP_WAGRR,col];
        t2 = arr[zKPI_CAL24, col];

        arr[zKPI_90,col] = Math.Round((t1 * t2), ROUND5);
      }

      SaveData(zKPI_90, "KPI0090");

   
      SumYTD_QTR(zKPI_90, ROUND5);

      SaveQuarter(zKPI_90, "KPI0090");

    }   //


    private void DoRow91()
    {

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_50,col];
        t1 = 1 - (t1 / 100);
        t2 = arr[zKPI_90,col];
        //arr[zKPI_91,col] = Math.Round((t1 * t2), ROUND5);
        arr[zKPI_91, col] = Math.Round((t1 * t2 * -1), ROUND5);      //2021-01-27
      }

      SaveData(zKPI_91, "KPI0091");

    
      SumYTD_QTR(zKPI_91, ROUND5);

      SaveQuarter(zKPI_91, "KPI0091");

    }   //


    private void DoRow92()
    {

      //(KPI0090 + KPI0091) * (1 - KPI0051 / 100)

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_51,col];
        t1 = 1 - (t1 / 100);
        t2 = arr[zKPI_90,col];
        t3 = arr[zKPI_91,col];
        //t4 = (t2 - t3) * t1;
        t4 = (t2 + t3) * t1;
        //arr[zKPI_92,col] = Math.Round(t4, ROUND5);
        arr[zKPI_92, col] = Math.Round(t4 * -1, ROUND5);     //2021-01-27

      }

      SaveData(zKPI_92, "KPI0092");

    
      SumYTD_QTR(zKPI_92, ROUND5);

      SaveQuarter(zKPI_92, "KPI0092");

    }   //


    private void DoRow93()
    {

      //(KPI0090 + KPI0091 + KPI0092) * (1 - KPI0052 / 100) * -1

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_52,col];
        t1 = 1 - (t1 / 100);
        t2 = arr[zKPI_90,col];
        t3 = arr[zKPI_91,col];
        t4 = arr[zKPI_92,col];
        //t4 = (t2 - t3 - t4) * t1;
        t4 = (t2 + t3 + t4) * t1;
        //arr[zKPI_93,col] = Math.Round(t4, ROUND5);
        arr[zKPI_93, col] = Math.Round(t4 * -1, ROUND5);    //2021-01-27
      }

      SaveData(zKPI_93, "KPI0093");

    
      SumYTD_QTR(zKPI_93, ROUND5);

      SaveQuarter(zKPI_93, "KPI0093");


    }   //


    private void DoRow94()
    {
      
      //(KPI0090 + KPI0091 + KPI0092 + KPI0093) * (1 - KPI0038 /100)

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_38,col];
        t1 = 1 - (t1 / 100);
        t2 = arr[zKPI_90,col];
        t3 = arr[zKPI_91,col];
        t4 = arr[zKPI_92,col];
        t5 = arr[zKPI_93,col];
        //t4 = (t2 - t3 - t4 - t5) * t1;
        t4 = (t2 + t3 + t4 + t5) * t1;
        //arr[zKPI_94,col] = Math.Round(t4, ROUND5);
        arr[zKPI_94, col] = Math.Round(t4 * -1, ROUND5);    //2021-01-27
      }

      SaveData(zKPI_94, "KPI0094");

      SumYTD_QTR(zKPI_94, ROUND5);

      SaveQuarter(zKPI_94, "KPI0094");

    }   //



    private void DoRow95()
    {

      //(KPI0090 + KPI0091 + KPI0092 + KPI0093 + KPI0094) * (1 - KPI0054 /100) * -1

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_54,col];
        t1 = 1 - (t1 / 100);
        t2 = arr[zKPI_90,col];
        t3 = arr[zKPI_91,col];
        t4 = arr[zKPI_92,col];
        t5 = arr[zKPI_93,col];
        t6 = arr[zKPI_94,col];
       
        t4 = (t2 + t3 + t4 + t5 + t6) * t1;
       
        arr[zKPI_95, col] = Math.Round(t4 * -1, ROUND5);  //2021-01-27
      }

      SaveData(zKPI_95, "KPI0095");

      SumYTD_QTR(zKPI_95, ROUND5);

      SaveQuarter(zKPI_95, "KPI0095");

    }   //


    private void DoRow96()
    {

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_90,col];
        t2 = arr[zKPI_91,col];
        t3 = arr[zKPI_92,col];
        t4 = arr[zKPI_93,col];
        t5 = arr[zKPI_94,col];
        t6 = arr[zKPI_95,col];
        //t1 = t1 - (t2 + t3 + t4 + t5 + t6);
        t1 = t1 + (t2 + t3 + t4 + t5 + t6);     //2021-01-27
        arr[zKPI_96,col] = Math.Round(t1, ROUND5);
      }

      SaveData(zKPI_96, "KPI0096");

      SumYTD_QTR(zKPI_96, ROUND5);

      SaveQuarter(zKPI_96, "KPI0096");

    }   //


    private void DoRow97()
    {

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_40,col];
        t1 = 1 - (t1 / 100);
        t2 = arr[zKPI_96,col];
        t1 = t1 * t2;
        //arr[zKPI_97,col] = Math.Round(t1, ROUND5);
        arr[zKPI_97, col] = Math.Round(t1 * -1, ROUND5);   //2021-01-27
      }

      SaveData(zKPI_97, "KPI0097");

    
      SumYTD_QTR(zKPI_97, ROUND5);

      SaveQuarter(zKPI_97, "KPI0097");

    }   //


    private void DoRow98()
    {

      //(KPI0096 + KPI0097) * (1 - KPI0042 / 100)    //2021-01-27
      //(KPI0096 + KPI0097) * (1 - (KPI0042 / KPI0042) / 100) * -1    //2021-02-01 as per Eric C-2102-0026 

      //(KPI0096 + KPI0097) * (1 - (KPI0042 / KPI0042)) * -1    //2021-03-02 as per Eric Email

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_42,col];
        t1 = 1 - ( cmath.div(t1 , t1));
        t2 = arr[zKPI_96,col];
        t3 = arr[zKPI_97,col];
        t7 = (t2 + t3) * t1;
        arr[zKPI_98, col] = Math.Round(t7 * -1, ROUND5); 
      }
            
      SaveData(zKPI_98, "KPI0098");
      
     
      SumYTD_QTR(zKPI_98, ROUND5);

      SaveQuarter(zKPI_98, "KPI0098");

    }   //


    private void DoRow99()
    {

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_93,col];
        t2 = arr[zKPI_94,col];
        t3 = arr[zKPI_97,col];
        t4 = arr[zKPI_98,col];
        t1 = (t1 + t2 + t3 + t4);
        arr[zKPI_99,col] = Math.Round(t1, ROUND5);
      }

      SaveData(zKPI_99, "KPI0099");

     
      SumYTD_QTR(zKPI_99, ROUND5);

      SaveQuarter(zKPI_99, "KPI0099");

    }   //


    private void DoRow100()
    {

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_99,col];
        t2 = arr[zKPI_95,col];
        t1 = (t1 + t2);
        arr[zKPI_100,col] = Math.Round(t1, ROUND5);
      }

      SaveData(zKPI_100, "KPI0100");

      SumYTD_QTR(zKPI_100, ROUND5);

      SaveQuarter(zKPI_100, "KPI0100");

    }   //


    private void DoRow101()
    {

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_91,col];
        t2 = arr[zKPI_92,col];
        t3 = arr[zKPI_93,col];
        t4 = arr[zKPI_94,col];
        t5 = arr[zKPI_95,col];
        t6 = arr[zKPI_97,col];
        t7 = arr[zKPI_98,col];
        t1 = (t1 + t2 + t3 + t4 + t5 + t6 + t7);
        arr[zKPI_101,col] = Math.Round(t1, ROUND5);
      }

      SaveData(zKPI_101, "KPI0101");

   
      SumYTD_QTR(zKPI_101, ROUND5);


      SaveQuarter(zKPI_101, "KPI0101");

    } //


    private void DoRow103()
    {
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_90,col];
        t2 = arr[zKPI_77,col];
        t3 = (t1 - t2);
        arr[zKPI_103,col] = Math.Round(t3, ROUND5);
      }

      SaveData(zKPI_103, "KPI0103");

    
      SumYTD_QTR(zKPI_103, ROUND5);

      SaveQuarter(zKPI_103, "KPI0103");

    } //


    private void DoRow104()
    {

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_91,col];
        t2 = arr[zKPI_78,col];
        t3 = (t1 - t2);
        arr[zKPI_104,col] = Math.Round(t3, ROUND5);
      }

      SaveData(zKPI_104, "KPI0104");

      SumYTD_QTR(zKPI_104, ROUND5);

      SaveQuarter(zKPI_104, "KPI0104");

    } //


    private void DoRow105()
    {
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_92,col];
        t2 = arr[zKPI_79,col];
        t3 = (t1 - t2);
        arr[zKPI_105,col] = Math.Round(t3, ROUND5);
      }

      SaveData(zKPI_105, "KPI0105");

      SumYTD_QTR(zKPI_105, ROUND5);

      SaveQuarter(zKPI_105, "KPI0105");

    }


    private void DoRow106()
    {
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_93,col];
        t2 = arr[zKPI_80,col];
        t3 = (t1 - t2);
        arr[zKPI_106, col] = Math.Round(t3, ROUND5);
      }

      SaveData(zKPI_106, "KPI0106");

      SumYTD_QTR(zKPI_106, ROUND5);

      SaveQuarter(zKPI_106, "KPI0106");

    }//


    private void DoRow107()
    {
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_94,col];
        t2 = arr[zKPI_81,col];
        t3 = (t1 - t2);
        arr[zKPI_107,col] = Math.Round(t3, ROUND5);
      }

      SaveData(zKPI_107, "KPI0107");

      SumYTD_QTR(zKPI_107, ROUND5);

      SaveQuarter(zKPI_107, "KPI0107");

    }


    private void DoRow108()
    {

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_95,col];
        t2 = arr[zKPI_82,col];
        t3 = (t1 - t2);
        arr[zKPI_108,col] = Math.Round(t3, ROUND5);
      }

      SaveData(zKPI_108, "KPI0108");


      SumYTD_QTR(zKPI_108, ROUND5);

      SaveQuarter(zKPI_108, "KPI0108");

    }


    private void DoRow109()
    {
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_96,col];
        t2 = arr[zKPI_83,col];
        t3 = (t1 - t2);
        arr[zKPI_109,col] = Math.Round(t3, ROUND5);
      }

      SaveData(zKPI_109, "KPI0109");

    
      SumYTD_QTR(zKPI_109, ROUND5);

      SaveQuarter(zKPI_109, "KPI0109");

    }//


    private void DoRow110()
    {

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_97,col];
        t2 = arr[zKPI_84,col];
        t3 = (t1 - t2);
        arr[zKPI_110,col] = Math.Round(t3, ROUND5);
      }

      SaveData(zKPI_110, "KPI0110");

      SumYTD_QTR(zKPI_110, ROUND5);

      SaveQuarter(zKPI_110, "KPI0110");

    } //


    private void DoRow111()
    {
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_98,col];
        t2 = arr[zKPI_85,col];
        t3 = (t1 - t2);
        arr[zKPI_111,col] = Math.Round(t3, ROUND5);
      }

      SaveData(zKPI_111, "KPI0111");

    
      SumYTD_QTR(zKPI_111, ROUND5);

      SaveQuarter(zKPI_111, "KPI0111");

    } //


    private void DoRow112()
    {

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_99,col];
        t2 = arr[zKPI_86,col];
        t3 = (t1 - t2);
        arr[zKPI_112,col] = Math.Round(t3, ROUND5);
      }

      SaveData(zKPI_112, "KPI0112");

      SumYTD_QTR(zKPI_112, ROUND5);

      SaveQuarter(zKPI_112, "KPI0112");

    }


    private void DoRow113()
    {
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_100,col];
        t2 = arr[zKPI_87,col];
        t3 = (t1 - t2);
        arr[zKPI_113,col] = Math.Round(t3, ROUND5);
      }

      SaveData(zKPI_113, "KPI0113");

   
      SumYTD_QTR(zKPI_113, ROUND5);

      SaveQuarter(zKPI_113, "KPI0113");

    } //


    private void DoRow114()
    {

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_101,col];
        t2 = arr[zKPI_88,col];
        t3 = (t1 - t2);
        arr[zKPI_114,col] = Math.Round(t3, ROUND5);
      }

      SaveData(zKPI_114, "KPI0114");

      SumYTD_QTR(zKPI_114, ROUND5);

      SaveQuarter(zKPI_114, "KPI0114");

    } //


    private void DoRow116()
    {

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zBP_BQR,col];
        t2 = arr[zBP_BPSY,col];
        t3 = ((t1 * t2) / 100);
        arr[zKPI_116,col] = Math.Round(t3, ROUND5);
      }

      SaveData(zKPI_116, "KPI0116");

      //CalcByHrs_YTD_QTR(zKPI_116, ROUND5);
      DoRow116_QCalc();

      SaveQuarter(zKPI_116, "KPI0116");

    } //


    private void DoRow116_QCalc(){

      //quarterly
      for (col = FROM_Q1; col <= TO_Q4; col++)
      {
        t1 = arr[zBP_BQR, col];
        t2 = arr[zBP_BPSY, col];
        t3 = ((t1 * t2) / 100);
        arr[zKPI_116, col] = Math.Round(t3, ROUND5);
      }


      //ytd
      for (col = FROM_YTD; col <= FROM_YTD; col++)
      {
        t1 = arr[zBP_BQR, col];
        t2 = arr[zBP_BPSY, col];
        t3 = ((t1 * t2) / 100);
        arr[zKPI_116, col] = Math.Round(t3, ROUND5);
      }
    }

    private void DoRow117()
    {

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_45,col];
        t3 = (t1 * 24);
        arr[zKPI_117,col] = Math.Round(t3, ROUND5);
      }

      SaveData(zKPI_117, "KPI0117");

      //CalcByHrs_YTD_QTR(zKPI_117, ROUND5);
      DoRow117_QCalc();

      SaveQuarter(zKPI_117, "KPI0117");

    } //

    

    private void DoRow117_QCalc()
    {
      //quarterly
      for (col = FROM_Q1; col <= TO_Q4; col++)
      {
        t1 = arr[zKPI_45, col];
        t3 = (t1 * 24);
        arr[zKPI_117, col] = Math.Round(t3, ROUND5);
      }


      //ytd
      for (col = FROM_YTD; col <= FROM_YTD; col++)
      {
        t1 = arr[zKPI_45, col];
        t3 = (t1 * 24);
        arr[zKPI_117, col] = Math.Round(t3, ROUND5);
      }

    }


     private void DoRow118()
    {

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_46,col];
        t3 = (t1 * 24);
        arr[zKPI_118, col] = Math.Round(t3, ROUND5);
      }

      SaveData(zKPI_118, "KPI0118");


      //CalcByHrs_YTD_QTR(zKPI_118, ROUND5);
      DoRow118_QCalc();

      SaveQuarter(zKPI_118, "KPI0118");

    } //


    private void DoRow118_QCalc(){

      //quarterly
      for (col = FROM_Q1; col <= TO_Q4; col++)
      {
        t1 = arr[zKPI_46, col];
        t3 = (t1 * 24);
        arr[zKPI_118, col] = Math.Round(t3, ROUND5);
      }


      //ytd
      for (col = FROM_YTD; col <= FROM_YTD; col++)
      {
        t1 = arr[zKPI_46, col];
        t3 = (t1 * 24);
        arr[zKPI_118, col] = Math.Round(t3, ROUND5);
      }

    } //


    private void DoRow119()
    {

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_35, col] * 24;
        
        arr[zKPI_119,col] = Math.Round(t1, ROUND5);
      }

      SaveData(zKPI_119, "KPI0119");


      //CalcByHrs_YTD_QTR(zKPI_119, ROUND5);
      DoRow119_QCalc();

      SaveQuarter(zKPI_119, "KPI0119");

    } //


    private void DoRow119_QCalc(){

      //quarterly
      for (col = FROM_Q1; col <= TO_Q4; col++)
      {
        t1 = arr[zKPI_35, col];
        t3 = (t1 * 24);
        arr[zKPI_119, col] = Math.Round(t3, ROUND5);
      }


      //ytd
      for (col = FROM_YTD; col <= FROM_YTD; col++)
      {
        t1 = arr[zKPI_35, col];
        t3 = (t1 * 24);
        arr[zKPI_119, col] = Math.Round(t3, ROUND5);
      }
    }


    private void DoKPI_120()
    {
      //(CMDT / ( CMDT + PTS + PLO + PLAP ) ) *KPI0092

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_CMDT,col];
        t2 = arr[zKPI_PTS,col];
        t3 = arr[zKPI_PLO,col];
        t4 = arr[zKPI_PLAP,col];
        t5 = arr[zKPI_92, col];
        t1 = cmath.div(t1, (t1 + t2 + t3 + t4)) * t5;
        arr[zKPI_120,col] = Math.Round(t1, ROUND5);
      }

      SaveData(zKPI_120, "KPI0120");

     
      SumYTD_QTR(zKPI_120, ROUND5);

      SaveQuarter(zKPI_120, "KPI0120");

    }//


    private void DoKPI_121()
    {
      //2021-03-18
      //(PTS / (CMDT + PTS + PLO + PLAP)) * KPI0092

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_PTS,col];
        t2 = arr[zKPI_CMDT,col];
        t3 = arr[zKPI_PLO,col];
        t4 = arr[zKPI_PLAP,col];
        t5 = arr[zKPI_92, col];
        t1 = cmath.div(t1, (t1 + t2 + t3 + t4)) * t5;
        arr[zKPI_121, col] = Math.Round(t1, ROUND5);
      }

      SaveData(zKPI_121, "KPI0121");

      
      SumYTD_QTR(zKPI_121, ROUND5);

      SaveQuarter(zKPI_121, "KPI0121");

    }//


    private void DoKPI_122()
    {
      //2021-03-18
      //(PLO / ( CMDT + PTS + PLO + PLAP ) ) * KPI0092

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_PLO,col];
        t2 = arr[zKPI_CMDT,col];
        t3 = arr[zKPI_PTS,col];
        t4 = arr[zKPI_PLAP, col];
        t5 = arr[zKPI_92,col];
        t1 = cmath.div(t1, (t1 + t2 + t3 + t4)) * t5;
        arr[zKPI_122,col] = Math.Round(t1, ROUND5);
      }

      SaveData(zKPI_122, "KPI0122");

    
      SumYTD_QTR(zKPI_122, ROUND5);

      SaveQuarter(zKPI_122, "KPI0122");

    } //


    private void DoKPI_123()
    {
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_PLMT,col];
        t2 = arr[zKPI_PLPS,col];
        t3 = arr[zKPI_PIBP,col];
        t4 = arr[zKPI_UPLE,col];
        t5 = arr[zKPI_PROC,col];
        t6 = arr[zKPI_93,col];
        t1 = cmath.div(t1, (t1 + t2 + t3 + t4 + t5)) * t6;
        arr[zKPI_123,col] = Math.Round(t1, ROUND5);
      }

      SaveData(zKPI_123, "KPI0123");

     
      SumYTD_QTR(zKPI_123, ROUND5);

      SaveQuarter(zKPI_123, "KPI0123");


    }//


    private void DoKPI_124()
    {
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_PLPS,col];
        t2 = arr[zKPI_PLMT,col];
        t3 = arr[zKPI_PIBP,col];
        t4 = arr[zKPI_UPLE,col];
        t5 = arr[zKPI_PROC,col];
        t6 = arr[zKPI_93,col];
        t1 = cmath.div(t1, (t1 + t2 + t3 + t4 + t5)) * t6;
        arr[zKPI_124, col] = Math.Round(t1, ROUND5);
      }

      SaveData(zKPI_124, "KPI0124");

      
      SumYTD_QTR(zKPI_124, ROUND5);

      SaveQuarter(zKPI_124, "KPI0124");


    }


    private void DoKPI_125()
    {
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_PIBP,col];
        t2 = arr[zKPI_PLMT,col];
        t3 = arr[zKPI_PLPS,col];
        t4 = arr[zKPI_UPLE,col];
        t5 = arr[zKPI_PROC,col];
        t6 = arr[zKPI_93,col];
        t1 = cmath.div(t1, (t1 + t2 + t3 + t4 + t5)) * t6;
        arr[zKPI_125,col] = Math.Round(t1, ROUND5);
      }

      SaveData(zKPI_125, "KPI0125");

     
      SumYTD_QTR(zKPI_125, ROUND5);

      SaveQuarter(zKPI_125, "KPI0125");


    }//


    private void DoKPI_126()
    {

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_UPLE,col];
        t2 = arr[zKPI_PLMT,col];
        t3 = arr[zKPI_PLPS,col];
        t4 = arr[zKPI_PIBP,col];
        t5 = arr[zKPI_PROC,col];
        t6 = arr[zKPI_93,col];
        t1 = cmath.div(t1, (t1 + t2 + t3 + t4 + t5)) * t6;
        arr[zKPI_126,col] = Math.Round(t1, ROUND5);
      }

      SaveData(zKPI_126, "KPI0126");

      
      SumYTD_QTR(zKPI_126, ROUND5);

      SaveQuarter(zKPI_126, "KPI0126");

    }//


    private void DoKPI_127()
    {
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_PROC,col];
        t2 = arr[zKPI_PLMT,col];
        t3 = arr[zKPI_PLPS,col];
        t4 = arr[zKPI_PIBP,col];
        t5 = arr[zKPI_UPLE,col];
        t6 = arr[zKPI_93,col];
        t1 = cmath.div(t1, (t1 + t2 + t3 + t4 + t5)) * t6;
        arr[zKPI_127,col] = Math.Round(t1, ROUND5);
      }

      SaveData(zKPI_127, "KPI0127");

     
      SumYTD_QTR(zKPI_127, ROUND5);

      SaveQuarter(zKPI_127, "KPI0127");

    }//


    private void DoKPI_128()
    {
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_DKL,col];
        t2 = arr[zKPI_TBS,col];
        t3 = arr[zKPI_94,col];
        t1 = cmath.div(t1, (t1 + t2)) * t3;
        arr[zKPI_128,col] = Math.Round(t1, ROUND5);
      }

      SaveData(zKPI_128, "KPI0128");
      
        SumYTD_QTR(zKPI_128, ROUND5);

      SaveQuarter(zKPI_128, "KPI0128");

    } //


    private void DoKPI_129()
    {
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_TBS,col];
        t2 = arr[zKPI_DKL,col];
        t3 = arr[zKPI_94,col];
        t1 = cmath.div(t1, (t1 + t2)) * t3;
        arr[zKPI_129, col] = Math.Round(t1, ROUND5);
      }

      SaveData(zKPI_129, "KPI0129");

      
      SumYTD_QTR(zKPI_129, ROUND5);

      SaveQuarter(zKPI_129, "KPI0129");

    }//


    private void DoKPI_130()
    {
      // BPCS / (BPTS1 + BPCS + BPTP + BPOS) * KPI0079


      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zBP_BPCS, col];
        t2 = arr[zBP_BPTS1, col];
        t3 = arr[zBP_BPCS, col];
        t4 = arr[zBP_BPTP, col];
        t5 = arr[zBP_BPOS, col];
        t6 = arr[zKPI_79, col];
        t7 = cmath.div(t1, (t2  + t3 + t4 + t5))  * t6  ;
        arr[zKPI_130, col] = Math.Round(t7, ROUND5);
      }

      SaveData(zKPI_130, "KPI0130");

        SumYTD_QTR(zKPI_130, ROUND5);

      SaveQuarter(zKPI_130, "KPI0130");

    }//



    private void DoKPI_131()
    {
      //2021-09-07
      // BPTP / (BPTS1 + BPCS + BPTP + BPOS) * KPI0079

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zBP_BPTP, col];
        t2 = arr[zBP_BPTS1, col];
        t3 = arr[zBP_BPCS, col];
        t4 = arr[zBP_BPTP, col];
        t5 = arr[zBP_BPOS, col];
        t6 = arr[zKPI_79, col];
        t7 = cmath.div(t1, (t2 + t3 + t4 + t5 ) ) * t6 ;

        arr[zKPI_131, col] = Math.Round(t7, ROUND5);
      }

      SaveData(zKPI_131, "KPI0131");

      
        SumYTD_QTR(zKPI_131, ROUND5);

      SaveQuarter(zKPI_131, "KPI0131");

    }//


    private void DoKPI_132()
    {

      // 2021-09-27
      // BPOS/ (BPTS1 + BPCS + BPTP + BPOS) * KPI0079


      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zBP_BPOS, col];
        t2 = arr[zBP_BPTS1, col];
        t3 = arr[zBP_BPCS, col];
        t4 = arr[zBP_BPTP, col];
        t5 = arr[zBP_BPOS, col];
        t6 = arr[zKPI_79, col];
        t7 = cmath.div( t1 , (t2 + t3 + t4 + t5 )) * t6;
        arr[zKPI_132, col] = Math.Round(t7, ROUND5);
      }


      SaveData(zKPI_132, "KPI0132");

     
        SumYTD_QTR(zKPI_132, ROUND5);

      SaveQuarter(zKPI_132, "KPI0132");

    }//


    private void DoKPI_133()
    {
      // KPI0120 - KPI0130

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_120, col];
        t2 = arr[zKPI_130, col];
        t1 = (t1 - t2);
        arr[zKPI_133, col] = Math.Round(t1, ROUND5);
      }

      SaveData(zKPI_133, "KPI0133");

      
        SumYTD_QTR(zKPI_133, ROUND5);

      SaveQuarter(zKPI_133, "KPI0133");

    }//


    private void DoKPI_134()
    {

       //KPI0121 - KPI0131

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_121, col];
        t2 = arr[zKPI_131, col];
        t1 = (t1 - t2);
        arr[zKPI_134, col] = Math.Round(t1, ROUND5);
      }

      SaveData(zKPI_134, "KPI0134");

   
        SumYTD_QTR(zKPI_134, ROUND5);

      SaveQuarter(zKPI_134, "KPI0134");

    }//


    private void DoKPI_135()
    {

      // KPI0122 - KPI0132

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_122, col];
        t2 = arr[zKPI_132, col];
        t1 = (t1 - t2);
        arr[zKPI_135, col] = Math.Round(t1, ROUND5);
      }

      SaveData(zKPI_135, "KPI0135");

    
      SumYTD_QTR(zKPI_135, ROUND5);

      SaveQuarter(zKPI_135, "KPI0135");

    } //


    private void DoKPI_153()
    {
      // = KPI0057-BOE

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_57, col];
        t2 = arr[zBP_BOE, col];
        t1 = (t1 - t2);
        arr[zKPI_153, col] = Math.Round(t1, ROUND5);
      }

        SaveData(zKPI_153, "KPI0153");

  
        DoRow153_Qcalc();
        
        
        SaveQuarter(zKPI_153, "KPI0153");

    }//


    private void DoRow153_Qcalc()       //C-2210-0105
    {
      //quarterly
      for (col = FROM_Q1; col <= TO_Q4; col++)
      {
        t1 = arr[zKPI_57, col];
        t2 = arr[zBP_BOE, col];
        t1 = (t1 - t2);
        arr[zKPI_153, col] = Math.Round(t1, ROUND5);
      }


      //ytd
      for (col = FROM_YTD; col <= FROM_YTD; col++)
      {
        t1 = arr[zKPI_57, col];
        t2 = arr[zBP_BOE, col];
        t1 = (t1 - t2);
        arr[zKPI_153, col] = Math.Round(t1, ROUND5);

      }
    }


      private void DoKPI_155()
    {
      //= 100 *  (KPI0038 / KPI0043) / (BPSY / BQR)

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_38, col];
        t2 = arr[zKPI_43, col];
        t3 = arr[zBP_BPSY, col];
        t4 = arr[zBP_BQR, col];
        t5 = 0;
        if ( (t2 != 0) && (t4 != 0))
          t5 = 100 * (t1 / t2) / (t3 / t4);
        
        arr[zKPI_155, col] = Math.Round(t5, ROUND5);
      }

      SaveData(zKPI_155, "KPI0155");

        DoRow155_Qcalc();
        
      SaveQuarter(zKPI_155, "KPI0155");

    }//


    private void DoRow155_Qcalc()       //C-2210-0105
    {
      //quarterly
      for (col = FROM_Q1; col <= TO_Q4; col++)
      {
        t1 = arr[zKPI_38, col];
        t2 = arr[zKPI_43, col];
        t3 = arr[zBP_BPSY, col];
        t4 = arr[zBP_BQR, col];
        t5 = 0;
        if ((t2 != 0) && (t4 != 0))
          t5 = 100 * (t1 / t2) / (t3 / t4);

        arr[zKPI_155, col] = Math.Round(t5, ROUND5);
      }


      //ytd
      for (col = FROM_YTD; col <= FROM_YTD; col++)
      {
        t1 = arr[zKPI_38, col];
        t2 = arr[zKPI_43, col];
        t3 = arr[zBP_BPSY, col];
        t4 = arr[zBP_BQR, col];
        t5 = 0;
        if ((t2 != 0) && (t4 != 0))
          t5 = 100 * (t1 / t2) / (t3 / t4);

        arr[zKPI_155, col] = Math.Round(t5, ROUND5);
      }

    }//



    private void DoKPI_156()
    {
      
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_66, col];
        t2 = arr[zKPI_60, col];
        t1 = (t1 * ((t2 / 100) - 1) );
        arr[zKPI_156, col] = Math.Round(t1, ROUND5);
      }

      SaveData(zKPI_156, "KPI0156");

      DoRow156_Qcalc();
        
      SaveQuarter(zKPI_156, "KPI0156");

    }//
    


    private void DoRow156_Qcalc()       //C-2210-0105
    {

      //quarterly
      for (col = FROM_Q1; col <= TO_Q4; col++)
      {
        t1 = arr[zKPI_66, col];
        t2 = arr[zKPI_60, col];
        t1 = (t1 * ((t2 / 100) - 1));
        arr[zKPI_156, col] = Math.Round(t1, ROUND5);
      }

      //ytd
      for (col = FROM_YTD; col <= FROM_YTD; col++)
      {
        t1 = arr[zKPI_66, col];
        t2 = arr[zKPI_60, col];
        t1 = (t1 * ((t2 / 100) - 1));
        arr[zKPI_156, col] = Math.Round(t1, ROUND5);
      }

    }//


    private void DoKPI_150()
    {

      //=  KPI0056-BPOME

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_56, col];
        t2 = arr[zBP_BPOME, col];
        t1 = (t1 - t2);
        arr[zKPI_150, col] = Math.Round(t1, ROUND5);
      }

      SaveData(zKPI_150, "KPI0150");

       DoRow150_Qcalc();
      
      SaveQuarter(zKPI_150, "KPI0150");

    }//


    private void DoRow150_Qcalc()
    {
      //quarterly
      for (col = FROM_Q1; col <= TO_Q4; col++)
      {
        t1 = arr[zKPI_56, col];
        t2 = arr[zBP_BPOME, col];
        t1 = (t1 - t2);
        arr[zKPI_150, col] = Math.Round(t1, ROUND5);
      }


      //ytd
      for (col = FROM_YTD; col <= FROM_YTD; col++)
      {
        t1 = arr[zKPI_56, col];
        t2 = arr[zBP_BPOME, col];
        t1 = (t1 - t2);
        arr[zKPI_150, col] = Math.Round(t1, ROUND5);
      }

    }//


    private void DoKPI_151()
    {

      //= ( (KPI0052 - BNPP) / (  (KPI0052 - BNPP) + (KPI0043 - BQR) )  ) * KPI0150

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_52, col];
        t2 = arr[zBP_BNPP, col];
        t3 = arr[zKPI_43, col];
        t4 = arr[zBP_BQR, col];
        t5 = arr[zKPI_150, col];

        t7 = cmath.div( (t1 - t2) , ((t1 - t2) + (t3 - t4) ) ) * t5;

        arr[zKPI_151, col] = Math.Round(t7, ROUND5);
      }

      SaveData(zKPI_151, "KPI0151");

      DoRow151_Qcalc(); 
        
      SaveQuarter(zKPI_151, "KPI0151");

    }//


    private void DoRow151_Qcalc()
    {

      //quarterly
      for (col = FROM_Q1; col <= TO_Q4; col++)
      {
        t1 = arr[zKPI_52, col];
        t2 = arr[zBP_BNPP, col];
        t3 = arr[zKPI_43, col];
        t4 = arr[zBP_BQR, col];
        t5 = arr[zKPI_150, col];

        t7 = cmath.div((t1 - t2), ((t1 - t2) + (t3 - t4))) * t5;

        arr[zKPI_151, col] = Math.Round(t7, ROUND5);
      }


      //ytd
      for (col = FROM_YTD; col <= FROM_YTD; col++)
      {
        t1 = arr[zKPI_52, col];
        t2 = arr[zBP_BNPP, col];
        t3 = arr[zKPI_43, col];
        t4 = arr[zBP_BQR, col];
        t5 = arr[zKPI_150, col];

        t7 = cmath.div((t1 - t2), ((t1 - t2) + (t3 - t4))) * t5;

        arr[zKPI_151, col] = Math.Round(t7, ROUND5);
      }

    }//



    private void DoKPI_152()
    {

       //= ((KPI0043 - BQR) / (((KPI0052 - BNPP) + (KPI0043 - BQR)))) * KPI0150

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_43, col];
        t2 = arr[zBP_BQR, col];
        t3 = arr[zKPI_52, col];
        t4 = arr[zBP_BNPP, col];
        t5 = arr[zKPI_150, col];
        t7 = cmath.div((t1 - t2), ((t3 - t4) + (t1 - t2))) * t5;
        arr[zKPI_152, col] = Math.Round(t7, ROUND5);
      }

      SaveData(zKPI_152, "KPI0152");

   
      DoRow152_Qcalc();
       

      SaveQuarter(zKPI_152, "KPI0152");

    }//


    private void DoRow152_Qcalc()
    {
      //quarterly
      for (col = FROM_Q1; col <= TO_Q4; col++)
      {
        t1 = arr[zKPI_43, col];
        t2 = arr[zBP_BQR, col];
        t3 = arr[zKPI_52, col];
        t4 = arr[zBP_BNPP, col];
        t5 = arr[zKPI_150, col];
        t7 = cmath.div((t1 - t2), ((t3 - t4) + (t1 - t2))) * t5;
        arr[zKPI_152, col] = Math.Round(t7, ROUND5);
      }


      //ytd
      for (col = FROM_YTD; col <= FROM_YTD; col++)
      {
        t1 = arr[zKPI_43, col];
        t2 = arr[zBP_BQR, col];
        t3 = arr[zKPI_52, col];
        t4 = arr[zBP_BNPP, col];
        t5 = arr[zKPI_150, col];
        t7 = cmath.div((t1 - t2), ((t3 - t4) + (t1 - t2))) * t5;
        arr[zKPI_152, col] = Math.Round(t7, ROUND5);
      }

    }//



    private void DoKPI_154()
    {
      // = KPI0153-KPI0152-KPI0151

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_153, col];
        t2 = arr[zKPI_152, col];
        t3 = arr[zKPI_151, col];

        t7 = (t1 - t2 - t3);
        arr[zKPI_154, col] = Math.Round(t7, ROUND5);
      }

      SaveData(zKPI_154, "KPI0154");

      DoRow154_Qcalc();
       
      SaveQuarter(zKPI_154, "KPI0154");

    }//

    private void DoRow154_Qcalc()
    {
      //quarterly
      for (col = FROM_Q1; col <= TO_Q4; col++)
      {
        t1 = arr[zKPI_153, col];
        t2 = arr[zKPI_152, col];
        t3 = arr[zKPI_151, col];

        t7 = (t1 - t2 - t3);
        arr[zKPI_154, col] = Math.Round(t7, ROUND5);
      }


      //ytd
      for (col = FROM_YTD; col <= FROM_YTD; col++)
      {
        t1 = arr[zKPI_153, col];
        t2 = arr[zKPI_152, col];
        t3 = arr[zKPI_151, col];

        t7 = (t1 - t2 - t3);
        arr[zKPI_154, col] = Math.Round(t7, ROUND5);
      }

    }//

    private void DoKPI_157()
    {
       //= (KPI0066 + KPI0156) * (KPI0061 - 1)    excel uses percentages so i have to ((KPI0061 / 100) - 1)

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_66, col];
        t2 = arr[zKPI_156, col];
        t3 = arr[zKPI_61, col];

        t7 = (t1 + t2) * ((t3 /100) - 1);

        arr[zKPI_157, col] = Math.Round(t7, ROUND5);
      }

      SaveData(zKPI_157, "KPI0157");

      DoRow157_Qcalc();
        
      SaveQuarter(zKPI_157, "KPI0157");

    }//


    private void DoRow157_Qcalc()
    {
       //= (KPI0066 + KPI0156) * (KPI0061 - 1)

      //quarterly
      for (col = FROM_Q1; col <= TO_Q4; col++)
      {
        t1 = arr[zKPI_66, col];
        t2 = arr[zKPI_156, col];
        t3 = arr[zKPI_61, col];

        t7 = (t1 + t2) * ((t3 / 100) - 1);

        arr[zKPI_157, col] = Math.Round(t7, ROUND5);
      }


      //ytd
      for (col = FROM_YTD; col <= FROM_YTD; col++)
      {
        t1 = arr[zKPI_66, col];
        t2 = arr[zKPI_156, col];
        t3 = arr[zKPI_61, col];

        t7 = (t1 + t2) * ((t3 / 100) - 1);

        arr[zKPI_157, col] = Math.Round(t7, ROUND5);

      }

    }


    private void DoKPI_158()
    {
      // = (KPI0066 + KPI0156 + KPI0157) * ((KPI0155 / 100) - 1)

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_66, col];
        t2 = arr[zKPI_156, col];
        t3 = arr[zKPI_157, col];
        t4 = arr[zKPI_155, col];

        t7 = (t1 + t2 + t3) * ((t4 /100) - 1);

        arr[zKPI_158, col] = Math.Round(t7, ROUND5);
      }

      SaveData(zKPI_158, "KPI0158");

   
      DoRow158_Qcalc();
        

      SaveQuarter(zKPI_158, "KPI0158");

    }//

    private void DoRow158_Qcalc()
    {

      //quarterly
      for (col = FROM_Q1; col <= TO_Q4; col++)
      {
        t1 = arr[zKPI_66, col];
        t2 = arr[zKPI_156, col];
        t3 = arr[zKPI_157, col];
        t4 = arr[zKPI_155, col];

        t7 = (t1 + t2 + t3) * ((t4 / 100) - 1);

        arr[zKPI_158, col] = Math.Round(t7, ROUND5);
      }


      //ytd
      for (col = FROM_YTD; col <= FROM_YTD; col++)
      {
        t1 = arr[zKPI_66, col];
        t2 = arr[zKPI_156, col];
        t3 = arr[zKPI_157, col];
        t4 = arr[zKPI_155, col];

        t7 = (t1 + t2 + t3) * ((t4 / 100) - 1);

        arr[zKPI_158, col] = Math.Round(t7, ROUND5);
      }

    }

    private void DoKPI_159()
    {

      //KPI0066 + KPI00156 + KPI0157 + KPI058

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_66, col];
        t2 = arr[zKPI_156, col];
        t3 = arr[zKPI_157, col];
        t4 = arr[zKPI_58, col];

        t7 = (t1 + t2 + t3 + t4);

        arr[zKPI_159, col] = Math.Round(t7, ROUND5);
      }

      SaveData(zKPI_159, "KPI0159");

      DoRow159_Qcalc();
       
      SaveQuarter(zKPI_159, "KPI0159");

    }


    private void DoRow159_Qcalc()
    {
      for (col = FROM_Q1; col <= TO_Q4; col++)
      {
        t1 = arr[zKPI_66, col];
        t2 = arr[zKPI_156, col];
        t3 = arr[zKPI_157, col];
        t4 = arr[zKPI_58, col];

        t7 = (t1 + t2 + t3 + t4);

        arr[zKPI_159, col] = Math.Round(t7, ROUND5);

      }

      //ytd
      for (col = FROM_YTD; col <= FROM_YTD; col++)
      {
        t1 = arr[zKPI_66, col];
        t2 = arr[zKPI_156, col];
        t3 = arr[zKPI_157, col];
        t4 = arr[zKPI_58, col];

        t7 = (t1 + t2 + t3 + t4);

        arr[zKPI_159, col] = Math.Round(t7, ROUND5);

      }

    } //



    private void DoKPI_137()
    {

      //2021-03-18 (PLAP / ( PLAP + CMDT + PTS + PLO ) * KPI0092

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_PLAP, col];
        t2 = arr[zKPI_CMDT, col];
        t3 = arr[zKPI_PTS, col];
        t4 = arr[zKPI_PLO, col];
        t5 = arr[zKPI_92, col];

        t7 = cmath.div(t1 , (t1 + t2 + t3 + t4)) * t5;

        arr[zKPI_137, col] = Math.Round(t7, ROUND5);

      }

      SaveData(zKPI_137, "KPI0137");

      SumYTD_QTR(zKPI_137, ROUND5);

      SaveQuarter(zKPI_137, "KPI0137");

    }//


    private void DoKPI_138()
    {
      //2021-03-19   KPI0137 - KPI0140

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_137, col];
        t2 = arr[zKPI_140, col];
        t7 = (t1 - t2);
        arr[zKPI_138, col] = Math.Round(t7, ROUND5);
      }

      SaveData(zKPI_138, "KPI0138");

  
      SumYTD_QTR(zKPI_138, ROUND5);

      SaveQuarter(zKPI_138, "KPI0138");

    }//


    private void DoKPI_139()
    {
      //2021-02-22      =TISS/(PLAP+TCSS+TISS)*KPI0079


      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_TISS, col];
        t2 = arr[zKPI_TCSS, col];
        t3 = arr[zKPI_PLAP, col];
        t4 = arr[zKPI_79, col];

        t7 = cmath.div(t1, (t1 + t2 + t3)) * t4;

        arr[zKPI_139, col] = Math.Round(t7, ROUND5);

      }

      SaveData(zKPI_139, "KPI0139");

      SumYTD_QTR(zKPI_139, ROUND5);

      SaveQuarter(zKPI_139, "KPI0139");

    }//


    private void DoKPI_140()
    {
      //2021-09-07 

       //BPTS1 / (BPTS1 + BPCS + BPTP + BPOS) * KPI0079


      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zBP_BPTS1, col];
        t2 = arr[zBP_BPTP, col];
        t3 = arr[zBP_BPCS, col];
        t4 = arr[zBP_BPOS, col];
        t5 = arr[zKPI_79, col];
        t7 = cmath.div(t1 , (t1 + t2 + t3 + t4)) * t5;
        arr[zKPI_140, col] = Math.Round(t7, ROUND5);
      }

      SaveData(zKPI_140, "KPI0140");

  
      SumYTD_QTR(zKPI_140, ROUND5);

      SaveQuarter(zKPI_140, "KPI0140");

    }//


    private void DoKPI_141()
    {
      //2021-02-22        =TCSS/(PLAP+TCSS+TISS)*KPI0092

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_TCSS, col];
        t2 = arr[zKPI_PLAP, col];
        t3 = arr[zKPI_TISS, col];
        t4 = arr[zKPI_92, col];

        t7 = cmath.div(t1, (t1 + t2 + t3)) * t4;

        arr[zKPI_141, col] = Math.Round(t7, ROUND5);

      }

      SaveData(zKPI_141, "KPI0141");

   
      SumYTD_QTR(zKPI_141, ROUND5);

      SaveQuarter(zKPI_141, "KPI0141");

    }//



    private void DoKPI_142()
    {
      //2021-02-22         =TISS/(PLAP+TCSS+TISS)*KPI0092

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_TISS, col];
        t2 = arr[zKPI_TCSS, col];
        t3 = arr[zKPI_PLAP, col];
        t4 = arr[zKPI_92, col];

        t7 = cmath.div(t1, (t1 + t2 + t3)) * t4;

        arr[zKPI_142, col] = Math.Round(t7, ROUND5);

      }

      SaveData(zKPI_142, "KPI0142");

      SumYTD_QTR(zKPI_142, ROUND5);

      SaveQuarter(zKPI_142, "KPI0142");

    }//



    private void DoKPI_143()
    {
     
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_138, col];
        t2 = arr[zKPI_141, col];
        t7 = (t1 - t2);
        arr[zKPI_143, col] = Math.Round(t7, ROUND5);
      }

      SaveData(zKPI_143, "KPI0143");


      SumYTD_QTR(zKPI_143, ROUND5);

      SaveQuarter(zKPI_143, "KPI0143");

    }//


    private void DoKPI_144()
    {
      //2021-02-22        = KPI0139-KPI0142

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_139, col];
        t2 = arr[zKPI_142, col];
        t7 = (t1 - t2);
        arr[zKPI_144, col] = Math.Round(t7, ROUND5);
      }

      SaveData(zKPI_144, "KPI0144");

      SumYTD_QTR(zKPI_144, ROUND5);

      SaveQuarter(zKPI_144, "KPI0144");

    }//


    private void DoKPI_145()
    {
      //KPI0081 * ( ( 1-BPMMM/100) / (1 - BPMMM/100) + (1-BPPPP/100)))

      //monthly
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_81, col];
        t2 = arr[zBP_BPMMM, col];
        t3 = arr[zBP_BPPPP, col];
        t7 = t1 * cmath.div((1 - t2 / 100), ((1 - t2 / 100) + (1 - t3 / 100)));
        arr[zKPI_145, col] = Math.Round(t7, ROUND5);
      }

      SaveData(zKPI_145, "KPI0145");

      SumYTD_QTR(zKPI_145, ROUND5);

      SaveQuarter(zKPI_145, "KPI0145");

    }//


    private void DoKPI_146()
    {
      //KPI0081 - KPI0145

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_81, col];
        t2 = arr[zKPI_145, col];
        t7 = t1 - t2;
        arr[zKPI_146, col] = Math.Round(t7, ROUND5);
      }

      SaveData(zKPI_146, "KPI0146");

      SumYTD_QTR(zKPI_146, ROUND5);

      SaveQuarter(zKPI_146, "KPI0146");

    }//


    private void DoKPI_147()
    {
      //KPI0128 - KPI0145

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_128, col];
        t2 = arr[zKPI_145, col];
        t7 = t1 - t2;
        arr[zKPI_147, col] = Math.Round(t7, ROUND5);
      }

      SaveData(zKPI_147, "KPI0147");

     
      //CalcByHrs_YTD_QTR(zKPI_147, ROUND5);      //relook at this
      DoRow147_QCalc();

      SaveQuarter(zKPI_147, "KPI0147");

    }//

    private void DoRow147_QCalc()
    {
      //quarterly
      for (col = FROM_Q1; col <= TO_Q4; col++)
      {
        t1 = arr[zKPI_128, col];
        t2 = arr[zKPI_145, col];
        t7 = t1 - t2;
        arr[zKPI_147, col] = Math.Round(t7, ROUND5);
      }

      //ytd
      for (col = FROM_YTD; col < FROM_YTD; col++)
      {
        t1 = arr[zKPI_128, col];
        t2 = arr[zKPI_145, col];
        t7 = t1 - t2;
        arr[zKPI_147, col] = Math.Round(t7, ROUND5);
      }
    }

    private void DoKPI_148()
    {
      //KPI0129 - KPI0146

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_129, col];
        t2 = arr[zKPI_146, col];
        t7 = t1 - t2;
        arr[zKPI_148, col] = Math.Round(t7, ROUND5);
      }

      SaveData(zKPI_148, "KPI0148");


      //CalcByHrs_YTD_QTR(zKPI_148, ROUND5);
      DoRow148_QCalc();

      SaveQuarter(zKPI_148, "KPI0148");

    }//


    private void DoRow148_QCalc()
    {

      //quarterly
      for (col = FROM_Q1; col <= TO_Q4; col++)
      {
        t1 = arr[zKPI_129, col];
        t2 = arr[zKPI_146, col];
        t7 = t1 - t2;
        arr[zKPI_148, col] = Math.Round(t7, ROUND5);
      }

      //ytd
      for (col = FROM_YTD; col < FROM_YTD; col++)
      {
        t1 = arr[zKPI_129, col];
        t2 = arr[zKPI_146, col];
        t7 = t1 - t2;
        arr[zKPI_148, col] = Math.Round(t7, ROUND5);
      }

    }


    private void DoKPI_149()
    {
      
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_107, col];
        t2 = arr[zKPI_110, col];
        t3 = arr[zKPI_111, col];
        t7 = t1 + t2 + t3;
        arr[zKPI_149, col] = Math.Round(t7, ROUND5);
      }

      SaveData(zKPI_149, "KPI0149");

      SumYTD_QTR(zKPI_149, ROUND5);

      SaveQuarter(zKPI_149, "KPI0149");

    }//



    private void DoKPI_160()
    {
      

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_83, col];
        t2 = arr[zKPI_108, col];
        t3 = arr[zKPI_107, col];
        t4 = arr[zKPI_110, col];
        t5 = arr[zKPI_111, col];
        t6 = arr[zKPI_106, col];
        t7 = t1 + t2 + t3 + t4 + t5 + t6;
        arr[zKPI_160, col] = Math.Round(t7, ROUND5);
      }

      SaveData(zKPI_160, "KPI0160");

      SumYTD_QTR(zKPI_160, ROUND5);

      SaveQuarter(zKPI_160, "KPI0160");

    }//



    private void DoKPI_161()
    {
      //2021-11-14 Daily Deviation     KPI0035 - NSPPM
      //string str = sSite + " " + sAreaID;

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_35, col];
        t2 = arr[zBP_NSPPM, col];
        t7 = (t1 - t2);
        arr[zKPI_161, col] = Math.Round(t7, ROUND5);
      }
      
      SaveData(zKPI_161, "KPI0161");

      SumYTD_QTR(zKPI_161, ROUND5);

      SaveQuarter(zKPI_161, "KPI0161");

    }//




    private void DoKPI_162()
    {
      //2021--11-14 Daily Deviation Perv    (KPI0035 / NSPPM) * 100


      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_35, col];
        t2 = arr[zBP_NSPPM, col];
        t7 = cmath.div(t1 , t2) * 100;
        arr[zKPI_162, col] = Math.Round(t7, ROUND5);
      }

      SaveData(zKPI_162, "KPI0162");


      DoRow162_QCalc();

      SaveQuarter(zKPI_162, "KPI0162");

    }//


    private void DoRow162_QCalc(){

      //quarterly
      for (col = FROM_Q1; col <= TO_Q4; col++)
      {
        t1 = arr[zKPI_35, col];
        t2 = arr[zBP_NSPPM, col];
        t7 = cmath.div(t1, t2) * 100;
        arr[zKPI_162, col] = Math.Round(t7, ROUND5);
      }

      //ytd
      for (col = FROM_YTD; col < FROM_YTD; col++)
      {
        t1 = arr[zKPI_35, col];
        t2 = arr[zBP_NSPPM, col];
        t7 = cmath.div(t1, t2) * 100;
        arr[zKPI_162, col] = Math.Round(t7, ROUND5);
      }
    }


    private void DoKPI_MSC1()
    {
      // just initialize for now to get a start

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        arr[zKPI_MSC1, col] = 0;
      }

      SaveData(zKPI_MSC1, "MSC1");

      SumYTD_QTR(zKPI_MSC1, ROUND5);

      SaveQuarter(zKPI_MSC1, "MSC1");

    }//



    private void DoKPI_MPDPOS()
    {
      // just initialize for now to get a start

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        arr[zKPI_MPDPOS, col] = 0;
      }

      SaveData(zKPI_MPDPOS, "MPDPOS");

      SumYTD_QTR(zKPI_MPDPOS, ROUND5);

      SaveQuarter(zKPI_MPDPOS, "MPDPOS");

    }//



    private void DoKPI_MISDM()
    {
      // IF(POR = 0, 0, KPI0138 + KPI0134)

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zBP_POR, col];
        t2 = arr[zKPI_138, col];
        t3 = arr[zKPI_134, col];
        t7 = t1 == 0 ? 0 : (t2 + t3);
        arr[zKPI_MISDM, col] = Math.Round(t7, ROUND5);
      }

      SaveData(zKPI_MISDM, "MISDM");

    
      SumYTD_QTR(zKPI_MISDM, ROUND5);

      SaveQuarter(zKPI_MISDM, "MISDM");

    }//



    private void DoKPI_MNSP()
    {

      //KPI0104 + KPI0105 + KPI0106 + KPI0108 + KPI0147 + KPI0148

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_104, col];
        t2 = arr[zKPI_105, col];
        t3 = arr[zKPI_106, col];
        t4 = arr[zKPI_108, col];
        t5 = arr[zKPI_147, col];
        t6 = arr[zKPI_148, col];
        t7 = (t1 + t2 + t3 + t4 + t5 + t6 );
        arr[zKPI_MNSP, col] = Math.Round(t7, ROUND5);
      }

      SaveData(zKPI_MNSP, "MNSP");

      SumYTD_QTR(zKPI_MNSP, ROUND5);

      SaveQuarter(zKPI_MNSP, "MNSP");

    }//



    private void DoKPI_MNSPP()
    {
      //MNSP + KPI0110
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_MNSP, col];
        t2 = arr[zKPI_110, col];
        t7 = (t1 + t2);
        arr[zKPI_MNSPP, col] = Math.Round(t7, ROUND5);
      }

      SaveData(zKPI_MNSPP, "MNSPP");

      SumYTD_QTR(zKPI_MNSPP, ROUND5);

      SaveQuarter(zKPI_MNSPP, "MNSPP");

    }//



    private void DoKPI_MPMJ()
    {
      //= KPI0104
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t7 = arr[zKPI_104, col];
        arr[zKPI_MPMJ, col] = Math.Round(t7, ROUND5);
      }

      SaveData(zKPI_MPMJ, "MPMJ");

      SumYTD_QTR(zKPI_MPMJ, ROUND5);

      SaveQuarter(zKPI_MPMJ, "MPMJ");

    } //



    private void DoKPI_MOJEDL()
    {


      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_106, col];
        t2 = arr[zKPI_108, col];
        t3 = arr[zKPI_148, col];
        t7 = t1 + t2 + t3;
        arr[zKPI_MOJEDL, col] = Math.Round(t7, ROUND5);
      }

      SaveData(zKPI_MOJEDL, "MOJEDL");

      SumYTD_QTR(zKPI_MOJEDL, ROUND5);

      SaveQuarter(zKPI_MOJEDL, "MOJEDL");

    }//



    private void DoKPI_MOJMPD()
    {
      //= KPI0147

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t7 = arr[zKPI_147, col];
        arr[zKPI_MOJMPD, col] = Math.Round(t7, ROUND5);
      }

      SaveData(zKPI_MOJMPD, "MOJMPD");

      SumYTD_QTR(zKPI_MOJMPD, ROUND5);

      SaveQuarter(zKPI_MOJMPD, "MOJMPD");

    }//


    private void DoKPI_MOJMDP()
    {
      //= KPI0110

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t7 = arr[zKPI_110, col];
        arr[zKPI_MOJMDP, col] = Math.Round(t7, ROUND5);
      }

      SaveData(zKPI_MOJMDP, "MOJMDP");

      SumYTD_QTR(zKPI_MOJMDP, ROUND5);

      SaveQuarter(zKPI_MOJMDP, "MOJMDP");

    }//



    private void DoKPI_MFJO()
    {
      //MSC1 * MPMJ / 1000000
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_MSC1, col];
        t2 = arr[zKPI_MPMJ, col];
        t7 = (t1 * t2) / 1000000;
        arr[zKPI_MFJO, col] = Math.Round(t7, ROUND5);
      }

      SaveData(zKPI_MFJO, "MFJO");

      SumYTD_QTR(zKPI_MFJO, ROUND5);

      SaveQuarter(zKPI_MFJO, "MFJO");

    } //


    private void DoKPI_MFJOED()
    {

      //MSC1 * MOJEDL / 1000000
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_MSC1, col];
        t2 = arr[zKPI_MOJEDL, col];
        t7 = (t1 * t2) / 1000000;
        arr[zKPI_MFJOED, col] = Math.Round(t7, ROUND5);
      }

      SaveData(zKPI_MFJOED, "MFJOED");

      SumYTD_QTR(zKPI_MFJOED, ROUND5);

      SaveQuarter(zKPI_MFJOED, "MFJOED");

    } //



    private void DoKPI_MFJOEDL()
    {
      //MSC1 * MOJMPD / 1000000

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_MSC1, col];
        t2 = arr[zKPI_MOJMPD, col];
        t7 = (t1 * t2) / 1000000;
        arr[zKPI_MFJOEDL, col] = Math.Round(t7, ROUND5);
      }

      SaveData(zKPI_MFJOEDL, "MFJOEDL");

      SumYTD_QTR(zKPI_MFJOEDL, ROUND5);

      SaveQuarter(zKPI_MFJOEDL, "MFJOEDL");


    }//



    private void DoKPI_MPDP()
    {
      //MPDPOS* MOJMPD / 1000000

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_MPDPOS, col];
        t2 = arr[zKPI_MOJMPD, col];
        t7 = (t1 * t2) / 1000000;
        arr[zKPI_MPDP, col] = Math.Round(t7, ROUND5);
      }

      SaveData(zKPI_MPDP, "MPDP");

      SumYTD_QTR(zKPI_MPDP, ROUND5);

      SaveQuarter(zKPI_MPDP, "MPDP");


    }//



    private void DoKPI_MNOJ()
    {

      //MFJOED + MFJOEDL + MPDP

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_MFJOED, col];
        t2 = arr[zKPI_MFJOEDL, col];
        t3 = arr[zKPI_MPDP, col];
        t7 = (t1 + t2 + t3);
        arr[zKPI_MNOJ, col] = Math.Round(t7, ROUND5);
      }

      SaveData(zKPI_MNOJ, "MNOJ");

      SumYTD_QTR(zKPI_MNOJ, ROUND5);

      SaveQuarter(zKPI_MNOJ, "MNOJ");

    }//



    private void DoKPI_MJSD()
    {
      //KPI0038 * MSC1 * MISDM / 100000000
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_38, col];
        t2 = arr[zKPI_MSC1, col];
        t3 = arr[zKPI_MISDM, col];
        t7 = (t1 * t2 * t3) / 100000000;
        arr[zKPI_MJSD, col] = Math.Round(t7, ROUND5);
      }

      SaveData(zKPI_MJSD, "MJSD");

      SumYTD_QTR(zKPI_MJSD, ROUND5);

      SaveQuarter(zKPI_MJSD, "MJSD");

    }//



    private void DoKPI_MJCS()
    {
      //KPI0133  * KPI0038 * MSC1 / 100000000
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_38, col];
        t2 = arr[zKPI_133, col];
        t3 = arr[zKPI_MSC1, col];
        t7 = (t1 * t2 * t3) / 100000000;
        arr[zKPI_MJCS, col] = Math.Round(t7, ROUND5);
      }

      SaveData(zKPI_MJCS, "MJCS");

      SumYTD_QTR(zKPI_MJCS, ROUND5);

      SaveQuarter(zKPI_MJCS, "MJCS");


    } //



    private void DoKPI_MFT()
    {
      //MFJO + MNOJ + MJSD + MJCS

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_MFJO, col];
        t2 = arr[zKPI_MNOJ, col];
        t3 = arr[zKPI_MJSD, col];
        t4 = arr[zKPI_MJCS, col];
        t7 = (t1 + t2 + t3 + t4);
        arr[zKPI_MFT, col] = Math.Round(t7, ROUND5);
      }

      SaveData(zKPI_MFT, "MFT");

      SumYTD_QTR(zKPI_MFT, ROUND5);

      SaveQuarter(zKPI_MFT, "MFT");

    }//



    private void DoKPI_MFAMAN()
    {
      //MFJOED + MPDP + MJSD + (MFJO * 0.5)
      
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_MFJOED, col];
        t2 = arr[zKPI_MPDP, col];
        t3 = arr[zKPI_MJSD, col];
        t4 = arr[zKPI_MFJO, col];
        t4 *= 0.5;
        t7 = (t1 + t2 + t3 + t4);
        arr[zKPI_MFAMAN, col] = Math.Round(t7, ROUND5);
      }

      SaveData(zKPI_MFAMAN, "MFAMAN");

      SumYTD_QTR(zKPI_MFAMAN, ROUND5);

      SaveQuarter(zKPI_MFAMAN, "MFAMAN");

    } //



    private void DoKPI_MFAMAR()
    {

      //(MFJO*0.5) + MFJOEDL + MJCS

      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 = arr[zKPI_MFJO, col];
        t1 *= 0.5;
        t2 = arr[zKPI_MFJOEDL, col];
        t3 = arr[zKPI_MJCS, col];
        t7 = (t1 + t2 + t3);
        arr[zKPI_MFAMAR, col] = Math.Round(t7, ROUND5);
      }

      SaveData(zKPI_MFAMAR, "MFAMAR");

      SumYTD_QTR(zKPI_MFAMAR, ROUND5);

      SaveQuarter(zKPI_MFAMAR, "MFAMAR");

    }//



    #endregion



    #region -----------------------SUNDRY-------------------------------------------

    private void DoCalendarTime(string sWeekDate)
    {
      int thisMonth = 0;
      double dHrs = 0;
      int thisYear = 0;

      cal = cal ?? new cCal();
      
      cal.GetThisMonthHrs(ref thisYear, ref dHrs, ref thisMonth);
    
      if (iSappiYear < thisYear)
      {
        thisMonth = 12;         //loop until the end of year because we are runing last year
      }

      int ndx = 1;

      //monthly
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        if (ndx <= thisMonth)
          arr[zKPI_CAL24, col] = arr[zSappiHours, col];
        else
          arr[zKPI_CAL24, col] = 0.0;

        ndx++;
      }


      if (iSappiYear == thisYear)
      {
        arr[zKPI_CAL24, FROM_MONTH + (thisMonth - 1)] = Math.Round(dHrs, ROUND1);   //update current month if running current year
      }


      if (boRunDaily == true)
      {
        arr[zKPI_CAL24, FROM_MONTH] = 24.0;   //24 hours for the day
      }

      if (boRunWeek == true)
      {
        
        cal.GetThisWeekHrs(sWeekDate, ref dHrs);
        arr[zKPI_CAL24, FROM_MONTH] = dHrs;   //running total for the week
      }

     

      SaveData(zKPI_CAL24, "CTT");

    
    }//



    private void SumYTD_QTR(int rowNum, int roundDec)
    {

      if (boRunMonth == false)
        return;

      t1 = 0.0;

      //YTD
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {
        t1 += arr[rowNum, col];
        //string str = arr[rowNum, col].ToString();
      }

      arr[rowNum, FROM_YTD] = Math.Round(t1, roundDec);


      //QUARTERS
      Q1 = 0; Q2 = 0; Q3 = 0; Q4 = 0;

      int ndx = 1;
      for (col = FROM_MONTH; col < MAX_COLS; col++)
      {

        if ((ndx >= 1) & (ndx <= 3))
          Q1 += arr[rowNum, col];

        if ((ndx >= 4) & (ndx <= 6))
          Q2 += arr[rowNum, col];

        if ((ndx >= 7) & (ndx <= 9))
          Q3 += arr[rowNum, col];

        if ((ndx >= 10) & (ndx <= 12))
          Q4 += arr[rowNum, col];

        ndx++;
      }

      arr[rowNum, FROM_Q1] = Math.Round(Q1, roundDec);
      arr[rowNum, FROM_Q1 + 1] = Math.Round(Q2, roundDec);
      arr[rowNum, FROM_Q1 + 2] = Math.Round(Q3, roundDec);
      arr[rowNum, FROM_Q1 + 3] = Math.Round(Q4, roundDec);

      //Showarr(rowNum);
    }//



    //Version 1
    private void CalcByHrs_YTD_QTR(int rowNum, int roundDec)
    {
      //weighted average using calendar

      //Showarr(zKPI_162);

      if (boRunMonth == false)
      {
        return;
      }

      if (rowNum == 213)
      {
        bool eee = false; 
      }
      //---
      Q1 = arr[rowNum, FROM_MONTH] * arr[zSappiHours, FROM_MONTH]
            + arr[rowNum, FROM_MONTH + 1] * arr[zSappiHours, FROM_MONTH + 1]
            + arr[rowNum, FROM_MONTH + 2] * arr[zSappiHours, FROM_MONTH + 2];

      Q1 = Math.Round(cmath.div(Q1, arr[zSappiHours, FROM_Q1]), roundDec);


      //---
      Q2 = arr[rowNum, FROM_MONTH + 3] * arr[zSappiHours, FROM_MONTH + 3]
            + arr[rowNum, FROM_MONTH + 4] * arr[zSappiHours, FROM_MONTH + 4]
            + arr[rowNum, FROM_MONTH + 5] * arr[zSappiHours, FROM_MONTH + 5];

      Showarr(rowNum);
      Q2 = Math.Round(cmath.div(Q2, arr[zSappiHours, FROM_Q1 + 1]), roundDec);


      //---
      Q3 = arr[rowNum, FROM_MONTH + 6] * arr[zSappiHours, FROM_MONTH + 6]
            + arr[rowNum, FROM_MONTH + 7] * arr[zSappiHours, FROM_MONTH + 7]
            + arr[rowNum, FROM_MONTH + 8] * arr[zSappiHours, FROM_MONTH + 8];

      Q3 = Math.Round(cmath.div(Q3, arr[zSappiHours, FROM_Q1 + 2]), roundDec);



      Q4 = arr[rowNum, FROM_MONTH + 9] * arr[zSappiHours, FROM_MONTH + 9]
              + arr[rowNum, FROM_MONTH + 10] * arr[zSappiHours, FROM_MONTH + 10]
              + arr[rowNum, FROM_MONTH + 11] * arr[zSappiHours, FROM_MONTH + 11];


      Q4 = Math.Round(cmath.div(Q4, arr[zSappiHours, FROM_Q1 + 3]), roundDec);


      arr[rowNum, FROM_Q1] = Q1;
      arr[rowNum, FROM_Q1 + 1] = Q2;
      arr[rowNum, FROM_Q1 + 2] = Q3;
      arr[rowNum, FROM_Q1 + 3] = Q4;


      t1 = arr[rowNum, FROM_Q1] * arr[zSappiHours, FROM_Q1]
            + arr[rowNum, FROM_Q1 + 1] * arr[zSappiHours, FROM_Q1 + 1]
            + arr[rowNum, FROM_Q1 + 2] * arr[zSappiHours, FROM_Q1 + 2]
            + arr[rowNum, FROM_Q1 + 3] * arr[zSappiHours, FROM_Q1 + 3];

      t2 = Math.Round(cmath.div(t1, arr[zSappiHours, FROM_YTD]), roundDec);


      arr[rowNum, FROM_YTD] = t2;

      Showarr(rowNum);

    }//



    //version 2
    private void CalcByHrs_YTD_QTR_NOZero(int rowNum, int roundDec)
    {
      //do not use the zero in calcs

      double sappiHrs = 0D;


      if (arr[rowNum, FROM_MONTH] > 0)
      {
        Q1 = arr[rowNum, FROM_MONTH] * arr[zSappiHours, FROM_MONTH];
        sappiHrs += arr[zSappiHours, FROM_MONTH];
      }

      if (arr[rowNum, FROM_MONTH + 1] > 0)
      {
        Q1 += arr[rowNum, FROM_MONTH + 1] * arr[zSappiHours, FROM_MONTH + 1];
        sappiHrs += arr[zSappiHours, FROM_MONTH + 1];
      }

      if (arr[rowNum, FROM_MONTH + 2] > 0)
      {
        Q1 += arr[rowNum, FROM_MONTH + 2] * arr[zSappiHours, FROM_MONTH + 2];
        sappiHrs += arr[zSappiHours, FROM_MONTH + 2];
      }

      Q1 = Math.Round(cmath.div(Q1, sappiHrs), roundDec);

      sappiHrs = 0D;


      if (arr[rowNum, FROM_MONTH + 3] > 0)
      {
        Q2 = arr[rowNum, FROM_MONTH + 3] * arr[zSappiHours, FROM_MONTH + 3];
        sappiHrs += arr[zSappiHours, FROM_MONTH + 3];
      }

      if (arr[rowNum, FROM_MONTH + 4] > 0)
      {
        Q2 += arr[rowNum, FROM_MONTH + 4] * arr[zSappiHours, FROM_MONTH + 4];
        sappiHrs += arr[zSappiHours, FROM_MONTH + 4];
      }

      if (arr[rowNum, FROM_MONTH + 5] > 0)
      {
        Q2 += arr[rowNum, FROM_MONTH + 5] * arr[zSappiHours, FROM_MONTH + 5];
        sappiHrs += arr[zSappiHours, FROM_MONTH + 5];
      }

      Q2 = Math.Round(cmath.div(Q2, sappiHrs), roundDec);

      sappiHrs = 0D;


      if (arr[rowNum, FROM_MONTH + 6] > 0)
      {
        Q3 = arr[rowNum, FROM_MONTH + 6] * arr[zSappiHours, FROM_MONTH + 6];
        sappiHrs += arr[zSappiHours, FROM_MONTH + 6];
      }

      if (arr[rowNum, FROM_MONTH + 7] > 0)
      {
        Q3 += arr[rowNum, FROM_MONTH + 7] * arr[zSappiHours, FROM_MONTH + 7];
        sappiHrs += arr[zSappiHours, FROM_MONTH + 7];
      }

      if (arr[rowNum, FROM_MONTH + 8] > 0)
      {
        Q3 += arr[rowNum, FROM_MONTH + 8] * arr[zSappiHours, FROM_MONTH + 8];
        sappiHrs += arr[zSappiHours, FROM_MONTH + 8];
      }

      Q3 = Math.Round(cmath.div(Q3, sappiHrs), roundDec);



      //Q4 = arr[rowNum,FROM_MONTH + 9] * arr[zSappiHours,FROM_MONTH + 9]
      //        + arr[rowNum,FROM_MONTH + 10] * arr[zSappiHours,FROM_MONTH + 10]
      //        + arr[rowNum,FROM_MONTH + 11] * arr[zSappiHours,FROM_MONTH + 11];

      if (arr[rowNum, FROM_MONTH + 9] > 0)
      {
        Q4 = arr[rowNum, FROM_MONTH + 9] * arr[zSappiHours, FROM_MONTH + 9];
        sappiHrs += arr[zSappiHours, FROM_MONTH + 9];
      }

      if (arr[rowNum, FROM_MONTH + 10] > 0)
      {
        Q4 += arr[rowNum, FROM_MONTH + 10] * arr[zSappiHours, FROM_MONTH + 10];
        sappiHrs += arr[zSappiHours, FROM_MONTH + 10];
      }

      if (arr[rowNum, FROM_MONTH + 11] > 0)
      {
        Q4 += arr[rowNum, FROM_MONTH + 11] * arr[zSappiHours, FROM_MONTH + 11];
        sappiHrs += arr[zSappiHours, FROM_MONTH + 11];
      }

      Q4 = Math.Round(cmath.div(Q4, sappiHrs), roundDec);


      arr[rowNum, FROM_Q1] = Q1;
      arr[rowNum, FROM_Q1 + 1] = Q2;
      arr[rowNum, FROM_Q1 + 2] = Q3;
      arr[rowNum, FROM_Q1 + 3] = Q4;


      t1 = arr[rowNum, FROM_Q1] * arr[zSappiHours, FROM_Q1]
            + arr[rowNum, FROM_Q1 + 1] * arr[zSappiHours, FROM_Q1 + 1]
            + arr[rowNum, FROM_Q1 + 2] * arr[zSappiHours, FROM_Q1 + 2]
            + arr[rowNum, FROM_Q1 + 3] * arr[zSappiHours, FROM_Q1 + 3];

      t2 = Math.Round(cmath.div(t1, arr[zSappiHours, FROM_YTD]), roundDec);


      arr[rowNum, FROM_YTD] = t2;


    }//



    public void SaveKPILastRun(string opt)
    {

      string sql = "";
        
      if (opt == "MONTH")
        sql = "Update Sundry set colValue = '" + cmath.getDateTimeNowStr() + "' where idx = 1";
      else
         sql = "Update Sundry set colValue = '" + cmath.getDateTimeNowStr() + "' where idx = 2";

      try
      {
        cSQL.OpenDB("ZAMDW");
        cSQL.ExecuteQuery(sql);
      }
      catch (Exception er)
      {
        cFG.LogIt( "Save Last KPI Run KPIJob " + er.Message);
        throw er;
      }
      finally
      {
        cSQL.CloseDB();
      }

    }



    #endregion



    #region SAVE DATA REGION----------------------------


    private void SaveData(int rowNum, string kpiCode)
    {

      string tmp = "";

      int iMonthID = iSqlMonthID;     //start of year

      int offset = 0;
      int count = 1;

      string sql = "";

      cSQL.OpenDB("ZAMDW");

      //if (kpiCode == "ABGP")
      //{
      //  Application.DoEvents();
      //}

      try
      {

        //array format
        //|0   |1  |2  |3  |4  |5   |6  |7  |8  |9  |10 |11 |12 |13|14 |15 |16 |
        //|ytd |q1 |q2 |q3 |q4 | m1 |m2 |m3 |m4 |m5 |m6 |m7 |m8 |m9 |m10|m11|m12|

        if (boRunMonth == true)
        {

          offset = iRunMonth - 1;
          iMonthID = iMonthID + offset;
          count = iRunMonth;

          for (col = FROM_MONTH + offset; col < MAX_COLS; col++)
          {
            if (count <= currMonth)     //save up to the current month
            {
              //tmp = arr[rowNum, col].ToString();
              sql = "UPDATE KPIMonthly set KPI_VALUE = " + arr[rowNum, col];
              sql += " , LASTUPDATED = '" + cmath.getDateTimeNowStr() + "'";
              sql += " where Mill_Code = '" + sSite + "'";
              sql += " and KPI_CODE = '" + kpiCode + "'";
              sql += " and SappiMonth_ID  = " + iMonthID;
              sql += " and FLID = " + sAreaID;
              sql += " IF @@ROWCOUNT=0";
              sql += " INSERT INTO KPIMonthly (MILL_CODE, KPI_CODE, SAPPIMONTH_ID,KPI_VALUE, LASTUPDATED,FLID)";
              sql += " VALUES ("
                    + "'" + sSite + "'"
                    + ",'" + kpiCode + "'"
                    + "," + iMonthID
                    + "," + arr[rowNum, col]
                    + ",'" + cmath.getDateTimeNowStr() + "'"
                    + "," + sAreaID
                    + " )";

              cSQL.ExecuteQuery(sql);

              iMonthID++;   //increment the sql server month id 
              count++;
            }

          } //for

        } // boRunMonth == true


        if  (boRunDaily == true)
        {


          col = FROM_MONTH;     //we are using the FROM_MONTH column for daily

          tmp = arr[rowNum, col].ToString();

          sql = "UPDATE KPIDaily set KPIVALUE = " + arr[rowNum, col];
          sql += " , LASTUPDATED = '" + cmath.getDateTimeNowStr() + "'";
          sql += " where MillCode = '" + sSite + "'";
          sql += " and KPICODE = '" + kpiCode + "'";
          sql += " and SappiDayID  = " + iSqlDayID;
          sql += " and FLID = " + sAreaID;
          sql += " IF @@ROWCOUNT=0";
          sql += " INSERT INTO KPIDaily (MILLCODE, KPICODE, SAPPIDAYID, KPIVALUE, LASTUPDATED, FLID)";
          sql += " VALUES ("
               + "'" + sSite + "'"
               + ",'" + kpiCode + "'"
               + "," + iSqlDayID
               + "," + arr[rowNum, col]
               + ",'" + cmath.getDateTimeNowStr() + "'"
               + "," + sAreaID
               + " )";


          cSQL.ExecuteQuery(sql);

        }// boRunDaily



        if (boRunWeek == true)
        {

          col = FROM_MONTH;     //we are using the FROM_MONTH column for weekly


          sql = "UPDATE KPIWeekly set KPIVALUE = " + arr[rowNum, col];
          sql += " , LASTUPDATED = '" + cmath.getDateTimeNowStr() + "'";
          sql += " where MillCode = '" + sSite + "'";
          sql += " and KPICODE = '" + kpiCode + "'";
          sql += " and SappiWeekID  = " + iSqlWeekID;
          sql += " and FLID = " + sAreaID;
          sql += " IF @@ROWCOUNT=0";
          sql += " INSERT INTO KPIWeekly (MILLCODE, KPICODE, SappiWeekID, KPIVALUE, LASTUPDATED, FLID)";
          sql += " VALUES ("
               + "'" + sSite + "'"
               + ",'" + kpiCode + "'"
               + "," + iSqlWeekID
               + "," + arr[rowNum, col]
               + ",'" + cmath.getDateTimeNowStr() + "'"
               + "," + sAreaID
               + " )";


          cSQL.ExecuteQuery(sql);

        }

      }
      catch (Exception er)
      {
        cFG.LogIt("SaveData KPIJob " + er.Message);
        cFG.LogIt(sql);
        throw new Exception("SaveData: " + er.Message);
      }
      finally
      {
        cSQL.CloseDB();
      }

    }//


    private void SaveQuarter(int rowNum, string kpiCode)
    {

      if (boRunMonth == false)
      {
        //no need to save quarter if running daily or weekly
        return;
      }

      string sql = "";

      cSQL.OpenDB("ZAMDW");

      try
      {
        sql = "UPDATE KPIQuarterly set LASTUPDATED = '" + cmath.getDateTimeNowStr() + "'";
        sql += ",  Q1 = " + arr[rowNum, FROM_Q1];
        sql += ",  Q2 = " + arr[rowNum, FROM_Q1 + 1];
        sql += ",  Q3 = " + arr[rowNum, FROM_Q1 + 2];
        sql += ",  Q4 = " + arr[rowNum, FROM_Q1 + 3];
        sql += ",  YTD = " + arr[rowNum, FROM_YTD];
        sql += " where MillCode = '" + sSite + "'";
        sql += " and KPICODE = '" + kpiCode + "'";
        sql += " and FLID = " + sAreaID;
        sql += " and SappiYear = " + iSappiYear;
        sql += " IF @@ROWCOUNT=0";
        sql += " INSERT INTO KPIQUARTERLY (MILLCODE, KPICODE, SAPPIYEAR, FLID, Q1, Q2, Q3, Q4, YTD, LASTUPDATED)";
        sql += " VALUES ("
             + " '" + sSite + "'"
             + ",'" + kpiCode + "'"
             + ", " + iSappiYear
             + ", " + sAreaID
             + ", " + arr[rowNum, FROM_Q1]
             + ", " + arr[rowNum, FROM_Q1 + 1]
             + ", " + arr[rowNum, FROM_Q1 + 2]
             + ", " + arr[rowNum, FROM_Q1 + 3]
             + ", " + arr[rowNum, FROM_YTD]
             + ",'" + cmath.getDateTimeNowStr() + "'"
             + " )";

        cSQL.ExecuteQuery(sql);

      }
      catch (Exception er)
      {
        cFG.LogIt("SaveQuarter KPIJob " + er.Message);
        throw new Exception("SaveQuarter: " + er.Message);
      }
      finally
      {
        cSQL.CloseDB();
      }

    }//


    private void SaveQM(int rowNum, string kpiCode, int sappiMonthID)
    {

      string sql = "";

      cSQL.OpenDB("ZAMDW");

      try
      {
        sql = "UPDATE KPIQM set LASTUPDATED = '" + cmath.getDateTimeNowStr() + "'";
        sql += ",  Q1 = " + arr[rowNum, FROM_Q1];
        sql += ",  Q2 = " + arr[rowNum, FROM_Q1 + 1];
        sql += ",  Q3 = " + arr[rowNum, FROM_Q1 + 2];
        sql += ",  Q4 = " + arr[rowNum, FROM_Q1 + 3];
        sql += ",  YTD = " + arr[rowNum, FROM_YTD];
        sql += " where MillCode = '" + sSite + "'";
        sql += " and KPICODE = '" + kpiCode + "'";
        sql += " and FLID = " + sAreaID;
        sql += " and SappiYear = " + iSappiYear;
        sql += " and SappiMonthID = " + sappiMonthID;
        sql += " IF @@ROWCOUNT=0";
        sql += " INSERT INTO KPIQM (MILLCODE, KPICODE, SAPPIYEAR, FLID, SappiMonthID, Q1, Q2, Q3, Q4, YTD, LASTUPDATED)";
        sql += " VALUES ("
             + " '" + sSite + "'"
             + ",'" + kpiCode + "'"
             + ", " + iSappiYear
             + ", " + sAreaID
             + ", " + sappiMonthID
             + ", " + arr[rowNum, FROM_Q1]
             + ", " + arr[rowNum, FROM_Q1 + 1]
             + ", " + arr[rowNum, FROM_Q1 + 2]
             + ", " + arr[rowNum, FROM_Q1 + 3]
             + ", " + arr[rowNum, FROM_YTD]
             + ",'" + cmath.getDateTimeNowStr() + "'"
             + " )";

        cSQL.ExecuteQuery(sql);

      }
      catch (Exception er)
      {
        cFG.LogIt("SaveQM KPIJob " + er.Message);
        throw new Exception("SaveQM: " + er.Message);
      }
      finally
      {
        cSQL.CloseDB();
      }


    }//


    #endregion




    #region -------------------WEIGHTED AVERAGE PROCESS -------------------------------

    public void DoWeightedAve(int pSappiYear, string pSite, string pSector, string pKeyArea1, string pArea2, string pArea3, string pArea4)
    {

      //calculate the weighted avarage for kpi's used in CEO report
      //Weighted avarage Budgeted OEE and Actual OEE againt production
      //Weighted avarage Budgeted OME and Actual OME againt production

      //loop thru every month saving wa data for each month for WABOME, WABOEE, WAOME and WAOEE
      //and then calculate the quarter and year to date for this site and sector

      string sKPICode = "";

      double dA_PM1 = 0;      //actual
      double dA_PM2 = 0;
      double dA_PM3 = 0;
      double dA_PM4 = 0;

      double dB_PM1 = 0;      //budget
      double dB_PM2 = 0;
      double dB_PM3 = 0;
      double dB_PM4 = 0;

      string sFLID = "";
      int iMonth = 0;
      int iMonthTemp = 0;     //for the control break
      int ndx = 0;
      string tmp = "";

      //global vars for the calculation of the Regional Weighted Average

      try
      {

         DoInit();     //Initialize the array

        //get year and month
        cal = cal ?? new cCal();

        cal.GetCal();
        iSappiYear = cal.iSappiYear;
        currMonth = cal.iCurrMonthOfYear;

        //if running job previous year
        if ((pSappiYear > 0) && (pSappiYear < iSappiYear))
        {
          iSappiYear = pSappiYear;
          currMonth = 12;             //if last year the the current month is 12 
        }

        //used for update per month starting at the first month
        iSqlMonthID = cal.GetFirstMonthID(iSappiYear);


      //----------------------------------------------------------
      //WABOEE    = budget oee
      string sql = "Select km.mill_code, m.millkey, km.FLID, kd.KpiCode, kd.KpiDesc, kd.Uom"
        + " ,km.sappimonth_id, sm.month_of_year, smn.name_short, sy.year, km.kpi_value"
        + " from    kpimonthly km"
        + " , kpidefinition kd"
        + " , DimPlant p"
        + " , DimMill m"
        + " , sappifiscalcalendar.dbo.tbl_months sm"
        + " , sappifiscalcalendar.dbo.tbl_month_names smn"
        + " , sappifiscalcalendar.dbo.tbl_years sy"
        + " where    km.KPI_CODE = kd.KpiCode"
        + " and km.flid = p.flid"
        + " and km.mill_code = m.SiteAbbrev"
        + " and m.millkey = p.millkey"
        + " and km.sappimonth_id = sm.month_id"
        + " and sm.month_name_id = smn.month_name_id"
        + " and sm.year_id = sy.year_id"
        + " and sy.year = " + iSappiYear
        + " and km.MILL_CODE = '" + pSite + "'";

        if (pArea4 != "")
          sql += " and km.FLID in (" + pKeyArea1 + "," + pArea2 + "," + pArea3 + "," + pArea4 + " )";
        else
          if (pArea3 != "")
              sql += " and km.FLID in (" + pKeyArea1 + "," + pArea2 + "," + pArea3 + " )";
            else
             if (pArea2 != "")
                sql += " and km.FLID in (" + pKeyArea1 + "," + pArea2 + " )";
             else
                 sql += " and km.FLID = " + pKeyArea1;

        sql += " and kd.kpicode in ('NSPPM', 'BOE')";
        sql += " order by month_of_year";

        cSQL.OpenDB("ZAMDW");
        DataTable dt = cSQL.GetDataTable(sql);
        cSQL.CloseDB();
        ndx = 0;

        dA_PM1 = 0D;
        dA_PM2 = 0D;
        dA_PM3 = 0D;
        dA_PM4 = 0D;

        dB_PM1 = 0D;
        dB_PM2 = 0D;
        dB_PM3 = 0D;
        dB_PM4 = 0D;


        Q1 = 0;
        Q2 = 0;
        Q3 = 0;
        Q4 = 0;

        while (ndx < dt.Rows.Count)
        {
          sKPICode = dt.Rows[ndx]["KpiCode"].ToString();
          iMonth = cmath.geti(dt.Rows[ndx]["month_of_year"].ToString());
          sFLID = dt.Rows[ndx]["FLID"].ToString();
          iMonthTemp = iMonth;

          while ((ndx < dt.Rows.Count) && (iMonth == iMonthTemp))
          {

            if ((sKPICode == "NSPPM") && (sFLID == pKeyArea1))
              dA_PM1 = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());

            if ((sKPICode == "NSPPM") && (sFLID == pArea2))
              dA_PM2 = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());

            if ((sKPICode == "BOE") && (sFLID == pKeyArea1))
              dB_PM1 = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());

            if ((sKPICode == "BOE") && (sFLID == pArea2))
              dB_PM2 = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());

            if (pArea3 != "")
            {
              if ((sKPICode == "NSPPM") && (sFLID == pArea3))
                dA_PM3 = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());

              if ((sKPICode == "BOE") && (sFLID == pArea3))
                dB_PM3 = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }

            if (pArea4 != "")
            {
              if ((sKPICode == "NSPPM") && (sFLID == pArea4))
                dA_PM4 = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());

              if ((sKPICode == "BOE") && (sFLID == pArea4))
                dB_PM4 = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }

            ndx++;
            if (ndx < dt.Rows.Count)
            {
              sKPICode = dt.Rows[ndx]["KpiCode"].ToString();
              iMonth = cmath.geti(dt.Rows[ndx]["month_of_year"].ToString());
              sFLID = dt.Rows[ndx]["FLID"].ToString();
            }
          }     //while 2


          //calculate and save values to the array for the month (n)
          if (pArea4 != "")
          {
            t1 = cmath.div((dA_PM1 * dB_PM1) + (dA_PM2 * dB_PM2) + (dA_PM3 * dB_PM3) + (dA_PM4 * dB_PM4), (dA_PM1 + dA_PM2 + dA_PM3 + dA_PM4));
          }
          else
            if (pArea3 != "")
            {
              t1 = cmath.div((dA_PM1 * dB_PM1) + (dA_PM2 * dB_PM2) + (dA_PM3 * dB_PM3), (dA_PM1 + dA_PM2 + dA_PM3));
            }
          else
            {
              t1 = cmath.div((dA_PM1 * dB_PM1) + (dA_PM2 * dB_PM2), (dA_PM1 + dA_PM2));
            }

          arr[zKPI_WABOEE, FROM_MONTH + (iMonthTemp - 1)] = Math.Round(t1, ROUND5);

        } // while 1

        
        sSite = pSite;
        SaveDataWA(zKPI_WABOEE, "WABOEE", pSector);
        cFG.LogIt("DoWeightedAverage Finished WABOEE" );

        
        //----------------------------------------------------------



        //----------------------------------------------------------
        //WAOEE       = actual oee
        sql = "Select km.mill_code, m.millkey, km.FLID, kd.KpiCode, kd.KpiDesc, kd.Uom"
          + " ,km.sappimonth_id, sm.month_of_year, smn.name_short, sy.year, km.kpi_value"
          + " from    kpimonthly km"
          + " , kpidefinition kd"
          + " , DimPlant p"
          + " , DimMill m"
          + " , sappifiscalcalendar.dbo.tbl_months sm"
          + " , sappifiscalcalendar.dbo.tbl_month_names smn"
          + " , sappifiscalcalendar.dbo.tbl_years sy"
          + " where    km.KPI_CODE = kd.KpiCode"
          + " and km.flid = p.flid"
          + " and km.mill_code = m.SiteAbbrev"
          + " and m.millkey = p.millkey"
          + " and km.sappimonth_id = sm.month_id"
          + " and sm.month_name_id = smn.month_name_id"
          + " and sm.year_id = sy.year_id"
          + " and sy.year = " + iSappiYear
          + " and km.MILL_CODE = '" + pSite + "'";

        if (pArea4 != "")
          sql += " and km.FLID in (" + pKeyArea1 + "," + pArea2 + "," + pArea3 + "," + pArea4 + " )";
        else
          if (pArea3 != "")
            sql += " and km.FLID in (" + pKeyArea1 + "," + pArea2 + "," + pArea3 + " )";
          else
           if (pArea2 != "")
              sql += " and km.FLID in (" + pKeyArea1 + "," + pArea2 + " )";
           else
              sql += " and km.FLID = " + pKeyArea1;

        sql += " and kd.kpicode in ('KPI0035', 'KPI0057')";
        sql += " order by month_of_year";
          
        //KPI0057 is the OEE

        cSQL.OpenDB("ZAMDW");
        dt = cSQL.GetDataTable(sql);
        cSQL.CloseDB();
        ndx = 0;

        dA_PM1 = 0D;
        dA_PM2 = 0D;
        dA_PM3 = 0D;
        dA_PM4 = 0D;

        dB_PM1 = 0D;
        dB_PM2 = 0D;
        dB_PM3 = 0D;
        dB_PM4 = 0D;

        while (ndx < dt.Rows.Count)
        {
          sKPICode = dt.Rows[ndx]["KpiCode"].ToString();
          iMonth = cmath.geti(dt.Rows[ndx]["month_of_year"].ToString());
          sFLID = dt.Rows[ndx]["FLID"].ToString();
          iMonthTemp = iMonth;

          while ((ndx < dt.Rows.Count) && (iMonth == iMonthTemp))
          {
            if ((sKPICode == "KPI0035") && (sFLID == pKeyArea1))
              dA_PM1 = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());

            if ((sKPICode == "KPI0035") && (sFLID == pArea2))
              dA_PM2 = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());

            if ((sKPICode == "KPI0057") && (sFLID == pKeyArea1))
              dB_PM1 = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());

            if ((sKPICode == "KPI0057") && (sFLID == pArea2))
              dB_PM2 = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());

            if (pArea3 != "")
            {
              if ((sKPICode == "KPI0035") && (sFLID == pArea3))
                dA_PM3 = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());

              if ((sKPICode == "KPI0057") && (sFLID == pArea3))
                dB_PM3 = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }

            if (pArea4 != "")
            {
              if ((sKPICode == "KPI0035") && (sFLID == pArea4))
                dA_PM4 = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());

              if ((sKPICode == "KPI0057") && (sFLID == pArea4))
                dB_PM4 = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }

            ndx++;
            if (ndx < dt.Rows.Count)
            {
              sKPICode = dt.Rows[ndx]["KpiCode"].ToString();
              iMonth = cmath.geti(dt.Rows[ndx]["month_of_year"].ToString());
              sFLID = dt.Rows[ndx]["FLID"].ToString();
            }
          }     //while 2

          //calculate and save values to the array for the month (n)
          if (pArea4 != "")
          {
            t1 = cmath.div((dA_PM1 * dB_PM1) + (dA_PM2 * dB_PM2) + (dA_PM3 * dB_PM3) + (dA_PM4 * dB_PM4), (dA_PM1 + dA_PM2 + dA_PM3 + dA_PM4));
          }
          else
          if (pArea3 != "")
          {
            t1 = cmath.div((dA_PM1 * dB_PM1) + (dA_PM2 * dB_PM2) + (dA_PM3 * dB_PM3), (dA_PM1 + dA_PM2 + dA_PM3));
          }
          else
          {
            t1 = cmath.div((dA_PM1 * dB_PM1) + (dA_PM2 * dB_PM2), (dA_PM1 + dA_PM2));
          }

          arr[zKPI_WAOEE, FROM_MONTH + (iMonthTemp - 1)] = t1;

        } // while 1

       
        sSite = pSite;
        SaveDataWA(zKPI_WAOEE, "WAOEE", pSector);
        cFG.LogIt("DoWeightedAverage Finished WAOEE");
        //----------------------------------------------------------



        //----------------------------------------------------------
        //WABOME       = budget OME
        sql = "Select km.mill_code, m.millkey, km.FLID, kd.KpiCode, kd.KpiDesc, kd.Uom"
          + " ,km.sappimonth_id, sm.month_of_year, smn.name_short, sy.year, km.kpi_value"
          + " from    kpimonthly km"
          + " , kpidefinition kd"
          + " , DimPlant p"
          + " , DimMill m"
          + " , sappifiscalcalendar.dbo.tbl_months sm"
          + " , sappifiscalcalendar.dbo.tbl_month_names smn"
          + " , sappifiscalcalendar.dbo.tbl_years sy"
          + " where    km.KPI_CODE = kd.KpiCode"
          + " and km.flid = p.flid"
          + " and km.mill_code = m.SiteAbbrev"
          + " and m.millkey = p.millkey"
          + " and km.sappimonth_id = sm.month_id"
          + " and sm.month_name_id = smn.month_name_id"
          + " and sm.year_id = sy.year_id"
          + " and sy.year = " + iSappiYear
          + " and km.MILL_CODE = '" + pSite + "'";

        if (pArea4 != "")
          sql += " and km.FLID in (" + pKeyArea1 + "," + pArea2 + "," + pArea3 + "," + pArea4 + " )";
        else
          if (pArea3 != "")
            sql += " and km.FLID in (" + pKeyArea1 + "," + pArea2 + "," + pArea3 + " )";
          else
           if (pArea2 != "")
              sql += " and km.FLID in (" + pKeyArea1 + "," + pArea2 + " )";
           else
            sql += " and km.FLID = " + pKeyArea1;

        sql += " and kd.kpicode in ('BPOME', 'NSPPM')";
        sql += " order by month_of_year";
         

        cSQL.OpenDB("ZAMDW");
        dt = cSQL.GetDataTable(sql);
        cSQL.CloseDB();
        ndx = 0;

        dA_PM1 = 0D;
        dA_PM2 = 0D;
        dA_PM3 = 0D;
        dA_PM4 = 0D;

        dB_PM1 = 0D;
        dB_PM2 = 0D;
        dB_PM3 = 0D;
        dB_PM4 = 0D;

        while (ndx < dt.Rows.Count)
        {
          sKPICode = dt.Rows[ndx]["KpiCode"].ToString();
          iMonth = cmath.geti(dt.Rows[ndx]["month_of_year"].ToString());
          sFLID = dt.Rows[ndx]["FLID"].ToString();
          iMonthTemp = iMonth;

          while ((ndx < dt.Rows.Count) && (iMonth == iMonthTemp))
          {
            if ((sKPICode == "NSPPM") && (sFLID == pKeyArea1))
              dA_PM1 = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());

            if ((sKPICode == "NSPPM") && (sFLID == pArea2))
              dA_PM2 = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());

            if ((sKPICode == "BPOME") && (sFLID == pKeyArea1))
              dB_PM1 = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());

            if ((sKPICode == "BPOME") && (sFLID == pArea2))
              dB_PM2 = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());

            if (pArea3 != "")
            {
              if ((sKPICode == "NSPPM") && (sFLID == pArea3))
                dA_PM3 = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());

              if ((sKPICode == "BPOME") && (sFLID == pArea3))
                dB_PM3 = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }

            if (pArea4 != "")
            {
              if ((sKPICode == "NSPPM") && (sFLID == pArea4))
                dA_PM4 = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());

              if ((sKPICode == "BPOME") && (sFLID == pArea4))
                dB_PM4 = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }

            ndx++;
            if (ndx < dt.Rows.Count)
            {
              sKPICode = dt.Rows[ndx]["KpiCode"].ToString();
              iMonth = cmath.geti(dt.Rows[ndx]["month_of_year"].ToString());
              sFLID = dt.Rows[ndx]["FLID"].ToString();
            }
          }     //while 2

          //calculate and save values to the array for the month (n)
          if (pArea4 != "")
          {
            t1 = cmath.div((dA_PM1 * dB_PM1) + (dA_PM2 * dB_PM2) + (dA_PM3 * dB_PM3) + (dA_PM4 * dB_PM4), (dA_PM1 + dA_PM2 + dA_PM3 + dA_PM4));
          }
          else
          if (pArea3 != "")
          {
            t1 = cmath.div((dA_PM1 * dB_PM1) + (dA_PM2 * dB_PM2) + (dA_PM3 * dB_PM3), (dA_PM1 + dA_PM2 + dA_PM3));
          }
          else
          {
            t1 = cmath.div((dA_PM1 * dB_PM1) + (dA_PM2 * dB_PM2), (dA_PM1 + dA_PM2));
          }

          arr[zKPI_WABOME, FROM_MONTH + (iMonthTemp - 1)] = Math.Round(t1, ROUND5);

        } // while 1

        sSite = pSite;
        SaveDataWA(zKPI_WABOME, "WABOME", pSector);
        cFG.LogIt("DoWeightedAverage Finished WABOME");
        //----------------------------------------------------------


        //----------------------------------------------------------
        //WAOME       = actual  OME
        sql = "Select km.mill_code, m.millkey, km.FLID, kd.KpiCode, kd.KpiDesc, kd.Uom"
          + " ,km.sappimonth_id, sm.month_of_year, smn.name_short, sy.year, km.kpi_value"
          + " from    kpimonthly km"
          + " , kpidefinition kd"
          + " , DimPlant p"
          + " , DimMill m"
          + " , sappifiscalcalendar.dbo.tbl_months sm"
          + " , sappifiscalcalendar.dbo.tbl_month_names smn"
          + " , sappifiscalcalendar.dbo.tbl_years sy"
          + " where    km.KPI_CODE = kd.KpiCode"
          + " and km.flid = p.flid"
          + " and km.mill_code = m.SiteAbbrev"
          + " and m.millkey = p.millkey"
          + " and km.sappimonth_id = sm.month_id"
          + " and sm.month_name_id = smn.month_name_id"
          + " and sm.year_id = sy.year_id"
          + " and sy.year = " + iSappiYear
          + " and km.MILL_CODE = '" + pSite + "'";

        if (pArea4 != "")
          sql += " and km.FLID in (" + pKeyArea1 + "," + pArea2 + "," + pArea3 + "," + pArea4 + " )";
        else
          if (pArea3 != "")
            sql += " and km.FLID in (" + pKeyArea1 + "," + pArea2 + "," + pArea3 + " )";
          else
           if (pArea2 != "")
              sql += " and km.FLID in (" + pKeyArea1 + "," + pArea2 + " )";
            else
              sql += " and km.FLID = " + pKeyArea1;

        sql += " and kd.kpicode in ('KPI0056', 'NSPPM')";
        sql += " order by month_of_year";


        cSQL.OpenDB("ZAMDW");
        dt = cSQL.GetDataTable(sql);
        cSQL.CloseDB();

        ndx = 0;

        dA_PM1 = 0D;
        dA_PM2 = 0D;
        dA_PM3 = 0D;
        dA_PM4 = 0D;

        dB_PM1 = 0D;
        dB_PM2 = 0D;
        dB_PM3 = 0D;
        dB_PM4 = 0D;

        while (ndx < dt.Rows.Count)
        {
          sKPICode = dt.Rows[ndx]["KpiCode"].ToString();
          iMonth = cmath.geti(dt.Rows[ndx]["month_of_year"].ToString());
          sFLID = dt.Rows[ndx]["FLID"].ToString();
          iMonthTemp = iMonth;

          while ((ndx < dt.Rows.Count) && (iMonth == iMonthTemp))
          {
            if ((sKPICode == "NSPPM") && (sFLID == pKeyArea1))
              dA_PM1 = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());

            if ((sKPICode == "NSPPM") && (sFLID == pArea2))
              dA_PM2 = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());

            if ((sKPICode == "KPI0056") && (sFLID == pKeyArea1))
              dB_PM1 = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());

            if ((sKPICode == "KPI0056") && (sFLID == pArea2))
              dB_PM2 = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());

            if (pArea3 != "")
            {
              if ((sKPICode == "NSPPM") && (sFLID == pArea3))
                dA_PM3 = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());

              if ((sKPICode == "KPI0056") && (sFLID == pArea3))
                dB_PM3 = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }

            if (pArea4 != "")
            {
              if ((sKPICode == "NSPPM") && (sFLID == pArea4))
                dA_PM4 = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());

              if ((sKPICode == "KPI0056") && (sFLID == pArea4))
                dB_PM4 = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }

            ndx++;
            if (ndx < dt.Rows.Count)
            {
              sKPICode = dt.Rows[ndx]["KpiCode"].ToString();
              iMonth = cmath.geti(dt.Rows[ndx]["month_of_year"].ToString());
              sFLID = dt.Rows[ndx]["FLID"].ToString();
            }
          }     //while 2


          //calculate and save values to the array for the month (n)
          if (pArea4 != "")
          {
            t1 = cmath.div((dA_PM1 * dB_PM1) + (dA_PM2 * dB_PM2) + (dA_PM3 * dB_PM3) + (dA_PM4 * dB_PM4), (dA_PM1 + dA_PM2 + dA_PM3 + dA_PM4));
          }
          else
            if (pArea3 != "")
            {
              t1 = cmath.div((dA_PM1 * dB_PM1) + (dA_PM2 * dB_PM2) + (dA_PM3 * dB_PM3), (dA_PM1 + dA_PM2 + dA_PM3));
            }
            else
            {
              t1 = cmath.div((dA_PM1 * dB_PM1) + (dA_PM2 * dB_PM2), (dA_PM1 + dA_PM2));
            }

          arr[zKPI_WAOME, FROM_MONTH + (iMonthTemp - 1)] = t1;

        } // while 1

        sSite = pSite;
        SaveDataWA(zKPI_WAOME, "WAOME", pSector);
        cFG.LogIt("DoWeightedAverage Finished WAOME");
        //----------------------------------------------------------


        ////calculate the quarter and year to date for this site      //old code use calendar for WA,  see new calculations 2024-03-12 I-2403-6663
        //sAreaID = pKeyArea1;
        //sSite = pSite;
        //LoadBudgets();      //load the array with the sappi hours in order to calculate the quarters and year to date
        //SumYTD_QTR(zSappiHours, ROUND1);
        //DoProcessWeightedAve_YTD_Q(pSector);


        ///NEW calculations  for ytd and quarters 2024-03-12 I-2403-6663
        double ytd = 0D;
        double q1 = 0D;
        double q2 = 0D;
        double q3 = 0D;
        double q4 = 0D;
        


        for (int monthNum = currMonth; monthNum > 0; monthNum--)
        {

          iSqlMonthID = cal.GetSappiMonthID(pSappiYear, monthNum);  // mssqlserver month number

          cSQL.OpenDB("ZAMDW");

          //WAOEE
          sql = "EXEC WA_QY " + pSappiYear + "," + iSqlMonthID + "," + pSite + "," + "'KPI0035'" + "," + "'KPI0057'"
                + "," + cmath.geti(pKeyArea1) + "," + cmath.geti(pArea2) + "," + cmath.geti(pArea3) + "," + cmath.geti(pArea4);

          dt = cSQL.GetDataTable(sql);
          foreach (DataRow item in dt.Rows)
          {
            //sql = $"WAOEE ytd {item["wa_ytd"].ToString()}, q1 {item["wa_q1"].ToString()}, q2 {item["wa_q2"].ToString()}, q3 {item["wa_q3"].ToString()}, q4 {item["wa_q4"].ToString()}";
            ytd = cmath.getd(item["wa_ytd"].ToString());
            q1 = cmath.getd(item["wa_q1"].ToString());
            q2 = cmath.getd(item["wa_q2"].ToString());
            q3 = cmath.getd(item["wa_q3"].ToString());
            q4 = cmath.getd(item["wa_q4"].ToString());
          }

          SaveWA_QY(pSite, "WAOEE", iSqlMonthID, pSector, ytd, q1, q2, q3, q4);


          ytd = 0D;
          q1 = 0D;
          q2 = 0D;
          q3 = 0D;
          q4 = 0D;



          //WABOEE
          sql = "EXEC WA_QY " + pSappiYear + "," + iSqlMonthID + "," + pSite + "," + "'NSPPM'" + "," + "'BOE'"
                + "," + cmath.geti(pKeyArea1) + "," + cmath.geti(pArea2) + "," + cmath.geti(pArea3) + "," + cmath.geti(pArea4);

          dt = cSQL.GetDataTable(sql);
          foreach (DataRow item in dt.Rows)
          {
            ytd = cmath.getd(item["wa_ytd"].ToString());
            q1 = cmath.getd(item["wa_q1"].ToString());
            q2 = cmath.getd(item["wa_q2"].ToString());
            q3 = cmath.getd(item["wa_q3"].ToString());
            q4 = cmath.getd(item["wa_q4"].ToString());
          }

          SaveWA_QY(pSite, "WABOEE", iSqlMonthID, pSector, ytd, q1, q2, q3, q4);

          ytd = 0D;
          q1 = 0D;
          q2 = 0D;
          q3 = 0D;
          q4 = 0D;

     

          //WAOME
          sql = "EXEC WA_QY " + pSappiYear + "," + iSqlMonthID + "," + pSite + "," + "'NSPPM'" + "," + "'KPI0056'"
                + "," + cmath.geti(pKeyArea1) + "," + cmath.geti(pArea2) + "," + cmath.geti(pArea3) + "," + cmath.geti(pArea4);

          dt = cSQL.GetDataTable(sql);
          foreach (DataRow item in dt.Rows)
          {
            ytd = cmath.getd(item["wa_ytd"].ToString());
            q1 = cmath.getd(item["wa_q1"].ToString());
            q2 = cmath.getd(item["wa_q2"].ToString());
            q3 = cmath.getd(item["wa_q3"].ToString());
            q4 = cmath.getd(item["wa_q4"].ToString());
          }

          SaveWA_QY(pSite, "WAOME", iSqlMonthID, pSector, ytd, q1, q2, q3, q4);

          ytd = 0D;
          q1 = 0D;
          q2 = 0D;
          q3 = 0D;
          q4 = 0D;

          //WABOME
          sql = "EXEC WA_QY " + pSappiYear + "," + iSqlMonthID + "," + pSite + "," + "'NSPPM'" + "," + "'BPOME'"
                + "," + cmath.geti(pKeyArea1) + "," + cmath.geti(pArea2) + "," + cmath.geti(pArea3) + "," + cmath.geti(pArea4);

          dt = cSQL.GetDataTable(sql);
          foreach (DataRow item in dt.Rows)
          {
            ytd = cmath.getd(item["wa_ytd"].ToString());
            q1 = cmath.getd(item["wa_q1"].ToString());
            q2 = cmath.getd(item["wa_q2"].ToString());
            q3 = cmath.getd(item["wa_q3"].ToString());
            q4 = cmath.getd(item["wa_q4"].ToString());
          }

          //cFG.LogIt(sql);
          SaveWA_QY(pSite, "WABOME", iSqlMonthID, pSector, ytd, q1, q2, q3, q4);

        }

      }
      catch (Exception er)
      {
        cFG.LogIt("DoWeightedAverage procedure");
        cFG.LogIt(er.Message);
      }
      finally
      {
        cSQL.CloseDB();
        cal = null;
      }


    } //


   
    public void DoWeightedAveRegionalPaper(int pSappiYear)
    {
      //calculate the weighted avarage for kpi's used in CEO report
      //Weighted average Budgeted OEE and Actual OEE againt production
      //Weighted average Budgeted OME and Actual OME againt production


      string sKPICode = "";
      string sSiteCode = "";
      string FLID = "";

      int iMonth = 0;
      int iMonthTemp = 0;     //for the control break
      int ndx = 0;
      string tmp = "";

      //vars for the calculation of the Regional Weighted Average
      double dNGX1_NSPPM = 0D;
      double dNGX2_NSPPM = 0D;
      double dNGX1_KPI = 0D;
      double dNGX2_KPI = 0D;

      double dSTA1_NSPPM = 0D;
      double dSTA2_NSPPM = 0D;
      double dSTA1_KPI = 0D;
      double dSTA2_KPI = 0D;

      double dTUG1_NSPPM = 0D;
      double dTUG1_KPI = 0D;


      DoInit();     //Initialize the array

    try
    {

        //get year and month

        cal = cal ?? new cCal();

        cal.GetCal();
        iSappiYear = cal.iSappiYear;
        currMonth = cal.iCurrMonthOfYear;


        //if running job previous year
        if ((pSappiYear > 0) && (pSappiYear < iSappiYear))
        {
          iSappiYear = pSappiYear;
          currMonth = 12;             //if last year the the current month is 12 
        }

        //used for update per month starting at the first month
        iSqlMonthID = cal.GetFirstMonthID(iSappiYear);


        //----------------------------------------------------------
        //WABOEE    = budget oee
        string sql = "Select km.mill_code, m.millkey, km.FLID, kd.KpiCode, kd.KpiDesc, kd.Uom"
          + " ,km.sappimonth_id, sm.month_of_year, smn.name_short, sy.year, km.kpi_value"
          + " from    kpimonthly km"
          + " , kpidefinition kd"
          + " , DimPlant p"
          + " , DimMill m"
          + " , sappifiscalcalendar.dbo.tbl_months sm"
          + " , sappifiscalcalendar.dbo.tbl_month_names smn"
          + " , sappifiscalcalendar.dbo.tbl_years sy"
          + " where    km.KPI_CODE = kd.KpiCode"
          + " and km.flid = p.flid"
          + " and km.mill_code = m.SiteAbbrev"
          + " and m.millkey = p.millkey"
          + " and km.sappimonth_id = sm.month_id"
          + " and sm.month_name_id = smn.month_name_id"
          + " and sm.year_id = sy.year_id"
          + " and sy.year = " + iSappiYear
          + " and km.MILL_CODE IN ('NGX', 'STA', 'TUG')";	

        sql += " and kd.kpicode in ('NSPPM', 'BOE')";
        sql += " order by month_of_year, km.mill_code, km.flid";

        cSQL.OpenDB("ZAMDW");
        DataTable dt = cSQL.GetDataTable(sql);
        cSQL.CloseDB();
        ndx = 0;
        dNGX1_NSPPM = 0D;
        dNGX2_NSPPM = 0D;
        dNGX1_KPI = 0D;
        dNGX2_KPI = 0D;
        dSTA1_NSPPM = 0D;
        dSTA2_NSPPM = 0D;
        dSTA1_KPI = 0D;
        dSTA2_KPI = 0D;
        dTUG1_NSPPM = 0D;
        dTUG1_KPI = 0D;

        while (ndx < dt.Rows.Count)
        {
          sKPICode = dt.Rows[ndx]["KpiCode"].ToString();
          iMonth = cmath.geti(dt.Rows[ndx]["month_of_year"].ToString());
          sSiteCode = dt.Rows[ndx]["mill_code"].ToString();
          FLID = dt.Rows[ndx]["FLID"].ToString();
          iMonthTemp = iMonth;

          while ((ndx < dt.Rows.Count) && (iMonth == iMonthTemp))
          {
            if ((sKPICode == "NSPPM") && (sSiteCode == "NGX") && (FLID == "139"))
            {
              dNGX1_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "NSPPM") && (sSiteCode == "NGX") && (FLID == "25"))
            {
              dNGX2_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }

            if ((sKPICode == "NSPPM") && (sSiteCode == "STA") && (FLID == "6"))
            {
              dSTA1_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "NSPPM") && (sSiteCode == "STA") && (FLID == "12"))
            {
              dSTA2_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }

            if ((sKPICode == "NSPPM") && (sSiteCode == "TUG") && (FLID == "118"))
            {
              dTUG1_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }


            if ((sKPICode == "BOE") && (sSiteCode == "NGX") && (FLID == "139"))
            {
              dNGX1_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "BOE") && (sSiteCode == "NGX") && (FLID == "25"))
            {
              dNGX2_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }

            if ((sKPICode == "BOE") && (sSiteCode == "STA") && (FLID == "6"))
            {
              dSTA1_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "BOE") && (sSiteCode == "STA") && (FLID == "12"))
            {
              dSTA2_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }

            if ((sKPICode == "BOE") && (sSiteCode == "TUG") && (FLID == "118"))
            {
              dTUG1_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }

            ndx++;
            if (ndx < dt.Rows.Count)
            {
              sKPICode = dt.Rows[ndx]["KpiCode"].ToString();
              iMonth = cmath.geti(dt.Rows[ndx]["month_of_year"].ToString());
              sSiteCode = dt.Rows[ndx]["mill_code"].ToString();
              FLID = dt.Rows[ndx]["FLID"].ToString();
            }
          }     //while 2

          //calculate and save values to the array for the month (n)
          t1 = (dNGX1_NSPPM * dNGX1_KPI) + (dNGX2_NSPPM * dNGX2_KPI)
                + (dSTA1_NSPPM * dSTA1_KPI) + (dSTA2_NSPPM * dSTA2_KPI)
                + (dTUG1_NSPPM * dTUG1_KPI);

          t2 = (dNGX1_NSPPM + dNGX2_NSPPM + dSTA1_NSPPM + dSTA2_NSPPM + dTUG1_NSPPM);
          t3 = cmath.div(t1,t2);

          arr[zKPI_WABOEE, FROM_MONTH + (iMonthTemp - 1)] = Math.Round(t3, ROUND5);

        } // while 1


        SaveDataWASector(zKPI_WABOEE, "WABOEE","PPP");
        cFG.LogIt("DoWeightedAverageRegionalPPP Finished WABOEE");
        //----------------------------------------------------------



        //----------------------------------------------------------
        //WAOEE       = actual oee
        sql = "Select km.mill_code, m.millkey, km.FLID, kd.KpiCode, kd.KpiDesc, kd.Uom"
          + " ,km.sappimonth_id, sm.month_of_year, smn.name_short, sy.year, km.kpi_value"
          + " from    kpimonthly km"
          + " , kpidefinition kd"
          + " , DimPlant p"
          + " , DimMill m"
          + " , sappifiscalcalendar.dbo.tbl_months sm"
          + " , sappifiscalcalendar.dbo.tbl_month_names smn"
          + " , sappifiscalcalendar.dbo.tbl_years sy"
          + " where    km.KPI_CODE = kd.KpiCode"
          + " and km.flid = p.flid"
          + " and km.mill_code = m.SiteAbbrev"
          + " and m.millkey = p.millkey"
          + " and km.sappimonth_id = sm.month_id"
          + " and sm.month_name_id = smn.month_name_id"
          + " and sm.year_id = sy.year_id"
          + " and sy.year = " + iSappiYear
          + " and km.MILL_CODE IN ('NGX', 'STA', 'TUG')";


        //KPI0057 is the OEE
        sql += " and kd.kpicode in ('KPI0035', 'KPI0057')";
        sql += " order by month_of_year, km.mill_code, km.flid";

        cSQL.OpenDB("ZAMDW");
        dt = cSQL.GetDataTable(sql);
        cSQL.CloseDB();
        ndx = 0;
        dNGX1_NSPPM = 0D;
        dNGX2_NSPPM = 0D;
        dNGX1_KPI = 0D;
        dNGX2_KPI = 0D;
        dSTA1_NSPPM = 0D;
        dSTA2_NSPPM = 0D;
        dSTA1_KPI = 0D;
        dSTA2_KPI = 0D;
        dTUG1_NSPPM = 0D;
        dTUG1_KPI = 0D;

        while (ndx < dt.Rows.Count)
        {
          sKPICode = dt.Rows[ndx]["KpiCode"].ToString();
          iMonth = cmath.geti(dt.Rows[ndx]["month_of_year"].ToString());
          sSiteCode = dt.Rows[ndx]["mill_code"].ToString();
          FLID = dt.Rows[ndx]["FLID"].ToString();
          iMonthTemp = iMonth;

          while ((ndx < dt.Rows.Count) && (iMonth == iMonthTemp))
          {
            if ((sKPICode == "KPI0035") && (sSiteCode == "NGX") && (FLID == "139"))
            {
              dNGX1_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "KPI0035") && (sSiteCode == "NGX") && (FLID == "25"))
            {
              dNGX2_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }

            if ((sKPICode == "KPI0035") && (sSiteCode == "STA") && (FLID == "6"))
            {
              dSTA1_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "KPI0035") && (sSiteCode == "STA") && (FLID == "12"))
            {
              dSTA2_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }

            if ((sKPICode == "KPI0035") && (sSiteCode == "TUG") && (FLID == "118"))
            {
              dTUG1_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }


            if ((sKPICode == "KPI0057") && (sSiteCode == "NGX") && (FLID == "139"))
            {
              dNGX1_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "KPI0057") && (sSiteCode == "NGX") && (FLID == "25"))
            {
              dNGX2_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }

            if ((sKPICode == "KPI0057") && (sSiteCode == "STA") && (FLID == "6"))
            {
              dSTA1_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "KPI0057") && (sSiteCode == "STA") && (FLID == "12"))
            {
              dSTA2_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }

            if ((sKPICode == "KPI0057") && (sSiteCode == "TUG") && (FLID == "118"))
            {
              dTUG1_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }

            ndx++;
            if (ndx < dt.Rows.Count)
            {
              sKPICode = dt.Rows[ndx]["KpiCode"].ToString();
              iMonth = cmath.geti(dt.Rows[ndx]["month_of_year"].ToString());
              sSiteCode = dt.Rows[ndx]["mill_code"].ToString();
              FLID = dt.Rows[ndx]["FLID"].ToString();
            }
          }     //while 2

          //calculate and save values to the array for the month (n)
          t1 = (dNGX1_NSPPM * dNGX1_KPI) + (dNGX2_NSPPM * dNGX2_KPI)
                + (dSTA1_NSPPM * dSTA1_KPI) + (dSTA2_NSPPM * dSTA2_KPI)
                + (dTUG1_NSPPM * dTUG1_KPI);

          t2 = (dNGX1_NSPPM + dNGX2_NSPPM + dSTA1_NSPPM + dSTA2_NSPPM + dTUG1_NSPPM);
          t3 = cmath.div(t1, t2);

          arr[zKPI_WAOEE, FROM_MONTH + (iMonthTemp - 1)] = Math.Round(t3, ROUND5);


        } // while 1


        SaveDataWASector(zKPI_WAOEE, "WAOEE","PPP");
        cFG.LogIt("DoWeightedAverageRegionalPPP Finished WAOEE");
        //----------------------------------------------------------


        //----------------------------------------------------------
        //WABOME       = budget OME
        sql = "Select km.mill_code, m.millkey, km.FLID, kd.KpiCode, kd.KpiDesc, kd.Uom"
          + " ,km.sappimonth_id, sm.month_of_year, smn.name_short, sy.year, km.kpi_value"
          + " from    kpimonthly km"
          + " , kpidefinition kd"
          + " , DimPlant p"
          + " , DimMill m"
          + " , sappifiscalcalendar.dbo.tbl_months sm"
          + " , sappifiscalcalendar.dbo.tbl_month_names smn"
          + " , sappifiscalcalendar.dbo.tbl_years sy"
          + " where    km.KPI_CODE = kd.KpiCode"
          + " and km.flid = p.flid"
          + " and km.mill_code = m.SiteAbbrev"
          + " and m.millkey = p.millkey"
          + " and km.sappimonth_id = sm.month_id"
          + " and sm.month_name_id = smn.month_name_id"
          + " and sm.year_id = sy.year_id"
          + " and sy.year = " + iSappiYear
          + " and km.MILL_CODE  IN ('NGX', 'STA', 'TUG')";

        sql += " and kd.kpicode in ('BPOME', 'NSPPM')";
        sql += " order by month_of_year, km.mill_code, km.flid";


        cSQL.OpenDB("ZAMDW");
        dt = cSQL.GetDataTable(sql);
        cSQL.CloseDB();
        ndx = 0;
        dNGX1_NSPPM = 0D;
        dNGX2_NSPPM = 0D;
        dNGX1_KPI = 0D;
        dNGX2_KPI = 0D;
        dSTA1_NSPPM = 0D;
        dSTA2_NSPPM = 0D;
        dSTA1_KPI = 0D;
        dSTA2_KPI = 0D;
        dTUG1_NSPPM = 0D;
        dTUG1_KPI = 0D;

        while (ndx < dt.Rows.Count)
        {
          sKPICode = dt.Rows[ndx]["KpiCode"].ToString();
          iMonth = cmath.geti(dt.Rows[ndx]["month_of_year"].ToString());
          sSiteCode = dt.Rows[ndx]["mill_code"].ToString();
          FLID = dt.Rows[ndx]["FLID"].ToString();
          iMonthTemp = iMonth;

          while ((ndx < dt.Rows.Count) && (iMonth == iMonthTemp))
          {
            if ((sKPICode == "NSPPM") && (sSiteCode == "NGX") && (FLID == "139"))
            {
              dNGX1_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "NSPPM") && (sSiteCode == "NGX") && (FLID == "25"))
            {
              dNGX2_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }

            if ((sKPICode == "NSPPM") && (sSiteCode == "STA") && (FLID == "6"))
            {
              dSTA1_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "NSPPM") && (sSiteCode == "STA") && (FLID == "12"))
            {
              dSTA2_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }

            if ((sKPICode == "NSPPM") && (sSiteCode == "TUG") && (FLID == "118"))
            {
              dTUG1_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }


            if ((sKPICode == "BPOME") && (sSiteCode == "NGX") && (FLID == "139"))
            {
              dNGX1_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "BPOME") && (sSiteCode == "NGX") && (FLID == "25"))
            {
              dNGX2_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }

            if ((sKPICode == "BPOME") && (sSiteCode == "STA") && (FLID == "6"))
            {
              dSTA1_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "BPOME") && (sSiteCode == "STA") && (FLID == "12"))
            {
              dSTA2_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }

            if ((sKPICode == "BPOME") && (sSiteCode == "TUG") && (FLID == "118"))
            {
              dTUG1_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }

            ndx++;
            if (ndx < dt.Rows.Count)
            {
              sKPICode = dt.Rows[ndx]["KpiCode"].ToString();
              iMonth = cmath.geti(dt.Rows[ndx]["month_of_year"].ToString());
              sSiteCode = dt.Rows[ndx]["mill_code"].ToString();
              FLID = dt.Rows[ndx]["FLID"].ToString();
            }
          }     //while 2

          //calculate and save values to the array for the month (n)
          t1 = (dNGX1_NSPPM * dNGX1_KPI) + (dNGX2_NSPPM * dNGX2_KPI)
                         + (dSTA1_NSPPM * dSTA1_KPI) + (dSTA2_NSPPM * dSTA2_KPI)
                         + (dTUG1_NSPPM * dTUG1_KPI);

          t2 = (dNGX1_NSPPM + dNGX2_NSPPM + dSTA1_NSPPM + dSTA2_NSPPM + dTUG1_NSPPM);
          t3 = cmath.div(t1, t2);

          arr[zKPI_WABOME, FROM_MONTH + (iMonthTemp - 1)] = Math.Round(t3, ROUND5);

        } // while 1


        SaveDataWASector(zKPI_WABOME, "WABOME","PPP");
        cFG.LogIt("DoWeightedAverageRegionalPPP Finished WABOME");
        //----------------------------------------------------------


        //----------------------------------------------------------
        //WAOME       = actual  OME
        sql = "Select km.mill_code, m.millkey, km.FLID, kd.KpiCode, kd.KpiDesc, kd.Uom"
          + " ,km.sappimonth_id, sm.month_of_year, smn.name_short, sy.year, km.kpi_value"
          + " from    kpimonthly km"
          + " , kpidefinition kd"
          + " , DimPlant p"
          + " , DimMill m"
          + " , sappifiscalcalendar.dbo.tbl_months sm"
          + " , sappifiscalcalendar.dbo.tbl_month_names smn"
          + " , sappifiscalcalendar.dbo.tbl_years sy"
          + " where    km.KPI_CODE = kd.KpiCode"
          + " and km.flid = p.flid"
          + " and km.mill_code = m.SiteAbbrev"
          + " and m.millkey = p.millkey"
          + " and km.sappimonth_id = sm.month_id"
          + " and sm.month_name_id = smn.month_name_id"
          + " and sm.year_id = sy.year_id"
          + " and sy.year = " + iSappiYear
          + " and km.MILL_CODE IN ('NGX', 'STA', 'TUG')";

        sql += " and kd.kpicode in ('KPI0056', 'NSPPM')";
        sql += " order by month_of_year, km.mill_code, km.flid";


        cSQL.OpenDB("ZAMDW");
        dt = cSQL.GetDataTable(sql);
        cSQL.CloseDB();
        ndx = 0;
        dNGX1_NSPPM = 0D;
        dNGX2_NSPPM = 0D;
        dNGX1_KPI = 0D;
        dNGX2_KPI = 0D;
        dSTA1_NSPPM = 0D;
        dSTA2_NSPPM = 0D;
        dSTA1_KPI = 0D;
        dSTA2_KPI = 0D;
        dTUG1_NSPPM = 0D;
        dTUG1_KPI = 0D;

        while (ndx < dt.Rows.Count)
        {
          sKPICode = dt.Rows[ndx]["KpiCode"].ToString();
          iMonth = cmath.geti(dt.Rows[ndx]["month_of_year"].ToString());
          FLID = dt.Rows[ndx]["FLID"].ToString();
          sSiteCode = dt.Rows[ndx]["mill_code"].ToString();
          iMonthTemp = iMonth;

          while ((ndx < dt.Rows.Count) && (iMonth == iMonthTemp))
          {
            if ((sKPICode == "NSPPM") && (sSiteCode == "NGX") && (FLID == "139"))
            {
              dNGX1_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "NSPPM") && (sSiteCode == "NGX") && (FLID == "25"))
            {
              dNGX2_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }

            if ((sKPICode == "NSPPM") && (sSiteCode == "STA") && (FLID == "6"))
            {
              dSTA1_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "NSPPM") && (sSiteCode == "STA") && (FLID == "12"))
            {
              dSTA2_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }

            if ((sKPICode == "NSPPM") && (sSiteCode == "TUG") && (FLID == "118"))
            {
              dTUG1_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }


            if ((sKPICode == "KPI0056") && (sSiteCode == "NGX") && (FLID == "139"))
            {
              dNGX1_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "KPI0056") && (sSiteCode == "NGX") && (FLID == "25"))
            {
              dNGX2_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }

            if ((sKPICode == "KPI0056") && (sSiteCode == "STA") && (FLID == "6"))
            {
              dSTA1_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "KPI0056") && (sSiteCode == "STA") && (FLID == "12"))
            {
              dSTA2_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }

            if ((sKPICode == "KPI0056") && (sSiteCode == "TUG") && (FLID == "118"))
            {
              dTUG1_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }

            ndx++;
            if (ndx < dt.Rows.Count)
            {
              sKPICode = dt.Rows[ndx]["KpiCode"].ToString();
              iMonth = cmath.geti(dt.Rows[ndx]["month_of_year"].ToString());
              sSiteCode = dt.Rows[ndx]["mill_code"].ToString();
              FLID = dt.Rows[ndx]["FLID"].ToString();
            }
          }     //while 2

          //calculate and save values to the array for the month (n)
          t1 = (dNGX1_NSPPM * dNGX1_KPI) + (dNGX2_NSPPM * dNGX2_KPI)
                         + (dSTA1_NSPPM * dSTA1_KPI) + (dSTA2_NSPPM * dSTA2_KPI)
                         + (dTUG1_NSPPM * dTUG1_KPI);

          t2 = (dNGX1_NSPPM + dNGX2_NSPPM + dSTA1_NSPPM + dSTA2_NSPPM + dTUG1_NSPPM);
          t3 = cmath.div(t1, t2);

          arr[zKPI_WAOME, FROM_MONTH + (iMonthTemp - 1)] = Math.Round(t3, ROUND5);


        } // while 1

        SaveDataWASector(zKPI_WAOME, "WAOME","PPP");
        cFG.LogIt("DoWeightedAverageRegionalPPP Finished WAOME");
        //----------------------------------------------------------

        //  //old code use calendar for WA,  see new calculations 2024-03-12 I-2403-6663
        ///calculate the quarter and year to date for this site
        //sSite = "NGX";        //defaults just to get the calendar budgets
        //sAreaID = "25";       //defaults just to get the calendar budgets
        //LoadBudgets();      //load the array with the sappi hours in order to calculate the quarters and year to date
        //SumYTD_QTR(zSappiHours, ROUND1);
        //DoProcessWeightedAveRegional_YTD_Q("PPP");

        ///NEW calculations  for ytd and quarters 2024-03-12 I-2403-6663
        double ytd = 0D;
        double q1 = 0D;
        double q2 = 0D;
        double q3 = 0D;
        double q4 = 0D;



        for (int monthNum = currMonth; monthNum > 0; monthNum--)
        {

          iSqlMonthID = cal.GetSappiMonthID(pSappiYear, monthNum);  // mssqlserver month number

          cSQL.OpenDB("ZAMDW");

          //WAOEE
          sql = "EXEC WA_QY_PPP " + pSappiYear + "," + iSqlMonthID + "," + "'KPI0035'" + "," + "'KPI0057'";

          dt = cSQL.GetDataTable(sql);
          foreach (DataRow item in dt.Rows)
          {
            ytd = cmath.getd(item["wa_ytd"].ToString());
            q1 = cmath.getd(item["wa_q1"].ToString());
            q2 = cmath.getd(item["wa_q2"].ToString());
            q3 = cmath.getd(item["wa_q3"].ToString());
            q4 = cmath.getd(item["wa_q4"].ToString());
          }

          SaveWASector_QY( "WAOEE", iSqlMonthID, "PPP", ytd, q1, q2, q3, q4);


          ytd = 0D;
          q1 = 0D;
          q2 = 0D;
          q3 = 0D;
          q4 = 0D;



          //WABOEE
          sql = "EXEC WA_QY_PPP " + pSappiYear + "," + iSqlMonthID + "," + "'NSPPM'" + "," + "'BOE'";
               

          dt = cSQL.GetDataTable(sql);
          foreach (DataRow item in dt.Rows)
          {
            ytd = cmath.getd(item["wa_ytd"].ToString());
            q1 = cmath.getd(item["wa_q1"].ToString());
            q2 = cmath.getd(item["wa_q2"].ToString());
            q3 = cmath.getd(item["wa_q3"].ToString());
            q4 = cmath.getd(item["wa_q4"].ToString());
          }

          SaveWASector_QY("WABOEE", iSqlMonthID, "PPP", ytd, q1, q2, q3, q4);

          ytd = 0D;
          q1 = 0D;
          q2 = 0D;
          q3 = 0D;
          q4 = 0D;



          //WAOME
          sql = "EXEC WA_QY_PPP " + pSappiYear + "," + iSqlMonthID  + "," + "'NSPPM'" + "," + "'KPI0056'";

          dt = cSQL.GetDataTable(sql);
          foreach (DataRow item in dt.Rows)
          {
            ytd = cmath.getd(item["wa_ytd"].ToString());
            q1 = cmath.getd(item["wa_q1"].ToString());
            q2 = cmath.getd(item["wa_q2"].ToString());
            q3 = cmath.getd(item["wa_q3"].ToString());
            q4 = cmath.getd(item["wa_q4"].ToString());
          }

          SaveWASector_QY("WAOME", iSqlMonthID, "PPP", ytd, q1, q2, q3, q4);

          ytd = 0D;
          q1 = 0D;
          q2 = 0D;
          q3 = 0D;
          q4 = 0D;

          //WABOME
          sql = "EXEC WA_QY_PPP " + pSappiYear + "," + iSqlMonthID + "," + "'NSPPM'" + "," + "'BPOME'";

          dt = cSQL.GetDataTable(sql);
          foreach (DataRow item in dt.Rows)
          {
            ytd = cmath.getd(item["wa_ytd"].ToString());
            q1 = cmath.getd(item["wa_q1"].ToString());
            q2 = cmath.getd(item["wa_q2"].ToString());
            q3 = cmath.getd(item["wa_q3"].ToString());
            q4 = cmath.getd(item["wa_q4"].ToString());
          }

          SaveWASector_QY("WABOME", iSqlMonthID, "PPP", ytd, q1, q2, q3, q4);

        }




      }
      catch (Exception er)
      {
        cFG.LogIt("DoWeightedAverageRegionalPPP procedure");
        cFG.LogIt(er.Message);
      }
      finally
      {
        cORA.CloseDB();
        cSQL.CloseDB();
        cal = null;
      }


    } //


    public void DoWeightedAveRegionalPulp(int pSappiYear)
    {
      //calculate the weighted avarage for kpi's used in CEO report
      //Weighted avarage Budgeted OEE and Actual OEE againt production
      //Weighted avarage Budgeted OME and Actual OME againt production


      string sKPICode = "";
      string sSiteCode = "";
      string FLID = "";

      int iMonth = 0;
      int iMonthTemp = 0;     //for the control break
      int ndx = 0;
      string tmp = "";

      //vars for the calculation of the Regional Weighted Average

      double dSAI1_NSPPM = 0D;
      double dSAI2_NSPPM = 0D;
      double dSAI3_NSPPM = 0D;

      double dSAI1_KPI = 0D;
      double dSAI2_KPI = 0D;
      double dSAI3_KPI = 0D;

      double dNGX1_NSPPM = 0D;
      double dNGX1_KPI = 0D;

      DoInit();     //Initialize the array

      try
      {

        //get year and month
        cal = cal ?? new cCal();

        cal.GetCal();
        iSappiYear = cal.iSappiYear;
        currMonth = cal.iCurrMonthOfYear;


        //if running job previous year
        if ((pSappiYear > 0) && (pSappiYear < iSappiYear))
        {
          iSappiYear = pSappiYear;
          currMonth = 12;             //if last year the the current month is 12 
        }

        //used for update per month starting at the first month
        iSqlMonthID = cal.GetFirstMonthID(iSappiYear);


        //----------------------------------------------------------
        //WABOEE    = budget oee
        string sql = "Select km.mill_code, m.millkey, km.FLID, kd.KpiCode, kd.KpiDesc, kd.Uom"
          + " ,km.sappimonth_id, sm.month_of_year, smn.name_short, sy.year, km.kpi_value"
          + " from    kpimonthly km"
          + " , kpidefinition kd"
          + " , DimPlant p"
          + " , DimMill m"
          + " , sappifiscalcalendar.dbo.tbl_months sm"
          + " , sappifiscalcalendar.dbo.tbl_month_names smn"
          + " , sappifiscalcalendar.dbo.tbl_years sy"
          + " where    km.KPI_CODE = kd.KpiCode"
          + " and km.flid = p.flid"
          + " and km.mill_code = m.SiteAbbrev"
          + " and m.millkey = p.millkey"
          + " and km.sappimonth_id = sm.month_id"
          + " and sm.month_name_id = smn.month_name_id"
          + " and sm.year_id = sy.year_id"
          + " and sy.year = " + iSappiYear
          + " and km.MILL_CODE IN ('SAI','NGX')";

        sql += " and kd.kpicode in ('NSPPM', 'BOE')";
        sql += " order by month_of_year, km.mill_code, km.flid";

        cSQL.OpenDB("ZAMDW");
        DataTable dt = cSQL.GetDataTable(sql);
        cSQL.CloseDB();
        ndx = 0;
        dSAI1_NSPPM = 0D;
        dSAI2_NSPPM = 0D;
        dSAI3_NSPPM = 0D;
        dSAI1_KPI = 0D;
        dSAI2_KPI = 0D;
        dSAI3_KPI = 0D;
        dNGX1_NSPPM = 0D;
        dNGX1_KPI = 0D;

        while (ndx < dt.Rows.Count)
        {
          sKPICode = dt.Rows[ndx]["KpiCode"].ToString();
          iMonth = cmath.geti(dt.Rows[ndx]["month_of_year"].ToString());
          sSiteCode = dt.Rows[ndx]["mill_code"].ToString();
          FLID = dt.Rows[ndx]["FLID"].ToString();
          iMonthTemp = iMonth;

          while ((ndx < dt.Rows.Count) && (iMonth == iMonthTemp))
          {
            if ((sKPICode == "NSPPM") && (sSiteCode == "SAI") && (FLID == "4"))
            {
              dSAI1_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "NSPPM") && (sSiteCode == "SAI") && (FLID == "5"))
            {
              dSAI2_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "NSPPM") && (sSiteCode == "SAI") && (FLID == "6"))
            {
              dSAI3_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "NSPPM") && (sSiteCode == "NGX") && (FLID == "13"))
            {
              dNGX1_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }

            if ((sKPICode == "BOE") && (sSiteCode == "SAI") && (FLID == "4"))
            {
              dSAI1_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "BOE") && (sSiteCode == "SAI") && (FLID == "5"))
            {
              dSAI2_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "BOE") && (sSiteCode == "SAI") && (FLID == "6"))
            {
              dSAI3_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "BOE") && (sSiteCode == "NGX") && (FLID == "13"))
            {
              dNGX1_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }

            ndx++;
            if (ndx < dt.Rows.Count)
            {
              sKPICode = dt.Rows[ndx]["KpiCode"].ToString();
              iMonth = cmath.geti(dt.Rows[ndx]["month_of_year"].ToString());
              sSiteCode = dt.Rows[ndx]["mill_code"].ToString();
              FLID = dt.Rows[ndx]["FLID"].ToString();
            }
          }     //while 2

          //calculate and save values to the array for the month (n)
          t1 = (dNGX1_NSPPM * dNGX1_KPI) + (dSAI1_NSPPM * dSAI1_KPI)
                + (dSAI2_NSPPM * dSAI2_KPI) + (dSAI3_NSPPM * dSAI3_KPI);


          t2 = (dNGX1_NSPPM + dSAI1_NSPPM + dSAI2_NSPPM + dSAI3_NSPPM );
          t3 = cmath.div(t1, t2);

          arr[zKPI_WABOEE, FROM_MONTH + (iMonthTemp - 1)] = Math.Round(t3, ROUND5);

        } // while 1


        SaveDataWASector(zKPI_WABOEE, "WABOEE", "DWP");
        cFG.LogIt("DoWeightedAverageRegionalDWP Finished WABOEE");
        //----------------------------------------------------------



        //----------------------------------------------------------
        //WAOEE       = actual oee
        sql = "Select km.mill_code, m.millkey, km.FLID, kd.KpiCode, kd.KpiDesc, kd.Uom"
          + " ,km.sappimonth_id, sm.month_of_year, smn.name_short, sy.year, km.kpi_value"
          + " from    kpimonthly km"
          + " , kpidefinition kd"
          + " , DimPlant p"
          + " , DimMill m"
          + " , sappifiscalcalendar.dbo.tbl_months sm"
          + " , sappifiscalcalendar.dbo.tbl_month_names smn"
          + " , sappifiscalcalendar.dbo.tbl_years sy"
          + " where    km.KPI_CODE = kd.KpiCode"
          + " and km.flid = p.flid"
          + " and km.mill_code = m.SiteAbbrev"
          + " and m.millkey = p.millkey"
          + " and km.sappimonth_id = sm.month_id"
          + " and sm.month_name_id = smn.month_name_id"
          + " and sm.year_id = sy.year_id"
          + " and sy.year = " + iSappiYear
          + " and km.MILL_CODE IN ('SAI','NGX')";


        //KPI0057 is the OEE
        sql += " and kd.kpicode in ('KPI0035', 'KPI0057')";
        sql += " order by month_of_year, km.mill_code, km.flid";

        cSQL.OpenDB("ZAMDW");
        dt = cSQL.GetDataTable(sql);
        cSQL.CloseDB();
        ndx = 0;
        dSAI1_NSPPM = 0D;
        dSAI2_NSPPM = 0D;
        dSAI3_NSPPM = 0D;
        dSAI1_KPI = 0D;
        dSAI2_KPI = 0D;
        dSAI3_KPI = 0D;
        dNGX1_NSPPM = 0D;
        dNGX1_KPI = 0D;

        while (ndx < dt.Rows.Count)
        {
          sKPICode = dt.Rows[ndx]["KpiCode"].ToString();
          iMonth = cmath.geti(dt.Rows[ndx]["month_of_year"].ToString());
          sSiteCode = dt.Rows[ndx]["mill_code"].ToString();
          FLID = dt.Rows[ndx]["FLID"].ToString();
          iMonthTemp = iMonth;

          while ((ndx < dt.Rows.Count) && (iMonth == iMonthTemp))
          {

            if ((sKPICode == "KPI0035") && (sSiteCode == "SAI") && (FLID == "4"))
            {
              dSAI1_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "KPI0035") && (sSiteCode == "SAI") && (FLID == "5"))
            {
              dSAI2_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "KPI0035") && (sSiteCode == "SAI") && (FLID == "6"))
            {
              dSAI3_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "KPI0035") && (sSiteCode == "NGX") && (FLID == "13"))
            {
              dNGX1_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }

            if ((sKPICode == "KPI0057") && (sSiteCode == "SAI") && (FLID == "4"))
            {
              dSAI1_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "KPI0057") && (sSiteCode == "SAI") && (FLID == "5"))
            {
              dSAI2_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "KPI0057") && (sSiteCode == "SAI") && (FLID == "6"))
            {
              dSAI3_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "KPI0057") && (sSiteCode == "NGX") && (FLID == "13"))
            {
              dNGX1_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }


            ndx++;
            if (ndx < dt.Rows.Count)
            {
              sKPICode = dt.Rows[ndx]["KpiCode"].ToString();
              iMonth = cmath.geti(dt.Rows[ndx]["month_of_year"].ToString());
              sSiteCode = dt.Rows[ndx]["mill_code"].ToString();
              FLID = dt.Rows[ndx]["FLID"].ToString();
            }
          }     //while 2

          //calculate and save values to the array for the month (n)
          t1 = (dNGX1_NSPPM * dNGX1_KPI) + (dSAI1_NSPPM * dSAI1_KPI)
                + (dSAI2_NSPPM * dSAI2_KPI) + (dSAI3_NSPPM * dSAI3_KPI);


          t2 = (dNGX1_NSPPM + dSAI1_NSPPM + dSAI2_NSPPM + dSAI3_NSPPM);
          t3 = cmath.div(t1, t2);

          arr[zKPI_WAOEE, FROM_MONTH + (iMonthTemp - 1)] = Math.Round(t3, ROUND5);


        } // while 1


        SaveDataWASector(zKPI_WAOEE, "WAOEE", "DWP");
        cFG.LogIt("DoWeightedAverageRegionalDWP Finished WAOEE");
        //----------------------------------------------------------


        //----------------------------------------------------------
        //WABOME       = budget OME
        sql = "Select km.mill_code, m.millkey, km.FLID, kd.KpiCode, kd.KpiDesc, kd.Uom"
          + " ,km.sappimonth_id, sm.month_of_year, smn.name_short, sy.year, km.kpi_value"
          + " from    kpimonthly km"
          + " , kpidefinition kd"
          + " , DimPlant p"
          + " , DimMill m"
          + " , sappifiscalcalendar.dbo.tbl_months sm"
          + " , sappifiscalcalendar.dbo.tbl_month_names smn"
          + " , sappifiscalcalendar.dbo.tbl_years sy"
          + " where    km.KPI_CODE = kd.KpiCode"
          + " and km.flid = p.flid"
          + " and km.mill_code = m.SiteAbbrev"
          + " and m.millkey = p.millkey"
          + " and km.sappimonth_id = sm.month_id"
          + " and sm.month_name_id = smn.month_name_id"
          + " and sm.year_id = sy.year_id"
          + " and sy.year = " + iSappiYear
          + " and km.MILL_CODE  IN ('SAI','NGX')";

        sql += " and kd.kpicode in ('BPOME', 'NSPPM')";
        sql += " order by month_of_year, km.mill_code, km.flid";


        cSQL.OpenDB("ZAMDW");
        dt = cSQL.GetDataTable(sql);
        cSQL.CloseDB();
        ndx = 0;
        dSAI1_NSPPM = 0D;
        dSAI2_NSPPM = 0D;
        dSAI3_NSPPM = 0D;
        dSAI1_KPI = 0D;
        dSAI2_KPI = 0D;
        dSAI3_KPI = 0D;
        dNGX1_NSPPM = 0D;
        dNGX1_KPI = 0D;

        while (ndx < dt.Rows.Count)
        {
          sKPICode = dt.Rows[ndx]["KpiCode"].ToString();
          iMonth = cmath.geti(dt.Rows[ndx]["month_of_year"].ToString());
          sSiteCode = dt.Rows[ndx]["mill_code"].ToString();
          FLID = dt.Rows[ndx]["FLID"].ToString();
          iMonthTemp = iMonth;

          while ((ndx < dt.Rows.Count) && (iMonth == iMonthTemp))
          {

            if ((sKPICode == "NSPPM") && (sSiteCode == "SAI") && (FLID == "4"))
            {
              dSAI1_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "NSPPM") && (sSiteCode == "SAI") && (FLID == "5"))
            {
              dSAI2_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "NSPPM") && (sSiteCode == "SAI") && (FLID == "6"))
            {
              dSAI3_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "NSPPM") && (sSiteCode == "NGX") && (FLID == "13"))
            {
              dNGX1_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }

            if ((sKPICode == "BPOME") && (sSiteCode == "SAI") && (FLID == "4"))
            {
              dSAI1_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "BPOME") && (sSiteCode == "SAI") && (FLID == "5"))
            {
              dSAI2_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "BPOME") && (sSiteCode == "SAI") && (FLID == "6"))
            {
              dSAI3_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "BPOME") && (sSiteCode == "NGX") && (FLID == "13"))
            {
              dNGX1_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }

            ndx++;
            if (ndx < dt.Rows.Count)
            {
              sKPICode = dt.Rows[ndx]["KpiCode"].ToString();
              iMonth = cmath.geti(dt.Rows[ndx]["month_of_year"].ToString());
              sSiteCode = dt.Rows[ndx]["mill_code"].ToString();
              FLID = dt.Rows[ndx]["FLID"].ToString();
            }
          }     //while 2

          //calculate and save values to the array for the month (n)
          t1 = (dNGX1_NSPPM * dNGX1_KPI) + (dSAI1_NSPPM * dSAI1_KPI)
                + (dSAI2_NSPPM * dSAI2_KPI) + (dSAI3_NSPPM * dSAI3_KPI);


          t2 = (dNGX1_NSPPM + dSAI1_NSPPM + dSAI2_NSPPM + dSAI3_NSPPM);
          t3 = cmath.div(t1, t2);

          arr[zKPI_WABOME, FROM_MONTH + (iMonthTemp - 1)] = Math.Round(t3, ROUND5);

        } // while 1


        SaveDataWASector(zKPI_WABOME, "WABOME", "DWP");
        cFG.LogIt("DoWeightedAverageRegionalDWP Finished WABOME");
        //----------------------------------------------------------


        //----------------------------------------------------------
        //WAOME       = actual  OME
        sql = "Select km.mill_code, m.millkey, km.FLID, kd.KpiCode, kd.KpiDesc, kd.Uom"
          + " ,km.sappimonth_id, sm.month_of_year, smn.name_short, sy.year, km.kpi_value"
          + " from    kpimonthly km"
          + " , kpidefinition kd"
          + " , DimPlant p"
          + " , DimMill m"
          + " , sappifiscalcalendar.dbo.tbl_months sm"
          + " , sappifiscalcalendar.dbo.tbl_month_names smn"
          + " , sappifiscalcalendar.dbo.tbl_years sy"
          + " where    km.KPI_CODE = kd.KpiCode"
          + " and km.flid = p.flid"
          + " and km.mill_code = m.SiteAbbrev"
          + " and m.millkey = p.millkey"
          + " and km.sappimonth_id = sm.month_id"
          + " and sm.month_name_id = smn.month_name_id"
          + " and sm.year_id = sy.year_id"
          + " and sy.year = " + iSappiYear
          + " and km.MILL_CODE IN ('SAI','NGX')";

        sql += " and kd.kpicode in ('KPI0056', 'NSPPM')";
        sql += " order by month_of_year, km.mill_code, km.flid";


        cSQL.OpenDB("ZAMDW");
        dt = cSQL.GetDataTable(sql);
        cSQL.CloseDB();
        ndx = 0;
        dSAI1_NSPPM = 0D;
        dSAI2_NSPPM = 0D;
        dSAI3_NSPPM = 0D;
        dSAI1_KPI = 0D;
        dSAI2_KPI = 0D;
        dSAI3_KPI = 0D;
        dNGX1_NSPPM = 0D;
        dNGX1_KPI = 0D;

        while (ndx < dt.Rows.Count)
        {
          sKPICode = dt.Rows[ndx]["KpiCode"].ToString();
          iMonth = cmath.geti(dt.Rows[ndx]["month_of_year"].ToString());
          FLID = dt.Rows[ndx]["FLID"].ToString();
          sSiteCode = dt.Rows[ndx]["mill_code"].ToString();
          iMonthTemp = iMonth;

          while ((ndx < dt.Rows.Count) && (iMonth == iMonthTemp))
          {
            if ((sKPICode == "NSPPM") && (sSiteCode == "SAI") && (FLID == "4"))
            {
              dSAI1_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "NSPPM") && (sSiteCode == "SAI") && (FLID == "5"))
            {
              dSAI2_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "NSPPM") && (sSiteCode == "SAI") && (FLID == "6"))
            {
              dSAI3_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "NSPPM") && (sSiteCode == "NGX") && (FLID == "13"))
            {
              dNGX1_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }

            if ((sKPICode == "KPI0056") && (sSiteCode == "SAI") && (FLID == "4"))
            {
              dSAI1_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "KPI0056") && (sSiteCode == "SAI") && (FLID == "5"))
            {
              dSAI2_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "KPI0056") && (sSiteCode == "SAI") && (FLID == "6"))
            {
              dSAI3_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "KPI0056") && (sSiteCode == "NGX") && (FLID == "13"))
            {
              dNGX1_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }


            ndx++;
            if (ndx < dt.Rows.Count)
            {
              sKPICode = dt.Rows[ndx]["KpiCode"].ToString();
              iMonth = cmath.geti(dt.Rows[ndx]["month_of_year"].ToString());
              sSiteCode = dt.Rows[ndx]["mill_code"].ToString();
              FLID = dt.Rows[ndx]["FLID"].ToString();
            }
          }     //while 2

          //calculate and save values to the array for the month (n)
          t1 = (dNGX1_NSPPM * dNGX1_KPI) + (dSAI1_NSPPM * dSAI1_KPI)
                + (dSAI2_NSPPM * dSAI2_KPI) + (dSAI3_NSPPM * dSAI3_KPI);


          t2 = (dNGX1_NSPPM + dSAI1_NSPPM + dSAI2_NSPPM + dSAI3_NSPPM);
          t3 = cmath.div(t1, t2);

          arr[zKPI_WAOME, FROM_MONTH + (iMonthTemp - 1)] = Math.Round(t3, ROUND5);


        } // while 1

        SaveDataWASector(zKPI_WAOME, "WAOME", "DWP");
        cFG.LogIt("DoWeightedAverageRegionalDWP Finished WAOME");
        //----------------------------------------------------------

        //  //old code use calendar for WA,  see new calculations 2024-03-12 I-2403-6663

        ///calculate the quarter and year to date for this site
        //sSite = "NGX";        //defaults just to get the calendar budgets
        //sAreaID = "25";       //defaults just to get the calendar budgets
        //LoadBudgets();      //load the array with the sappi hours in order to calculate the quarters and year to date
        //SumYTD_QTR(zSappiHours, ROUND1);
        //DoProcessWeightedAveRegional_YTD_Q("DWP");


        ///NEW calculations  for ytd and quarters 2024-03-12 I-2403-6663
        double ytd = 0D;
        double q1 = 0D;
        double q2 = 0D;
        double q3 = 0D;
        double q4 = 0D;



        for (int monthNum = currMonth; monthNum > 0; monthNum--)
        {

          iSqlMonthID = cal.GetSappiMonthID(pSappiYear, monthNum);  // mssqlserver month number

          cSQL.OpenDB("ZAMDW");

          //WAOEE
          sql = "EXEC WA_QY_DWP " + pSappiYear + "," + iSqlMonthID + "," + "'KPI0035'" + "," + "'KPI0057'";

          dt = cSQL.GetDataTable(sql);
          foreach (DataRow item in dt.Rows)
          {
            ytd = cmath.getd(item["wa_ytd"].ToString());
            q1 = cmath.getd(item["wa_q1"].ToString());
            q2 = cmath.getd(item["wa_q2"].ToString());
            q3 = cmath.getd(item["wa_q3"].ToString());
            q4 = cmath.getd(item["wa_q4"].ToString());
          }

          SaveWASector_QY("WAOEE", iSqlMonthID, "DWP", ytd, q1, q2, q3, q4);


          ytd = 0D;
          q1 = 0D;
          q2 = 0D;
          q3 = 0D;
          q4 = 0D;



          //WABOEE
          sql = "EXEC WA_QY_DWP " + pSappiYear + "," + iSqlMonthID + "," + "'NSPPM'" + "," + "'BOE'";


          dt = cSQL.GetDataTable(sql);
          foreach (DataRow item in dt.Rows)
          {
            ytd = cmath.getd(item["wa_ytd"].ToString());
            q1 = cmath.getd(item["wa_q1"].ToString());
            q2 = cmath.getd(item["wa_q2"].ToString());
            q3 = cmath.getd(item["wa_q3"].ToString());
            q4 = cmath.getd(item["wa_q4"].ToString());
          }

          SaveWASector_QY("WABOEE", iSqlMonthID, "DWP", ytd, q1, q2, q3, q4);

          ytd = 0D;
          q1 = 0D;
          q2 = 0D;
          q3 = 0D;
          q4 = 0D;



          //WAOME
          sql = "EXEC WA_QY_DWP " + pSappiYear + "," + iSqlMonthID + "," + "'NSPPM'" + "," + "'KPI0056'";

          dt = cSQL.GetDataTable(sql);
          foreach (DataRow item in dt.Rows)
          {
            ytd = cmath.getd(item["wa_ytd"].ToString());
            q1 = cmath.getd(item["wa_q1"].ToString());
            q2 = cmath.getd(item["wa_q2"].ToString());
            q3 = cmath.getd(item["wa_q3"].ToString());
            q4 = cmath.getd(item["wa_q4"].ToString());
          }

          SaveWASector_QY("WAOME", iSqlMonthID, "DWP", ytd, q1, q2, q3, q4);

          ytd = 0D;
          q1 = 0D;
          q2 = 0D;
          q3 = 0D;
          q4 = 0D;

          //WABOME
          sql = "EXEC WA_QY_DWP " + pSappiYear + "," + iSqlMonthID + "," + "'NSPPM'" + "," + "'BPOME'";

          dt = cSQL.GetDataTable(sql);
          foreach (DataRow item in dt.Rows)
          {
            ytd = cmath.getd(item["wa_ytd"].ToString());
            q1 = cmath.getd(item["wa_q1"].ToString());
            q2 = cmath.getd(item["wa_q2"].ToString());
            q3 = cmath.getd(item["wa_q3"].ToString());
            q4 = cmath.getd(item["wa_q4"].ToString());
          }

          SaveWASector_QY("WABOME", iSqlMonthID, "DWP", ytd, q1, q2, q3, q4);

        }


      }
      catch (Exception er)
      {
        cFG.LogIt("DoWeightedAverageRegionalDWP procedure");
        cFG.LogIt(er.Message);
      }
      finally
      {
        cSQL.CloseDB();
        cal = null;
      }


    } //


    public void DoWeightedAveRegionalDigBatch(int pSappiYear)
    {
      //calculate the weighted avarage for kpi's used in CEO report
      //Weighted avarage Budgeted OEE and Actual OEE againt production
      //Weighted avarage Budgeted OME and Actual OME againt production

      //BATCH DIGESTERS


      string sKPICode = "";
      string sSiteCode = "";
      string FLID = "";

      int iMonth = 0;
      int iMonthTemp = 0;     //for the control break
      int ndx = 0;
      string tmp = "";

      //vars for the calculation of the Regional Weighted Average

      double dSAI1_NSPPM = 0D;
      double dSAI2_NSPPM = 0D;
      double dSAI3_NSPPM = 0D;
      double dSAI4_NSPPM = 0D;

      double dSAI1_KPI = 0D;
      double dSAI2_KPI = 0D;
      double dSAI3_KPI = 0D;
      double dSAI4_KPI = 0D;

      double dNGX1_NSPPM = 0D;
      double dNGX1_KPI = 0D;

      DoInit();     //Initialize the array

      try
      {

        cal = cal ?? new cCal();

        cal.GetCal();
        iSappiYear = cal.iSappiYear;
        currMonth = cal.iCurrMonthOfYear;


        //if running job previous year
        if ((pSappiYear > 0) && (pSappiYear < iSappiYear))
        {
          iSappiYear = pSappiYear;
          currMonth = 12;             //if last year the the current month is 12 
        }

        //used for update per month starting at the first month
        iSqlMonthID = cal.GetFirstMonthID(iSappiYear);


        //----------------------------------------------------------
        //WABOEE    = budget oee
        string sql = "Select km.mill_code, m.millkey, km.FLID, kd.KpiCode, kd.KpiDesc, kd.Uom"
          + " ,km.sappimonth_id, sm.month_of_year, smn.name_short, sy.year, km.kpi_value"
          + " from    kpimonthly km"
          + " , kpidefinition kd"
          + " , DimPlant p"
          + " , DimMill m"
          + " , sappifiscalcalendar.dbo.tbl_months sm"
          + " , sappifiscalcalendar.dbo.tbl_month_names smn"
          + " , sappifiscalcalendar.dbo.tbl_years sy"
          + " where    km.KPI_CODE = kd.KpiCode"
          + " and km.flid = p.flid"
          + " and km.mill_code = m.SiteAbbrev"
          + " and m.millkey = p.millkey"
          + " and km.sappimonth_id = sm.month_id"
          + " and sm.month_name_id = smn.month_name_id"
          + " and sm.year_id = sy.year_id"
          + " and sy.year = " + iSappiYear
          + " and km.MILL_CODE IN ('SAI','NGX')";

        sql += " and kd.kpicode in ('NSPPM', 'BOE')";
        sql += " order by month_of_year, km.mill_code, km.flid";

        cSQL.OpenDB("ZAMDW");
        DataTable dt = cSQL.GetDataTable(sql);
        cSQL.CloseDB();
        ndx = 0;
        dSAI1_NSPPM = 0D;
        dSAI2_NSPPM = 0D;
        dSAI3_NSPPM = 0D;
        dSAI4_NSPPM = 0D;

        dSAI1_KPI = 0D;
        dSAI2_KPI = 0D;
        dSAI3_KPI = 0D;
        dSAI4_KPI = 0D;

        dNGX1_NSPPM = 0D;
        dNGX1_KPI = 0D;

        while (ndx < dt.Rows.Count)
        {
          sKPICode = dt.Rows[ndx]["KpiCode"].ToString();
          iMonth = cmath.geti(dt.Rows[ndx]["month_of_year"].ToString());
          sSiteCode = dt.Rows[ndx]["mill_code"].ToString();
          FLID = dt.Rows[ndx]["FLID"].ToString();
          iMonthTemp = iMonth;

          while ((ndx < dt.Rows.Count) && (iMonth == iMonthTemp))
          {
            if ((sKPICode == "NSPPM") && (sSiteCode == "SAI") && (FLID == "1"))
            {
              dSAI1_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "NSPPM") && (sSiteCode == "SAI") && (FLID == "2"))
            {
              dSAI2_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "NSPPM") && (sSiteCode == "SAI") && (FLID == "3"))
            {
              dSAI3_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "NSPPM") && (sSiteCode == "SAI") && (FLID == "65"))
            {
              dSAI4_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "NSPPM") && (sSiteCode == "NGX") && (FLID == "161"))
            {
              dNGX1_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }

            if ((sKPICode == "BOE") && (sSiteCode == "SAI") && (FLID == "1"))
            {
              dSAI1_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "BOE") && (sSiteCode == "SAI") && (FLID == "2"))
            {
              dSAI2_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "BOE") && (sSiteCode == "SAI") && (FLID == "3"))
            {
              dSAI3_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "BOE") && (sSiteCode == "SAI") && (FLID == "65"))
            {
              dSAI4_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "BOE") && (sSiteCode == "NGX") && (FLID == "161"))
            {
              dNGX1_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }

            ndx++;
            if (ndx < dt.Rows.Count)
            {
              sKPICode = dt.Rows[ndx]["KpiCode"].ToString();
              iMonth = cmath.geti(dt.Rows[ndx]["month_of_year"].ToString());
              sSiteCode = dt.Rows[ndx]["mill_code"].ToString();
              FLID = dt.Rows[ndx]["FLID"].ToString();
            }
          }     //while 2

          //calculate and save values to the array for the month (n)
          t1 = (dNGX1_NSPPM * dNGX1_KPI) + (dSAI1_NSPPM * dSAI1_KPI)
                + (dSAI2_NSPPM * dSAI2_KPI) + (dSAI3_NSPPM * dSAI3_KPI)
                + (dSAI4_NSPPM * dSAI4_KPI);


          t2 = (dNGX1_NSPPM + dSAI1_NSPPM + dSAI2_NSPPM + dSAI3_NSPPM + dSAI4_NSPPM);
          t3 = cmath.div(t1, t2);

          arr[zKPI_WABOEE, FROM_MONTH + (iMonthTemp - 1)] = Math.Round(t3, ROUND5);

        } // while 1


        SaveDataWASector(zKPI_WABOEE, "WABOEE", "DIGB");    //batch digesters
        cFG.LogIt("DoWeightedAverageRegionalDIGB Finished WABOEE");
        //----------------------------------------------------------



        //----------------------------------------------------------
        //WAOEE       = actual oee
        sql = "Select km.mill_code, m.millkey, km.FLID, kd.KpiCode, kd.KpiDesc, kd.Uom"
          + " ,km.sappimonth_id, sm.month_of_year, smn.name_short, sy.year, km.kpi_value"
          + " from    kpimonthly km"
          + " , kpidefinition kd"
          + " , DimPlant p"
          + " , DimMill m"
          + " , sappifiscalcalendar.dbo.tbl_months sm"
          + " , sappifiscalcalendar.dbo.tbl_month_names smn"
          + " , sappifiscalcalendar.dbo.tbl_years sy"
          + " where    km.KPI_CODE = kd.KpiCode"
          + " and km.flid = p.flid"
          + " and km.mill_code = m.SiteAbbrev"
          + " and m.millkey = p.millkey"
          + " and km.sappimonth_id = sm.month_id"
          + " and sm.month_name_id = smn.month_name_id"
          + " and sm.year_id = sy.year_id"
          + " and sy.year = " + iSappiYear
          + " and km.MILL_CODE IN ('SAI','NGX')";


        //KPI0057 is the OEE
        sql += " and kd.kpicode in ('KPI0035', 'KPI0057')";
        sql += " order by month_of_year, km.mill_code, km.flid";

        cSQL.OpenDB("ZAMDW");
        dt = cSQL.GetDataTable(sql);
        cSQL.CloseDB();
        ndx = 0;
        dSAI1_NSPPM = 0D;
        dSAI2_NSPPM = 0D;
        dSAI3_NSPPM = 0D;
        dSAI1_KPI = 0D;
        dSAI2_KPI = 0D;
        dSAI3_KPI = 0D;
        dNGX1_NSPPM = 0D;
        dNGX1_KPI = 0D;

        while (ndx < dt.Rows.Count)
        {
          sKPICode = dt.Rows[ndx]["KpiCode"].ToString();
          iMonth = cmath.geti(dt.Rows[ndx]["month_of_year"].ToString());
          sSiteCode = dt.Rows[ndx]["mill_code"].ToString();
          FLID = dt.Rows[ndx]["FLID"].ToString();
          iMonthTemp = iMonth;

          while ((ndx < dt.Rows.Count) && (iMonth == iMonthTemp))
          {

            if ((sKPICode == "KPI0035") && (sSiteCode == "SAI") && (FLID == "1"))
            {
              dSAI1_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "KPI0035") && (sSiteCode == "SAI") && (FLID == "2"))
            {
              dSAI2_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "KPI0035") && (sSiteCode == "SAI") && (FLID == "3"))
            {
              dSAI3_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "KPI0035") && (sSiteCode == "SAI") && (FLID == "65"))
            {
              dSAI4_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "KPI0035") && (sSiteCode == "NGX") && (FLID == "161"))
            {
              dNGX1_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }


            if ((sKPICode == "KPI0057") && (sSiteCode == "SAI") && (FLID == "1"))
            {
              dSAI1_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "KPI0057") && (sSiteCode == "SAI") && (FLID == "2"))
            {
              dSAI2_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "KPI0057") && (sSiteCode == "SAI") && (FLID == "3"))
            {
              dSAI3_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "KPI0057") && (sSiteCode == "SAI") && (FLID == "65"))
            {
              dSAI4_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "KPI0057") && (sSiteCode == "NGX") && (FLID == "161"))
            {
              dNGX1_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }


            ndx++;
            if (ndx < dt.Rows.Count)
            {
              sKPICode = dt.Rows[ndx]["KpiCode"].ToString();
              iMonth = cmath.geti(dt.Rows[ndx]["month_of_year"].ToString());
              sSiteCode = dt.Rows[ndx]["mill_code"].ToString();
              FLID = dt.Rows[ndx]["FLID"].ToString();
            }
          }     //while 2

          //calculate and save values to the array for the month (n)
          t1 = (dNGX1_NSPPM * dNGX1_KPI) + (dSAI1_NSPPM * dSAI1_KPI)
                + (dSAI2_NSPPM * dSAI2_KPI) + (dSAI3_NSPPM * dSAI3_KPI)
                + (dSAI4_NSPPM * dSAI4_KPI);


          t2 = (dNGX1_NSPPM + dSAI1_NSPPM + dSAI2_NSPPM + dSAI3_NSPPM + dSAI4_NSPPM);
          t3 = cmath.div(t1, t2);

          arr[zKPI_WAOEE, FROM_MONTH + (iMonthTemp - 1)] = Math.Round(t3, ROUND5);


        } // while 1


        SaveDataWASector(zKPI_WAOEE, "WAOEE", "DIGB");
        cFG.LogIt("DoWeightedAverageRegionalDIGB Finished WAOEE");
        //----------------------------------------------------------


        //----------------------------------------------------------
        //WABOME       = budget OME
        sql = "Select km.mill_code, m.millkey, km.FLID, kd.KpiCode, kd.KpiDesc, kd.Uom"
          + " ,km.sappimonth_id, sm.month_of_year, smn.name_short, sy.year, km.kpi_value"
          + " from    kpimonthly km"
          + " , kpidefinition kd"
          + " , DimPlant p"
          + " , DimMill m"
          + " , sappifiscalcalendar.dbo.tbl_months sm"
          + " , sappifiscalcalendar.dbo.tbl_month_names smn"
          + " , sappifiscalcalendar.dbo.tbl_years sy"
          + " where    km.KPI_CODE = kd.KpiCode"
          + " and km.flid = p.flid"
          + " and km.mill_code = m.SiteAbbrev"
          + " and m.millkey = p.millkey"
          + " and km.sappimonth_id = sm.month_id"
          + " and sm.month_name_id = smn.month_name_id"
          + " and sm.year_id = sy.year_id"
          + " and sy.year = " + iSappiYear
          + " and km.MILL_CODE  IN ('SAI','NGX')";

        sql += " and kd.kpicode in ('BPOME', 'NSPPM')";
        sql += " order by month_of_year, km.mill_code, km.flid";


        cSQL.OpenDB("ZAMDW");
        dt = cSQL.GetDataTable(sql);
        cSQL.CloseDB();
        ndx = 0;
        dSAI1_NSPPM = 0D;
        dSAI2_NSPPM = 0D;
        dSAI3_NSPPM = 0D;
        dSAI1_KPI = 0D;
        dSAI2_KPI = 0D;
        dSAI3_KPI = 0D;
        dNGX1_NSPPM = 0D;
        dNGX1_KPI = 0D;

        while (ndx < dt.Rows.Count)
        {
          sKPICode = dt.Rows[ndx]["KpiCode"].ToString();
          iMonth = cmath.geti(dt.Rows[ndx]["month_of_year"].ToString());
          sSiteCode = dt.Rows[ndx]["mill_code"].ToString();
          FLID = dt.Rows[ndx]["FLID"].ToString();
          iMonthTemp = iMonth;

          while ((ndx < dt.Rows.Count) && (iMonth == iMonthTemp))
          {

            if ((sKPICode == "NSPPM") && (sSiteCode == "SAI") && (FLID == "1"))
            {
              dSAI1_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "NSPPM") && (sSiteCode == "SAI") && (FLID == "2"))
            {
              dSAI2_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "NSPPM") && (sSiteCode == "SAI") && (FLID == "3"))
            {
              dSAI3_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "NSPPM") && (sSiteCode == "SAI") && (FLID == "65"))
            {
              dSAI4_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "NSPPM") && (sSiteCode == "NGX") && (FLID == "161"))
            {
              dNGX1_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }

            if ((sKPICode == "BPOME") && (sSiteCode == "SAI") && (FLID == "1"))
            {
              dSAI1_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "BPOME") && (sSiteCode == "SAI") && (FLID == "2"))
            {
              dSAI2_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "BPOME") && (sSiteCode == "SAI") && (FLID == "3"))
            {
              dSAI3_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "BPOME") && (sSiteCode == "SAI") && (FLID == "65"))
            {
              dSAI4_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "BPOME") && (sSiteCode == "NGX") && (FLID == "161"))
            {
              dNGX1_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }

            ndx++;
            if (ndx < dt.Rows.Count)
            {
              sKPICode = dt.Rows[ndx]["KpiCode"].ToString();
              iMonth = cmath.geti(dt.Rows[ndx]["month_of_year"].ToString());
              sSiteCode = dt.Rows[ndx]["mill_code"].ToString();
              FLID = dt.Rows[ndx]["FLID"].ToString();
            }
          }     //while 2

          //calculate and save values to the array for the month (n)
          t1 = (dNGX1_NSPPM * dNGX1_KPI) + (dSAI1_NSPPM * dSAI1_KPI)
                + (dSAI2_NSPPM * dSAI2_KPI) + (dSAI3_NSPPM * dSAI3_KPI) + (dSAI4_NSPPM * dSAI4_KPI);


          t2 = (dNGX1_NSPPM + dSAI1_NSPPM + dSAI2_NSPPM + dSAI3_NSPPM + dSAI4_NSPPM);
          t3 = cmath.div(t1, t2);

          arr[zKPI_WABOME, FROM_MONTH + (iMonthTemp - 1)] = Math.Round(t3, ROUND5);

        } // while 1


        SaveDataWASector(zKPI_WABOME, "WABOME", "DIGB");
        cFG.LogIt("DoWeightedAverageRegionalDIGB Finished WABOME");
        //----------------------------------------------------------


        //----------------------------------------------------------
        //WAOME       = actual  OME
        sql = "Select km.mill_code, m.millkey, km.FLID, kd.KpiCode, kd.KpiDesc, kd.Uom"
          + " ,km.sappimonth_id, sm.month_of_year, smn.name_short, sy.year, km.kpi_value"
          + " from    kpimonthly km"
          + " , kpidefinition kd"
          + " , DimPlant p"
          + " , DimMill m"
          + " , sappifiscalcalendar.dbo.tbl_months sm"
          + " , sappifiscalcalendar.dbo.tbl_month_names smn"
          + " , sappifiscalcalendar.dbo.tbl_years sy"
          + " where    km.KPI_CODE = kd.KpiCode"
          + " and km.flid = p.flid"
          + " and km.mill_code = m.SiteAbbrev"
          + " and m.millkey = p.millkey"
          + " and km.sappimonth_id = sm.month_id"
          + " and sm.month_name_id = smn.month_name_id"
          + " and sm.year_id = sy.year_id"
          + " and sy.year = " + iSappiYear
          + " and km.MILL_CODE IN ('SAI','NGX')";

        sql += " and kd.kpicode in ('KPI0056', 'NSPPM')";
        sql += " order by month_of_year, km.mill_code, km.flid";


        cSQL.OpenDB("ZAMDW");
        dt = cSQL.GetDataTable(sql);
        cSQL.CloseDB();
        ndx = 0;
        dSAI1_NSPPM = 0D;
        dSAI2_NSPPM = 0D;
        dSAI3_NSPPM = 0D;
        dSAI1_KPI = 0D;
        dSAI2_KPI = 0D;
        dSAI3_KPI = 0D;
        dNGX1_NSPPM = 0D;
        dNGX1_KPI = 0D;


        while (ndx < dt.Rows.Count)
        {
          sKPICode = dt.Rows[ndx]["KpiCode"].ToString();
          iMonth = cmath.geti(dt.Rows[ndx]["month_of_year"].ToString());
          FLID = dt.Rows[ndx]["FLID"].ToString();
          sSiteCode = dt.Rows[ndx]["mill_code"].ToString();
          iMonthTemp = iMonth;

          while ((ndx < dt.Rows.Count) && (iMonth == iMonthTemp))
          {
            if ((sKPICode == "NSPPM") && (sSiteCode == "SAI") && (FLID == "1"))
            {
              dSAI1_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "NSPPM") && (sSiteCode == "SAI") && (FLID == "2"))
            {
              dSAI2_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "NSPPM") && (sSiteCode == "SAI") && (FLID == "3"))
            {
              dSAI3_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "NSPPM") && (sSiteCode == "SAI") && (FLID == "65"))
            {
              dSAI4_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "NSPPM") && (sSiteCode == "NGX") && (FLID == "161"))
            {
              dNGX1_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }

            if ((sKPICode == "KPI0056") && (sSiteCode == "SAI") && (FLID == "1"))
            {
              dSAI1_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "KPI0056") && (sSiteCode == "SAI") && (FLID == "2"))
            {
              dSAI2_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "KPI0056") && (sSiteCode == "SAI") && (FLID == "3"))
            {
              dSAI3_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "KPI0056") && (sSiteCode == "SAI") && (FLID == "65"))
            {
              dSAI4_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "KPI0056") && (sSiteCode == "NGX") && (FLID == "161"))
            {
              dNGX1_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }


            ndx++;
            if (ndx < dt.Rows.Count)
            {
              sKPICode = dt.Rows[ndx]["KpiCode"].ToString();
              iMonth = cmath.geti(dt.Rows[ndx]["month_of_year"].ToString());
              sSiteCode = dt.Rows[ndx]["mill_code"].ToString();
              FLID = dt.Rows[ndx]["FLID"].ToString();
            }
          }     //while 2

          //calculate and save values to the array for the month (n)
          t1 = (dNGX1_NSPPM * dNGX1_KPI) + (dSAI1_NSPPM * dSAI1_KPI)
                + (dSAI2_NSPPM * dSAI2_KPI) + (dSAI3_NSPPM * dSAI3_KPI) + (dSAI4_NSPPM * dSAI4_KPI);


          t2 = (dNGX1_NSPPM + dSAI1_NSPPM + dSAI2_NSPPM + dSAI3_NSPPM + dSAI4_NSPPM);
          t3 = cmath.div(t1, t2);

          arr[zKPI_WAOME, FROM_MONTH + (iMonthTemp - 1)] = Math.Round(t3, ROUND5);


        } // while 1

        SaveDataWASector(zKPI_WAOME, "WAOME", "DIGB");
        cFG.LogIt("DoWeightedAverageRegionalDIGB Finished WAOME");
        //----------------------------------------------------------

        //  //old code use calendar for WA,  see new calculations 2024-03-12 I-2403-6663

        ///calculate the quarter and year to date for this site
        //sSite = "NGX";        //defaults just to get the calendar budgets
        //sAreaID = "25";       //defaults just to get the calendar budgets
        //LoadBudgets();      //load the array with the sappi hours in order to calculate the quarters and year to date
        //SumYTD_QTR(zSappiHours, ROUND1);
        //DoProcessWeightedAveRegional_YTD_Q("DIGB");


        ///NEW calculations  for ytd and quarters 2024-03-12 I-2403-6663
        double ytd = 0D;
        double q1 = 0D;
        double q2 = 0D;
        double q3 = 0D;
        double q4 = 0D;



        for (int monthNum = currMonth; monthNum > 0; monthNum--)
        {

          iSqlMonthID = cal.GetSappiMonthID(pSappiYear, monthNum);  // mssqlserver month number

          cSQL.OpenDB("ZAMDW");

          //WAOEE
          sql = "EXEC WA_QY_DIGB " + pSappiYear + "," + iSqlMonthID + "," + "'KPI0035'" + "," + "'KPI0057'";

          dt = cSQL.GetDataTable(sql);
          foreach (DataRow item in dt.Rows)
          {
            ytd = cmath.getd(item["wa_ytd"].ToString());
            q1 = cmath.getd(item["wa_q1"].ToString());
            q2 = cmath.getd(item["wa_q2"].ToString());
            q3 = cmath.getd(item["wa_q3"].ToString());
            q4 = cmath.getd(item["wa_q4"].ToString());
          }

          SaveWASector_QY("WAOEE", iSqlMonthID, "DIGB", ytd, q1, q2, q3, q4);


          ytd = 0D;
          q1 = 0D;
          q2 = 0D;
          q3 = 0D;
          q4 = 0D;



          //WABOEE
          sql = "EXEC WA_QY_DIGB " + pSappiYear + "," + iSqlMonthID + "," + "'NSPPM'" + "," + "'BOE'";


          dt = cSQL.GetDataTable(sql);
          foreach (DataRow item in dt.Rows)
          {
            ytd = cmath.getd(item["wa_ytd"].ToString());
            q1 = cmath.getd(item["wa_q1"].ToString());
            q2 = cmath.getd(item["wa_q2"].ToString());
            q3 = cmath.getd(item["wa_q3"].ToString());
            q4 = cmath.getd(item["wa_q4"].ToString());
          }

          SaveWASector_QY("WABOEE", iSqlMonthID, "DIGB", ytd, q1, q2, q3, q4);

          ytd = 0D;
          q1 = 0D;
          q2 = 0D;
          q3 = 0D;
          q4 = 0D;



          //WAOME
          sql = "EXEC WA_QY_DIGB " + pSappiYear + "," + iSqlMonthID + "," + "'NSPPM'" + "," + "'KPI0056'";

          dt = cSQL.GetDataTable(sql);
          foreach (DataRow item in dt.Rows)
          {
            ytd = cmath.getd(item["wa_ytd"].ToString());
            q1 = cmath.getd(item["wa_q1"].ToString());
            q2 = cmath.getd(item["wa_q2"].ToString());
            q3 = cmath.getd(item["wa_q3"].ToString());
            q4 = cmath.getd(item["wa_q4"].ToString());
          }

          SaveWASector_QY("WAOME", iSqlMonthID, "DIGB", ytd, q1, q2, q3, q4);

          ytd = 0D;
          q1 = 0D;
          q2 = 0D;
          q3 = 0D;
          q4 = 0D;

          //WABOME
          sql = "EXEC WA_QY_DIGB " + pSappiYear + "," + iSqlMonthID + "," + "'NSPPM'" + "," + "'BPOME'";

          dt = cSQL.GetDataTable(sql);
          foreach (DataRow item in dt.Rows)
          {
            ytd = cmath.getd(item["wa_ytd"].ToString());
            q1 = cmath.getd(item["wa_q1"].ToString());
            q2 = cmath.getd(item["wa_q2"].ToString());
            q3 = cmath.getd(item["wa_q3"].ToString());
            q4 = cmath.getd(item["wa_q4"].ToString());
          }

          SaveWASector_QY("WABOME", iSqlMonthID, "DIGB", ytd, q1, q2, q3, q4);

        }
      
      }
      catch (Exception er)
      {
        cFG.LogIt("DoWeightedAverageRegionalDIGB procedure");
        cFG.LogIt(er.Message);
      }
      finally
      {
        cSQL.CloseDB();
        cal = null;
      }


    } //


    public void DoWeightedAveRegionalDigCont(int pSappiYear)
    {
      //calculate the weighted avarage for kpi's used in CEO report
      //Weighted avarage Budgeted OEE and Actual OEE againt production
      //Weighted avarage Budgeted OME and Actual OME againt production

      //CONTINUOUS DIGESTERS


      string sKPICode = "";
      string sSiteCode = "";
      string FLID = "";

      int iMonth = 0;
      int iMonthTemp = 0;     //for the control break
      int ndx = 0;
      string tmp = "";

      //vars for the calculation of the Regional Weighted Average

      double dNGX1_NSPPM = 0D;
      double dNGX1_KPI = 0D;

      double dTUG1_NSPPM = 0D;
      double dTUG1_KPI = 0D;

      double dSTA1_NSPPM = 0D;
      double dSTA1_KPI = 0D;

      DoInit();     //Initialize the array

      try
      {
        cal = cal ?? new cCal();

        //get year and month

        cal.GetCal();
        iSappiYear = cal.iSappiYear;
        currMonth = cal.iCurrMonthOfYear;


        //if running job previous year
        if ((pSappiYear > 0) && (pSappiYear < iSappiYear))
        {
          iSappiYear = pSappiYear;
          currMonth = 12;             //if last year the the current month is 12 
        }

        //used for update per month starting at the first month
        iSqlMonthID = cal.GetFirstMonthID(iSappiYear);


        //----------------------------------------------------------
        //WABOEE    = budget oee
        string sql = "Select km.mill_code, m.millkey, km.FLID, kd.KpiCode, kd.KpiDesc, kd.Uom"
          + " ,km.sappimonth_id, sm.month_of_year, smn.name_short, sy.year, km.kpi_value"
          + " from    kpimonthly km"
          + " , kpidefinition kd"
          + " , DimPlant p"
          + " , DimMill m"
          + " , sappifiscalcalendar.dbo.tbl_months sm"
          + " , sappifiscalcalendar.dbo.tbl_month_names smn"
          + " , sappifiscalcalendar.dbo.tbl_years sy"
          + " where    km.KPI_CODE = kd.KpiCode"
          + " and km.flid = p.flid"
          + " and km.mill_code = m.SiteAbbrev"
          + " and m.millkey = p.millkey"
          + " and km.sappimonth_id = sm.month_id"
          + " and sm.month_name_id = smn.month_name_id"
          + " and sm.year_id = sy.year_id"
          + " and sy.year = " + iSappiYear
          + " and km.MILL_CODE IN ('STA','NGX','TUG')";

        sql += " and kd.kpicode in ('NSPPM', 'BOE')";
        sql += " order by month_of_year, km.mill_code, km.flid";

        cSQL.OpenDB("ZAMDW");
        DataTable dt = cSQL.GetDataTable(sql);
        cSQL.CloseDB();
        ndx = 0;
        
        dNGX1_NSPPM = 0D;
        dNGX1_KPI = 0D;

        dTUG1_NSPPM = 0D;
        dTUG1_KPI = 0D;

        dSTA1_NSPPM = 0D;
        dSTA1_KPI = 0D;
        


        while (ndx < dt.Rows.Count)
        {
          sKPICode = dt.Rows[ndx]["KpiCode"].ToString();
          iMonth = cmath.geti(dt.Rows[ndx]["month_of_year"].ToString());
          sSiteCode = dt.Rows[ndx]["mill_code"].ToString();
          FLID = dt.Rows[ndx]["FLID"].ToString();
          iMonthTemp = iMonth;

          while ((ndx < dt.Rows.Count) && (iMonth == iMonthTemp))
          {
            if ((sKPICode == "NSPPM") && (sSiteCode == "STA") && (FLID == "1"))
            {
              dSTA1_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "NSPPM") && (sSiteCode == "TUG") && (FLID == "126"))
            {
              dTUG1_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "NSPPM") && (sSiteCode == "NGX") && (FLID == "5"))
            {
              dNGX1_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }

            if ((sKPICode == "BOE") && (sSiteCode == "STA") && (FLID == "1"))
            {
              dSTA1_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "BOE") && (sSiteCode == "TUG") && (FLID == "126"))
            {
              dTUG1_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "BOE") && (sSiteCode == "NGX") && (FLID == "5"))
            {
              dNGX1_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }

            ndx++;
            if (ndx < dt.Rows.Count)
            {
              sKPICode = dt.Rows[ndx]["KpiCode"].ToString();
              iMonth = cmath.geti(dt.Rows[ndx]["month_of_year"].ToString());
              sSiteCode = dt.Rows[ndx]["mill_code"].ToString();
              FLID = dt.Rows[ndx]["FLID"].ToString();
            }
          }     //while 2

          //calculate and save values to the array for the month (n)
          t1 = (dNGX1_NSPPM * dNGX1_KPI) + (dSTA1_NSPPM * dSTA1_KPI) + (dTUG1_NSPPM * dTUG1_KPI);

          t2 = (dNGX1_NSPPM + dSTA1_NSPPM + dTUG1_NSPPM);
          t3 = cmath.div(t1, t2);

          arr[zKPI_WABOEE, FROM_MONTH + (iMonthTemp - 1)] = Math.Round(t3, ROUND5);

          
        } // while 1


        SaveDataWASector(zKPI_WABOEE, "WABOEE", "DIGC");    //cont digesters
        cFG.LogIt("DoWeightedAverageRegionalDIGC Finished WABOEE");
        //----------------------------------------------------------



        //----------------------------------------------------------
        //WAOEE       = actual oee
        sql = "Select km.mill_code, m.millkey, km.FLID, kd.KpiCode, kd.KpiDesc, kd.Uom"
          + " ,km.sappimonth_id, sm.month_of_year, smn.name_short, sy.year, km.kpi_value"
          + " from    kpimonthly km"
          + " , kpidefinition kd"
          + " , DimPlant p"
          + " , DimMill m"
          + " , sappifiscalcalendar.dbo.tbl_months sm"
          + " , sappifiscalcalendar.dbo.tbl_month_names smn"
          + " , sappifiscalcalendar.dbo.tbl_years sy"
          + " where    km.KPI_CODE = kd.KpiCode"
          + " and km.flid = p.flid"
          + " and km.mill_code = m.SiteAbbrev"
          + " and m.millkey = p.millkey"
          + " and km.sappimonth_id = sm.month_id"
          + " and sm.month_name_id = smn.month_name_id"
          + " and sm.year_id = sy.year_id"
          + " and sy.year = " + iSappiYear
          + " and km.MILL_CODE IN ('STA','NGX','TUG')";


        //KPI0057 is the OEE
        sql += " and kd.kpicode in ('KPI0035', 'KPI0057')";
        sql += " order by month_of_year, km.mill_code, km.flid";

        cSQL.OpenDB("ZAMDW");
        dt = cSQL.GetDataTable(sql);
        cSQL.CloseDB();
        ndx = 0;

        dNGX1_NSPPM = 0D;
        dNGX1_KPI = 0D;

        dTUG1_NSPPM = 0D;
        dTUG1_KPI = 0D;

        dSTA1_NSPPM = 0D;
        dSTA1_KPI = 0D;

        while (ndx < dt.Rows.Count)
        {
          sKPICode = dt.Rows[ndx]["KpiCode"].ToString();
          iMonth = cmath.geti(dt.Rows[ndx]["month_of_year"].ToString());
          sSiteCode = dt.Rows[ndx]["mill_code"].ToString();
          FLID = dt.Rows[ndx]["FLID"].ToString();
          iMonthTemp = iMonth;

          while ((ndx < dt.Rows.Count) && (iMonth == iMonthTemp))
          {

            if ((sKPICode == "KPI0035") && (sSiteCode == "STA") && (FLID == "1"))
            {
              dSTA1_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "KPI0035") && (sSiteCode == "TUG") && (FLID == "126"))
            {
              dTUG1_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "KPI0035") && (sSiteCode == "NGX") && (FLID == "5"))
            {
              dNGX1_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }


            if ((sKPICode == "KPI0057") && (sSiteCode == "STA") && (FLID == "1"))
            {
              dSTA1_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "KPI0057") && (sSiteCode == "TUG") && (FLID == "126"))
            {
              dTUG1_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "KPI0057") && (sSiteCode == "NGX") && (FLID == "5"))
            {
              dNGX1_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }


            ndx++;
            if (ndx < dt.Rows.Count)
            {
              sKPICode = dt.Rows[ndx]["KpiCode"].ToString();
              iMonth = cmath.geti(dt.Rows[ndx]["month_of_year"].ToString());
              sSiteCode = dt.Rows[ndx]["mill_code"].ToString();
              FLID = dt.Rows[ndx]["FLID"].ToString();
            }
          }     //while 2

          //calculate and save values to the array for the month (n)
          t1 = (dNGX1_NSPPM * dNGX1_KPI) + (dSTA1_NSPPM * dSTA1_KPI) + (dTUG1_NSPPM * dTUG1_KPI);

          t2 = (dNGX1_NSPPM + dSTA1_NSPPM + dTUG1_NSPPM);

          t3 = cmath.div(t1, t2);

          arr[zKPI_WAOEE, FROM_MONTH + (iMonthTemp - 1)] = Math.Round(t3, ROUND5);


        } // while 1


        SaveDataWASector(zKPI_WAOEE, "WAOEE", "DIGC");
        cFG.LogIt("DoWeightedAverageRegionalDIGC Finished WAOEE");
        //----------------------------------------------------------


        //----------------------------------------------------------
        //WABOME       = budget OME
        sql = "Select km.mill_code, m.millkey, km.FLID, kd.KpiCode, kd.KpiDesc, kd.Uom"
          + " ,km.sappimonth_id, sm.month_of_year, smn.name_short, sy.year, km.kpi_value"
          + " from    kpimonthly km"
          + " , kpidefinition kd"
          + " , DimPlant p"
          + " , DimMill m"
          + " , sappifiscalcalendar.dbo.tbl_months sm"
          + " , sappifiscalcalendar.dbo.tbl_month_names smn"
          + " , sappifiscalcalendar.dbo.tbl_years sy"
          + " where    km.KPI_CODE = kd.KpiCode"
          + " and km.flid = p.flid"
          + " and km.mill_code = m.SiteAbbrev"
          + " and m.millkey = p.millkey"
          + " and km.sappimonth_id = sm.month_id"
          + " and sm.month_name_id = smn.month_name_id"
          + " and sm.year_id = sy.year_id"
          + " and sy.year = " + iSappiYear
          + " and km.MILL_CODE  IN ('STA','NGX','TUG')";

        sql += " and kd.kpicode in ('BPOME', 'NSPPM')";
        sql += " order by month_of_year, km.mill_code, km.flid";


        cSQL.OpenDB("ZAMDW");
        dt = cSQL.GetDataTable(sql);
        cSQL.CloseDB();
        ndx = 0;
        dNGX1_NSPPM = 0D;
        dNGX1_KPI = 0D;

        dTUG1_NSPPM = 0D;
        dTUG1_KPI = 0D;

        dSTA1_NSPPM = 0D;
        dSTA1_KPI = 0D;

        while (ndx < dt.Rows.Count)
        {
          sKPICode = dt.Rows[ndx]["KpiCode"].ToString();
          iMonth = cmath.geti(dt.Rows[ndx]["month_of_year"].ToString());
          sSiteCode = dt.Rows[ndx]["mill_code"].ToString();
          FLID = dt.Rows[ndx]["FLID"].ToString();
          iMonthTemp = iMonth;

          while ((ndx < dt.Rows.Count) && (iMonth == iMonthTemp))
          {

            if ((sKPICode == "NSPPM") && (sSiteCode == "STA") && (FLID == "1"))
            {
              dSTA1_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "NSPPM") && (sSiteCode == "TUG") && (FLID == "126"))
            {
              dTUG1_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "NSPPM") && (sSiteCode == "NGX") && (FLID == "5"))
            {
              dNGX1_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }

            if ((sKPICode == "BPOME") && (sSiteCode == "STA") && (FLID == "1"))
            {
              dSTA1_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());       //swop I-2208-6864
            }
            if ((sKPICode == "BPOME") && (sSiteCode == "TUG") && (FLID == "126"))
            {
              dTUG1_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());       //swop I-2208-6864
            }
            if ((sKPICode == "BPOME") && (sSiteCode == "NGX") && (FLID == "5"))
            {
              dNGX1_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }

            ndx++;
            if (ndx < dt.Rows.Count)
            {
              sKPICode = dt.Rows[ndx]["KpiCode"].ToString();
              iMonth = cmath.geti(dt.Rows[ndx]["month_of_year"].ToString());
              sSiteCode = dt.Rows[ndx]["mill_code"].ToString();
              FLID = dt.Rows[ndx]["FLID"].ToString();
            }
          }     //while 2

          //calculate and save values to the array for the month (n)
          t1 = (dNGX1_NSPPM * dNGX1_KPI) + (dSTA1_NSPPM * dSTA1_KPI) + (dTUG1_NSPPM * dTUG1_KPI);

          t2 = (dNGX1_NSPPM + dSTA1_NSPPM + dTUG1_NSPPM);

          t3 = cmath.div(t1, t2);

          arr[zKPI_WABOME, FROM_MONTH + (iMonthTemp - 1)] = Math.Round(t3, ROUND5);

        } // while 1


        SaveDataWASector(zKPI_WABOME, "WABOME", "DIGC");
        cFG.LogIt("DoWeightedAverageRegionalDIGC Finished WABOME");
        //----------------------------------------------------------


        //----------------------------------------------------------
        //WAOME       = actual  OME
        sql = "Select km.mill_code, m.millkey, km.FLID, kd.KpiCode, kd.KpiDesc, kd.Uom"
          + " ,km.sappimonth_id, sm.month_of_year, smn.name_short, sy.year, km.kpi_value"
          + " from    kpimonthly km"
          + " , kpidefinition kd"
          + " , DimPlant p"
          + " , DimMill m"
          + " , sappifiscalcalendar.dbo.tbl_months sm"
          + " , sappifiscalcalendar.dbo.tbl_month_names smn"
          + " , sappifiscalcalendar.dbo.tbl_years sy"
          + " where    km.KPI_CODE = kd.KpiCode"
          + " and km.flid = p.flid"
          + " and km.mill_code = m.SiteAbbrev"
          + " and m.millkey = p.millkey"
          + " and km.sappimonth_id = sm.month_id"
          + " and sm.month_name_id = smn.month_name_id"
          + " and sm.year_id = sy.year_id"
          + " and sy.year = " + iSappiYear
          + " and km.MILL_CODE IN ('STA','NGX','TUG')";

        sql += " and kd.kpicode in ('KPI0056', 'NSPPM')";
        sql += " order by month_of_year, km.mill_code, km.flid";


        cSQL.OpenDB("ZAMDW");
        dt = cSQL.GetDataTable(sql);
        cSQL.CloseDB();
        ndx = 0;

        dNGX1_NSPPM = 0D;
        dNGX1_KPI = 0D;

        dTUG1_NSPPM = 0D;
        dTUG1_KPI = 0D;

        dSTA1_NSPPM = 0D;
        dSTA1_KPI = 0D;

        while (ndx < dt.Rows.Count)
        {
          sKPICode = dt.Rows[ndx]["KpiCode"].ToString();
          iMonth = cmath.geti(dt.Rows[ndx]["month_of_year"].ToString());
          FLID = dt.Rows[ndx]["FLID"].ToString();
          sSiteCode = dt.Rows[ndx]["mill_code"].ToString();
          iMonthTemp = iMonth;

          while ((ndx < dt.Rows.Count) && (iMonth == iMonthTemp))
          {
            if ((sKPICode == "NSPPM") && (sSiteCode == "STA") && (FLID == "1"))
            {
              dSTA1_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "NSPPM") && (sSiteCode == "TUG") && (FLID == "126"))
            {
              dTUG1_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "NSPPM") && (sSiteCode == "NGX") && (FLID == "5"))
            {
              dNGX1_NSPPM = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }

            if ((sKPICode == "KPI0056") && (sSiteCode == "STA") && (FLID == "1"))
            {
              dSTA1_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "KPI0056") && (sSiteCode == "TUG") && (FLID == "126"))
            {
              dTUG1_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }
            if ((sKPICode == "KPI0056") && (sSiteCode == "NGX") && (FLID == "5"))
            {
              dNGX1_KPI = cmath.getd(dt.Rows[ndx]["kpi_value"].ToString());
            }


            ndx++;
            if (ndx < dt.Rows.Count)
            {
              sKPICode = dt.Rows[ndx]["KpiCode"].ToString();
              iMonth = cmath.geti(dt.Rows[ndx]["month_of_year"].ToString());
              sSiteCode = dt.Rows[ndx]["mill_code"].ToString();
              FLID = dt.Rows[ndx]["FLID"].ToString();
            }
          }     //while 2

          //calculate and save values to the array for the month (n)
          t1 = (dNGX1_NSPPM * dNGX1_KPI) + (dSTA1_NSPPM * dSTA1_KPI) + (dTUG1_NSPPM * dTUG1_KPI);
          t2 = (dNGX1_NSPPM + dSTA1_NSPPM + dTUG1_NSPPM);
          t3 = cmath.div(t1, t2);

          arr[zKPI_WAOME, FROM_MONTH + (iMonthTemp - 1)] = Math.Round(t3, ROUND5);


        } // while 1

        SaveDataWASector(zKPI_WAOME, "WAOME", "DIGC");
        cFG.LogIt("DoWeightedAverageRegionalDIGC Finished WAOME");
        //----------------------------------------------------------


        //  //old code use calendar for WA,  see new calculations 2024-03-12 I-2403-6663

        ///calculate the quarter and year to date for this site
        //sSite = "NGX";        //defaults just to get the calendar budgets
        //sAreaID = "25";       //defaults just to get the calendar budgets
        //LoadBudgets();      //load the array with the sappi hours in order to calculate the quarters and year to date
        //SumYTD_QTR(zSappiHours, ROUND1);
        //DoProcessWeightedAveRegional_YTD_Q("DIGC");

        ///NEW calculations  for ytd and quarters 2024-03-12 I-2403-6663
        double ytd = 0D;
        double q1 = 0D;
        double q2 = 0D;
        double q3 = 0D;
        double q4 = 0D;


        for (int monthNum = currMonth; monthNum > 0; monthNum--)
        {

          iSqlMonthID = cal.GetSappiMonthID(pSappiYear, monthNum);  // mssqlserver month number

          cSQL.OpenDB("ZAMDW");

          //WAOEE
          sql = "EXEC WA_QY_DIGC " + pSappiYear + "," + iSqlMonthID + "," + "'KPI0035'" + "," + "'KPI0057'";

          dt = cSQL.GetDataTable(sql);
          foreach (DataRow item in dt.Rows)
          {
            ytd = cmath.getd(item["wa_ytd"].ToString());
            q1 = cmath.getd(item["wa_q1"].ToString());
            q2 = cmath.getd(item["wa_q2"].ToString());
            q3 = cmath.getd(item["wa_q3"].ToString());
            q4 = cmath.getd(item["wa_q4"].ToString());
          }

          SaveWASector_QY("WAOEE", iSqlMonthID, "DIGC", ytd, q1, q2, q3, q4);


          ytd = 0D;
          q1 = 0D;
          q2 = 0D;
          q3 = 0D;
          q4 = 0D;



          //WABOEE
          sql = "EXEC WA_QY_DIGC " + pSappiYear + "," + iSqlMonthID + "," + "'NSPPM'" + "," + "'BOE'";


          dt = cSQL.GetDataTable(sql);
          foreach (DataRow item in dt.Rows)
          {
            ytd = cmath.getd(item["wa_ytd"].ToString());
            q1 = cmath.getd(item["wa_q1"].ToString());
            q2 = cmath.getd(item["wa_q2"].ToString());
            q3 = cmath.getd(item["wa_q3"].ToString());
            q4 = cmath.getd(item["wa_q4"].ToString());
          }

          SaveWASector_QY("WABOEE", iSqlMonthID, "DIGC", ytd, q1, q2, q3, q4);

          ytd = 0D;
          q1 = 0D;
          q2 = 0D;
          q3 = 0D;
          q4 = 0D;



          //WAOME
          sql = "EXEC WA_QY_DIGC " + pSappiYear + "," + iSqlMonthID + "," + "'NSPPM'" + "," + "'KPI0056'";

          dt = cSQL.GetDataTable(sql);
          foreach (DataRow item in dt.Rows)
          {
            ytd = cmath.getd(item["wa_ytd"].ToString());
            q1 = cmath.getd(item["wa_q1"].ToString());
            q2 = cmath.getd(item["wa_q2"].ToString());
            q3 = cmath.getd(item["wa_q3"].ToString());
            q4 = cmath.getd(item["wa_q4"].ToString());
          }

          SaveWASector_QY("WAOME", iSqlMonthID, "DIGC", ytd, q1, q2, q3, q4);

          ytd = 0D;
          q1 = 0D;
          q2 = 0D;
          q3 = 0D;
          q4 = 0D;

          //WABOME
          sql = "EXEC WA_QY_DIGC " + pSappiYear + "," + iSqlMonthID + "," + "'NSPPM'" + "," + "'BPOME'";

          dt = cSQL.GetDataTable(sql);
          foreach (DataRow item in dt.Rows)
          {
            ytd = cmath.getd(item["wa_ytd"].ToString());
            q1 = cmath.getd(item["wa_q1"].ToString());
            q2 = cmath.getd(item["wa_q2"].ToString());
            q3 = cmath.getd(item["wa_q3"].ToString());
            q4 = cmath.getd(item["wa_q4"].ToString());
          }

          SaveWASector_QY("WABOME", iSqlMonthID, "DIGC", ytd, q1, q2, q3, q4);

        }

      }
      catch (Exception er)
      {
        cFG.LogIt("DoWeightedAverageRegionalDIGC procedure");
        cFG.LogIt(er.Message);
      }
      finally
      {
        cSQL.CloseDB();
        cal = null;
      }


    } //


    private void SaveDataWA(int rowNum, string kpiCode, string sector)
    {
      //save data for the Weighted Average Calculations

      int iMonthID = iSqlMonthID;

      int ndx = 1;

      string sql = "";

      cSQL.OpenDB("ZAMDW");

      try
      {

        if (boRunMonth == true)
        {
          for (col = FROM_MONTH; col < MAX_COLS; col++)
          {
            if (ndx <= currMonth)       //save up to current month
            {
              //tmp = arr[rowNum, col].ToString();
              sql = "Update WAMill set KPIValue = " + arr[rowNum, col];
              sql += " , LastUpdated = '" + cmath.getDateTimeNowStr() + "'";
              sql += " where MillCode = '" + sSite + "'";
              sql += " and SappiMonthID  = " + iMonthID;
              sql += " and KPICode = '" + kpiCode + "'";
              sql += " and Sector = '" + sector + "'";
              sql += " IF @@ROWCOUNT=0";
              sql += " Insert into WAMill (millCode, sector, kpiCode, sappimonthId, kpiValue, lastupdated)";
              sql += " VALUES ("
                    + "'" + sSite + "'"
                    + ",'" + sector + "'"
                    + ",'" + kpiCode + "'"
                    + "," + iMonthID
                    + "," + arr[rowNum, col]
                    + ",'" + cmath.getDateTimeNowStr() + "'"
                    + " )";

              cSQL.ExecuteQuery(sql);
            }

            iMonthID++;   //increment the sql server month id 
            ndx++;

          } //for

        }// boRunMonth == true



      }
      catch (Exception er)
      {
        cFG.LogIt("SaveDataWA KPIJob " + er.Message);
        cFG.LogIt(sql);
        throw new Exception("SaveDataWA: " + er.Message);
      }
      finally
      {
        cSQL.CloseDB();
      }

    }//


    private void SaveDataWASector(int rowNum, string kpiCode, string sector)
    {
      //save data for the Weighted Average Calculations per sector

      int iMonthID = iSqlMonthID;

      int ndx = 1;

      string sql = "";

      cSQL.OpenDB("ZAMDW");

      try
      {

        if (boRunMonth == true)
        {
          for (col = FROM_MONTH; col < MAX_COLS; col++)
          {
            if (ndx <= currMonth)       //save up to current month
            {
              //tmp = arr[rowNum, col].ToString();
              sql = "Update WASector set KPIValue = " + arr[rowNum, col];
              sql += " , LastUpdated = '" + cmath.getDateTimeNowStr() + "'";
              sql += " where Sector = '" + sector + "'";
              sql += " and SappiMonthID  = " + iMonthID;
              sql += " and KPICode = '" + kpiCode + "'";
              sql += " IF @@ROWCOUNT=0";
              sql += " Insert into WASector (Sector, kpiCode, sappimonthId, kpiValue, lastupdated)";
              sql += " VALUES ("
                    + "'" + sector + "'"
                    + ",'" + kpiCode + "'"
                    + "," + iMonthID
                    + "," + arr[rowNum, col]
                    + ",'" + cmath.getDateTimeNowStr() + "'"
                    + " )";

              cSQL.ExecuteQuery(sql);
            }

            iMonthID++;   //increment the sql server month id 
            ndx++;

          } //for

        }// boRunMonth == true



      }
      catch (Exception er)
      {
        cFG.LogIt("SaveDataWASector KPIJob " + er.Message);
        cFG.LogIt(sql);
        throw new Exception("SaveDataWASector: " + er.Message);
      }
      finally
      {
        cSQL.CloseDB();
      }

    }//


    private void SaveWA_QY(string pSite, string kpiCode, int sappiMonthID, string sector, double ytd, double q1, double q2, double q3, double q4)
    {

      //save data for the Quarterly and YTD Weighted Average Calculations 
      //1.0.2.21 2024-03-12   I-2403-6663 OME/OEE WA Calculation


      string sql = "";

      //cSQL.OpenDB("ZAMDW");
      //t7 = arr[rowNum, FROM_Q1];
      try
      {
        sql = "Update WAMillQM set LastUpdated = '" + cmath.getDateTimeNowStr() + "'";
        sql += ",  Q1 = " + q1;
        sql += ",  Q2 = " + q2;
        sql += ",  Q3 = " + q3;
        sql += ",  Q4 = " + q4;
        sql += ",  YTD = " + ytd;
        sql += " where MillCode = '" + pSite + "'";
        sql += " and SappiMonthID = " + sappiMonthID;
        sql += " and KPICode = '" + kpiCode + "'";
        sql += " and sector = '" + sector + "'";
        sql += " IF @@ROWCOUNT=0";
        sql += " Insert into WAMillQM (millcode, sector, kpicode, sappimonthId, q1, q2, q3, q4, ytd, lastupdated)";
        sql += " VALUES ("
             + " '" + pSite + "'"
             + ",'" + sector + "'"
             + ",'" + kpiCode + "'"
             + ", " + sappiMonthID
             + ", " + q1
             + ", " + q2
             + ", " + q3
             + ", " + q4
             + ", " + ytd
             + ",'" + cmath.getDateTimeNowStr() + "'"
             + " )";

        cSQL.ExecuteQuery(sql);

      }
      catch (Exception er)
      {
        cFG.LogIt("SaveWAQM KPIJob " + er.Message);
        throw new Exception("SaveWAQM: " + er.Message);
      }
      finally
      {
        //cSQL.CloseDB();
      }

    }//


    private void SaveWASector_QY(string kpiCode, int sappiMonthID, string sector, double ytd, double q1, double q2, double q3, double q4)
    {
      //save data for the Quarterly Weighted Average Calculations  per sector
      //1.0.2.21 2024-03-12   I-2403-6663 OME/OEE WA Calculation
      string sql = "";



      try
      {
        sql = "Update WASectorQM set LastUpdated = '" + cmath.getDateTimeNowStr() + "'";
        sql += ",  Q1 = " + q1;
        sql += ",  Q2 = " + q2;
        sql += ",  Q3 = " + q3;
        sql += ",  Q4 = " + q4;
        sql += ",  YTD = " + ytd;
        sql += " where Sector = '" + sector + "'";
        sql += " and SappiMonthID = " + sappiMonthID;
        sql += " and KPICode = '" + kpiCode + "'";
        sql += " IF @@ROWCOUNT=0";
        sql += " Insert into WASectorQM (sector, kpicode, sappimonthId, q1, q2, q3, q4, ytd, lastupdated)";
        sql += " VALUES ("
             + " '" + sector + "'"
             + ",'" + kpiCode + "'"
             + ", " + sappiMonthID
             + ", " + q1
             + ", " + q2
             + ", " + q3
             + ", " + q4
             + ", " + ytd
             + ",'" + cmath.getDateTimeNowStr() + "'"
             + " )";

        cSQL.ExecuteQuery(sql);

      }
      catch (Exception er)
      {
        cFG.LogIt("SaveWASectorQM KPIJob " + er.Message);
        throw new Exception("SaveWASectorQM: " + er.Message);
      }
      finally
      {
      }


    }//

    #endregion  




  }///
}///







