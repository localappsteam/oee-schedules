﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using System.Windows.Forms;

//update the Fact Tables in ZAMDW for the Global report (Walter)

namespace Schedules
{
  
   class cGlobalReport
   {

    string sReportDate = "";
    string sMonthStartDate = "";
    string sMonthEndDate = "";
    string sWeekStartDate = "";
    string sWeekEndDate = "";

    int iSappiDayID = 0;
    int iSappiMonthID = 0;
    int iSappiWeekID = 0;

    int iSappiYear = 0;
    int iMonthOFYear = 0;

    double numDays = 0;

    cCal cal = new cCal();

   
    const byte NGX_PM1 = 1;
    const byte NGX_PM2 = 3;
    const byte NGX_UT3 = 4;
    const byte NGX_FL2 = 5;
    const byte NGX_UT2 = 11;      //no data in mem

    const byte SAI_C3 = 12;
    const byte SAI_C4 = 13;
    const byte SAI_C5 = 14;
    const byte SAI_DIG = 16;

    const byte STA_TM = 6;
    const byte STA_PM = 7;
    const byte STA_BH = 8;

    const byte TUG_PM = 9;
    const byte TUG_NSSC = 10;

    double dAvailTime = 0;
    double dProdTime = 0;
    double dBrokeTons = 0;
    double dNetTons = 0;
    double dBGradeTons = 0;
   

    double dBPAvailTime = 0;
    double dBPProdTime = 0;
    double dBPNetTons = 0;
    double dBPBrokeTons = 0;
    double dBPSpeed = 0;
 


    public void RunJob(string runDate)
    {

      sReportDate = cmath.getDateStr(runDate);

      cal = cal ?? new cCal();


      try
      {

        cal.GetSappiMonthDates(sReportDate, ref sMonthStartDate, ref sMonthEndDate);    //month

        iSappiDayID = cal.GetSappiDayID(sReportDate);

        cal.GetCal(sReportDate);
        iSappiYear = cal.iSappiYear;
        iMonthOFYear = cal.iCurrMonthOfYear;
        iSappiMonthID = cal.iCurrMonthID;
        sWeekStartDate = cmath.getDateStr(cal.sWeekStartDate);
        sWeekEndDate = cmath.getDateStr(cal.sWeekEndDate);
        iSappiWeekID = cal.iCurrWeekID;

        DateTime dtFrom = cmath.getDateTime(sMonthStartDate);
        DateTime dtTo = cmath.getDateTime(sReportDate);      //to report date
        TimeSpan ts = dtTo.Subtract(dtFrom);

        numDays = ts.Days + 1;

        cFG.LogIt("Gobal Report: " + sReportDate);

        System.Windows.Forms.Application.DoEvents();

        //DAILY production--------------------------------------------

        ///NGX 139 PlantKey = 1 --------------------------------------
        GetMEMProdDaily("NGX", 139, sReportDate);
        UpdFactProdDaily(NGX_PM1);


        GetMEMProdDaily("NGX", 25, sReportDate);
        UpdFactProdDaily(NGX_PM2);


        GetMEMProdDaily("NGX", 13, sReportDate);
        UpdFactProdDaily(NGX_UT3);

        GetMEMProdDaily("NGX", 5, sReportDate);
        UpdFactProdDaily(NGX_FL2);

        GetMEMProdDaily("NGX", 15, sReportDate);
        UpdFactProdDaily(NGX_UT2);


        GetMEMProdDaily("SAI", 4, sReportDate);
        UpdFactProdDaily(SAI_C3);


        GetMEMProdDaily("SAI", 5, sReportDate);
        UpdFactProdDaily(SAI_C4);

        GetMEMProdDaily("SAI", 6, sReportDate);
        UpdFactProdDaily(SAI_C5);

        GetMEMProdDaily("STA", 6, sReportDate);
        UpdFactProdDaily(STA_PM);

        GetMEMProdDaily("STA", 12, sReportDate);
        UpdFactProdDaily(STA_TM);

        GetMEMProdDaily("STA", 1, sReportDate);
        UpdFactProdDaily(STA_BH);

        GetMEMProdDaily("TUG", 118, sReportDate);
        UpdFactProdDaily(TUG_PM);

        GetMEMProdDaily("TUG", 126, sReportDate);
        UpdFactProdDaily(TUG_NSSC);

        DoPMLengthNGX();

        DoPMLengthSTA();

        DoPMLengthTUG();

        GetZADaily("4", "DIG");
        UpdFactProdDaily(SAI_DIG);


        //MONTHLY budget-----------------------------------------

        GetMEMBgtMonth("NGX", 139, sReportDate);
        UpdFactBudgetMonth(NGX_PM1);

        GetMEMBgtMonth("NGX", 25, sReportDate);
        UpdFactBudgetMonth(NGX_PM2);

        GetMEMBgtMonth("NGX", 13, sReportDate);
        UpdFactBudgetMonth(NGX_UT3);

        GetMEMBgtMonth("NGX", 5, sReportDate);
        UpdFactBudgetMonth(NGX_FL2);

        GetMEMBgtMonth("NGX", 15, sReportDate);       //no data in mem
        UpdFactBudgetMonth(NGX_UT2);

        GetMEMBgtMonth("SAI", 4, sReportDate);
        UpdFactBudgetMonth(SAI_C3);

        GetMEMBgtMonth("SAI", 5, sReportDate);
        UpdFactBudgetMonth(SAI_C4);

        GetMEMBgtMonth("SAI", 6, sReportDate);
        UpdFactBudgetMonth(SAI_C5);

        GetMEMBgtMonth("STA", 6, sReportDate);
        UpdFactBudgetMonth(STA_PM);

        GetMEMBgtMonth("STA", 12, sReportDate);
        UpdFactBudgetMonth(STA_TM);

        GetMEMBgtMonth("STA", 1, sReportDate);
        UpdFactBudgetMonth(STA_BH);


        GetMEMBgtMonth("TUG", 118, sReportDate);
        UpdFactBudgetMonth(TUG_PM);

        GetMEMBgtMonth("TUG", 126, sReportDate);
        UpdFactBudgetMonth(TUG_NSSC);


        //-----daily budget------
        GetMEMBgtDaily("SAI",4,sReportDate);
        UpdFactBudgetDaily(SAI_C3);

        GetMEMBgtDaily("SAI", 5, sReportDate);
        UpdFactBudgetDaily(SAI_C4);

        GetMEMBgtDaily("SAI", 6, sReportDate);
        UpdFactBudgetDaily(SAI_C5);


        GetMEMBgtDaily("NGX", 139, sReportDate);
        UpdFactBudgetDaily(NGX_PM1);

        GetMEMBgtDaily("NGX", 25, sReportDate);
        UpdFactBudgetDaily(NGX_PM2);

        GetMEMBgtDaily("NGX", 13, sReportDate);
        UpdFactBudgetDaily(NGX_UT3);

        GetMEMBgtDaily("STA", 6, sReportDate);
        UpdFactBudgetDaily( STA_PM);

        GetMEMBgtDaily("STA", 12, sReportDate);
        UpdFactBudgetDaily(STA_TM);

        GetMEMBgtDaily("TUG", 118, sReportDate);
        UpdFactBudgetDaily( TUG_PM);

      }
      catch (Exception er)
      {
        cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
      }
      finally
      {
        cSQL.CloseDB();
        cal = null;
      }

    } //






    private void GetMEMProdDaily(string pMillCode, int pFLID, string pRunDate)
    {

 
      double dPPAG = 0;

      try
      {
        string sql = "select k.id, k.sappidayid, sd.day_start, sm.month_of_year, k.millcode, k.flid"
          + " ,p.plantname, p.plantcode, p.altplantcode, k.kpicode, k.kpivalue, kd.kpidesc, sd.day_of_month"
          + " from kpidaily k"
          + " , kpidefinition kd"
          + " , dimplant p"
          + " , dimmill m"
          + " , sappifiscalcalendar.dbo.tbl_days sd"
          + " , sappifiscalcalendar.dbo.tbl_months sm"
          + " , sappifiscalcalendar.dbo.tbl_month_names smn"
          + " , sappifiscalcalendar.dbo.tbl_years sy"
          + " where   k.kpicode = kd.kpicode"
          + " and k.flid = p.flid"
          + " and k.millcode = m.siteabbrev"
          + " and m.millkey = p.millkey"
          + " and k.sappidayid = sd.day_id"
          + " and sd.month_id = sm.month_id"
          + " and sm.month_name_id = smn.month_name_id"
          + " and sm.year_id = sy.year_id"
          + " and k.millcode = '" + pMillCode + "'"
          + " and k.flid = " + pFLID
         + " and k.kpicode in('KPI0026', 'KPI0028', 'TBS', 'KPI0035', 'DKL', 'PPAG')"
          + " and cast(sd.day_start as date) = '" + pRunDate + "'"
          + " ";

        cSQL.OpenDB("ZAMDW");

        DataTable dt = cSQL.GetDataTable(sql);

        dAvailTime = 0;
        dProdTime = 0;
        dBrokeTons = 0;
        dNetTons = 0;
        dBGradeTons = 0;
      

        foreach (DataRow item in dt.Rows)
        {
          if (item["KPICode"].ToString() == "KPI0026")
            dAvailTime = cmath.getd(item["KPIValue"].ToString());

          if (item["KPICode"].ToString() == "KPI0028")
            dProdTime = cmath.getd(item["KPIValue"].ToString());

          if (item["KPICode"].ToString() == "KPI0035")
            dNetTons = cmath.getd(item["KPIValue"].ToString());

          //broke tones in global is combined TBS and DKL from MEM
          if (item["KPICode"].ToString() == "TBS")
            dBrokeTons += cmath.getd(item["KPIValue"].ToString());

          if (item["KPICode"].ToString() == "DKL")
            dBrokeTons += cmath.getd(item["KPIValue"].ToString());

          if (pMillCode == "SAI")
          {
            if (item["KPICode"].ToString() == "PPAG")
              dPPAG = cmath.getd(item["KPIValue"].ToString());
          }
          
        
        }


        if (pMillCode == "SAI")
        {
          dBGradeTons = dNetTons - dPPAG;
        }


      }
      catch (Exception er)
      {
        //throw er;
        cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
        cFG.LogIt(pMillCode + " " + pFLID);
      }
      finally
      {
        cSQL.CloseDB();
      }


    }//



    private void GetMEMBgtDaily(string pMillCode, int pFLID, string pRunDate)
    {
      
      double dBNPP = 0;

      try
      {
        string sql = "select k.id, k.sappidayid, sd.day_start, sm.month_of_year, k.millcode, k.flid"
          + " ,p.plantname, p.plantcode, p.altplantcode, k.kpicode, k.kpivalue, kd.kpidesc, sd.day_of_month"
          + " from kpidaily k"
          + " , kpidefinition kd"
          + " , dimplant p"
          + " , dimmill m"
          + " , sappifiscalcalendar.dbo.tbl_days sd"
          + " , sappifiscalcalendar.dbo.tbl_months sm"
          + " , sappifiscalcalendar.dbo.tbl_month_names smn"
          + " , sappifiscalcalendar.dbo.tbl_years sy"
          + " where   k.kpicode = kd.kpicode"
          + " and k.flid = p.flid"
          + " and k.millcode = m.siteabbrev"
          + " and m.millkey = p.millkey"
          + " and k.sappidayid = sd.day_id"
          + " and sd.month_id = sm.month_id"
          + " and sm.month_name_id = smn.month_name_id"
          + " and sm.year_id = sy.year_id"
          + " and k.millcode = '" + pMillCode + "'"
          + " and k.flid = " + pFLID
          + " and k.kpicode in('KPI0163', 'BNPP','NSPPM')"
          + " and cast(sd.day_start as date) = '" + pRunDate + "'"
          + " ";

        cSQL.OpenDB("ZAMDW");

        DataTable dt = cSQL.GetDataTable(sql);

        dBPAvailTime = 0;
        dBPProdTime = 0;
        dBPNetTons = 0;
        dBPBrokeTons = 0;
        dBPSpeed = 0;


        foreach (DataRow item in dt.Rows)
        {

               //if (item["KPICode"].ToString() == "BPOH")
               //  dBPAvailTime = cmath.getd(item["KPIValue"].ToString());

               //if (item["KPICode"].ToString() == "BPNPT")
               //  dBPProdTime = cmath.getd(item["KPIValue"].ToString());



               //if (pMillCode == "STA" || pMillCode == "TUG")
               //{
               //   if (pFLID == 6 || pFLID == 12 || pFLID == 118)
               //   {
               //      if (item["KPICode"].ToString() == "NSPPM")
               //         dBPNetTons = cmath.getd(item["KPIValue"].ToString());
               //   }
               //}
               //else
               //{
               //   if (item["KPICode"].ToString() == "KPI0163")
               //      dBPNetTons = cmath.getd(item["KPIValue"].ToString());
               //}


               //2025-02-20
               if (item["KPICode"].ToString() == "NSPPM")
                        dBPNetTons = cmath.getd(item["KPIValue"].ToString());

               if (item["KPICode"].ToString() == "BNPP")
                  dBNPP = cmath.getd(item["KPIValue"].ToString());

        }

       

        ///2023-11-14  this overides the POR calculation, we cant have a budget figure dependent on actual
        if (dBPNetTons == 0)
        {
          dBPAvailTime = 0;
          dBPProdTime = 0;
        }
        else{
          dBPAvailTime = 24;
          dBPProdTime = (24 * dBNPP) / 100;
        }


      }
      catch (Exception er)
      {
        cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
        cFG.LogIt(pMillCode + " " + pFLID);
      }
      finally
      {
        cSQL.CloseDB();
      }


    }//



    private void GetMEMBgtMonth(string pMillCode, int pFLID, string pRunDate)
    {
      double dBGP = 0;
      double dBQR = 0;

      try
      {
        string sql = "select k.kpimonth_id, k.sappimonth_id, sm.month_of_year, sy.year, k.mill_code, k.flid, p.plantname"
	        + " ,p.plantcode, p.altplantcode, k.kpi_code, k.kpi_value, kd.kpidesc"
          + " from  KPIMonthly k "
          + " , kpidefinition kd"
          + " , dimplant p"
          + " , dimmill m"
          + " , sappifiscalcalendar.dbo.tbl_months sm"
          + " , sappifiscalcalendar.dbo.tbl_month_names smn"
          + " , sappifiscalcalendar.dbo.tbl_years sy"
          + " where k.kpi_code = kd.kpicode"
          + " and k.flid = p.flid"
          + " and k.mill_code = m.siteabbrev"
          + " and m.millkey = p.millkey"
          + " and k.sappimonth_id = sm.month_id"
          + " and sm.month_name_id = smn.month_name_id"
          + " and sm.year_id = sy.year_id"
          + " and sy.year = " + iSappiYear
          + " and k.mill_code = '" + pMillCode + "'"
          + " and k.flid = " + pFLID
          + " and k.kpi_code in('BPOH', 'BPNPT', 'NSPPM', 'BGP', 'BQR', 'WAGRR')"
          + " and sm.month_of_year = " + iMonthOFYear
          + " ";

        cSQL.OpenDB("ZAMDW");

        DataTable dt = cSQL.GetDataTable(sql);

        dBPAvailTime = 0;
        dBPProdTime = 0;
        dBPNetTons = 0;
        dBPBrokeTons = 0;
        dBPSpeed = 0;

        foreach (DataRow item in dt.Rows)
        {
          if (item["KPI_Code"].ToString() == "BPOH")
            dBPAvailTime = cmath.getd(item["KPI_Value"].ToString());

          if (item["KPI_Code"].ToString() == "BPNPT")
            dBPProdTime = cmath.getd(item["KPI_Value"].ToString());

          if (item["KPI_Code"].ToString() == "NSPPM")
            dBPNetTons = cmath.getd(item["KPI_Value"].ToString());

          if (item["KPI_Code"].ToString() == "BGP")
            dBGP = cmath.getd(item["KPI_Value"].ToString());

          if (item["KPI_Code"].ToString() == "BQR")
            dBQR = cmath.getd(item["KPI_Value"].ToString());
          
          if (item["KPI_Code"].ToString() == "WAGRR")
              dBPSpeed = cmath.getd(item["KPI_Value"].ToString());

        }

        if (pMillCode == "SAI")
        {
          dBPBrokeTons =  cmath.div(dBPNetTons , (dBQR / 100)) - dBPNetTons;
        }
        else{
          dBPBrokeTons = dBGP * ((100 - dBQR) / 100);
        }

      }
      catch (Exception er)
      {
        // throw er;
        cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
        cFG.LogIt(pMillCode + " " + pFLID);
      }
      finally
      {
        cSQL.CloseDB();
      }


    }//




    private void UpdFactProdDaily(byte plantKey)
    {
      try
      {
        string sql = "UPDATE FactProdDaily set AvailTime = '" + dAvailTime + "'"
          + " , ProdTime = " + dProdTime
          + " , BrokeTons = " + dBrokeTons
          + " , NetTons = " + dNetTons
          + " , BGradeTons = " + dBGradeTons
          + " Where SappiDayKey = " + iSappiDayID
          + "  and PlantKey  = " + plantKey
          + " IF @@ROWCOUNT=0"
          + " INSERT INTO FactProdDaily  ([PlantKey], [SappiDayKey], [AvailTime], [ProdTime],[BrokeTons],[NetTons],[BGradeTons] )"
          + " VALUES ("
          + "" + plantKey
          + "," + iSappiDayID
          + "," + dAvailTime
          + "," + dProdTime
          + "," + dBrokeTons
          + "," + dNetTons
          + "," + dBGradeTons
          + " )";


        cSQL.OpenDB("ZAMDW");

        cSQL.ExecuteQuery(sql);


      }
      catch (Exception er)
      {
       
        cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
        cFG.LogIt(plantKey.ToString());
      }
      finally
      {
        cSQL.CloseDB();
      }


    }///



    private void UpdFactBudgetMonth(byte plantKey)
    {
      try
      {
        string sql = "UPDATE FactProdBudgetMonth set BpAvailTime = '" + dBPAvailTime + "'"
          + " , BpProdTime = " + dBPProdTime
          + " , BpNetTons = " + dBPNetTons
          + " , BpBrokeTons = " + dBPBrokeTons
          + " , BpSpeed = " + dBPSpeed
          + " Where SappiMonthKey = " + iSappiMonthID
          + "  and PlantKey  = " + plantKey
          + " IF @@ROWCOUNT=0"
          + " INSERT INTO FactProdBudgetMonth  ([PlantKey], [SappiMonthKey], [BpAvailTime], [BpProdTime], [BpBrokeTons], [BpNetTons], [BpSpeed])"
          + " VALUES ("
          + "" + plantKey
          + "," + iSappiMonthID
          + "," + dBPAvailTime
          + "," + dBPProdTime
          + "," + dBPBrokeTons
          + "," + dBPNetTons
          + "," + dBPSpeed
          + " )";


        cSQL.OpenDB("ZAMDW");

        cSQL.ExecuteQuery(sql);


      }
      catch (Exception er)
      {
        //throw er;
        cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
        cFG.LogIt(plantKey.ToString());
      }
      finally
      {
        cSQL.CloseDB();
      }


    }///


    private void UpdFactBudgetDaily(byte plantKey)
    {
      try
      {
        string sql = "UPDATE FactProdBudgetDaily set BpAvailTime = '" + dBPAvailTime + "'"
          + " , BpProdTime = " + dBPProdTime
          + " , BpNetTons = " + dBPNetTons
          + " , BpBrokeTons = " + dBPBrokeTons
          + " , BpSpeed = " + dBPSpeed
          + " Where SappiDayKey = " + iSappiDayID
          + "  and PlantKey  = " + plantKey
          + " IF @@ROWCOUNT=0"
          + " INSERT INTO FactProdBudgetDaily  ([PlantKey], [SappiDayKey], [BpAvailTime], [BpProdTime], [BpBrokeTons], [BpNetTons], [BpSpeed])"
          + " VALUES ("
          + "" + plantKey
          + "," + iSappiDayID
          + "," + dBPAvailTime
          + "," + dBPProdTime
          + "," + dBPBrokeTons
          + "," + dBPNetTons
          + "," + dBPSpeed
          + " )";


        cSQL.OpenDB("ZAMDW");

        cSQL.ExecuteQuery(sql);


      }
      catch (Exception er)
      {
        //throw er;
        cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
        cFG.LogIt(plantKey.ToString());
      }
      finally
      {
        cSQL.CloseDB();
      }


    }///



    private void DoPMLengthNGX()
    {
      double pmLength = 0;
      string sdate = cmath.OraDateSlash(sReportDate);       //MM/DD/YYYY

      try
      {
        //------------------PM1  NGX ------------------------------------------------------------------------

        cSQL.OpenDB("ZAMDW");

        cORA.OpenDB("PMPS");
        
        cORA.cmd = cORA.con.CreateCommand();
        cORA.cmd.CommandText = "pmps.pck_oee.getPmLength";
        cORA.cmd.CommandType = CommandType.StoredProcedure;

        OracleCommandBuilder.DeriveParameters(cORA.cmd);
        cORA.cmd.BindByName = true;
        cORA.cmd.Parameters["p_StartDate"].Value = sdate;                    // "05/02/2023";          //MM/DD/YYYY
        cORA.cmd.Parameters["p_Pm"].Value = 1;

        DataTable dt = cORA.ExecuteQuerySp(cORA.cmd);
        foreach (DataRow item in dt.Rows)
        {
          pmLength = cmath.getd(item["pm_length"].ToString());
        }



        string sql = "UPDATE FactProdDaily set LengthInPM = '" + pmLength + "'"
            + " Where SappiDayKey = " + iSappiDayID
            + "  and PlantKey  = " + NGX_PM1
            + " IF @@ROWCOUNT=0"
            + " INSERT INTO FactProdDaily  ([PlantKey], [SappiDayKey], [LengthInPM])"
            + " VALUES ("
            + "" + NGX_PM1
            + "," + iSappiDayID
            + "," + pmLength
            + " )";

        cSQL.ExecuteQuery(sql);



        //------------------PM2  NGX ------------------------------------------------------------------------
        pmLength = 0;

        cORA.cmd = cORA.con.CreateCommand();
        cORA.cmd.CommandText = "pmps.pck_oee.getPmLength";
        cORA.cmd.CommandType = CommandType.StoredProcedure;

        OracleCommandBuilder.DeriveParameters(cORA.cmd);
        cORA.cmd.BindByName = true;
        cORA.cmd.Parameters["p_StartDate"].Value = sdate;                    //MM/DD/YYYY
        cORA.cmd.Parameters["p_Pm"].Value = 2;

        dt = cORA.ExecuteQuerySp(cORA.cmd);
        foreach (DataRow item in dt.Rows)
        {
          pmLength = cmath.getd(item["pm_length"].ToString());
        }

        sql = "UPDATE FactProdDaily set LengthInPM = '" + pmLength + "'"
            + " Where SappiDayKey = " + iSappiDayID
            + "  and PlantKey  = " + NGX_PM2
            + " IF @@ROWCOUNT=0"
            + " INSERT INTO FactProdDaily  ([PlantKey], [SappiDayKey], [LengthInPM])"
            + " VALUES ("
            + "" + NGX_PM2
            + "," + iSappiDayID
            + "," + pmLength
            + " )";

        cSQL.ExecuteQuery(sql);

      }
      catch (Exception er)
      {
        //throw er;
        cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
      }
      finally
      {
        cORA.CloseDB();
        cSQL.CloseDB();
      }


    }///



    private void DoPMLengthSTA()
    {
      double pmLength = 0;
      string sdate = cmath.OraDate(sReportDate);
      try
      {
        //------------------PM STA------------------------------------------------------------------------

        cSQL.OpenDB("ZAMDW");

        cORA.OpenDB("SPIS");

        string sql = "select nvl(sum(paper_length),0) paper_length from JUMBOS t"
          + " where paper_type = 'P'"
          + " and trunc(creation_date) = " + sdate
          + "";

        DataTable dt = cORA.GetDataTable(sql);
        foreach (DataRow item in dt.Rows)
        {
          pmLength = cmath.getd(item["paper_length"].ToString());
        }



        sql = "UPDATE FactProdDaily set LengthInPM = '" + pmLength + "'"
            + " Where SappiDayKey = " + iSappiDayID
            + "  and PlantKey  = " + STA_PM
            + " IF @@ROWCOUNT=0"
            + " INSERT INTO FactProdDaily  ([PlantKey], [SappiDayKey], [LengthInPM])"
            + " VALUES ("
            + "" + STA_PM
            + "," + iSappiDayID
            + "," + pmLength
            + " )";

        cSQL.ExecuteQuery(sql);



        //------------------TM STA------------------------------------------------------------------------
        pmLength = 0;

        sql = "select nvl(sum(paper_length),0) paper_length from JUMBOS t"
          + " where paper_type = 'T'"
          + " and trunc(creation_date) = " + sdate
          + "";


        dt = cORA.GetDataTable(sql);
        foreach (DataRow item in dt.Rows)
        {
          pmLength = cmath.getd(item["paper_length"].ToString());
        }

        sql = "UPDATE FactProdDaily set LengthInPM = '" + pmLength + "'"
            + " Where SappiDayKey = " + iSappiDayID
            + "  and PlantKey  = " + STA_TM
            + " IF @@ROWCOUNT=0"
            + " INSERT INTO FactProdDaily  ([PlantKey], [SappiDayKey], [LengthInPM])"
            + " VALUES ("
            + "" + STA_TM
            + "," + iSappiDayID
            + "," + pmLength
            + " )";

        cSQL.ExecuteQuery(sql);

      }
      catch (Exception er)
      {
        //throw er;
        cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
      }
      finally
      {
        cORA.CloseDB();
        cSQL.CloseDB();
      }


    }///



    private void DoPMLengthTUG()
    {
      double pmLength = 0;
      string sdate = cmath.OraDate(sReportDate);
      try
      {
        //------------------PM TUG------------------------------------------------------------------------

        cSQL.OpenDB("ZAMDW");

        cORA.OpenDB("PROG_REP");

        string sql = "select  pm_no , trunc(date_wrapped) production_date , sum(length_m) total_length from h_roll"
          + " where trunc(date_wrapped) = " + sdate
          + "       and pm_no = 2"
          + " group by pm_no , trunc(date_wrapped)";

        DataTable dt = cORA.GetDataTable(sql);
        foreach (DataRow item in dt.Rows)
        {
          pmLength = cmath.getd(item["total_length"].ToString());
        }



        sql = "UPDATE FactProdDaily set LengthInPM = '" + pmLength + "'"
            + " Where SappiDayKey = " + iSappiDayID
            + "  and PlantKey  = " + TUG_PM
            + " IF @@ROWCOUNT=0"
            + " INSERT INTO FactProdDaily  ([PlantKey], [SappiDayKey], [LengthInPM])"
            + " VALUES ("
            + "" + TUG_PM
            + "," + iSappiDayID
            + "," + pmLength
            + " )";

        cSQL.ExecuteQuery(sql);


      }
      catch (Exception er)
      {
        //throw er;
        cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
      }
      finally
      {
        cORA.CloseDB();
        cSQL.CloseDB();
      }


    }///


    private void GetZADaily(string pMillKey, string plantCode)
    {
      try
      {
        string sql = "select format(ReportDate,'yyyy-MM-dd') reportDate, [MillKey], [PlantCode], [PulpNetProdBgtDaily], [PulpNetProdActDaily]"
          + " from [ZADailyMTDReport]"
          + " where ReportDate = '" + sReportDate + "'"
          + " and MillKey = " + pMillKey
          + " and plantCode = '" + plantCode + "'"
          + "";

        cSQL.OpenDB("ZAMDW");

        DataTable dt = cSQL.GetDataTable(sql);

        dBPAvailTime = 0;
        dBPProdTime = 0;
        dBPNetTons = 0;
        dBPBrokeTons = 0;
        dBPSpeed = 0;

        dAvailTime = 0;
        dProdTime = 0;
        dBrokeTons = 0;
        dNetTons = 0;
        dBGradeTons = 0;
  

        foreach (DataRow item in dt.Rows)
        {
          dNetTons = cmath.getd(item["PulpNetProdActDaily"].ToString());

        }


      }
      catch (Exception er)
      {
        // throw er;
        cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
        cFG.LogIt(pMillKey + " " + plantCode);
      }
      finally
      {
        cSQL.CloseDB();
      }



    }



  }///
}///










