﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;


namespace Schedules
{
  static class cSQL
  {


    private static SqlConnection con = null;


    public static string sErr = "";

    public static void CloseDB()
    {
      if (con == null)
        return;
      try
      {
        if (con.State != ConnectionState.Closed)
          con.Close();

      }
      catch (Exception)
      { }
      con = null;
      return;
    }




    public static void OpenDB(string site = "")
    {

      string connStr = "";

      if (con != null)
        if (con.State == ConnectionState.Open)
        {
          return;
        }

      switch (site)
      {
        case "ZAMDW":
          //DEV DEV DEV DEV DEV DEV DEV DEV
          connStr = "Data Source=TERWSSQLD1;Initial Catalog=zaMDW;Integrated Security=False;User id=ZaMDWuser;Password=ZaMDWPassword";

         //SANDBOX
         //connStr = "Data Source=TERWSSQLD1;Initial Catalog=zaMDWSandbox;Integrated Security=False;User id=ZaMDWuser;Password=ZaMDWPassword";


         //LIVE LIVE LIVE LIVE LIVE LIVE
         //connStr = "Data Source=TERWSSQL02;Initial Catalog=zaMDW;Integrated Security=False;User id=ZaMDWuser;Password=ZaMDWPassword";
         break;
 

        case "ZAMDWDEV":
          connStr = "Data Source=TERWSSQLD1;Initial Catalog=zaMDW;Integrated Security=False;User id=ZaMDWuser;Password=ZaMDWPassword";
          break;

        case "SAIMES":
          //connStr = "Data Source=Saivss01\\PhsSai;Initial Catalog=MESQualitySystem;Integrated Security=False;Connection Timeout=90;User id=MesReader;Password=Sappi12";
          //2022-11-25
          connStr = "Data Source=SAI-PHS-SQL\\PHSSAI;Initial Catalog=MESQualitySystem;Integrated Security=False;Connection Timeout=180;User id=MesReader;Password=Sappi12";
          break;

        case "NGXMES":
          //connStr = "Data Source=NGXVSS04\\PhsNgx;Initial Catalog=MESQualitySystem;Integrated Security=False;User id=MesReader;Password=Sappi12";
          connStr = "Data Source=NGXWSSQL01\\PhsNgx;Initial Catalog=MESQualitySystem;Integrated Security=False;User id=MesReader;Password=Sappi12";
          break;

        case "NGXVSS04":
          //2022-06-24
          connStr = "Data Source=NGXWSSQL01;Initial Catalog=Sappi;Integrated Security=False;User id=NGX.MII.User;Password=ngx-mii-password";
          //connStr = "Data Source=NGXVSS04;Initial Catalog=Sappi;Integrated Security=False;User id=NGX.MII.User;Password=ngx-mii-password";
          break;

        case "MDTDEV":
          connStr = "Data Source=BRAVSSD01;Initial Catalog=MDT;Integrated Security=False;User id=MDTuser;Password=MDTpassword";
          break;

        case "MDT":
          connStr = "Data Source=TERWSSQL02;Initial Catalog=MDT;Integrated Security=False;User id=MDTuser;Password=MDTpassword";
          break;

        case "OEELOMATI":
          connStr = "Data Source=BARVSS02\\EVOLUTION;Initial Catalog=OEE;Integrated Security=False;User id=OEE;Password=OEE;Connect Timeout=45";
          break;

        case "TUGVSS04":        // TUGVSS04 - LIS
            connStr = "Data Source=TUGVSS04;Initial Catalog=LIS;Integrated Security=False;Connection Timeout=90;User id=LIS;Password=lis";
          break;

        case "HRFeed":
          connStr = "Data Source=TERWSSQL02;Initial Catalog=HRFeed;Integrated Security=False;User id=HRFeedOEE;Password=oee";
          break;

        //SCANNERS_HISTORY FOR LOMATI
        case "SCANNERS":
          connStr = "Data Source=BARVSS02\\EVOLUTION;Initial Catalog=Scanners_History;Integrated Security=False;User id=ScannersUser;Password=ScannersUser";
          break;


        //LOMATI EVOLUTION
        case "EVOLUTION":
          connStr = "Data Source=BARVSS02\\EVOLUTION;Initial Catalog=LomatiEvolution;Integrated Security=False;User id=ScannersUser;Password=ScannersUser";
          break;

      }


      con = new SqlConnection(connStr);

      try
      {
       
        con.Open();
        return;
      }
      catch (SqlException ee)
      {
        cFG.LogIt(connStr);
        throw ee;
      }
    }


    
    public static int ExecuteQuery(string sSql)
    {
      try
      {
        OpenDB();
        SqlCommand cmd = new SqlCommand(sSql, con);
        cmd.CommandTimeout = 120;
        return cmd.ExecuteNonQuery();
      }
      catch (Exception e)
      {
        throw e;
      }
    }




    public static int ExecScalar(string sql)
    {
      int rv = 0;
      try
      {
        OpenDB();
        SqlCommand cmd = new SqlCommand(sql, con);
        object result = cmd.ExecuteScalar();
        if (result != null)
          int.TryParse(result.ToString(), out rv);
        return rv;
      }
      catch (Exception e)
      {
        throw e;
      }
    }



    public static DataSet GetDataSet(string sql)
    {
      try
      {
        OpenDB();
        SqlDataAdapter da = new SqlDataAdapter(sql, con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
      }
      catch (Exception e)
      {
        throw e;
      }

    }



    public static DataTable GetDataTable(string sql, bool useTimeOut = false)
    {
      try
      {
        OpenDB();
        SqlDataAdapter da = new SqlDataAdapter(sql, con);
        DataTable dt = new DataTable();

        if (useTimeOut == false)
          da.SelectCommand.CommandTimeout = 180;      //original
        else
          da.SelectCommand.CommandTimeout = 0;        //specially for Saiccor

        da.Fill(dt);
        return dt;
      }
      catch (Exception e)
      {
        throw e;
      }

    }


    public static SqlDataReader GetDataReader(string sql)
    {
      try
      {
        OpenDB();
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandText = sql;
        SqlDataReader rd = cmd.ExecuteReader();
        return rd;
      }
      catch (Exception ee)
      {
        throw ee;
      }
    }


    /// <summary>
    /// Return a DataSet from a Stored Procedure
    /// </summary>
    /// <param name="procName">The Stored Procedure Name</param>
    /// <param name="catalog">the Database</param>
    /// <param name="procParams">Parameters to the Procedure</param>
    /// <returns></returns>
    public static DataSet GetDataSetSP(string procName
                                , params IDataParameter[] procParams)
    {

      DataSet ds = new DataSet();
      SqlDataAdapter da = new SqlDataAdapter();
      SqlCommand cmd = null;
      try
      {
        OpenDB();
        cmd = new SqlCommand(procName);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Connection = con;
        if (procParams != null)
        {
          for (int index = 0; index < procParams.Length; index++)
          {
            cmd.Parameters.Add(procParams[index]);
          }
        }
        da.SelectCommand = (SqlCommand)cmd;
        da.Fill(ds);
      }
      catch (SqlException e)
      {
        throw e;
      }
      return ds;
    }

    



  }//
}///
