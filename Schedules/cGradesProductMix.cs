﻿using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using System.Data.Common;
//using System.Configuration;
using System.Windows.Forms;
using System.Data;
using Oracle.ManagedDataAccess.Client;



namespace Schedules
{
  class cGradesProductMix
  {

    //grades product mix monthly data only. 
    //read from PTS/PHS
    //Update OEE 

    string sSite = "";
    string sAreaID = "";
    int iSappiYear = 0;
    int currMonth = 0;
    int iLoopMonth = 0;

    cCal cal = new cCal();


    public void RunJob(string pSite,  int pSappiYear, int pStartMonth)
    {

      cal = cal ?? new cCal();
      string sStartDate = "";
      string sEndDate = "";

      try
      {

        sSite = pSite;

        // get current month, start and end dates, finyear
        
        cal.GetCal(cmath.getDateNowStr(-1));
        iSappiYear = cal.iSappiYear;
        currMonth = cal.iCurrMonthOfYear;
        sStartDate = cal.sMonthStartDate;
        sEndDate = cal.sMonthEndDate;

        if (pSappiYear > iSappiYear)
        {
          cFG.LogIt("cGradesProductMix RunYear > current year");
          return;
        }

        Application.DoEvents();


        //start from a month number
        if (pStartMonth > 0)
          iLoopMonth = pStartMonth;     //start from the month number
        else
          iLoopMonth = currMonth;       //current month only


        //if running for previous years
        if ((pSappiYear > 0) && (pSappiYear < iSappiYear))
        {
          iSappiYear = pSappiYear;
          iLoopMonth = 1;         //start from 1st month until month 12  unless
          currMonth = 12;         //loop from month 1 to 12
          if (pStartMonth > 0)
            iLoopMonth = pStartMonth;     //or from month (N) to 12 
        }



        //then loop thru until the current month
        while (iLoopMonth <= currMonth)
        {

          Application.DoEvents();

          //cORA.OpenDB(sSite);
          //cal.GetMonth(iSappiYear, iLoopMonth, ref sStartDate, ref sEndDate);      //get start date and end date for the month
          //cORA.CloseDB();

          cSQL.OpenDB("ZAMDW");
          cal.GetMonthMSSQL(iSappiYear, iLoopMonth, ref sStartDate, ref sEndDate);
          cSQL.CloseDB();

          //this year and current month then enddate is up to yesterday
          if ((pSappiYear == 0) && (iLoopMonth == currMonth))    
          {
            sEndDate = cmath.getDateEODStr(cmath.getDateStr(DateTime.Now.ToString(), -1));       //yesterday
          }


          cFG.LogIt("cGradesProductMix Process Month# " + iLoopMonth + " Date Start : " + sStartDate + " , Date End " + sEndDate);

          if (sSite == "TUG")
          {
            DoTUG_PM(sStartDate, sEndDate, iLoopMonth);
          }

          if (sSite == "STA")
          {
            DoSTA_TM(sStartDate, sEndDate, iLoopMonth);
            Application.DoEvents();
            DoSTA_PM(sStartDate, sEndDate, iLoopMonth);
          }

          if (sSite == "SAI")
          {
            DoSAI(sStartDate, sEndDate, iLoopMonth);      
          }

          if (sSite == "NGX")
          {
            DoNGX_UT3(sStartDate, sEndDate, iLoopMonth);

            Application.DoEvents();
            DoNGX_PM1(sStartDate, sEndDate, iLoopMonth);

            Application.DoEvents();
            DoNGX_PM2(sStartDate, sEndDate, iLoopMonth);
          }

          iLoopMonth++;

        } //loop month


      }
      catch (Exception er)
      {
        throw er;
      }
      finally
      {
        cORA.CloseDB();
        cSQL.CloseDB();
        cal = null;
      }


    }//



    private void DoTUG_PM(string pStartDate, string pEndDate, int pMonth)
    {

      bool boFound = false;
      string sql = "";
      string sGradeID = "";
      string sPTSCode = "";
      string sGSM = "";
      string sGradeDesc = "";


      string rvGradeCode = "";
      string rvGSM = "";
      double rvGross = 0;
      double rvMachHrs = 0;

      int rv = 0;

      pStartDate = cmath.OraDate(pStartDate);
      pEndDate = cmath.OraDate(pEndDate);

      try
      {

        sql = "select make_as_grade, make_as_gsm, round((sum(run_time) / 60),5) Hrs, (sum(jumbo_kg_g) / 1000) prod"
          + " from  h_jumbo"
          + " where trunc(jumbo_end_date) between " + pStartDate + " and " +  pEndDate 
          + " group by  make_as_grade, make_as_gsm"
          + " order by make_as_grade, make_as_gsm";

        cORA.OpenDB("SAPPITUG");
        DataTable dtTUG = cORA.GetDataTable(sql);
        cORA.CloseDB();


        //OEE database
        cORA.OpenDB(sSite);

        sAreaID = "118";     //tugela pm

        //multiply the GSM by 10 for the tugela gsm
        sql = "Select gradeid, gradedesc, (GSM * 10) GSM, ptsCode from Grades where flid = " + sAreaID
          + "  and active = 1 and length(ptsCode) > 0";

        DataTable dtGrades = cORA.GetDataTable(sql);

        //loop OEE grades
        foreach (DataRow item in dtGrades.Rows)
        {

          sGradeID = item["gradeid"].ToString();
          sPTSCode = item["ptsCode"].ToString();
          sGSM = item["gsm"].ToString();
          sGradeDesc = item["gradeDesc"].ToString();

          rvGradeCode = "";
          rvGSM = "";
          rvGross = 0;
          rvMachHrs = 0;

          boFound = false; ;

          //jumbomaterial, substance, runtime, tonnes
          foreach (DataRow tugItem in dtTUG.Rows)
          {
            rvGradeCode = tugItem["make_as_grade"].ToString();
            rvGSM = tugItem["make_as_gsm"].ToString();
            rvGross = cmath.getd(tugItem["prod"].ToString());
            rvMachHrs = cmath.getd(tugItem["hrs"].ToString());
            if ((sPTSCode == rvGradeCode) && (sGSM == rvGSM))
            {
              boFound = true;
              break;
            }
          }

          if (boFound == false)
            cFG.LogIt("Grade and PTS Code NOT FOUND at TUG-PM AreaID=" + sAreaID + " " + sGradeID + " " + sGradeDesc + " " + sPTSCode);
          else
            cFG.LogIt("Grade and PTS Code AreaID=" + sAreaID + " " + sGradeID + " " + sGradeDesc + " " + sPTSCode);


          if (boFound == false)
          {
            rvGradeCode = "";
            rvGSM = "";
            rvGross = 0;
            rvMachHrs = 0;
          }

          sql = "Update gradesbudgets_month set prodValue = " + rvGross;
          sql += ", prodHrs = " + rvMachHrs;
          sql += " where gradeid = " + sGradeID;
          sql += " and flid = " + sAreaID;
          sql += " and sappiYear = " + iSappiYear;
          sql += " and sappiMonth = " + pMonth;

          rv = cORA.ExecQuery(sql);

          if (rv == 0)
          {
            sql = "Insert into gradesbudgets_month(GradeID, FLID, SappiYear, SappiMonth, ProdHrs, ProdValue, Active)";
            sql += "Values(";
            sql += "" + sGradeID;
            sql += "," + sAreaID;
            sql += "," + iSappiYear;
            sql += "," + pMonth;
            sql += "," + rvMachHrs;
            sql += "," + rvGross;
            sql += ",1";
            sql += ")";

            rv = cORA.ExecQuery(sql);
          }

        }  //loop grades

      }
      catch (Exception ee)
      {
        cFG.LogIt("DoSTA_PM: " + ee.Message);
        throw ee;
      }
      finally
      {
        cSQL.CloseDB();
        cORA.CloseDB();
      }

    }//


    private void DoSAI(string pStartDate, string pEndDate, int pMonth)
    {

      /*
       * FIELDS RETURNED FROM QUERY
       * code
       * admt_tons
       * machine_hrs
      */

      string sql = "";
      string sPTSCode = "";
      string sGradeID = "";
      string sGradeDesc = "";
      int iQuarter = 0;
      int iQuarterNum = 0;
      double dTons = 0;
      double dHrs = 0;
      string sRetCode = "";
      int rv = 0;
      bool boOK = true; ;

      DataTable dt = null;

      pStartDate = cmath.getDateTimeStr(pStartDate);

      pEndDate = cmath.getDateTimeStr(pEndDate);

      try
      {
        //loop the OEE grades to get the 'ptscode' to pass as parameter to the stored proc
        // update gradesbudget_month with the production and hrs 

        cSQL.OpenDB("SAIMES");

        cORA.OpenDB(sSite);

        //------------------------------------------------------------------------
        //c3  flid = 4
        for (int area = 4; area <= 6; area++)
        {

          sAreaID = area.ToString();

          sql = "Select gradeid, gradedesc, ptsCode, quarter from Grades where flid = " + sAreaID
            + "  and active = 1 and length(ptsCode) > 0";

          DataTable dtGrades = cORA.GetDataTable(sql);

          foreach (DataRow item in dtGrades.Rows)
          {
            Application.DoEvents();

            sGradeID = item["gradeid"].ToString();
            sPTSCode = item["ptsCode"].ToString();
            sGradeDesc = item["gradedesc"].ToString();
            iQuarter = cmath.geti(item["quarter"].ToString());

            sql = "EXECUTE [sts].[Sp_Production_Hrs_perBM_Code] '" + sPTSCode + "', '" + pStartDate + "', '" + pEndDate + "'";

            dt = cSQL.GetDataTable(sql);

            if (dt.Rows.Count == 0)
              cFG.LogIt("Grade and PTS Code NOT FOUND at Saiccor AreaID=" + sAreaID + " "  + sGradeID + " " + sGradeDesc + " " + sPTSCode);
            else
              cFG.LogIt("Grade and PTS Code AreaID=" + sAreaID + " " + sGradeID + " " + sGradeDesc + " " + sPTSCode);


            dTons = 0;
            dTons = 0;
            sRetCode = "";
            foreach (DataRow row in dt.Rows)
            {
              dTons = cmath.getd(row["admt_tons"].ToString());           //
              dHrs = cmath.getd(row["machine_hrs"].ToString());          //
              sRetCode = row["code"].ToString();                         //
            }


            if (iQuarter > 0)
            {
              //cal.GetQuarter(pStartDate, ref iQuarterNum);
              cal.GetCal(pStartDate);
              iQuarterNum = cal.iQuarterOfYear;

              if (iQuarter != iQuarterNum)
                continue;             //do not update gradesbudgets
            }


            sql = "Update gradesbudgets_month set prodValue = " + dTons;
            sql += ", prodHrs = " + dHrs;
            sql += " where gradeid = " + sGradeID;
            sql += " and flid = " + sAreaID;
            sql += " and sappiYear = " + iSappiYear;
            sql += " and sappiMonth = " + pMonth;


            rv = cORA.ExecQuery(sql);

            if (rv == 0)
            {
              sql = "Insert into gradesbudgets_month(GradeID, FLID, SappiYear, SappiMonth, ProdHrs, ProdValue, Active)";
              sql += "Values(";
              sql += "" + sGradeID;
              sql += "," + sAreaID;
              sql += "," + iSappiYear;
              sql += "," + pMonth;
              sql += "," + dHrs;
              sql += "," + dTons;
              sql += ",1";
              sql += ")";

              rv = cORA.ExecQuery(sql);
            }

          } //loop grades

      }  // loop area

      }
      catch (Exception ee)
      {
        cFG.LogIt("DoSAIProductMix: " + ee.Message);
        throw ee;
      }
      finally
      {
        cSQL.CloseDB();
        cORA.CloseDB();
      }


    }//



    private void DoNGX_UT3(string pStartDate, string pEndDate, int pMonth)
    {

      /*
       * FIELDS RETURNED FROM QUERY
       * code
       * admt_tons
       * machine_hrs
      */


      string sql = "";
      string sPTSCode = "";
      string sGradeID = "";
      string sGradeDesc = "";
      double dTons = 0;
      int iMonth = 0;
      double dHrs = 0;
      string sRetCode = "";
      int rv = 0;

      DataTable dt = null;

      pStartDate = cmath.getDateTimeStr(pStartDate);

      pEndDate = cmath.getDateTimeStr(pEndDate);

      try
      {

        cSQL.OpenDB("NGXMES");

        cORA.OpenDB(sSite);

        sAreaID = "13";     //UT3

        sql = "Select gradeid, gradedesc, ptsCode, quarter from Grades where flid = " + sAreaID
          + "  and active = 1 and length(ptsCode) > 0";

        DataTable dtGrades = cORA.GetDataTable(sql);

        foreach (DataRow item in dtGrades.Rows)
        {

          Application.DoEvents();

          sGradeID = item["gradeid"].ToString();
          sPTSCode = item["ptsCode"].ToString();
          sGradeDesc = item["gradedesc"].ToString();
          iMonth = cmath.geti(item["quarter"].ToString());      // for NGX its regarded as month

          sql = "EXECUTE [sts].[Sp_Production_Hrs_perBM_Code] '" + sPTSCode + "', '" + pStartDate + "', '" + pEndDate + "'";

          dt = cSQL.GetDataTable(sql);

          if (dt.Rows.Count == 0)
            cFG.LogIt("Grade and PTS Code NOT FOUND at NGX AreaID=" + sAreaID + " " + sGradeID + " " + sGradeDesc + " " + sPTSCode);
          else
            cFG.LogIt("Grade and PTS Code AreaID=" + sAreaID + " " + sGradeID + " " + sGradeDesc + " " + sPTSCode);


          dTons = 0;
          dTons = 0;
          sRetCode = "";
          foreach (DataRow row in dt.Rows)
          {
            dTons = cmath.getd(row["admt_tons"].ToString());           //
            dHrs = cmath.getd(row["machine_hrs"].ToString());          //
            sRetCode = row["code"].ToString();                         //
          }

          if (iMonth > 0)
          {
            if (iMonth != pMonth)
              continue;             //do not update gradesbudgets
          }


          sql = "Update gradesbudgets_month set prodValue = " + dTons;
          sql += ", prodHrs = " + dHrs;
          sql += " where gradeid = " + sGradeID;
          sql += " and flid = " + sAreaID;
          sql += " and sappiYear = " + iSappiYear;
          sql += " and sappiMonth = " + pMonth;

          rv = cORA.ExecQuery(sql);

          if (rv == 0)
          {
            sql = "Insert into gradesbudgets_month(GradeID, FLID, SappiYear, SappiMonth, ProdHrs, ProdValue, Active)";
            sql += "Values(";
            sql += "" + sGradeID;
            sql += "," + sAreaID;
            sql += "," + iSappiYear;
            sql += "," + pMonth;
            sql += "," + dHrs;
            sql += "," + dTons;
            sql += ",1";
            sql += ")";

            rv = cORA.ExecQuery(sql);
          }

        } //loop grades


      }
      catch (Exception ee)
      {
        cFG.LogIt("DoNGX: " + ee.Message);
        throw ee;
      }
      finally
      {
        cSQL.CloseDB();
        cORA.CloseDB();
      }


    }//



    private void DoNGX_PM1(string pStartDate, string pEndDate, int pMonth)
    {

      /*
       * FIELDS RETURNED FROM QUERY
      */

      bool boFound = false;
      string sql = "";
      string sGradeID = "";
      string sPTSCode = "";
      string sGSM = "";
      string sGradeDesc = "";
      int iMonth = 0;

      //return values from SP
      string rvGradeCode = "";
      string rvGSM = "";
      double rvGross = 0;
      double rvMachHrs = 0;

      int rv = 0;


      pStartDate = cmath.getDateStr(pStartDate);

      pEndDate = cmath.getDateStr(pStartDate);

      try
      {

        cORA.OpenDB("SAPXMIINGX");
        cORA.cmd = cORA.con.CreateCommand();
        cORA.cmd.CommandText = "sapxmii.api_manufacturing_efficiency_model.gross_and_machine_hours_of ";
        cORA.cmd.CommandType = CommandType.StoredProcedure;
        OracleCommandBuilder.DeriveParameters(cORA.cmd);
        cORA.cmd.BindByName = true;
        cORA.cmd.Parameters["i_pm_no"].Value = 1;                   // kraft PM1
        cORA.cmd.Parameters["i_date_from"].Value = pStartDate;      //    2021-08-10
        cORA.cmd.Parameters["i_date_to"].Value = pEndDate;


        //return PM_NO, GRADE_CODE, GSM, GROSS, MACHINE_HOURS
        DataTable sapDT = cORA.ExecuteQuerySp(cORA.cmd);

        cORA.CloseDB();


        //OEE database
        cORA.OpenDB(sSite);

        sAreaID = "139";     //PM1  kraft Liner board

        sql = "Select gradeid, gradedesc, GSM, ptsCode, quarter from Grades where flid = " + sAreaID
          + "  and active = 1 and length(ptsCode) > 0";

        DataTable dtGrades = cORA.GetDataTable(sql);



        //loop OEE grades
        foreach (DataRow item in dtGrades.Rows)
        {

          sGradeID = item["gradeid"].ToString();
          sPTSCode = item["ptsCode"].ToString();
          sGSM = item["gsm"].ToString();
          sGradeDesc = item["gradeDesc"].ToString();
          iMonth = cmath.geti(item["quarter"].ToString());     // for NGX its regarded as month

          rvGradeCode = "";
          rvGSM = "";
          rvGross = 0;
          rvMachHrs = 0;

          boFound = false; ;

          //loop values from SAPXMII and if found assign the gross and hrs
          foreach (DataRow sapitem in sapDT.Rows)
          {
            rvGradeCode = sapitem["grade_code"].ToString();
            rvGSM = sapitem["gsm"].ToString();
            rvGross = cmath.getd(sapitem["gross"].ToString());
            rvMachHrs = cmath.getd(sapitem["machine_hours"].ToString());
            if ( (sPTSCode == rvGradeCode) && (sGSM == rvGSM) )
            {
              boFound = true;
              break;
            }
          }

          if (boFound == false)
            cFG.LogIt("Grade and PTS Code NOT FOUND at NGX AreaID=" + sAreaID + " " + sGradeID + " " + sGradeDesc + " " + sPTSCode);
          else
            cFG.LogIt("Grade and PTS Code AreaID=" + sAreaID + " " + sGradeID + " " + sGradeDesc + " " + sPTSCode);

          if (boFound == false)
          {
            rvGradeCode = "";
            rvGSM = "";
            rvGross = 0;
            rvMachHrs = 0;
          }

          if (iMonth > 0)
          {
            if (iMonth != pMonth)
              continue;             //do not update gradesbudgets
          }

          sql = "Update gradesbudgets_month set prodValue = " + rvGross;
          sql += ", prodHrs = " + rvMachHrs;
          sql += " where gradeid = " + sGradeID;
          sql += " and flid = " + sAreaID;
          sql += " and sappiYear = " + iSappiYear;
          sql += " and sappiMonth = " + pMonth;

          rv = cORA.ExecQuery(sql);

          if (rv == 0)
          {
            sql = "Insert into gradesbudgets_month(GradeID, FLID, SappiYear, SappiMonth, ProdHrs, ProdValue, Active)";
            sql += "Values(";
            sql += "" + sGradeID;
            sql += "," + sAreaID;
            sql += "," + iSappiYear;
            sql += "," + pMonth;
            sql += "," + rvMachHrs;
            sql += "," + rvGross;
            sql += ",1";
            sql += ")";

            rv = cORA.ExecQuery(sql);
          }



        }  //loop grades

      }
      catch (Exception ee)
      {
        cFG.LogIt("DoNGX_PM1: " + ee.Message);
        throw ee;
      }
      finally
      {
        cSQL.CloseDB();
        cORA.CloseDB();
      }


    }//


    private void DoNGX_PM2(string pStartDate, string pEndDate, int pMonth)
    {

      /*
       * FIELDS RETURNED FROM QUERY
      */

      bool boFound = false;
      string sql = "";
      string sGradeID = "";
      string sPTSCode = "";
      string sGSM = "";
      string sGradeDesc = "";
      int iMonth = 0;


      //return values from SP
      string rvGradeCode = "";
      string rvGSM = "";
      double rvGross = 0;
      double rvMachHrs = 0;

      int rv = 0;


      pStartDate = cmath.getDateStr(pStartDate);

      pEndDate = cmath.getDateStr(pStartDate);

      try
      {

        cORA.OpenDB("SAPXMIINGX");
        cORA.cmd = cORA.con.CreateCommand();
        cORA.cmd.CommandText = "sapxmii.api_manufacturing_efficiency_model.gross_and_machine_hours_of ";
        cORA.cmd.CommandType = CommandType.StoredProcedure;
        OracleCommandBuilder.DeriveParameters(cORA.cmd);
        cORA.cmd.BindByName = true;
        cORA.cmd.Parameters["i_pm_no"].Value = 2;                   // newsprint PM2
        cORA.cmd.Parameters["i_date_from"].Value = pStartDate;      //    2021-08-10
        cORA.cmd.Parameters["i_date_to"].Value = pEndDate;


        //return PM_NO, GRADE_CODE, GSM, GROSS, MACHINE_HOURS
        DataTable sapDT = cORA.ExecuteQuerySp(cORA.cmd);

        cORA.CloseDB();


        //OEE database
        cORA.OpenDB(sSite);

        sAreaID = "25";     //PM2 newsprint

        sql = "Select gradeid, gradedesc, GSM, ptsCode, quarter from Grades where flid = " + sAreaID
          + "  and active = 1 and length(ptsCode) > 0";

        DataTable dtGrades = cORA.GetDataTable(sql);


        //loop OEE grades
        foreach (DataRow item in dtGrades.Rows)
        {

          sGradeID = item["gradeid"].ToString();
          sPTSCode = item["ptsCode"].ToString();
          sGSM = item["gsm"].ToString();
          sGradeDesc = item["gradeDesc"].ToString();
          iMonth = cmath.geti(item["quarter"].ToString());      //its regarded as a month for NGX

          rvGradeCode = "";
          rvGSM = "";
          rvGross = 0;
          rvMachHrs = 0;

          boFound = false; ;

          //loop values from SAPXMII and if found assing the gross and hrs
          foreach (DataRow sapitem in sapDT.Rows)
          {
            rvGradeCode = sapitem["grade_code"].ToString();
            rvGSM = sapitem["gsm"].ToString();
            rvGross = cmath.getd(sapitem["gross"].ToString());
            rvMachHrs = cmath.getd(sapitem["machine_hours"].ToString());
            if ((sPTSCode == rvGradeCode) && (sGSM == rvGSM))
            {
              boFound = true;
              break;
            }
          }

          if (boFound == false)
            cFG.LogIt("Grade and PTS Code NOT FOUND at NGX AreaID=" + sAreaID + " " + sGradeID + " " + sGradeDesc + " " + sPTSCode);
          else
            cFG.LogIt("Grade and PTS Code AreaID=" + sAreaID + " " + sGradeID + " " + sGradeDesc + " " + sPTSCode);

          if (boFound == false)
          {
            rvGradeCode = "";
            rvGSM = "";
            rvGross = 0;
            rvMachHrs = 0;
          }

          if (iMonth > 0)
          {
            if (iMonth != pMonth)
              continue;             //do not update gradesbudgets
          }


          sql = "Update gradesbudgets_month set prodValue = " + rvGross;
          sql += ", prodHrs = " + rvMachHrs;
          sql += " where gradeid = " + sGradeID;
          sql += " and flid = " + sAreaID;
          sql += " and sappiYear = " + iSappiYear;
          sql += " and sappiMonth = " + pMonth;

          rv = cORA.ExecQuery(sql);

          if (rv == 0)
          {
            sql = "Insert into gradesbudgets_month(GradeID, FLID, SappiYear, SappiMonth, ProdHrs, ProdValue, Active)";
            sql += "Values(";
            sql += "" + sGradeID;
            sql += "," + sAreaID;
            sql += "," + iSappiYear;
            sql += "," + pMonth;
            sql += "," + rvMachHrs;
            sql += "," + rvGross;
            sql += ",1";
            sql += ")";

            rv = cORA.ExecQuery(sql);
          }



        }  //loop grades

      }
      catch (Exception ee)
      {
        cFG.LogIt("DoNGX_PM2: " + ee.Message);
        throw ee;
      }
      finally
      {
        cSQL.CloseDB();
        cORA.CloseDB();
      }


    }//


    private void DoSTA_PM(string pStartDate, string pEndDate, int pMonth)
    {

      bool boFound = false;
      string sql = "";
      string sGradeID = "";
      string sPTSCode = "";
      string sGSM = "";
      string sGradeDesc = "";

      //return values from SP
      string rvGradeCode = "";
      string rvGSM = "";
      double rvGross = 0;
      double rvMachHrs = 0;

      int rv = 0;

      pStartDate = cmath.getDateStr(pStartDate);
      pEndDate = cmath.getDateStr(pEndDate);

      try
      {

        cORA.OpenDB("SPIS");

        DataTable DT = new DataTable();

        OracleCommand cmd = cORA.con.CreateCommand();
        cmd.Connection = cORA.con;
        cmd.CommandText = "spis.api_mes_jumbo_summary.paper_oee_dt_of";
        cmd.CommandType = CommandType.StoredProcedure;
        OracleCommandBuilder.DeriveParameters(cmd);
        cmd.BindByName = true;

        cmd.Parameters["i_start_date"].Value = pStartDate;
        cmd.Parameters["i_end_date"].Value = pEndDate;
        cmd.Parameters["i_coated"].Value = "";

        OracleDataAdapter da = new OracleDataAdapter(cmd);

        //use this when the pl/sql temporary table has an on commit-rollback
        //OracleTransaction transaction;
        //transaction = cmd.Connection.BeginTransaction();
        //
        cmd.ExecuteNonQuery();
        da.Fill(DT);

        //transaction.Rollback();

        cORA.CloseDB();


        //OEE database
        cORA.OpenDB(sSite);

        sAreaID = "6";     //Stanger PM1

        sql = "Select gradeid, gradedesc, GSM, ptsCode from Grades where flid = " + sAreaID
          + "  and active = 1 and length(ptsCode) > 0";

        DataTable dtGrades = cORA.GetDataTable(sql);

        //loop OEE grades
        foreach (DataRow item in dtGrades.Rows)
        {

          sGradeID = item["gradeid"].ToString();
          sPTSCode = item["ptsCode"].ToString();
          sGSM = item["gsm"].ToString();
          sGradeDesc = item["gradeDesc"].ToString();

          rvGradeCode = "";
          rvGSM = "";
          rvGross = 0;
          rvMachHrs = 0;

          boFound = false; ;

          //jumbomaterial, substance, runtime, tonnes
          foreach (DataRow sapitem in DT.Rows)
          {
            rvGradeCode = sapitem["jumbomaterial"].ToString();
            rvGSM = sapitem["substance"].ToString();
            rvGross = cmath.getd(sapitem["tonnes"].ToString());
            rvMachHrs = cmath.getd(sapitem["runtime"].ToString());
            if ((sPTSCode == rvGradeCode) && (sGSM == rvGSM))
            {
              boFound = true;
              break;
            }
          }

          if (boFound == false)
            cFG.LogIt("Grade and PTS Code NOT FOUND at STA-PM AreaID=" + sAreaID + " " + sGradeID + " " + sGradeDesc + " " + sPTSCode);
          else
            cFG.LogIt("Grade and PTS Code AreaID=" + sAreaID + " " + sGradeID + " " + sGradeDesc + " " + sPTSCode);


          if (boFound == false)
          {
            rvGradeCode = "";
            rvGSM = "";
            rvGross = 0;
            rvMachHrs = 0;
          }

          sql = "Update gradesbudgets_month set prodValue = " + rvGross;
          sql += ", prodHrs = " + rvMachHrs;
          sql += " where gradeid = " + sGradeID;
          sql += " and flid = " + sAreaID;
          sql += " and sappiYear = " + iSappiYear;
          sql += " and sappiMonth = " + pMonth;

          rv = cORA.ExecQuery(sql);

          if (rv == 0)
          {
            sql = "Insert into gradesbudgets_month(GradeID, FLID, SappiYear, SappiMonth, ProdHrs, ProdValue, Active)";
            sql += "Values(";
            sql += "" + sGradeID;
            sql += "," + sAreaID;
            sql += "," + iSappiYear;
            sql += "," + pMonth;
            sql += "," + rvMachHrs;
            sql += "," + rvGross;
            sql += ",1";
            sql += ")";

            rv = cORA.ExecQuery(sql);
          }

        }  //loop grades

      }
      catch (Exception ee)
      {
        cFG.LogIt("DoSTA_PM: " + ee.Message);
        throw ee;
      }
      finally
      {
        cSQL.CloseDB();
        cORA.CloseDB();
      }

    }//



    private void DoSTA_TM(string pStartDate, string pEndDate, int pMonth)
    {
      bool boFound = false;
      string sql = "";
      string sGradeID = "";
      string sPTSCode = "";
      string sGSM = "";
      string sGradeDesc = "";

      //return values from SP
      string rvGradeCode = "";
      string rvGSM = "";
      double rvGross = 0;
      double rvMachHrs = 0;

      int rv = 0;

      pStartDate = cmath.getDateStr(pStartDate);
      pEndDate = cmath.getDateStr(pEndDate);

      try
      {

        cORA.OpenDB("SPIS");

        DataTable DT = new DataTable();

        OracleCommand cmd = cORA.con.CreateCommand();
        cmd.Connection = cORA.con;
        cmd.CommandText = "spis.api_mes_jumbo_summary.tissue_oee_dt_of";
        cmd.CommandType = CommandType.StoredProcedure;
        OracleCommandBuilder.DeriveParameters(cmd);
        cmd.BindByName = true;

        cmd.Parameters["i_start_date"].Value = pStartDate;
        cmd.Parameters["i_end_date"].Value = pEndDate;

        OracleDataAdapter da = new OracleDataAdapter(cmd);

        //use this when the pl/sql temporary table has an on commit-rollback
        //OracleTransaction transaction;
        //transaction = cmd.Connection.BeginTransaction();
        //
        cmd.ExecuteNonQuery();
        da.Fill(DT);

        //transaction.Rollback();

        cORA.CloseDB();


        //OEE database
        cORA.OpenDB(sSite);

        sAreaID = "12";     //Stanger Tissue machine

        sql = "Select gradeid, gradedesc, GSM, ptsCode from Grades where flid = " + sAreaID
          + "  and active = 1 and length(ptsCode) > 0";

        DataTable dtGrades = cORA.GetDataTable(sql);

        //loop OEE grades
        foreach (DataRow item in dtGrades.Rows)
        {

          sGradeID = item["gradeid"].ToString();
          sPTSCode = item["ptsCode"].ToString();
          sGSM = item["gsm"].ToString();
          sGradeDesc = item["gradeDesc"].ToString();

          rvGradeCode = "";
          rvGSM = "";
          rvGross = 0;
          rvMachHrs = 0;

          boFound = false; ;

          //jumbomaterial, substance, runtime, tonnes
          foreach (DataRow sapitem in DT.Rows)
          {
            rvGradeCode = sapitem["jumbomaterial"].ToString();
            rvGSM = sapitem["substance"].ToString();
            rvGross = cmath.getd(sapitem["tonnes"].ToString());
            rvMachHrs = cmath.getd(sapitem["runtime"].ToString());
            if ((sPTSCode == rvGradeCode) && (sGSM == rvGSM))
            {
              boFound = true;
              break;
            }
          }

          if (boFound == false)
            cFG.LogIt("Grade and PTS Code NOT FOUND at STA-TM AreaID=" + sAreaID + " " + sGradeID + " " + sGradeDesc + " " + sPTSCode);
          else
            cFG.LogIt("Grade and PTS Code AreaID=" + sAreaID + " " + sGradeID + " " + sGradeDesc + " " + sPTSCode);


          if (boFound == false)
          {
            rvGradeCode = "";
            rvGSM = "";
            rvGross = 0;
            rvMachHrs = 0;
          }

          sql = "Update gradesbudgets_month set prodValue = " + rvGross;
          sql += ", prodHrs = " + rvMachHrs;
          sql += " where gradeid = " + sGradeID;
          sql += " and flid = " + sAreaID;
          sql += " and sappiYear = " + iSappiYear;
          sql += " and sappiMonth = " + pMonth;

          rv = cORA.ExecQuery(sql);

          if (rv == 0)
          {
            sql = "Insert into gradesbudgets_month(GradeID, FLID, SappiYear, SappiMonth, ProdHrs, ProdValue, Active)";
            sql += "Values(";
            sql += "" + sGradeID;
            sql += "," + sAreaID;
            sql += "," + iSappiYear;
            sql += "," + pMonth;
            sql += "," + rvMachHrs;
            sql += "," + rvGross;
            sql += ",1";
            sql += ")";

            rv = cORA.ExecQuery(sql);
          }



        }  //loop grades

      }
      catch (Exception ee)
      {
        cFG.LogIt("DoSTA_TM: " + ee.Message);
        throw ee;
      }
      finally
      {
        cSQL.CloseDB();
        cORA.CloseDB();
      }

    }//




  }///
}///
