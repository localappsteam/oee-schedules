﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data;
using Oracle.ManagedDataAccess.Client;

namespace Schedules
{
  static class cORA
  {

    public static OracleConnection con;
    public static OracleCommand cmd;

    public static void CloseDB()
    {
      if (con == null)
        return;
      try
      {
        if (con.State != ConnectionState.Closed)
        {
          con.Close();
        }
      }
      catch (Exception)
      { }


      con = null;
      return;

    } //




    public static void OpenDB(string site = "")
    {

      string connStr = "";

      if (con?.State == ConnectionState.Open)
      {
        return;
      }

      switch (site)
      {
        case "NGX":
          connStr = "User Id=oee;Password=oee;" + "Data Source=ngxvox02/KNP2.za.sappi.com;";
          break;


        case "NGXDEV":
          connStr = "User Id=oee;Password=oee;" + "Data Source=ngxvoxd02/KNP2.za.sappi.com;";
          break;

        case "NGXNIS":
          //NGX NIS
          connStr = "User Id=NIS;Password=nis;" + "Data Source=ngxvox02/KNP2.za.sappi.com;";
          break;

        case "PMPS":
          //ngodwana production
          connStr = "User Id=pmps;Password=pmps;" + "Data Source=ngxvox01/KNP1.za.sappi.com;";
          break;

        case "SAPXMIINGX":
          //ngodwana production
          connStr = "User Id=SAPXMII;Password=sapxmii;" + "Data Source=ngxvox01/KNP1.za.sappi.com;";
          break;

        case "SAPXMIINGX2":
          //for ngodwana DIGESTER 2 package
          connStr = "User Id=SAPXMII;Password=sapxmii;" + "Data Source=ngxvox02/KNP2.za.sappi.com;";
          break;

        case "STA":
          //"Data Source=(DESCRIPTION=(CONNECT_DATA=(SERVICE_NAME=PSP1.za.sappi.com))(ADDRESS=(PROTOCOL=TCP)(HOST=stavox01.za.sappi.com)(PORT=1521)(SERVER=DEDICATED)));User ID=soee;Password=soee;"
          
           //live server
          connStr = "User Id=soee;Password=soee;" + "Data Source=stavox01/PSP1.za.sappi.com";

          ///dev server
          //connStr = "User Id=soee;Password=soee;" + "Data Source=stavoxd01/PSD1.za.sappi.com";
          break;

      
        case "RTC":
          //stanger namespace for pulp Stanger Stores Issues
          connStr = "User Id=RTC;Password=rtc;" + "Data Source=stavox01/PSP1.za.sappi.com";
          break;

        case "TUG":
           //"Data Source=(DESCRIPTION=(CONNECT_DATA=(SERVICE_NAME=KTP1.za.sappi.com))(ADDRESS=(PROTOCOL=TCP)(HOST=tugvox01.za.sappi.com)(PORT=1521)(SERVER=DEDICATED)));User ID=OEE;Password=oee;" />
           connStr = "User Id=oee;Password=oee;" + "Data Source=tugvox01/KTP1.za.sappi.com;";
          break;

        case "TUGDEV":
          connStr = "User Id=oee;Password=oee;" + "Data Source=tugvod03;";
          break;

        case "SAI":
             //"Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=tcp)(HOST=SAIVOS03)(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=SAICC)));User ID=OEE;Password=oee;" />
             connStr = "Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=tcp)(HOST=SAIVOS03)(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=SAICC)));User ID=OEE;Password=oee;";
          break;

        case "SAIDEV":
          connStr = "User Id=oee;Password=oee;" + "Data Source=SAIVOD02;";
          break;


        case "SPIS":
          //stanger production
          connStr = "User Id=spis;Password=spis;" + "Data Source=stavox01/PSP1.za.sappi.com;";
          break;

        case "PROG_REP":
          //tugela production
          connStr = "User Id=prog_rep;Password=prog_rep;" + "Data Source=tugvox02/KTP2.za.sappi.com;";
          break;

        case "SAPXMII":
          //tugela production
          connStr = "User Id=SAPXMII;Password=sapxmii;" + "Data Source=tugvox02/KTP2.za.sappi.com;";
          break;

        case "SAPPITUG":
          //tugela budgets
          connStr = "User Id=sappi;Password=sappi;" + "Data Source=tugvox02/KTP2.za.sappi.com;";
          break;

        default:
          connStr = "";
          break;
      }

      try
      {
        con = new OracleConnection(connStr);
        con.Open();
      }
      catch (Exception ee)
      {
        con = null;
        cFG.LogIt(connStr);
        throw new Exception("Cannot connect to Database : " + ee.Message);
      }

    } //




    public static DataSet GetDataset(string sql)
    {
      try
      {
        DataSet ds = new DataSet();
        OracleDataAdapter da = new OracleDataAdapter(sql, con);
        da.Fill(ds);
        return ds;
      }
      catch (Exception e)
      {
        throw e;
      }
    }




    public static DataTable GetDataTable(string sql)
    {
      try
      {
        OracleDataAdapter da = new OracleDataAdapter(sql, con);
        DataTable dt = new DataTable();
        da.Fill(dt);
        return dt;
      }
      catch (Exception e)
      {
        throw e;
      }
    }



    public static int ExecScalar(string sql)
    {
      int rv = 0;
      try
      {
        OracleCommand cmd = new OracleCommand(sql);
        cmd.Connection = con;
        object result = cmd.ExecuteScalar();
        if (result != null)
          int.TryParse(result.ToString(), out rv);
      }
      catch (Exception er)
      {
        cFG.LogIt("Exec Scalar " + er.Message);
        rv = 0;
      }

      return rv;

    } //




    public static int ExecQuery(string sql)
    {
      try
      {
        OracleCommand cmd = new OracleCommand(sql);
        cmd.Connection = con;
        int rs = cmd.ExecuteNonQuery();
        return rs;
      }
      catch (Exception ee)
      {
        throw ee;
      }
    }


    public static void addDT(string sql, DataTable dt)
    {
      try
      {
        OracleDataAdapter da = new OracleDataAdapter(sql, con);
        da.Fill(dt);
      }
      catch (Exception eish)
      {
        throw eish;
      }
    }


    public static OracleParameter GetCursorParam()
    {
      OracleParameter cp = new OracleParameter
      {
        //ParameterName = "pCursor",
        ParameterName = "l_result",
        OracleDbType = OracleDbType.RefCursor,
        Direction = ParameterDirection.Output
      };
      return cp;
    }


    public static DataTable ExecuteQuerySp(OracleCommand cmd)
    {
      //stored procedure, connection must be open
      DataTable dt = new DataTable();
      cmd.Connection = con;
      OracleDataAdapter da = new OracleDataAdapter(cmd);
      cmd.ExecuteNonQuery();
      da.Fill(dt);
      return dt;
    }


  }//
}///
