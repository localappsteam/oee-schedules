﻿namespace Schedules
{
  partial class frmMain
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
         this.components = new System.ComponentModel.Container();
         this.timer1 = new System.Windows.Forms.Timer(this.components);
         this.SBar = new System.Windows.Forms.StatusStrip();
         this.sbMsg = new System.Windows.Forms.ToolStripStatusLabel();
         this.sbText = new System.Windows.Forms.ToolStripStatusLabel();
         this.menuStrip1 = new System.Windows.Forms.MenuStrip();
         this.mnuFile = new System.Windows.Forms.ToolStripMenuItem();
         this.mnuExit = new System.Windows.Forms.ToolStripMenuItem();
         this.mnuHelp = new System.Windows.Forms.ToolStripMenuItem();
         this.mnuAbout = new System.Windows.Forms.ToolStripMenuItem();
         this.Fldr = new System.Windows.Forms.FolderBrowserDialog();
         this.cmdCancel = new System.Windows.Forms.Button();
         this.lblMsg = new System.Windows.Forms.Label();
         this.cmdTest = new System.Windows.Forms.Button();
         this.cmdGradesProdMix = new System.Windows.Forms.Button();
         this.cmdZADailyReport = new System.Windows.Forms.Button();
         this.cmdWA = new System.Windows.Forms.Button();
         this.cmdFullRun = new System.Windows.Forms.Button();
         this.txtFinYear = new System.Windows.Forms.TextBox();
         this.lblFinYear = new System.Windows.Forms.Label();
         this.label1 = new System.Windows.Forms.Label();
         this.txtStartMonth = new System.Windows.Forms.TextBox();
         this.txtPrevDays = new System.Windows.Forms.TextBox();
         this.label2 = new System.Windows.Forms.Label();
         this.cmdClose = new System.Windows.Forms.Button();
         this.cboAreas = new System.Windows.Forms.ComboBox();
         this.label3 = new System.Windows.Forms.Label();
         this.rbRunWeek = new System.Windows.Forms.RadioButton();
         this.rbRunMonth = new System.Windows.Forms.RadioButton();
         this.rbRunDaily = new System.Windows.Forms.RadioButton();
         this.cmdStartRun = new System.Windows.Forms.Button();
         this.cmdUserHR = new System.Windows.Forms.Button();
         this.cmdGlobalReport = new System.Windows.Forms.Button();
         this.cmdBudgetProduction = new System.Windows.Forms.Button();
         this.cmdDailyBudget = new System.Windows.Forms.Button();
         this.tmpStartDate = new System.Windows.Forms.TextBox();
         this.label4 = new System.Windows.Forms.Label();
         this.SBar.SuspendLayout();
         this.menuStrip1.SuspendLayout();
         this.SuspendLayout();
         // 
         // timer1
         // 
         this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
         // 
         // SBar
         // 
         this.SBar.ImageScalingSize = new System.Drawing.Size(20, 20);
         this.SBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sbMsg,
            this.sbText});
         this.SBar.Location = new System.Drawing.Point(0, 615);
         this.SBar.Name = "SBar";
         this.SBar.Padding = new System.Windows.Forms.Padding(1, 0, 13, 0);
         this.SBar.Size = new System.Drawing.Size(1149, 26);
         this.SBar.TabIndex = 0;
         this.SBar.Text = "StatusBar";
         // 
         // sbMsg
         // 
         this.sbMsg.Name = "sbMsg";
         this.sbMsg.Size = new System.Drawing.Size(67, 20);
         this.sbMsg.Text = "Message";
         // 
         // sbText
         // 
         this.sbText.Name = "sbText";
         this.sbText.Size = new System.Drawing.Size(67, 20);
         this.sbText.Text = "Message";
         // 
         // menuStrip1
         // 
         this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
         this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuFile,
            this.mnuHelp});
         this.menuStrip1.Location = new System.Drawing.Point(0, 0);
         this.menuStrip1.Name = "menuStrip1";
         this.menuStrip1.Padding = new System.Windows.Forms.Padding(5, 2, 0, 2);
         this.menuStrip1.Size = new System.Drawing.Size(1149, 28);
         this.menuStrip1.TabIndex = 17;
         this.menuStrip1.Text = "menuStrip1";
         // 
         // mnuFile
         // 
         this.mnuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuExit});
         this.mnuFile.Name = "mnuFile";
         this.mnuFile.Size = new System.Drawing.Size(46, 24);
         this.mnuFile.Text = "&File";
         // 
         // mnuExit
         // 
         this.mnuExit.Name = "mnuExit";
         this.mnuExit.Size = new System.Drawing.Size(116, 26);
         this.mnuExit.Text = "E&xit";
         this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
         // 
         // mnuHelp
         // 
         this.mnuHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuAbout});
         this.mnuHelp.Name = "mnuHelp";
         this.mnuHelp.Size = new System.Drawing.Size(55, 24);
         this.mnuHelp.Text = "&Help";
         // 
         // mnuAbout
         // 
         this.mnuAbout.Name = "mnuAbout";
         this.mnuAbout.Size = new System.Drawing.Size(83, 26);
         // 
         // cmdCancel
         // 
         this.cmdCancel.Location = new System.Drawing.Point(74, 503);
         this.cmdCancel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
         this.cmdCancel.Name = "cmdCancel";
         this.cmdCancel.Size = new System.Drawing.Size(476, 36);
         this.cmdCancel.TabIndex = 13;
         this.cmdCancel.Text = "Cancel";
         this.cmdCancel.UseVisualStyleBackColor = true;
         this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
         // 
         // lblMsg
         // 
         this.lblMsg.BackColor = System.Drawing.SystemColors.ButtonHighlight;
         this.lblMsg.ForeColor = System.Drawing.Color.Red;
         this.lblMsg.Location = new System.Drawing.Point(12, 541);
         this.lblMsg.Name = "lblMsg";
         this.lblMsg.Size = new System.Drawing.Size(1117, 66);
         this.lblMsg.TabIndex = 19;
         this.lblMsg.UseMnemonic = false;
         // 
         // cmdTest
         // 
         this.cmdTest.Location = new System.Drawing.Point(873, 359);
         this.cmdTest.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
         this.cmdTest.Name = "cmdTest";
         this.cmdTest.Size = new System.Drawing.Size(193, 45);
         this.cmdTest.TabIndex = 49;
         this.cmdTest.Text = "Test";
         this.cmdTest.UseVisualStyleBackColor = true;
         this.cmdTest.Click += new System.EventHandler(this.cmdTest_Click);
         // 
         // cmdGradesProdMix
         // 
         this.cmdGradesProdMix.Location = new System.Drawing.Point(662, 437);
         this.cmdGradesProdMix.Name = "cmdGradesProdMix";
         this.cmdGradesProdMix.Size = new System.Drawing.Size(171, 45);
         this.cmdGradesProdMix.TabIndex = 10;
         this.cmdGradesProdMix.Text = "GradesProductMix";
         this.cmdGradesProdMix.UseVisualStyleBackColor = true;
         this.cmdGradesProdMix.Click += new System.EventHandler(this.cmdGradesProdMix_Click);
         // 
         // cmdZADailyReport
         // 
         this.cmdZADailyReport.Location = new System.Drawing.Point(74, 437);
         this.cmdZADailyReport.Name = "cmdZADailyReport";
         this.cmdZADailyReport.Size = new System.Drawing.Size(194, 45);
         this.cmdZADailyReport.TabIndex = 9;
         this.cmdZADailyReport.Text = "ZA Daily Report Run";
         this.cmdZADailyReport.UseVisualStyleBackColor = true;
         this.cmdZADailyReport.Click += new System.EventHandler(this.cmdZADailyReport_Click);
         // 
         // cmdWA
         // 
         this.cmdWA.Location = new System.Drawing.Point(481, 359);
         this.cmdWA.Name = "cmdWA";
         this.cmdWA.Size = new System.Drawing.Size(157, 45);
         this.cmdWA.TabIndex = 11;
         this.cmdWA.Text = "Weighted Average";
         this.cmdWA.UseVisualStyleBackColor = true;
         this.cmdWA.Click += new System.EventHandler(this.cmdWA_Click);
         // 
         // cmdFullRun
         // 
         this.cmdFullRun.Location = new System.Drawing.Point(290, 359);
         this.cmdFullRun.Name = "cmdFullRun";
         this.cmdFullRun.Size = new System.Drawing.Size(171, 45);
         this.cmdFullRun.TabIndex = 8;
         this.cmdFullRun.Text = "Full Run";
         this.cmdFullRun.UseVisualStyleBackColor = true;
         this.cmdFullRun.Click += new System.EventHandler(this.cmdFullRun_Click);
         // 
         // txtFinYear
         // 
         this.txtFinYear.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.txtFinYear.Location = new System.Drawing.Point(224, 56);
         this.txtFinYear.Name = "txtFinYear";
         this.txtFinYear.Size = new System.Drawing.Size(138, 26);
         this.txtFinYear.TabIndex = 0;
         this.txtFinYear.Text = "0";
         // 
         // lblFinYear
         // 
         this.lblFinYear.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.lblFinYear.Location = new System.Drawing.Point(70, 59);
         this.lblFinYear.Name = "lblFinYear";
         this.lblFinYear.Size = new System.Drawing.Size(138, 25);
         this.lblFinYear.TabIndex = 41;
         this.lblFinYear.Text = "Fin Year";
         // 
         // label1
         // 
         this.label1.AutoSize = true;
         this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label1.Location = new System.Drawing.Point(70, 113);
         this.label1.Name = "label1";
         this.label1.Size = new System.Drawing.Size(227, 20);
         this.label1.TabIndex = 42;
         this.label1.Text = "Month Data Run, Start Month";
         // 
         // txtStartMonth
         // 
         this.txtStartMonth.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.txtStartMonth.Location = new System.Drawing.Point(558, 110);
         this.txtStartMonth.Name = "txtStartMonth";
         this.txtStartMonth.Size = new System.Drawing.Size(138, 26);
         this.txtStartMonth.TabIndex = 1;
         this.txtStartMonth.Text = "0";
         // 
         // txtPrevDays
         // 
         this.txtPrevDays.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.txtPrevDays.Location = new System.Drawing.Point(558, 189);
         this.txtPrevDays.Name = "txtPrevDays";
         this.txtPrevDays.Size = new System.Drawing.Size(138, 26);
         this.txtPrevDays.TabIndex = 2;
         this.txtPrevDays.Text = "0";
         // 
         // label2
         // 
         this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label2.Location = new System.Drawing.Point(70, 189);
         this.label2.Name = "label2";
         this.label2.Size = new System.Drawing.Size(469, 25);
         this.label2.TabIndex = 46;
         this.label2.Text = "Daily /Weekly Run - Number of previous days / weeks to run";
         // 
         // cmdClose
         // 
         this.cmdClose.Location = new System.Drawing.Point(851, 487);
         this.cmdClose.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
         this.cmdClose.Name = "cmdClose";
         this.cmdClose.Size = new System.Drawing.Size(215, 45);
         this.cmdClose.TabIndex = 14;
         this.cmdClose.Text = "Close";
         this.cmdClose.UseVisualStyleBackColor = true;
         this.cmdClose.Click += new System.EventHandler(this.cmdClose_Click);
         // 
         // cboAreas
         // 
         this.cboAreas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
         this.cboAreas.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.cboAreas.FormattingEnabled = true;
         this.cboAreas.Location = new System.Drawing.Point(558, 256);
         this.cboAreas.Name = "cboAreas";
         this.cboAreas.Size = new System.Drawing.Size(542, 26);
         this.cboAreas.TabIndex = 3;
         // 
         // label3
         // 
         this.label3.AutoSize = true;
         this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label3.Location = new System.Drawing.Point(71, 256);
         this.label3.Name = "label3";
         this.label3.Size = new System.Drawing.Size(150, 20);
         this.label3.TabIndex = 50;
         this.label3.Text = "Select Area to Run";
         // 
         // rbRunWeek
         // 
         this.rbRunWeek.AutoSize = true;
         this.rbRunWeek.Location = new System.Drawing.Point(224, 302);
         this.rbRunWeek.Name = "rbRunWeek";
         this.rbRunWeek.Size = new System.Drawing.Size(91, 20);
         this.rbRunWeek.TabIndex = 5;
         this.rbRunWeek.TabStop = true;
         this.rbRunWeek.Text = "Run Week";
         this.rbRunWeek.UseVisualStyleBackColor = true;
         // 
         // rbRunMonth
         // 
         this.rbRunMonth.AutoSize = true;
         this.rbRunMonth.Location = new System.Drawing.Point(81, 302);
         this.rbRunMonth.Name = "rbRunMonth";
         this.rbRunMonth.Size = new System.Drawing.Size(91, 20);
         this.rbRunMonth.TabIndex = 4;
         this.rbRunMonth.TabStop = true;
         this.rbRunMonth.Text = "Run Month";
         this.rbRunMonth.UseVisualStyleBackColor = true;
         // 
         // rbRunDaily
         // 
         this.rbRunDaily.AutoSize = true;
         this.rbRunDaily.Location = new System.Drawing.Point(345, 302);
         this.rbRunDaily.Name = "rbRunDaily";
         this.rbRunDaily.Size = new System.Drawing.Size(86, 20);
         this.rbRunDaily.TabIndex = 6;
         this.rbRunDaily.TabStop = true;
         this.rbRunDaily.Text = "Run Daily";
         this.rbRunDaily.UseMnemonic = false;
         this.rbRunDaily.UseVisualStyleBackColor = true;
         // 
         // cmdStartRun
         // 
         this.cmdStartRun.Location = new System.Drawing.Point(75, 359);
         this.cmdStartRun.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
         this.cmdStartRun.Name = "cmdStartRun";
         this.cmdStartRun.Size = new System.Drawing.Size(193, 45);
         this.cmdStartRun.TabIndex = 7;
         this.cmdStartRun.Text = "Start Selected Run";
         this.cmdStartRun.UseVisualStyleBackColor = true;
         this.cmdStartRun.Click += new System.EventHandler(this.cmdStartRun_Click);
         // 
         // cmdUserHR
         // 
         this.cmdUserHR.Location = new System.Drawing.Point(851, 437);
         this.cmdUserHR.Name = "cmdUserHR";
         this.cmdUserHR.Size = new System.Drawing.Size(215, 45);
         this.cmdUserHR.TabIndex = 12;
         this.cmdUserHR.Text = "Update OEE User HR";
         this.cmdUserHR.UseVisualStyleBackColor = true;
         this.cmdUserHR.Click += new System.EventHandler(this.cmdUserHR_Click);
         // 
         // cmdGlobalReport
         // 
         this.cmdGlobalReport.Location = new System.Drawing.Point(315, 437);
         this.cmdGlobalReport.Name = "cmdGlobalReport";
         this.cmdGlobalReport.Size = new System.Drawing.Size(165, 45);
         this.cmdGlobalReport.TabIndex = 51;
         this.cmdGlobalReport.Text = "Global Report";
         this.cmdGlobalReport.UseVisualStyleBackColor = true;
         this.cmdGlobalReport.Click += new System.EventHandler(this.cmdGlobalReport_Click);
         // 
         // cmdBudgetProduction
         // 
         this.cmdBudgetProduction.Location = new System.Drawing.Point(491, 437);
         this.cmdBudgetProduction.Name = "cmdBudgetProduction";
         this.cmdBudgetProduction.Size = new System.Drawing.Size(147, 45);
         this.cmdBudgetProduction.TabIndex = 52;
         this.cmdBudgetProduction.Text = "BudgetProduction";
         this.cmdBudgetProduction.UseVisualStyleBackColor = true;
         this.cmdBudgetProduction.Click += new System.EventHandler(this.cmdBudgetProduction_Click);
         // 
         // cmdDailyBudget
         // 
         this.cmdDailyBudget.Location = new System.Drawing.Point(662, 359);
         this.cmdDailyBudget.Name = "cmdDailyBudget";
         this.cmdDailyBudget.Size = new System.Drawing.Size(157, 45);
         this.cmdDailyBudget.TabIndex = 53;
         this.cmdDailyBudget.Text = "OEE Daily Budget";
         this.cmdDailyBudget.UseVisualStyleBackColor = true;
         this.cmdDailyBudget.Click += new System.EventHandler(this.cmdDailyBudget_Click);
         // 
         // tmpStartDate
         // 
         this.tmpStartDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.tmpStartDate.Location = new System.Drawing.Point(873, 189);
         this.tmpStartDate.Name = "tmpStartDate";
         this.tmpStartDate.Size = new System.Drawing.Size(193, 26);
         this.tmpStartDate.TabIndex = 54;
         // 
         // label4
         // 
         this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label4.Location = new System.Drawing.Point(869, 161);
         this.label4.Name = "label4";
         this.label4.Size = new System.Drawing.Size(231, 25);
         this.label4.TabIndex = 55;
         this.label4.Text = "Start Date When Testing";
         // 
         // frmMain
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(1149, 641);
         this.Controls.Add(this.label4);
         this.Controls.Add(this.tmpStartDate);
         this.Controls.Add(this.cmdDailyBudget);
         this.Controls.Add(this.cmdBudgetProduction);
         this.Controls.Add(this.cmdGlobalReport);
         this.Controls.Add(this.cmdUserHR);
         this.Controls.Add(this.cmdWA);
         this.Controls.Add(this.cmdGradesProdMix);
         this.Controls.Add(this.cmdTest);
         this.Controls.Add(this.cmdZADailyReport);
         this.Controls.Add(this.cmdStartRun);
         this.Controls.Add(this.cmdFullRun);
         this.Controls.Add(this.rbRunDaily);
         this.Controls.Add(this.rbRunMonth);
         this.Controls.Add(this.rbRunWeek);
         this.Controls.Add(this.label3);
         this.Controls.Add(this.cboAreas);
         this.Controls.Add(this.cmdClose);
         this.Controls.Add(this.label2);
         this.Controls.Add(this.txtPrevDays);
         this.Controls.Add(this.txtStartMonth);
         this.Controls.Add(this.label1);
         this.Controls.Add(this.lblFinYear);
         this.Controls.Add(this.txtFinYear);
         this.Controls.Add(this.lblMsg);
         this.Controls.Add(this.cmdCancel);
         this.Controls.Add(this.menuStrip1);
         this.Controls.Add(this.SBar);
         this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
         this.MaximizeBox = false;
         this.MinimizeBox = false;
         this.Name = "frmMain";
         this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
         this.Text = "Schedules";
         this.SBar.ResumeLayout(false);
         this.SBar.PerformLayout();
         this.menuStrip1.ResumeLayout(false);
         this.menuStrip1.PerformLayout();
         this.ResumeLayout(false);
         this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Timer timer1;
    private System.Windows.Forms.StatusStrip SBar;
    private System.Windows.Forms.ToolStripStatusLabel sbMsg;
    private System.Windows.Forms.ToolStripStatusLabel sbText;
    private System.Windows.Forms.MenuStrip menuStrip1;
    private System.Windows.Forms.ToolStripMenuItem mnuFile;
    private System.Windows.Forms.ToolStripMenuItem mnuExit;
    private System.Windows.Forms.ToolStripMenuItem mnuHelp;
    private System.Windows.Forms.ToolStripMenuItem mnuAbout;
    private System.Windows.Forms.FolderBrowserDialog Fldr;
    private System.Windows.Forms.Button cmdCancel;
    private System.Windows.Forms.Label lblMsg;
    private System.Windows.Forms.Button cmdWA;
    private System.Windows.Forms.Button cmdZADailyReport;
    private System.Windows.Forms.Button cmdGradesProdMix;
    private System.Windows.Forms.TextBox txtFinYear;
    private System.Windows.Forms.Label lblFinYear;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.TextBox txtStartMonth;
    private System.Windows.Forms.TextBox txtPrevDays;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Button cmdFullRun;
    private System.Windows.Forms.Button cmdClose;
    private System.Windows.Forms.Button cmdTest;
    private System.Windows.Forms.ComboBox cboAreas;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.RadioButton rbRunWeek;
    private System.Windows.Forms.RadioButton rbRunMonth;
    private System.Windows.Forms.RadioButton rbRunDaily;
    private System.Windows.Forms.Button cmdStartRun;
    private System.Windows.Forms.Button cmdUserHR;
    private System.Windows.Forms.Button cmdGlobalReport;
    private System.Windows.Forms.Button cmdBudgetProduction;
    private System.Windows.Forms.Button cmdDailyBudget;
      private System.Windows.Forms.TextBox tmpStartDate;
      private System.Windows.Forms.Label label4;
   }
}

