﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Oracle.ManagedDataAccess.Client;



namespace Schedules
{

  class cZADailyMTDReport
  {

    string sReportDate = "";
    string sql = "";

    string sODateFrom = "";
    string sODateTo = "";

    string sOraDateFrom = "";
    string sOraDateTo = "";

    string sDateFrom = "";

    string s90DaysBack = "";
    double numDays = 0;


    cCal cal = new cCal();


    public void RunJob(string runDate)
    {

      sReportDate = cmath.getDateStr(runDate);

      cal = cal ?? new cCal();
      string sStartDate = "";
      string sEndDate = "";


      try
      {

        //calculate the dates and day


        cal.GetSappiMonthDates(sReportDate, ref sStartDate, ref sEndDate);    //month

        DateTime dtF = cmath.getDateTime(sStartDate);
        DateTime dtT = cmath.getDateTime(sReportDate);      //to report date
        TimeSpan ts = dtT.Subtract(dtF);

        numDays = ts.Days + 1;

        sDateFrom = cmath.getDateStr(sStartDate);

        sOraDateFrom = cmath.OraDateSlash(sStartDate);      //  MM/dd/yyyy
        sOraDateTo = cmath.OraDateSlash(sReportDate);       //  MM/dd/yyyy

        sODateFrom = cmath.OraDate(sStartDate);             //from start of fiscal month "to_Date('" + getDateStr_DDMMYYYY(pDateTime) + "'," + "'DD-MM-yyyy')";
        sODateTo = cmath.OraDate(sReportDate);             //to report date  "to_Date('" + getDateStr_DDMMYYYY(pDateTime) + "'," + "'DD-MM-yyyy')";               

        s90DaysBack = cmath.getDateStr(sReportDate, -90);   //90 days back from report date yyyy-mm-dd

        cFG.LogIt("--------------------------------------------------------------");
        cFG.LogIt("Process Date: " + sReportDate);
        cFG.LogIt("--------------------------------------------------------------");

        System.Windows.Forms.Application.DoEvents();


        DoNGX_FL2_MTD();

        DoNGX_GW_MTD();

        DoNGX_WastePlant_MTD();

        DoTUG_NSSC_MTD();

        System.Windows.Forms.Application.DoEvents();

        DoSTA_BH_MTD();

        DoSTA_STO_MTD();

        DoPulpBudgets_MTD();    //stanger

        DoPulpSai_MTD();

        DoPulpNGX_MTD();

        System.Windows.Forms.Application.DoEvents();

        //----daily figures

        DoNGX_FL2_Daily();

        DoNGX_GW_Daily();

        DoNGX_WastePlant_Daily();

        DoTUG_NSSC_Daily();

        DoSTA_BH_Daily();

        DoSTA_STO_Daily();

        System.Windows.Forms.Application.DoEvents();

        DoPulpBudgets_Daily();       //stanger

        DoPulpSai_DAILY();

        DoPulpNGX_DAILY();

        DoPulpSai_WEEKLY();

        DoPulpNGX_WEEKLY();

      //getout:
        //cal = null;

      }
      catch (Exception er)
      {
        //throw er;
        cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
      }
      finally
      {
        cORA.CloseDB();
        cSQL.CloseDB();
        cal = null;
      }

    } //



    private void DoNGX_FL2_MTD()
    {

      //NGX figures for pulp  FL2  flid = 5   2 Dig Softwood Kraft

      //get daily figure from PTS and update ZADailyMTDReport table for the SSRS MTD Report

      //this was the daily figure now converted to MTD  2021-06-29
      //sql = "select sum(nvl(production_figure, 0)) NetProdAct"
      //+ " from  production_figures t"
      //+ " where production_date = " + cmath.OraDate(DateTime.Now.AddDays(-1))       //to_date('27-01-2021', 'DD-MM-yyyy')
      //+ " and plantid = 'FL2'"
      //+ "";

      double dPulpNetProdActNGX5 = 0;
      double dPulpBudget = 0;

      try
      {

        cORA.OpenDB("SAPXMIINGX2");

        cORA.cmd = cORA.con.CreateCommand();
        cORA.cmd.CommandText = "sapxmii.api_manufacturing_efficiency_model.budget_and_actual_production_of";
        cORA.cmd.CommandType = CommandType.StoredProcedure;

        OracleCommandBuilder.DeriveParameters(cORA.cmd);
        cORA.cmd.BindByName = true;
        cORA.cmd.Parameters["I_PLANTID"].Value = "DG2";
        cORA.cmd.Parameters["i_date_from"].Value = sOraDateFrom;      //MM/dd/2021
        cORA.cmd.Parameters["i_date_to"].Value = sOraDateTo;
        //cORA.cmd.Parameters["I_DATE_FROM"].Value = "06/28/2021";
        //cORA.cmd.Parameters["I_DATE_TO"].Value = "07/07/2021";

        DataTable dt = cORA.ExecuteQuerySp(cORA.cmd);

        cORA.CloseDB();

        cFG.LogIt("Records found NGX(FL2) FLID(5) : " + dt.Rows.Count);

        cSQL.OpenDB("ZAMDW");

        foreach (DataRow item in dt.Rows)
        {

          dPulpBudget = numDays * cmath.getd(item["Budget"].ToString());
          dPulpNetProdActNGX5 = cmath.getd(item["Actual_Production"].ToString());

          sql = "UPDATE ZADailyMTDReport set LastUpdate = '" + cmath.getDateTimeNowStr() + "'"
          + " , PulpNetProdBgtMTD = " + dPulpBudget
          + " , PulpNetProdActMTD = " + dPulpNetProdActNGX5
          + " Where ReportDate = '" + sReportDate + "'"
          + "       And MillKey = 1"
          + "       And PlantCode = 'FL2'"
          + " IF @@ROWCOUNT=0"
          + " INSERT INTO ZADailyMTDReport (ReportDate, MillKey, PlantCode , PulpNetProdBgtMTD, PulpNetProdActMTD, LastUpdate )"
          + " VALUES ("
          + "'" + sReportDate + "'"                   //reportdate
          + "," + 1                                   //millkey
          + ",'" + "FL2" + "'"                        //plantcode
          + "," + dPulpBudget                         //PulpNetProdBgtMTD
          + "," + dPulpNetProdActNGX5                 //PulpNetProdActMTD
          + ",'" + cmath.getDateTimeNowStr() + "'"        //lastUpdated
          + " )";


          int rv = cSQL.ExecuteQuery(sql);


        } //foreach


      }
      catch (Exception er)
      {
        //throw er;
        cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
      }
      finally
      {
        cORA.CloseDB();
        cSQL.CloseDB();
      }

    }//


    private void DoNGX_GW_MTD()
    {

      //NGX figures for pulp   flid = 158   Groundwood

      double dPulpNetProdAct = 0;
      double dPulpBudget = 0;

      try
      {

        cORA.OpenDB("SAPXMIINGX2");

        cORA.cmd = cORA.con.CreateCommand();
        cORA.cmd.CommandText = "sapxmii.api_manufacturing_efficiency_model.budget_and_actual_production_of";
        cORA.cmd.CommandType = CommandType.StoredProcedure;

        OracleCommandBuilder.DeriveParameters(cORA.cmd);
        cORA.cmd.BindByName = true;
        cORA.cmd.Parameters["I_PLANTID"].Value = "GW";
        cORA.cmd.Parameters["i_date_from"].Value = sOraDateFrom;      //MM/dd/2021
        cORA.cmd.Parameters["i_date_to"].Value = sOraDateTo;

        DataTable dt = cORA.ExecuteQuerySp(cORA.cmd);

        cORA.CloseDB();

        cFG.LogIt("Records found NGX(GW) FLID(158) : " + dt.Rows.Count);

        cSQL.OpenDB("ZAMDW");

        foreach (DataRow item in dt.Rows)
        {

          dPulpBudget = numDays * cmath.getd(item["Budget"].ToString());
          dPulpNetProdAct = cmath.getd(item["Actual_Production"].ToString());

          sql = "UPDATE ZADailyMTDReport set LastUpdate = '" + cmath.getDateTimeNowStr() + "'"
          + " , PulpNetProdBgtMTD = " + dPulpBudget
          + " , PulpNetProdActMTD = " + dPulpNetProdAct
          + " Where ReportDate = '" + sReportDate + "'"
          + "       And MillKey = 1"
          + "       And PlantCode = 'GW'"
          + " IF @@ROWCOUNT=0"
          + " INSERT INTO ZADailyMTDReport (ReportDate, MillKey, PlantCode, PulpNetProdBgtMTD, PulpNetProdActMTD, LastUpdate )"
          + " VALUES ("
          + "'" + sReportDate + "'"                   //reportdate
          + "," + 1                                   //millkey
          + ",'" + "GW" + "'"                         //plantcode
          + "," + dPulpBudget                         //PulpNetProdBgt
          + "," + dPulpNetProdAct                     //PulpNetProdAct
          + ",'" + cmath.getDateTimeNowStr() + "'"        //lastUpdated
          + " )";


          int rv = cSQL.ExecuteQuery(sql);

        } //foreach


      }
      catch (Exception er)
      {
        // throw er;
        cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
      }
      finally
      {
        cORA.CloseDB();
        cSQL.CloseDB();
      }

    }//



    private void DoNGX_WastePlant_MTD()
    {

      //NGX figures for pulp   flid = 23   Waste Plant  KN/PAP-044

      double dPulpNetProdAct = 0;
      double dPulpBudget = 0;

      try
      {

        cORA.OpenDB("SAPXMIINGX2");

        cORA.cmd = cORA.con.CreateCommand();
        cORA.cmd.CommandText = "sapxmii.api_manufacturing_efficiency_model.budget_and_actual_production_of";
        cORA.cmd.CommandType = CommandType.StoredProcedure;

        OracleCommandBuilder.DeriveParameters(cORA.cmd);
        cORA.cmd.BindByName = true;
        cORA.cmd.Parameters["I_PLANTID"].Value = "W";
        cORA.cmd.Parameters["i_date_from"].Value = sOraDateFrom;      //MM/dd/2021
        cORA.cmd.Parameters["i_date_to"].Value = sOraDateTo;

        DataTable dt = cORA.ExecuteQuerySp(cORA.cmd);

        cORA.CloseDB();

        cFG.LogIt("Records found NGX(W) FLID(23) : " + dt.Rows.Count);

        cSQL.OpenDB("ZAMDW");

        foreach (DataRow item in dt.Rows)
        {

          dPulpBudget = numDays * cmath.getd(item["Budget"].ToString());
          dPulpNetProdAct = cmath.getd(item["Actual_Production"].ToString());

          sql = "UPDATE ZADailyMTDReport set LastUpdate = '" + cmath.getDateTimeNowStr() + "'"
          + " , PulpNetProdBgtWasteMTD = " + dPulpBudget
          + " , PulpNetProdActWasteMTD = " + dPulpNetProdAct
          + " Where ReportDate = '" + sReportDate + "'"
          + "       And MillKey = 1"
          + "       And PlantCode = 'W'"
          + " IF @@ROWCOUNT=0"
          + " INSERT INTO ZADailyMTDReport (ReportDate, MillKey, PlantCode, PulpNetProdBgtWasteMTD, PulpNetProdActWasteMTD, LastUpdate )"
          + " VALUES ("
          + "'" + sReportDate + "'"                   //reportdate
          + "," + 1                                   //millkey
          + ",'" + "W" + "'"                          //plantcode
          + "," + dPulpBudget                         //PulpNetProdBgtWaste
          + "," + dPulpNetProdAct                     //PulpNetProdActWaste
          + ",'" + cmath.getDateTimeNowStr() + "'"        //lastUpdated
          + " )";


          int rv = cSQL.ExecuteQuery(sql);

        } //foreach


      }
      catch (Exception er)
      {
        cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
      }
      finally
      {
        cORA.CloseDB();
        cSQL.CloseDB();
      }

    }//


    private void DoTUG_NSSC_MTD()
    {
      //TUG figures for pulp  NSSC  flid = 126

      double dPulpNetProdAct = 0;
      double dBudget = 0;

      double dPulpNetProdActWaste = 0;
      double dBudgetWaste = 0;

      try
      {

        sql = "select sum(NSSC)NSSC, sum(Waste_Prod) Waste "
          + " from   Daily_Production_Figures"
          + " where    trunc(Date_production) >= " + sODateFrom
          + " and trunc(Date_production) <= " + sODateTo;


        cORA.OpenDB("SAPXMII");
        DataTable dt = cORA.GetDataTable(sql);
        cORA.CloseDB();

        cFG.LogIt("Records found TUG(NSSC) FLID(126) : " + dt.Rows.Count);

        cSQL.OpenDB("ZAMDW");

        foreach (DataRow item in dt.Rows)
        {
          dPulpNetProdAct = cmath.getd(item["NSSC"].ToString());
          dPulpNetProdActWaste = cmath.getd(item["Waste"].ToString());
        }


        sql = "Select cal_year, month, month_name, mnth_begin, mnth_end, nssc_net_tons_pd, refibre_prod_pd"
          + " from      budget_production"
          + " where " + sODateTo + " between mnth_begin and mnth_end"
          + "";

        cORA.OpenDB("SAPPITUG");
        dt = cORA.GetDataTable(sql);
        cORA.CloseDB();

        foreach (DataRow item in dt.Rows)
        {
          dBudget = numDays * cmath.getd(item["nssc_net_tons_pd"].ToString());
          dBudgetWaste =  numDays *  cmath.getd(item["refibre_prod_pd"].ToString());
        }


        sql = "UPDATE ZADailyMTDReport set LastUpdate = '" + cmath.getDateTimeNowStr() + "'"
        + " , PulpNetProdBgtMTD = " + dBudget        
        + " , PulpNetProdBgtWasteMTD = " + dBudgetWaste
        + " , PulpNetProdActMTD = " + dPulpNetProdAct
        + " , PulpNetProdActWasteMTD = " + dPulpNetProdActWaste
        + " Where ReportDate = '" + sReportDate + "'"
        + "       And MillKey = 3"
        + "       And PlantCode = 'NSSC'"
        + " IF @@ROWCOUNT=0"
        + " INSERT INTO ZADailyMTDReport (ReportDate, MillKey, PlantCode, PulpNetProdBgtMTD, PulpNetProdBgtWasteMTD, PulpNetProdActMTD, PulpNetProdActWasteMTD,  LastUpdate)"
        + " VALUES ("
        + "'" + sReportDate + "'"                   //reportdate
        + "," + 3                                   //millkey
        + ",'" + "NSSC" + "'"                       //plantcode
        + "," + dBudget                             //PulpNetProdBgt
        + "," + dBudgetWaste                        //PulpNetProdBgtWaste
        + "," + dPulpNetProdAct                     //PulpNetProdAct
        + "," + dPulpNetProdActWaste                //PulpNetProdActWaste
        + ",'" + cmath.getDateTimeNowStr() + "'"        //lastUpdated
        + " )";



        int rv = cSQL.ExecuteQuery(sql);

        

      }
      catch (Exception er)
      {
        cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
      }
      finally
      {
        cORA.CloseDB();
        cSQL.CloseDB();
      }

    }//



    private void DoSTA_BH_MTD()
    {
      //STA figures for pulp  BH  flid = 3
      //PRODUCTION
      //daily figure from PTS update ZADailyMTDReport table for the SSRS MTD Report

      double dPulpNetProdAct = 0;

      try
      {
        //2021-06-29  was daily now MTD see below
        //sql = " select dp.machine_code"
        //+ " ,report_date"
        //+ " ,nvl(machine_gross, 0) as Gross"
        //+ " ,nvl(machine_gross, 0) * 1000 as Gross_Ext"
        //+ " ,nvl(output, 0) as NetProdAct"
        //+ " ,nvl(output, 0) * 1000 as Net_ext"
        //+ " ,nvl(shrinkage, 0) as Shrink"
        //+ " ,(nvl(machine_gross, 0) - nvl(output, 0)) * 1000 CombLoss"
        //+ " from daily_production dp"
        //+ " where trunc(report_date) = " + cmath.OraDate(DateTime.Now.AddDays(-1))
        //+ " and prod_type = 'D'"
        //+ " and dp.machine_code = 'BH'"
        //+ "";


        // 2021-06-29  this is  MTD
        sql = "select dp.machine_code"
        + " , sum(nvl(output, 0)) as NetProdAct"
        + " from daily_production dp"
        + " where trunc(report_date) >= " + sODateFrom
        + " and trunc(report_date) <= " + sODateTo
        + " and prod_type = 'D'"
        + " and dp.machine_code = 'BH'"
        + " group by dp.machine_code"
        + " ";


        cORA.OpenDB("SPIS");
        DataTable dt = cORA.GetDataTable(sql);
        cORA.CloseDB();

        cFG.LogIt("Records found STA(BH) FLID(1) : " + dt.Rows.Count);

        cSQL.OpenDB("ZAMDW");

        foreach (DataRow item in dt.Rows)
        {

          dPulpNetProdAct = cmath.getd(item["NetProdAct"].ToString());

          sql = "UPDATE ZADailyMTDReport set LastUpdate = '" + cmath.getDateTimeNowStr() + "'"
          + " , PulpNetProdActMTD = " + dPulpNetProdAct
          + " Where ReportDate = '" + sReportDate + "'"
          + "       And MillKey = 2"
          + "       And PlantCode = 'BH'"
          + " IF @@ROWCOUNT=0"
          + " INSERT INTO ZADailyMTDReport (ReportDate, MillKey, PlantCode, PulpNetProdActMTD, LastUpdate)"
          + " VALUES ("
          + "'" + sReportDate + "'"                 //reportdate
          + "," + 2                                 //millkey
          + ",'" + "BH" + "'"                       //plantcode
          + "," + dPulpNetProdAct                   //PulpNetProdAct
          + ",'" + cmath.getDateTimeNowStr() + "'"      //lastUpdated
          + " )";

          int rv = cSQL.ExecuteQuery(sql);

        } //foreach

      }
      catch (Exception er)
      {
        cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
      }
      finally
      {
        cORA.CloseDB();
        cSQL.CloseDB();
      }

    }//



    private void DoSTA_STO_MTD()
    {

      //STA figures for 'STO' Stanger Stores Issues PS/SER-STO  (not in OEE)

      double dProd = 0;


      string sStartDate = sDateFrom.Replace("-", "");

      string sEndDate = sReportDate.Replace("-", "");

      try
      {

        sql = "select sum(issues) Issues from tbl_material_levels"
          + " where from_date between '" + sStartDate + "'" + " and " + "'" + sEndDate + "'"
          + "";

        cORA.OpenDB("RTC");
        DataTable dt = cORA.GetDataTable(sql);
        cORA.CloseDB();

        cFG.LogIt("Records found STA(STO) FLID(0) : " + dt.Rows.Count);

        cSQL.OpenDB("ZAMDW");

        foreach (DataRow item in dt.Rows)
        {

          dProd = cmath.getd(item["Issues"].ToString());

          sql = "UPDATE ZADailyMTDReport set LastUpdate = '" + cmath.getDateTimeNowStr() + "'"
          + " , PulpNetProdActWasteMTD = " + dProd
          + " Where ReportDate = '" + sReportDate + "'"
          + "       And MillKey = 2"
          + "       And PlantCode = 'STO'"
          + " IF @@ROWCOUNT=0"
          + " INSERT INTO ZADailyMTDReport (ReportDate, MillKey, PlantCode, PulpNetProdActWasteMTD, LastUpdate)"
          + " VALUES ("
          + "'" + sReportDate + "'"                   //reportdate
          + "," + 2                                   //millkey
          + ",'" + "STO" + "'"                        //plantcode
          + "," + dProd                               //[PulpNetProdActWasteDaily]
          + ",'" + cmath.getDateTimeNowStr() + "'"        //lastUpdated
          + " )";

          int rv = cSQL.ExecuteQuery(sql);

        } //foreach

      }
      catch (Exception er)
      {
        cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
      }
      finally
      {
        cORA.CloseDB();
        cSQL.CloseDB();
      }

    }//



    private void DoPulpBudgets_MTD()
    {
      //--------------------
      //pulp budgets from mdw's FactProdBudgetMonth,  this is temporary until pulp arrives on MEM


      double dPulpNetProdBgt = 0D;


      try
      {
        sql = "SELECT MonthBpKey, SappiMonthKey , PlantKey, (BpNetTons / sm.month_days) NetProdBgt "
        + " from FactProdBudgetMonth  bm join SappiFiscalCalendar.dbo.tbl_months AS sm ON bm.SappiMonthKey = sm.month_id"
        + " where plantkey = 8 "
        + " and '" + sReportDate + "' between sm.month_start and sm.month_end"
        + "";

        //sql = "select k.id, k.sappidayid, sd.day_start, CAST( sd.day_start as date), k.millcode, k.flid, k.kpivalue"
        //    + " from   kpidaily k , kpidefinition kd 	, sappifiscalcalendar.dbo.tbl_days sd"
        //    + " where	k.kpicode = kd.kpicode"
        //    + " and	k.sappidayid = sd.day_id"
        //    + " and CAST( sd.day_start as date) = '" + sReportDate + "'"
        //    + " and	k.millcode = 'STA'"
        //    + " and   k.flid = 1"           // bleach plant
        //    + " and   k.kpicode in('NSPPM')"
        //    + "";

        cSQL.OpenDB("ZAMDW");

        DataTable dt = cSQL.GetDataTable(sql);

        cFG.LogIt("Records found Pulp Budgets  : " + dt.Rows.Count);

        foreach (DataRow item in dt.Rows)
        {

          dPulpNetProdBgt = numDays * cmath.getd(item["NetProdBgt"].ToString());

          if (item["PlantKey"].ToString() == "8")    // stanger
          {
            sql = "UPDATE ZADailyMTDReport set LastUpdate = '" + cmath.getDateTimeNowStr() + "'"
            + " , PulpNetProdBgtMTD = " + dPulpNetProdBgt
            + " Where ReportDate = '" + sReportDate + "'"
            + "       And MillKey = 2"
            + "       And PlantCode = 'BH'"
            + " IF @@ROWCOUNT=0"
            + " INSERT INTO ZADailyMTDReport (ReportDate, MillKey, PlantCode, PulpNetProdBgtMTD, LastUpdate)"
            + " VALUES ("
            + "'" + sReportDate + "'"                 //reportdate
            + "," + 2                                 //millkey
            + ",'" + "BH" + "'"                       //plantcode
            + "," + dPulpNetProdBgt                   //PulpNetProdBgt
            + ",'" + cmath.getDateTimeNowStr() + "'"      //lastUpdated
            + " )";

            int rv = cSQL.ExecuteQuery(sql);
          }


          //if (item["PlantKey"].ToString() == "10")    // tugela
          //{
          //  sql = "UPDATE ZADailyMTDReport set LastUpdate = '" + cmath.getDateNowStr() + "'"
          //  + " , PulpNetProdBgt = " + dPulpNetProdBgt
          //  + " Where ReportDate = '" + sReportDate + "'"
          //  + "       And MillKey = 3"
          //  + "       And PlantCode = 'NSSC'"
          //  //+ "       And FLID = 126"
          //  + " IF @@ROWCOUNT=0"
          //  + " INSERT INTO ZADailyMTDReport (ReportDate, MillKey, PlantCode, Machine"
          //  + "     ,ProdDays, PulpNetProdBgt, PulpNetProdAct, PulpMTDDeviation,LastUpdate)"
          //  + " VALUES ("
          //  + "'" + sReportDate + "'"                 //reportdate
          //  + "," + 3                                 //millkey
          //  + ",'" + "NSSC" + "'"                     //plantcode
          //                                            // + "," + 126                               //flid
          //  + ",'" + "TUG NSSC" + "'"                 //Machine
          //  + "," + 0                                 //ProdDays
          //  + "," + dPulpNetProdBgt                   //PulpNetProdBgt
          //  + "," + 0                                 //PulpNetProdAct
          //  + "," + 0                                 //PulpMTDDeviation
          //  + ",'" + cmath.getDateNowStr() + "'"      //lastUpdated
          //  + " )";

          //  int rv = cSQL.ExecuteQuery(sql);
          //}


        }

      }
      catch (Exception er)
      {
        cFG.LogIt("DoPulpBudgets_MTD " + er.Message);
      }
      finally
      {
        cSQL.CloseDB();
      }

    } //



    private void DoPulpSai_MTD()
    {
      //------------------------------------------------------------------------------
      //pulp COOKS calculations  SAICCOR
      //Step 1. get the 3 month total (  90 days ) for C3, C4 and C5 plants
      //Total Mill Actual Production from MEM (MEM KPI = GP)  from the Daily table 
      //------------------------------------------------------------------------------

      double dGP = 0;
      double dTotalCooks3M = 0;
      double dTotalCooksMTD = 0;
      double dDigProdTons = 0;
      double d83TotSai = 0;

      try
      {

        //the GP total for the last 90 days
        sql = " select sum(k.kpivalue) as total from kpidaily k"
          + "     ,sappifiscalcalendar.dbo.tbl_days sd"
          + "     ,sappifiscalcalendar.dbo.tbl_months sm"
          + "     ,sappifiscalcalendar.dbo.tbl_years sy"
          + "   where   sd.month_id = sm.month_id"
          + "    and k.SappiDayID = sd.day_id"
          + "    and sm.year_id = sy.year_id"
          + "    and MILLCODE = 'SAI'"
          + "    and kpicode = 'KPI0035'"
          + "    and k.FLID in ('4','5','6')"
          + "    and cast(sd.day_start as date) >= '" + s90DaysBack + "'"
          + "    and cast(sd.day_end as date) <= '" + sReportDate + "'"
          + "";

        cSQL.OpenDB("ZAMDW");
        DataTable dt = cSQL.GetDataTable(sql);

        foreach (DataRow item in dt.Rows)
        {
          dGP = cmath.getd(item["total"].ToString());
        }


        //get the MTD total KPI0083 for Saiccor for this month ( pulp budget )
        sql = "SELECT sum(KPI_Value) total"
        + " FROM  KPIMonthly K  , KPIDefinition KD   , sappifiscalcalendar.dbo.tbl_months sm"
        + " WHERE  K.KPI_Code = KD.KpiCode"
        + "     and  K.SAPPIMONTH_ID = sm.month_id"
        + "     and '" + sReportDate + "'  between sm.month_start and sm.month_end"
        + "     And KD.KpiCode = 'KPI0083'"
        + "     And K.MILL_CODE = 'SAI'"
        + "    and k.FLID in ('4','5','6')"
        + "";

        dt = cSQL.GetDataTable(sql);

        foreach (DataRow item in dt.Rows)
        {
          d83TotSai = cmath.getd(item["total"].ToString());
        }
     

        cSQL.CloseDB();


        //------------------------------------------------------------------------------
        //step 2. get the cooks totals from SAICCOR storec proc 2021-07-01
        //--SelectedDate_TotalCooks, MTD_TotalCooks, MTD_AvgCooks,  MTD_TotalDays, 3M_TotalCooks, 3M_AvgCooks, 3M_TotalDays,
        //-- 80                           236              78             3                4938       54            90
        //------------------------------------------------------------------------------


        sql = "EXECUTE [sts].[Sp_Digester_Saiccor_Cooks] '" + sReportDate + "'," + "'All'";

        cSQL.OpenDB("SAIMES");
        dt = cSQL.GetDataTable(sql);
        cSQL.CloseDB();

        cFG.LogIt("Records found Cooks Saiccor  : " + dt.Rows.Count);

        foreach (DataRow item in dt.Rows)
        {
          dTotalCooks3M = cmath.getd(item["3M_TotalCooks"].ToString());
          dTotalCooksMTD = cmath.getd(item["MTD_TotalCooks"].ToString());
        }

        
        //do some calcs
        dDigProdTons = cmath.div(dGP, dTotalCooks3M);
        dDigProdTons = dDigProdTons * dTotalCooksMTD;

        //add-update the ZADailyMTDReport
        //I have created a new plant code "DIG"  for all digestors at Saiccor
        sql = "UPDATE ZADailyMTDReport set LastUpdate = '" + cmath.getDateTimeNowStr() + "'"
         + " , PulpNetProdBgtMTD = " + d83TotSai
         + " , PulpNetProdActMTD = " + dDigProdTons
         + " Where ReportDate = '" + sReportDate + "'"
         + "       And MillKey = 4"          //saiccor
         + "       And PlantCode = 'DIG'"    //DIG (All)
         + " IF @@ROWCOUNT=0"
         + " INSERT INTO ZADailyMTDReport (ReportDate, MillKey, PlantCode, PulpNetProdBgtMTD, PulpNetProdActMTD,LastUpdate)"
         + " VALUES ("
         + "'" + sReportDate + "'"                   //reportdate
         + "," + 4                                   //millkey
         + ",'" + "DIG" + "'"                        //plantcode
         + "," + d83TotSai                           //PulpNetProdBgtMTD
         + "," + dDigProdTons                        //PulpNetProdActMTD
         + ",'" + cmath.getDateTimeNowStr() + "'"        //lastUpdated
         + " )";


        cSQL.OpenDB("ZAMDW");
        int rv = cSQL.ExecuteQuery(sql);

      }
      catch (Exception er)
      {
        cFG.LogIt("DoPulpSAI_MTD " + er.Message);
      }
      finally
      {
        cSQL.CloseDB();
      }

    }//



    private void DoPulpNGX_MTD()
    {
      try
      {

        //------------------------------------------------------------------------------
        //pulp COOKS calculations  NGODWANA  FL3  (161)
        //use figures from UT3 for the dGP  and kpi0083
        //pulp actual from NGX  2021-07-01
        //--SelectedDate_TotalCooks, MTD_TotalCooks, MTD_AvgCooks,  MTD_TotalDays, 3M_TotalCooks, 3M_AvgCooks, 3M_TotalDays,
        //------------------------------------------------------------------------------

        double dGP = 0;
        double dTotalCooks3M = 0;
        double dTotalCooksMTD = 0;
        double dDigProdTons = 0;
        double d83TotUT3 = 0;

        //the GP total
        sql = " select sum(k.kpivalue) as total from kpidaily k"
          + "     ,sappifiscalcalendar.dbo.tbl_days sd"
          + "     ,sappifiscalcalendar.dbo.tbl_months sm"
          + "     ,sappifiscalcalendar.dbo.tbl_years sy"
          + "   where   sd.month_id = sm.month_id"
          + "    and k.SappiDayID = sd.day_id"
          + "    and sm.year_id = sy.year_id"
          + "    and MILLCODE = 'NGX'"
          + "    and FLID = 13 "
          + "    and kpicode = 'KPI0035'"
          + "    and cast(sd.day_start as date) >= '" + s90DaysBack + "'"
          + "    and cast(sd.day_end as date) <= '" + sReportDate + "'"
          + "";

        cSQL.OpenDB("ZAMDW");
        DataTable dt = cSQL.GetDataTable(sql);

        foreach (DataRow item in dt.Rows)
        {
          dGP = cmath.getd(item["total"].ToString());
        }


        //get the MTD total KPI0083 for NGX UT3 for this month ( pulp budget )
        sql = "SELECT sum(KPI_Value) total"
        + " FROM  KPIMonthly K, KPIDefinition KD, sappifiscalcalendar.dbo.tbl_months sm"
        + " WHERE  K.KPI_Code = KD.KpiCode"
        + "    And  K.SAPPIMONTH_ID = sm.month_id"
        + "    And '" + sReportDate + "'  between sm.month_start and sm.month_end"
        + "    And KD.KpiCode = 'KPI0083'"
        + "    And K.MILL_CODE = 'NGX'"
        + "    And FLID = 13 "
        + "";

        dt = cSQL.GetDataTable(sql);
        cSQL.CloseDB();

        foreach (DataRow item in dt.Rows)
        {
          d83TotUT3 = cmath.getd(item["total"].ToString());
        }


        dDigProdTons = 0;
        dDigProdTons = 0;
        dTotalCooks3M = 0;
        dTotalCooksMTD = 0;

        sql = "EXECUTE [sts].[Sp_Digester_Ngx_Cooks] '" + sReportDate + "'";

        cSQL.OpenDB("NGXMES");
        dt = cSQL.GetDataTable(sql);
        cSQL.CloseDB();

        cFG.LogIt("Records found Cooks Ngodwana  : " + dt.Rows.Count);

        foreach (DataRow item in dt.Rows)
        {
          dTotalCooks3M = cmath.getd(item["3M_TotalCooks"].ToString());
          dTotalCooksMTD = cmath.getd(item["MTD_TotalCooks"].ToString());
        }

        //the d35TotUT3  is the KPI0035 from UT3
        dDigProdTons = cmath.div(dGP, dTotalCooks3M);
        dDigProdTons = dDigProdTons * dTotalCooksMTD;

        //add-update the ZADailyMTDReport
        sql = "UPDATE ZADailyMTDReport set LastUpdate = '" + cmath.getDateTimeNowStr() + "'"
         + " , PulpNetProdBgtMTD = " + d83TotUT3               // update the budget from the UT3's KPI0083
         + " , PulpNetProdActMTD = " + dDigProdTons
         + " Where ReportDate = '" + sReportDate + "'"
         + "       And MillKey = 1"          //ngodwana
         + "       And PlantCode = 'FL3'"    //Fl3
         + " IF @@ROWCOUNT=0"
         + " INSERT INTO ZADailyMTDReport (ReportDate, MillKey, PlantCode, PulpNetProdBgtMTD, PulpNetProdActMTD, LastUpdate)"
         + " VALUES ("
         + "'" + sReportDate + "'"                   //reportdate
         + "," + 1                                   //millkey
         + ",'" + "FL3" + "'"                        //plantcode
         + "," + d83TotUT3                           //PulpNetProdBgtMTD
         + "," + dDigProdTons                        //PulpNetProdActMTD
         + ",'" + cmath.getDateTimeNowStr() + "'"        //lastUpdated
         + " )";


        cSQL.OpenDB("ZAMDW");

        int rv = cSQL.ExecuteQuery(sql);

      }
      catch (Exception er)
      {
        cFG.LogIt("DoPulpNGX_MTD " + er.Message);
      }
      finally
      {
        cSQL.CloseDB();
      }

    }//




    //-------------------------------------------------------------------------
    //DAILY
    //-------------------------------------------------------------------------


    private void DoNGX_FL2_Daily()
    {

      double dPulpNetProdActNGX5 = 0;
      double dPulpBudget = 0;

      try
      {


        //sql = "select sum(nvl(production_figure, 0)) NetProdAct"
        //+ " from  production_figures t"
        //+ " where production_date = "  + cmath.OraDate(sReportDate)
        //+ " and plantid = 'FL2'"
        //+ "";

        cORA.OpenDB("SAPXMIINGX2");

        cORA.cmd = cORA.con.CreateCommand();
        cORA.cmd.CommandText = "sapxmii.api_manufacturing_efficiency_model.budget_and_actual_production_of";
        cORA.cmd.CommandType = CommandType.StoredProcedure;

        OracleCommandBuilder.DeriveParameters(cORA.cmd);
        cORA.cmd.BindByName = true;
        cORA.cmd.Parameters["i_plantid"].Value = "DG2";
        cORA.cmd.Parameters["i_date_from"].Value = sOraDateTo;      //MM/dd/2021
        cORA.cmd.Parameters["i_date_to"].Value = sOraDateTo;

        DataTable dt = cORA.ExecuteQuerySp(cORA.cmd);

        cORA.CloseDB();

        cFG.LogIt("Records found NGX(FL2) FLID(5) : " + dt.Rows.Count);

        cSQL.OpenDB("ZAMDW");

        foreach (DataRow item in dt.Rows)
        {

          dPulpBudget = cmath.getd(item["Budget"].ToString());
          dPulpNetProdActNGX5 = cmath.getd(item["Actual_Production"].ToString());


          sql = "UPDATE ZADailyMTDReport set LastUpdate = '" + cmath.getDateTimeNowStr() + "'"
          + " , PulpNetProdBgtDaily = " + dPulpBudget
          + " , PulpNetProdActDaily = " + dPulpNetProdActNGX5
          + " Where ReportDate = '" + sReportDate + "'"
          + "       And MillKey = 1"
          + "       And PlantCode = 'FL2'"
          + " IF @@ROWCOUNT=0"
          + " INSERT INTO ZADailyMTDReport (ReportDate, MillKey, PlantCode, PulpNetProdBgtDaily, PulpNetProdActDaily, LastUpdate )"
          + " VALUES ("
          + "'" + sReportDate + "'"                   //reportdate
          + "," + 1                                   //millkey
          + ",'" + "FL2" + "'"                        //plantcode
          + "," + dPulpBudget                         //PulpNetProdBgtDaily
          + "," + dPulpNetProdActNGX5                 //PulpNetProdActDaily
          + ",'" + cmath.getDateTimeNowStr() + "'"        //lastUpdated
          + " )";

          int rv = cSQL.ExecuteQuery(sql);

        } //foreach


      }
      catch (Exception er)
      {
        cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
      }
      finally
      {
        cORA.CloseDB();
        cSQL.CloseDB();
      }

    }//


    private void DoNGX_GW_Daily()
    {

      //NGX figures for pulp   flid = 158   Groundwood

      double dPulpNetProdAct = 0;
      double dPulpBudget = 0;

      try
      {

        cORA.OpenDB("SAPXMIINGX2");

        cORA.cmd = cORA.con.CreateCommand();
        cORA.cmd.CommandText = "sapxmii.api_manufacturing_efficiency_model.budget_and_actual_production_of";
        cORA.cmd.CommandType = CommandType.StoredProcedure;

        OracleCommandBuilder.DeriveParameters(cORA.cmd);
        cORA.cmd.BindByName = true;
        cORA.cmd.Parameters["I_PLANTID"].Value = "GW";
        cORA.cmd.Parameters["i_date_from"].Value = sOraDateTo;
        cORA.cmd.Parameters["i_date_to"].Value = sOraDateTo;

        DataTable dt = cORA.ExecuteQuerySp(cORA.cmd);

        cORA.CloseDB();

        cFG.LogIt("Records found NGX(GW) FLID(158) : " + dt.Rows.Count);

        cSQL.OpenDB("ZAMDW");

        foreach (DataRow item in dt.Rows)
        {

          dPulpBudget = cmath.getd(item["Budget"].ToString());
          dPulpNetProdAct = cmath.getd(item["Actual_Production"].ToString());

          sql = "UPDATE ZADailyMTDReport set LastUpdate = '" + cmath.getDateTimeNowStr() + "'"
          + " , PulpNetProdBgtDaily = " + dPulpBudget
          + " , PulpNetProdActDaily = " + dPulpNetProdAct
          + " Where ReportDate = '" + sReportDate + "'"
          + "       And MillKey = 1"
          + "       And PlantCode = 'GW'"
          + " IF @@ROWCOUNT=0"
          + " INSERT INTO ZADailyMTDReport (ReportDate, MillKey, PlantCode, PulpNetProdBgtDaily, PulpNetProdActDaily, LastUpdate )"
          + " VALUES ("
          + "'" + sReportDate + "'"                   //reportdate
          + "," + 1                                   //millkey
          + ",'" + "GW" + "'"                         //plantcode
          + "," + dPulpBudget                         //PulpNetProdBgt
          + "," + dPulpNetProdAct                     //PulpNetProdAct
          + ",'" + cmath.getDateTimeNowStr() + "'"        //lastUpdated
          + " )";


          int rv = cSQL.ExecuteQuery(sql);

        } //foreach


      }
      catch (Exception er)
      {
        cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
      }
      finally
      {
        cORA.CloseDB();
        cSQL.CloseDB();
      }

    }//


    private void DoNGX_WastePlant_Daily()
    {

      //NGX figures for pulp   flid = 23   Waste Plant  KN/PAP-044

      double dPulpNetProdAct = 0;
      double dPulpBudget = 0;

      try
      {

        cORA.OpenDB("SAPXMIINGX2");

        cORA.cmd = cORA.con.CreateCommand();
        cORA.cmd.CommandText = "sapxmii.api_manufacturing_efficiency_model.budget_and_actual_production_of";
        cORA.cmd.CommandType = CommandType.StoredProcedure;

        OracleCommandBuilder.DeriveParameters(cORA.cmd);
        cORA.cmd.BindByName = true;
        cORA.cmd.Parameters["I_PLANTID"].Value = "W";
        cORA.cmd.Parameters["i_date_from"].Value = sOraDateTo;      //MM/dd/2021
        cORA.cmd.Parameters["i_date_to"].Value = sOraDateTo;

        DataTable dt = cORA.ExecuteQuerySp(cORA.cmd);

        cORA.CloseDB();

        cFG.LogIt("Records found NGX(W) FLID(23) : " + dt.Rows.Count);

        cSQL.OpenDB("ZAMDW");

        foreach (DataRow item in dt.Rows)
        {

          dPulpBudget = cmath.getd(item["Budget"].ToString());
          dPulpNetProdAct = cmath.getd(item["Actual_Production"].ToString());

          sql = "UPDATE ZADailyMTDReport set LastUpdate = '" + cmath.getDateTimeNowStr() + "'"
          + " , PulpNetProdBgtWasteDaily = " + dPulpBudget
          + " , PulpNetProdActWasteDaily = " + dPulpNetProdAct
          + " Where ReportDate = '" + sReportDate + "'"
          + "       And MillKey = 1"
          + "       And PlantCode = 'W'"
          + " IF @@ROWCOUNT=0"
          + " INSERT INTO ZADailyMTDReport (ReportDate, MillKey, PlantCode, PulpNetProdBgtWasteDaily, PulpNetProdActWasteDaily, LastUpdate )"
          + " VALUES ("
          + "'" + sReportDate + "'"                   //reportdate
          + "," + 1                                   //millkey
          + ",'" + "W" + "'"                          //plantcode
          + "," + dPulpBudget                         //PulpNetProdBgtWaste
          + "," + dPulpNetProdAct                     //PulpNetProdActWaste
          + ",'" + cmath.getDateTimeNowStr() + "'"        //lastUpdated
          + " )";


          int rv = cSQL.ExecuteQuery(sql);

        } //foreach


      }
      catch (Exception er)
      {
        cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
      }
      finally
      {
        cORA.CloseDB();
        cSQL.CloseDB();
      }

    }//


    private void DoTUG_NSSC_Daily()
    {
      //TUG figures for pulp  NSSC  flid = 126
      //daily figure from PTS. Update ZADailyMTDReport table for the SSRS MTD Report

      double dPulpNetProdAct = 0;
      double dBudget = 0;

      double dPulpNetProdActWaste = 0;
      double dBudgetWaste = 0;

      try
      {

        //ACTUAL PRODUCTION
        sql = "select sum(NSSC)NSSC, sum(Waste_Prod) Waste "
          + " from   Daily_Production_Figures"
          + " where    Date_production >= " + cmath.OraDate(sReportDate)
          + " and Date_production <= " + cmath.OraDate(sReportDate);


        cORA.OpenDB("SAPXMII");
        DataTable dt = cORA.GetDataTable(sql);
        cORA.CloseDB();

        cFG.LogIt("Records found TUG(NSSC) FLID(126) : " + dt.Rows.Count);

        cSQL.OpenDB("ZAMDW");

        foreach (DataRow item in dt.Rows)
        {
          dPulpNetProdAct = cmath.getd(item["NSSC"].ToString());
          dPulpNetProdActWaste = cmath.getd(item["Waste"].ToString());
        }


        //2021-07-26  get the budgets for tugela
        sql = "Select cal_year, month, month_name, mnth_begin, mnth_end, nssc_net_tons_pd, refibre_prod_pd"
          + " from      budget_production"
          + " where " + sODateTo + " between mnth_begin and mnth_end"
          + "";

        cORA.OpenDB("SAPPITUG");
        dt = cORA.GetDataTable(sql);
        cORA.CloseDB();

        foreach (DataRow item in dt.Rows)
        {
          dBudget = cmath.getd(item["nssc_net_tons_pd"].ToString());
          dBudgetWaste = cmath.getd(item["refibre_prod_pd"].ToString());
        }


        sql = "UPDATE ZADailyMTDReport set LastUpdate = '" + cmath.getDateTimeNowStr() + "'"
          + " , PulpNetProdBgtDaily = " + dBudget
          + " , PulpNetProdBgtWasteDaily = " + dBudgetWaste
          + " , PulpNetProdActDaily = " + dPulpNetProdAct
          + " , PulpNetProdActWasteDaily = " + dPulpNetProdActWaste
          + " Where ReportDate = '" + sReportDate + "'"
          + "       And MillKey = 3"
          + "       And PlantCode = 'NSSC'"
          //+ "       And FLID = 126"
          + " IF @@ROWCOUNT=0"
          + " INSERT INTO ZADailyMTDReport (ReportDate, MillKey, PlantCode, PulpNetProdBgtDaily,  PulpNetProdBgtWasteDaily, PulpNetProdActDaily,PulpNetProdActWasteDaily, LastUpdate)"
          + " VALUES ("
          + "'" + sReportDate + "'"                   //reportdate
          + "," + 3                                   //millkey
          + ",'" + "NSSC" + "'"                       //plantcode
          + "," + dBudget                             //PulpNetProdBgtDaily
          + "," + dBudgetWaste                        //PulpNetProdBgtWasteDaily
          + "," + dPulpNetProdAct                     //PulpNetProdActDaily
          + "," + dPulpNetProdActWaste                //PulpNetProdActWasteDaily
          + ",'" + cmath.getDateTimeNowStr() + "'"        //lastUpdated
          + " )";

          int rv = cSQL.ExecuteQuery(sql);

      }
      catch (Exception er)
      {
        cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
      }
      finally
      {
        cORA.CloseDB();
        cSQL.CloseDB();
      }

    }//


    private void DoSTA_BH_Daily()
    {
      //STA figures for pulp  BH  flid = 1
      //PRODUCTION
      //daily figure from PTS update ZADailyMTDReport table for the SSRS MTD Report

      double dPulpNetProdAct = 0;

      try
      {

        sql = "select dp.machine_code"
        + " , sum(nvl(output, 0)) as NetProdAct"
        + " from daily_production dp"
        + " where trunc(report_date) = " + cmath.OraDate(sReportDate)
        + " and prod_type = 'D'"
        + " and dp.machine_code = 'BH'"
        + " group by dp.machine_code"
        + " ";


        cORA.OpenDB("SPIS");
        DataTable dt = cORA.GetDataTable(sql);
        cORA.CloseDB();

        cFG.LogIt("Records found STA(BH) FLID(1) : " + dt.Rows.Count);

        cSQL.OpenDB("ZAMDW");

        foreach (DataRow item in dt.Rows)
        {

          dPulpNetProdAct = cmath.getd(item["NetProdAct"].ToString());

          sql = "UPDATE ZADailyMTDReport set LastUpdate = '" + cmath.getDateTimeNowStr() + "'"
          + " , PulpNetProdActDaily = " + dPulpNetProdAct
          + " Where ReportDate = '" + sReportDate + "'"
          + "       And MillKey = 2"
          + "       And PlantCode = 'BH'"
          + " IF @@ROWCOUNT=0"
          + " INSERT INTO ZADailyMTDReport (ReportDate, MillKey, PlantCode,  PulpNetProdActDaily, LastUpdate)"
          + " VALUES ("
          + "'" + sReportDate + "'"                 //reportdate
          + "," + 2                                 //millkey
          + ",'" + "BH" + "'"                       //plantcode
          + "," + dPulpNetProdAct                   //PulpNetProdAct
          + ",'" + cmath.getDateTimeNowStr() + "'"      //lastUpdated
          + " )";

          int rv = cSQL.ExecuteQuery(sql);

        } //foreach

      }
      catch (Exception er)
      {
        cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
      }
      finally
      {
        cORA.CloseDB();
        cSQL.CloseDB();
      }

    }//


    private void DoSTA_STO_Daily()
    {

      //STA figures for 'STO' Stanger Stores Issues PS/SER-STO  (not in OEE though)

      double dProd = 0;


      string sStartDate = cmath.getDateStr(sReportDate, -2);    // start from 3 days ago
      sStartDate = sStartDate.Replace("-", "");

      string sEndDate = sReportDate.Replace("-","");

      try
      {

        // average of 3 days
        sql = "select round((sum(issues) / 3),5) Issues from tbl_material_levels"
          + " where from_date between '" + sStartDate + "'" + " and " + "'" + sEndDate + "'"
          + "";

        cORA.OpenDB("RTC");
        DataTable dt = cORA.GetDataTable(sql);
        cORA.CloseDB();

        cFG.LogIt("Records found STA(STO) FLID(0) : " + dt.Rows.Count);

        cSQL.OpenDB("ZAMDW");

        foreach (DataRow item in dt.Rows)
        {

          dProd = cmath.getd(item["Issues"].ToString());

          sql = "UPDATE ZADailyMTDReport set LastUpdate = '" + cmath.getDateTimeNowStr() + "'"
          + " , PulpNetProdActWasteDaily = " + dProd
          + " Where ReportDate = '" + sReportDate + "'"
          + "       And MillKey = 2"
          + "       And PlantCode = 'STO'"
          + " IF @@ROWCOUNT=0"
          + " INSERT INTO ZADailyMTDReport (ReportDate, MillKey, PlantCode, PulpNetProdActWasteDaily, LastUpdate)"
          + " VALUES ("
          + "'" + sReportDate + "'"                   //reportdate
          + "," + 2                                   //millkey
          + ",'" + "STO" + "'"                        //plantcode
          + "," + dProd                               //[PulpNetProdActWasteDaily]
          + ",'" + cmath.getDateTimeNowStr() + "'"        //lastUpdated
          + " )";

          int rv = cSQL.ExecuteQuery(sql);

        } //foreach

      }
      catch (Exception er)
      {
        cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
      }
      finally
      {
        cORA.CloseDB();
        cSQL.CloseDB();
      }

    }//


    private void DoPulpBudgets_Daily()
    {
      //--------------------
      //pulp budgets from mdw's FactProdBudgetMonth,  this is temporary until pulp arrives on MEM
      //for Stanger and Tugela

      //2021-07-20 STANGER ONLY SEE FUNCTION FOR TUGELA BUDGETS
      
      //2022-06-09 Stanger Budget must come from ZAMDW Daily NSPPM

      double dPulpNetProdBgt = 0D;


      try
      {
        //sql = "SELECT MonthBpKey, SappiMonthKey , PlantKey, (BpNetTons / sm.month_days) NetProdBgt "
        //+ " from FactProdBudgetMonth  bm join SappiFiscalCalendar.dbo.tbl_months AS sm ON bm.SappiMonthKey = sm.month_id"
        ////+ " where (plantkey = 8 or plantkey = 10 or plantkey = 5) "      //  8  tugela   10 stanger   5 ngx
        //+ " where plantkey = 8 "               //  8  tugela
        //+ " and '" + sReportDate + "' between sm.month_start and sm.month_end"
        //+ "";

        //2022-06-09
        sql = "select k.id, k.sappidayid, sd.day_start, CAST( sd.day_start as date), k.millcode, k.flid, k.kpivalue"
          + " from   kpidaily k , kpidefinition kd 	, sappifiscalcalendar.dbo.tbl_days sd"
          + " where	k.kpicode = kd.kpicode"
          + " and	k.sappidayid = sd.day_id"
          + " and CAST( sd.day_start as date) = '" +  sReportDate + "'"
          + " and	k.millcode = 'STA'"
          + " and   k.flid = 1"           // bleach plant
          + " and   k.kpicode in('NSPPM')"
          + "";


        cSQL.OpenDB("ZAMDW");

        DataTable dt = cSQL.GetDataTable(sql);

        cFG.LogIt("Records found Pulp Budgets  : " + dt.Rows.Count);

        foreach (DataRow item in dt.Rows)
        {

          dPulpNetProdBgt = cmath.getd(item["KPIValue"].ToString());

          sql = "UPDATE ZADailyMTDReport set LastUpdate = '" + cmath.getDateTimeNowStr() + "'"
          + " , PulpNetProdBgtDaily = " + dPulpNetProdBgt
          + " Where ReportDate = '" + sReportDate + "'"
          + "       And MillKey = 2"          //stanger
          + "       And PlantCode = 'BH'"
          + " IF @@ROWCOUNT=0"
          + " INSERT INTO ZADailyMTDReport (ReportDate, MillKey, PlantCode, PulpNetProdBgtDaily,  LastUpdate)"
          + " VALUES ("
          + "'" + sReportDate + "'"                 //reportdate
          + "," + 2                                 //millkey
          + ",'" + "BH" + "'"                       //plantcode
          + "," + dPulpNetProdBgt                   //PulpNetProdBgtDaily
          + ",'" + cmath.getDateTimeNowStr() + "'"      //lastUpdated
          + " )";

          int rv = cSQL.ExecuteQuery(sql);



         // if (item["PlantKey"].ToString() == "10")    // tugela
         // {
         //   sql = "UPDATE ZADailyMTDReport set LastUpdate = '" + cmath.getDateNowStr() + "'"
         //   + " , PulpNetProdBgtDaily = " + dPulpNetProdBgt
         //   + " Where ReportDate = '" + sReportDate + "'"
         //   + "       And MillKey = 3"
         //   + "       And PlantCode = 'NSSC'"
         //   //+ "       And FLID = 126"
         //   + " IF @@ROWCOUNT=0"
         //   + " INSERT INTO ZADailyMTDReport (ReportDate, MillKey, PlantCode, Machine"
         //   + "     , PulpNetProdBgtDaily, LastUpdate)"
         //   + " VALUES ("
         //   + "'" + sReportDate + "'"                 //reportdate
         //   + "," + 3                                 //millkey
         //   + ",'" + "NSSC" + "'"                     //plantcode
         //// + "," + 126                               //flid
         //   + ",'" + "TUG NSSC" + "'"                 //Machine
         //   + "," + dPulpNetProdBgt                   //PulpNetProdBgtDaily
         //   + ",'" + cmath.getDateNowStr() + "'"      //lastUpdated
         //   + " )";

         //   int rv = cSQL.ExecuteQuery(sql);
         // }

        }

      }
      catch (Exception er)
      {
        cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
      }
      finally
      {
        cSQL.CloseDB();
      }

    } //


    private void DoPulpSai_DAILY()
    {

      //------------------------------------------------------------------------------
      //pulp COOKS calculations  SAICCOR
      //Step 1. get the 3 month total (  90 days ) for C3, C4 and C5 plants
      //Total Mill Actual Production from MEM (MEM KPI = GP)  from the Daily table 
      //------------------------------------------------------------------------------

      double dGP = 0;
      double dTotalCooks3M = 0;
      double dTotalCooks = 0;
      double dDigProdTons = 0;
      double d83TotSai = 0;

      try
      {

        //the GP total for the last 90 days
        sql = " select sum(k.kpivalue) as total from kpidaily k"
          + "     ,sappifiscalcalendar.dbo.tbl_days sd"
          + "     ,sappifiscalcalendar.dbo.tbl_months sm"
          + "     ,sappifiscalcalendar.dbo.tbl_years sy"
          + "   where   sd.month_id = sm.month_id"
          + "    and k.SappiDayID = sd.day_id"
          + "    and sm.year_id = sy.year_id"
          + "    and millCode = 'SAI'"
          + "    and kpicode = 'KPI0035'"
          + "    and k.FLID in ('4','5','6')"
          + "    and cast(sd.day_start as date) >= '" + s90DaysBack + "'"
          + "    and cast(sd.day_end as date) <= '" + sReportDate + "'"
          + "";

        cSQL.OpenDB("ZAMDW");
        DataTable dt = cSQL.GetDataTable(sql);

        foreach (DataRow item in dt.Rows)
        {
          dGP = cmath.getd(item["total"].ToString());
        }


        //get the Daily total NSPPM for Saiccor for the day ( pulp budget )
        sql = "select sum(k.kpivalue) as total"
          + " from kpidaily k"
          + "   , sappifiscalcalendar.dbo.tbl_days sd"
          + "   where   k.SappiDayID = sd.day_id"
          + "           and	millCode = 'SAI'"
          + "           and kpicode = 'NSPPM' "
          + "           and k.FLID in ('4','5','6')"
          + "           and cast(sd.day_start as date) = '" + sReportDate + "'"
          + "";


        dt = cSQL.GetDataTable(sql);

        foreach (DataRow item in dt.Rows)
        {
          d83TotSai = cmath.getd(item["total"].ToString());
        }


        cSQL.CloseDB();


        //------------------------------------------------------------------------------
        //step 2. get the cooks totals from SAICCOR storec proc 2021-07-01
        //--SelectedDate_TotalCooks, MTD_TotalCooks, MTD_AvgCooks,  MTD_TotalDays, 3M_TotalCooks, 3M_AvgCooks, 3M_TotalDays,
        //-- 80                           236              78             3                4938       54            90
        //------------------------------------------------------------------------------


        sql = "EXECUTE [sts].[Sp_Digester_Saiccor_Cooks] '" + sReportDate + "'," + "'All'";

        cSQL.OpenDB("SAIMES");
        dt = cSQL.GetDataTable(sql);
        cSQL.CloseDB();

        cFG.LogIt("Records found Cooks Saiccor  : " + dt.Rows.Count);

        foreach (DataRow item in dt.Rows)
        {
          dTotalCooks3M = cmath.getd(item["3M_TotalCooks"].ToString());
          dTotalCooks = cmath.getd(item["SelectedDate_TotalCooks"].ToString());
        }


        //do some calcs
        dDigProdTons = cmath.div(dGP, dTotalCooks3M);
        dDigProdTons = dDigProdTons * dTotalCooks;


        //add-update the ZADailyMTDReport
        //I have created a new plant code "DIG"  for all digestors at Saiccor
        sql = "UPDATE ZADailyMTDReport set LastUpdate = '" + cmath.getDateTimeNowStr() + "'"
         + " , PulpNetProdBgtDaily = " + d83TotSai
         + " , PulpNetProdActDaily = " + dDigProdTons
         + " Where ReportDate = '" + sReportDate + "'"
         + "       And MillKey = 4"          //saiccor
         + "       And PlantCode = 'DIG'"    //DIG (All)
         + " IF @@ROWCOUNT=0"
         + " INSERT INTO ZADailyMTDReport (ReportDate, MillKey, PlantCode, PulpNetProdBgtDaily, PulpNetProdActDaily, LastUpdate)"
         + " VALUES ("
         + "'" + sReportDate + "'"                   //reportdate
         + "," + 4                                   //millkey
         + ",'" + "DIG" + "'"                        //plantcode
         + "," + d83TotSai                           //PulpNetProdBgt
         + "," + dDigProdTons                        //PulpNetProdAct
         + ",'" + cmath.getDateTimeNowStr() + "'"        //lastUpdated
         + " )";


        cSQL.OpenDB("ZAMDW");
        int rv = cSQL.ExecuteQuery(sql);

      }
      catch (Exception er)
      {
        cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
      }
      finally
      {
        cSQL.CloseDB();
      }

    }//


    private void DoPulpNGX_DAILY()
    {
      try
      {

        //------------------------------------------------------------------------------
        //pulp COOKS calculations  NGODWANA  FL3  (161)
        //use figures from UT3 for the dGP  and nsppm
        //pulp actual from NGX  2021-07-01
        //--SelectedDate_TotalCooks, MTD_TotalCooks, MTD_AvgCooks,  MTD_TotalDays, 3M_TotalCooks, 3M_AvgCooks, 3M_TotalDays,
        //------------------------------------------------------------------------------

        double dGP = 0;
        double dTotalCooks3M = 0;
        double dTotalCooks = 0;
        double dDigProdTons = 0;
        double dTotUT3 = 0;

        //the GP total
        sql = " select sum(k.kpivalue) as total from kpidaily k"
          + "     ,sappifiscalcalendar.dbo.tbl_days sd"
          + "     ,sappifiscalcalendar.dbo.tbl_months sm"
          + "     ,sappifiscalcalendar.dbo.tbl_years sy"
          + "   where   sd.month_id = sm.month_id"
          + "    and k.SappiDayID = sd.day_id"
          + "    and sm.year_id = sy.year_id"
          + "    and MILLCODE = 'NGX'"
          + "    and FLID = 13 "
          + "    and kpicode = 'KPI0035'"
          + "    and cast(sd.day_start as date) >= '" + s90DaysBack + "'"
          + "    and cast(sd.day_end as date) <= '" + sReportDate + "'"
          + "";

        cSQL.OpenDB("ZAMDW");

        DataTable dt = cSQL.GetDataTable(sql);

        foreach (DataRow item in dt.Rows)
        {
          dGP = cmath.getd(item["total"].ToString());
        }


        //get the daily total NSPPM for NGX UT3 ( pulp budget )
        sql = "select sum(k.kpivalue) as total"
          + " from kpidaily k"
          + "   , sappifiscalcalendar.dbo.tbl_days sd"
          + "   where   k.SappiDayID = sd.day_id"
          + "           and	MILLCODE = 'NGX'"
          + "           and kpicode = 'NSPPM' "
          + "           and FLID = 13 "
          + "           and cast(sd.day_start as date) = '" + sReportDate + "'"
          + "";

        dt = cSQL.GetDataTable(sql);

        cSQL.CloseDB();

        foreach (DataRow item in dt.Rows)
        {
          dTotUT3 = cmath.getd(item["total"].ToString());
        }

        dDigProdTons = 0;
        dDigProdTons = 0;
        dTotalCooks3M = 0;
        dTotalCooks = 0;

        sql = "EXECUTE [sts].[Sp_Digester_Ngx_Cooks] '" + sReportDate + "'";

        cSQL.OpenDB("NGXMES");
        dt = cSQL.GetDataTable(sql);
        cSQL.CloseDB();

        cFG.LogIt("Records found Cooks Ngodwana  : " + dt.Rows.Count);

        foreach (DataRow item in dt.Rows)
        {
          dTotalCooks3M = cmath.getd(item["3M_TotalCooks"].ToString());
          dTotalCooks = cmath.getd(item["SelectedDate_TotalCooks"].ToString());
        }

        dDigProdTons = cmath.div(dGP, dTotalCooks3M);
        dDigProdTons = dDigProdTons * dTotalCooks;

        //add-update the ZADailyMTDReport
        sql = "UPDATE ZADailyMTDReport set LastUpdate = '" + cmath.getDateTimeNowStr() + "'"
         + " , PulpNetProdBgtDaily = " + dTotUT3              
         + " , PulpNetProdActDaily = " + dDigProdTons
         + " Where ReportDate = '" + sReportDate + "'"
         + "       And MillKey = 1"          //ngodwana
         + "       And PlantCode = 'FL3'"    //Fl3
         + " IF @@ROWCOUNT=0"
         + " INSERT INTO ZADailyMTDReport (ReportDate, MillKey, PlantCode, PulpNetProdBgtDaily, PulpNetProdActDaily, LastUpdate)"
         + " VALUES ("
         + "'" + sReportDate + "'"                   //reportdate
         + "," + 1                                   //millkey
         + ",'" + "FL3" + "'"                        //plantcode
         + "," + dTotUT3                             //PulpNetProdBgtDaily
         + "," + dDigProdTons                        //PulpNetProdActDaily
         + ",'" + cmath.getDateTimeNowStr() + "'"        //lastUpdated
         + " )";


        cSQL.OpenDB("ZAMDW");

        int rv = cSQL.ExecuteQuery(sql);

      }
      catch (Exception er)
      {
        cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
      }
      finally
      {
        cSQL.CloseDB();
      }


    }//


    private void DoPulpSai_WEEKLY()
    {
      //2021-08-30  ask ivan to provide a WTD_TotalCooks

      //------------------------------------------------------------------------------
      //pulp COOKS calculations  SAICCOR
      //Step 1. get the 3 month total (  90 days ) for C3, C4 and C5 plants
      //Total Mill Actual Production from MEM (MEM KPI = GP)  from the Daily table 
      //------------------------------------------------------------------------------

      double dGP = 0;
      double dTotalCooks3M = 0;
      double dTotalCooksMTD = 0;
      double dDigProdTons = 0;
      double d83TotSai = 0;

      try
      {

        //the GP total for the last 90 days
        sql = " select sum(k.kpivalue) as total from kpidaily k"
          + "     ,sappifiscalcalendar.dbo.tbl_days sd"
          + "     ,sappifiscalcalendar.dbo.tbl_months sm"
          + "     ,sappifiscalcalendar.dbo.tbl_years sy"
          + "   where   sd.month_id = sm.month_id"
          + "    and k.SappiDayID = sd.day_id"
          + "    and sm.year_id = sy.year_id"
          + "    and MILLCODE = 'SAI'"
          + "    and kpicode = 'KPI0035'"
          + "    and k.FLID in ('4','5','6')"
          + "    and cast(sd.day_start as date) >= '" + s90DaysBack + "'"
          + "    and cast(sd.day_end as date) <= '" + sReportDate + "'"
          + "";

        cSQL.OpenDB("ZAMDW");
        DataTable dt = cSQL.GetDataTable(sql);

        foreach (DataRow item in dt.Rows)
        {
          dGP = cmath.getd(item["total"].ToString());
        }


        //get the Weekly total KPI0083 for Saiccor
        sql = "SELECT round(sum(KPIValue),5) total FROM  KPIWeekly K, KPIDefinition KD, sappifiscalcalendar.dbo.tbl_weeks sw"
        + " WHERE  K.KPICode = KD.KpiCode"
        + "     and  K.SappiWeekID = sw.week_id"
        + "     and '" + sReportDate + "'  between sw.week_start and sw.week_end"
        + "     And KD.KpiCode = 'KPI0083'"
        + "     And K.MILLCODE = 'SAI'"
        + "    and k.FLID in ('4','5','6')"
        + "";

        dt = cSQL.GetDataTable(sql);

        foreach (DataRow item in dt.Rows)
        {
          d83TotSai = cmath.getd(item["total"].ToString());
        }


        cSQL.CloseDB();


        //------------------------------------------------------------------------------
        //step 2. get the cooks totals from SAICCOR storec proc 2021-07-01
        //--SelectedDate_TotalCooks, MTD_TotalCooks, MTD_AvgCooks,  MTD_TotalDays, 3M_TotalCooks, 3M_AvgCooks, 3M_TotalDays,
        //-- 80                           236              78             3                4938       54            90
        //------------------------------------------------------------------------------


        sql = "EXECUTE [sts].[Sp_Digester_Saiccor_Cooks] '" + sReportDate + "'," + "'All'";


        cSQL.OpenDB("SAIMES");
        dt = cSQL.GetDataTable(sql);
        cSQL.CloseDB();

        cFG.LogIt("Records found Cooks Saiccor  : " + dt.Rows.Count);

        foreach (DataRow item in dt.Rows)
        {
          dTotalCooks3M = cmath.getd(item["3M_TotalCooks"].ToString());
          dTotalCooksMTD = cmath.getd(item["WTD_TotalCooks"].ToString());   //week to date
        }


        //do some calcs
        dDigProdTons = cmath.div(dGP, dTotalCooks3M);
        dDigProdTons = dDigProdTons * dTotalCooksMTD;

        //add-update the ZADailyMTDReport
        //I have created a new plant code "DIG"  for all digestors at Saiccor
        sql = "UPDATE ZADailyMTDReport set LastUpdate = '" + cmath.getDateTimeNowStr() + "'"
         + " , PulpNetProdBgtWeekly = " + d83TotSai
         + " , PulpNetProdActWeekly = " + dDigProdTons
         + " Where ReportDate = '" + sReportDate + "'"
         + "       And MillKey = 4"          //saiccor
         + "       And PlantCode = 'DIG'"    //DIG (All)
         + " IF @@ROWCOUNT=0"
         + " INSERT INTO ZADailyMTDReport (ReportDate, MillKey, PlantCode, PulpNetProdBgtWeekly, PulpNetProdActWeekly, LastUpdate)"
         + " VALUES ("
         + "'" + sReportDate + "'"                   //reportdate
         + "," + 4                                   //millkey
         + ",'" + "DIG" + "'"                        //plantcode
         + "," + d83TotSai                           //PulpNetProdBgtweekly
         + "," + dDigProdTons                        //PulpNetProdActweekly
         + ",'" + cmath.getDateTimeNowStr() + "'"        //lastUpdated
         + " )";


        cSQL.OpenDB("ZAMDW");
        int rv = cSQL.ExecuteQuery(sql);

      }
      catch (Exception er)
      {
        cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
      }
      finally
      {
        cSQL.CloseDB();
      }

    }//


    private void DoPulpNGX_WEEKLY()
    {
      try
      {

        //------------------------------------------------------------------------------
        //pulp COOKS calculations  NGODWANA  FL3  (161)
        //use figures from UT3 for the dGP  and kpi0083
        //pulp actual from NGX  2021-07-01
        //--SelectedDate_TotalCooks, MTD_TotalCooks, MTD_AvgCooks,  MTD_TotalDays, 3M_TotalCooks, 3M_AvgCooks, 3M_TotalDays,
        //------------------------------------------------------------------------------

        double dGP = 0;
        double dTotalCooks3M = 0;
        double dTotalCooksMTD = 0;
        double dDigProdTons = 0;
        double d83TotUT3 = 0;

        //the GP total
        sql = " select sum(k.kpivalue) as total from kpidaily k"
          + "     ,sappifiscalcalendar.dbo.tbl_days sd"
          + "     ,sappifiscalcalendar.dbo.tbl_months sm"
          + "     ,sappifiscalcalendar.dbo.tbl_years sy"
          + "   where   sd.month_id = sm.month_id"
          + "    and k.SappiDayID = sd.day_id"
          + "    and sm.year_id = sy.year_id"
          + "    and MILLCODE = 'NGX'"
          + "    and FLID = 13 "
          + "    and kpicode = 'KPI0035'"
          + "    and cast(sd.day_start as date) >= '" + s90DaysBack + "'"
          + "    and cast(sd.day_end as date) <= '" + sReportDate + "'"
          + "";

        cSQL.OpenDB("ZAMDW");
        DataTable dt = cSQL.GetDataTable(sql);

        foreach (DataRow item in dt.Rows)
        {
          dGP = cmath.getd(item["total"].ToString());
        }

        //THIS WAS INCORRECT USING THE MONTH TABLE  2021-11-11
        //get the MTD total KPI0083 for NGX UT3 for this month ( pulp budget )
        //sql = "SELECT sum(KPI_Value) total"
        //+ " FROM  KPIMonthly K, KPIDefinition KD, sappifiscalcalendar.dbo.tbl_months sm"
        //+ " WHERE  K.KPI_Code = KD.KpiCode" 
        //+ "    And  K.SAPPIMONTH_ID = sm.month_id"
        //+ "    And '" + sReportDate + "'  between sm.month_start and sm.month_end"
        //+ "    And KD.KpiCode = 'KPI0083'"
        //+ "    And K.MILL_CODE = 'NGX'"
        //+ "    And FLID = 13 "
        //+ "";


        //NOW USING THE WEEK TABLE  2021-11-11
        sql = "SELECT sum(KPIValue) total "
          + " FROM  KPIWeekly K	, KPIDefinition KD	, sappifiscalcalendar.dbo.tbl_weeks sw"
          + " WHERE  K.KPICode = KD.KpiCode"
          + "     And  K.SappiWeekID = sw.week_id"
          + "     And '" + sReportDate + "'  between sw.week_start and sw.week_end"
          + "     And KD.KpiCode = 'KPI0083'"
          + "     And K.MILLCODE = 'NGX'"
          + "     And FLID = 13 "
          + "";

        dt = cSQL.GetDataTable(sql);
        cSQL.CloseDB();

        foreach (DataRow item in dt.Rows)
        {
          d83TotUT3 = cmath.getd(item["total"].ToString());
        }


        dDigProdTons = 0;
        dDigProdTons = 0;
        dTotalCooks3M = 0;
        dTotalCooksMTD = 0;

        sql = "EXECUTE [sts].[Sp_Digester_Ngx_Cooks] '" + sReportDate + "'";

        cSQL.OpenDB("NGXMES");
        dt = cSQL.GetDataTable(sql);
        cSQL.CloseDB();

        cFG.LogIt("Records found Cooks Ngodwana  : " + dt.Rows.Count);

        foreach (DataRow item in dt.Rows)
        {
          dTotalCooks3M = cmath.getd(item["3M_TotalCooks"].ToString());
          dTotalCooksMTD = cmath.getd(item["WTD_TotalCooks"].ToString());
        }

        //the d35TotUT3  is the KPI0035 from UT3
        dDigProdTons = cmath.div(dGP, dTotalCooks3M);
        dDigProdTons = dDigProdTons * dTotalCooksMTD;

        //add-update the ZADailyMTDReport
        sql = "UPDATE ZADailyMTDReport set LastUpdate = '" + cmath.getDateTimeNowStr() + "'"
         + " , PulpNetProdBgtWeekly = " + d83TotUT3               // update the budget from the UT3's KPI0083
         + " , PulpNetProdActWeekly = " + dDigProdTons
         + " Where ReportDate = '" + sReportDate + "'"
         + "       And MillKey = 1"          //ngodwana
         + "       And PlantCode = 'FL3'"    //Fl3
         + " IF @@ROWCOUNT=0"
         + " INSERT INTO ZADailyMTDReport (ReportDate, MillKey, PlantCode, PulpNetProdBgtWeekly, PulpNetProdActWeekly, LastUpdate)"
         + " VALUES ("
         + "'" + sReportDate + "'"                   //reportdate
         + "," + 1                                   //millkey
         + ",'" + "FL3" + "'"                        //plantcode
         + "," + d83TotUT3                           //PulpNetProdBgtweekly
         + "," + dDigProdTons                        //PulpNetProdActweekly
         + ",'" + cmath.getDateTimeNowStr() + "'"        //lastUpdated
         + " )";


        cSQL.OpenDB("ZAMDW");

        int rv = cSQL.ExecuteQuery(sql);

      }
      catch (Exception er)
      {
        cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
      }
      finally
      {
        cSQL.CloseDB();
      }

    }//


    





    


    




  }///
}///


