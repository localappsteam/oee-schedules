﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;

namespace Schedules
{
  static class cFG
  {

    public const byte NGODWANA = 1;
    public const byte STANGER = 2;
    public const byte TUGELA = 4;
    public const byte SAICCOR = 6;

    public const string NGX = "NGX";
    public const string SAI = "SAI";
    public const string STA = "STA";
    public const string TUG = "TUG";

    public const sbyte CAT_UNPLANNED = 0;
    public const sbyte CAT_PLANNED = 1;
    public const sbyte CAT_COMMENT = 2;
    public const sbyte CAT_RUNRATE = -1;

    public const byte STATUS_SCHEDULED = 1;
    public const byte STATUS_INPROGRESS = 2;
    public const byte STATUS_ERROR = 3;
    public const byte STATUS_COMPELETED = 4;

    public const byte JOBTYPE_RUNNALLPLANTS = 1;
    public const byte JOBTYPE_RUNSELECTEDPLANT = 2;

    public static string APPPATH = GetAppPath();

    public static string GetAppPath()
    {
      string ss = "";

      string dummy = Application.StartupPath;
      //dummy = Application.UserAppDataPath;
      //dummy = Application.LocalUserAppDataPath;
      try
      {


#if !DEBUG
         ss = Application.StartupPath + "\\";
#else
        ss = "C:\\Development\\MemETL\\";
#endif


        //if (dummy.IndexOf("\\bin") > 0)
        //{
        //  ss = cmath.SplitStr(ref dummy, '\\') + "\\";
        //  ss += cmath.SplitStr(ref dummy, '\\') + "\\";
        //  ss += cmath.SplitStr(ref dummy, '\\') + "\\";
        //}
        //else
        //{
        //  ss = Application.StartupPath + "\\";
        //}

        string sFolderLog = ss + "logs\\";

        if (!Directory.Exists(sFolderLog))
        {
          Directory.CreateDirectory(sFolderLog);
        }

#if !DEBUG
        return sFolderLog;
#else
        return ss;
#endif

        
      }
      catch (Exception)
      {
        return "";
      }
    }//



    public static void LogIt(string str)
    {
      //string sPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
      //string sPath = Environment.GetFolderPath(Environment.SpecialFolder.CommonDocuments);

      string sPath = APPPATH +  cmath.getDateNowStr().Replace("-", "") + "-zMemETL.log";
      try
      {
        using (StreamWriter wr = new StreamWriter(sPath, true))
        {
          wr.WriteLine(cmath.getTimeNowStr() + " " + str);

          wr.Close();
        }
      }
      catch (Exception e)
      {
        sPath = e.Message;
      }
      finally
      {
        sPath = "";
      }
    }



  } ///
}///
