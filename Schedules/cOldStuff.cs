﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schedules
{
   internal class cOldStuff
   {


    //public void RunLomati22(int pRunYear = 0, int pStartMonth = 0)
    //{
    //  int iMonth = 0;     // oracle month
    //  int iSQLMonthID = 0;   // this is the ID used in MSSQLSERVER    kpiMonthly

    //  string sStartDate = "";
    //  string sEndDate = "";
    //  int currMonth = 0;
    //  int currYear = 0;

    //  int iDayStart = 0;
    //  int iDayEnd = 0;

    //  try
    //  {

    //    cal = cal ?? new cCal();

    //    cORA.OpenDB("NGX");     //current month
    //    cal.GetRunMonth(ref currYear, ref currMonth, ref sStartDate, ref sEndDate);
    //    cORA.CloseDB();

    //    cal.GetCal(cmath.getDateNowStr(-1));
    //    currYear = cal.iSappiYear;
    //    currMonth = cal.iCurrMonthOfYear;
    //    sStartDate  = cal.sDayStart;
    //    sEndDate = cal.sDayEnd;


    //    iMonth = pStartMonth;     //start from the month number
    //    iSappiYear = pRunYear;    //run year

    //    if (iMonth > currMonth)
    //      iMonth = currMonth;

    //    //if running for previous years
    //    if (iSappiYear < currYear)
    //    {
    //      currMonth = 12;     //set to last month of year
    //    }

    //    if (iSappiYear > currYear)
    //    {
    //      cFG.LogIt("SiteData RunLomati  RunYear > current year");
    //      return;
    //    }


    //    //then loop thru until the current month
    //    while (iMonth <= currMonth)
    //    {

    //      cORA.OpenDB("NGX");

    //      cal.GetMonth(iSappiYear, iMonth, ref sStartDate, ref sEndDate);      //get start date and end date for the month

    //      iDayStart = cal.GetSappiDay(cmath.OraDate(sStartDate));            //sappi days  start of month
    //      iDayEnd = cal.GetSappiDay(cmath.OraDate(sEndDate));                // sappi days end of month


    //      cal.GetMonthMSSQL(iSappiYear, iMonth, ref sStartDate, ref sEndDate);

    //      cal.GetCal(cmath.getDateStr(sStartDate));
    //      iDayStart = cal.iDayOfYear;

    //      cal.GetCal(cmath.getDateStr(sEndDate));
    //      iDayEnd = cal.iDayOfYear;


    //      iSQLMonthID = cal.GetSappiMonthID(iSappiYear, iMonth);               // from sql server to save at sql server



    //      if ((iMonth <= 0) || (iSQLMonthID <= 0))
    //      {
    //        cFG.LogIt("SiteData RunLomati Invalid Month ID : " + iMonth + " , " + iSQLMonthID);
    //        break;
    //      }

    //      if (iMonth == currMonth)     //enddate is up to yesterday
    //      {
    //        sEndDate = cmath.getDateEODStr(cmath.getDateStr(DateTime.Now.ToString(), -1));       //yesterday
    //        iDayEnd = cal.GetSappiDay(cmath.OraDate(sEndDate));
    //      }

    //      cORA.CloseDB();

    //      Application.DoEvents();

    //      cFG.LogIt("SiteData RunLomati Process Month# " + iMonth + " Date Start : " + sStartDate + " , Date End " + sEndDate);

    //      //DoLomati(iSQLMonthID, iSappiYear.ToString(), iMonth.ToString(), sStartDate, sEndDate);
    //      DoLomati(iSappiYear.ToString(), iMonth.ToString(), sStartDate, sEndDate, "month");


    //      //quarterly figures
    //      cal.GetCal(cmath.getDateStr(sStartDate));   //get the quarter start / end dates
    //      sStartDate = cal.sQuartertartDate;
    //      //sEndDate = is the actual month end date for the quarter calculations
    //      DoLomati(iSappiYear.ToString(), iMonth.ToString(), sStartDate, sEndDate, "quarter");


    //      //YTD figures
    //      sStartDate = cal.sYearStartDate;
    //      //sEndDate = is the actual month end date from the last date given in the loop
    //      DoLomati(iSappiYear.ToString(), iMonth.ToString(), sStartDate, sEndDate, "ytd");


    //      iMonth++;

    //    } //loop




    //  }
    //  catch (Exception ee)
    //  {
    //    cFG.LogIt("RunLomati " + ee.Message);
    //    //throw ee;
    //  }
    //  finally
    //  {
    //    cSQL.CloseDB();
    //    cORA.CloseDB();
    //  }

    //}// 




    /*
     * 
     *   /// <summary>
       /// from ORACLE OEE
       /// </summary>

       public DataTable GetFinYearList()
     {

       try
       {

         string sql = "select sappi_year  from sappi_years where sappi_year >= 2018  order by sappi_year desc"
           + "";

         return cORA.GetDataTable(sql);

       }
       catch (Exception ee)
       {
         throw ee;
       }

     }//
     * 
     * 
     * */





    /*
     * 
     *     /// <summary>
     /// from ORACLE OEE
     /// </summary>
     public void GetDaySelectedExp(string pDate, ref int finYear,ref int currMonth, ref int currDay, ref string startDate, ref string endDate)
     {

       //returns year, sappiday and start and end dates for a selected date

       try
       {

         string sql = " select sd.start_of_day, sd.end_of_day, sd.sappi_day, sd.sappi_month, sd.sappi_year, sd.day_of_month, sd.sappi_week, sd.day_of_week"
             + " from sappi_days sd where start_of_day = " + cmath.OraDate(pDate);

         DataTable dt = cORA.GetDataTable(sql);
         foreach (DataRow item in dt.Rows)
         {
           finYear = cmath.geti(item["sappi_year"].ToString());
           startDate = item["start_of_day"].ToString();
           endDate = item["end_of_day"].ToString();
           currDay = cmath.geti(item["sappi_day"].ToString());
           currMonth = cmath.geti(item["sappi_month"].ToString());
         }

       }
       catch (Exception ee)
       {
         throw ee;
       }

     }//


     * 
     * */







    /*
     * 
     *  /// <summary>
     /// from ORACLE OEE
     /// </summary>
     public void GetDayCurrent(ref int finYear, ref int currDay, ref string startDate, ref string endDate)
     {

       //returns current year, sappiday and start and end dates for today

       try
       {

         string sql = " select sd.start_of_day, sd.end_of_day, sd.sappi_day, sd.sappi_month, sd.sappi_year, sd.day_of_month, sd.sappi_week, sd.day_of_week"
             + " from sappi_days sd where start_of_day = trunc(sysdate)";


         DataTable dt = cORA.GetDataTable(sql);
         foreach (DataRow item in dt.Rows)
         {
           finYear = cmath.geti(item["sappi_year"].ToString());
           startDate = item["start_of_day"].ToString();
           endDate = item["end_of_day"].ToString();
           currDay = cmath.geti(item["sappi_day"].ToString());
         }

       }
       catch (Exception ee)
       {
         throw ee;
       }

     }//
     * 
     * */




    //public void GetLastWeek(int finYear, ref int lastWeek)
    //{

    //  try
    //  {

    //    string sql = "select max(sappi_week) sappi_week  from sappi_weeks w"
    //      + " where w.sappi_year = " + finYear
    //      + "";

    //    DataTable dt = cORA.GetDataTable(sql);
    //    foreach (DataRow item in dt.Rows)
    //    {
    //      lastWeek = cmath.geti(item["sappi_week"].ToString());
    //    }

    //  }
    //  catch (Exception ee)
    //  {
    //    throw ee;
    //  }

    //}//




    //public void GetWeek(string sRunDate, ref string startDate, ref string endDate)
    //{

    //  try
    //  {

    //    string sql = "select w.start_of_week , w.end_of_week, w.sappi_year, w.sappi_month, w.sappi_week, w.week_of_month"
    //      + " , (trunc(w.end_of_week) - trunc(w.start_of_week) + 1) numDays"
    //      + " from sappi_weeks w"
    //      + " where " + cmath.OraDateMon(sRunDate) + " between start_of_week and end_of_week"
    //      + "";


    //    DataTable dt = cORA.GetDataTable(sql);
    //    foreach (DataRow item in dt.Rows)
    //    {
    //      startDate = item["start_of_week"].ToString();
    //      endDate = item["end_of_week"].ToString();
    //    }

    //  }
    //  catch (Exception ee)
    //  {
    //    throw ee;
    //  }

    //}//



    //public void GetYearDates(int finYear, ref string startDate, ref string endDate)
    //{
    //  //return selected year, and start and end dates for the year
    //  try
    //  {

    //    string sql = "select sy.start_of_year, sy.end_of_year"
    //        + " from sappi_years sy"
    //        + " where sy.sappi_year = " + finYear
    //        + "";

    //    DataTable dt = cORA.GetDataTable(sql);
    //    foreach (DataRow item in dt.Rows)
    //    {
    //      startDate = item["start_of_year"].ToString();
    //      endDate = item["end_of_year"].ToString();
    //    }

    //  }
    //  catch (Exception ee)
    //  {
    //    throw ee;
    //  }

    //}//




    //2025-02-19  replaced by above
    //private void DoBPTS1Daily()
    //{

    //   //monthBPTS1 contains the month value / calendar hours
    //   int daysToSplit = (int)(Math.Round(monthBPTS, 2) / 24);

    //   double mtdBPTS1 = 0;
    //   double dayBPTS1 = 0;

    //   //if (sDailyStartDate == "2025-01-25")
    //   //{
    //   //   dayBPTS1 = 0;
    //   //}

    //   cSQL.OpenDB("ZAMDW");

    //   int dayOfMonth = cal.GetDayOfMonth(sDailyStartDate);

    //   //get mtd value of daily bpts1
    //   string sql = "select d.MillCode, d.FLID, d.kpicode, sum(d.KPIValue) KPIValue  from KPIDaily d"
    //    + ", kpidefinition kd "
    //    + ", DimPlant p"
    //    + ", DimMill m"
    //    + ", sappifiscalcalendar.dbo.tbl_days sd"
    //    + ", sappifiscalcalendar.dbo.tbl_months sm"
    //    + ", sappifiscalcalendar.dbo.tbl_month_names smn"
    //    + ", sappifiscalcalendar.dbo.tbl_years sy"
    //    + " where d.KPICODE = kd.KpiCode"
    //    + " and   d.flid = p.flid"
    //    + " and   d.millcode = m.SiteAbbrev"
    //    + " and   m.millkey = p.millkey"
    //    + " and  d.sappidayid = sd.day_id"
    //    + " and   sd.month_id = sm.month_id"
    //    + " and sm.month_name_id = smn.month_name_id"
    //    + " and   sm.year_id = sy.year_id"
    //    + " and sy.year = " + iSappiYear
    //    + " and   sm.month_of_year = " + currMonth
    //    + " and d.MILLCODE = '" + sSite + "'"
    //    + " and   d.FLID = " + sAreaID
    //    + " and   d.kpicode  = 'BPTS1'"
    //    + " and sd.day_start < '" + sDailyStartDate + "'"
    //    + " group by d.MillCode, d.FLID, d.KpiCode"
    //    + "";

    //   DataTable dt = cSQL.GetDataTable(sql);
    //   foreach (DataRow item in dt.Rows)
    //   {
    //      mtdBPTS1 = cmath.getd(item["KPIValue"].ToString());
    //   }

    //   int daysLeft = (numOfDaysMonth - dayOfMonth + 1);

    //   if (daysLeft <= daysToSplit)
    //   {
    //      dayBPTS1 = (monthBPTS1 - mtdBPTS1) / daysLeft;
    //   }

    //   arr[zBP_BPTS1, FROM_MONTH] = dayBPTS1;


    //   SaveData(zBP_BPTS1, "BPTS1");
    //   SumYTD_QTR(zBP_BPTS1, ROUND5);
    //   SaveQuarter(zBP_BPTS1, "BPTS1");

    //   cSQL.CloseDB();


    //}



    /*
     *       private void DoBPCSDaily()
          {

                //monthBPTS1 contains the month value / calendar hours
                int daysToSplit = (int)(Math.Round(monthBPCS, 2) / 24);

                double mtdBPCS = 0;
                double dayBPCS = 0;

                //if (sDailyStartDate == "2025-01-25")
                //{
                //   dayBPTS1 = 0;
                //}

                cSQL.OpenDB("ZAMDW");

                int dayOfMonth = cal.GetDayOfMonth(sDailyStartDate);

                //get mtd value of daily bpts1
                string sql = "select d.MillCode, d.FLID, d.kpicode, sum(d.KPIValue) KPIValue  from KPIDaily d"
                 + ", kpidefinition kd "
                 + ", DimPlant p"
                 + ", DimMill m"
                 + ", sappifiscalcalendar.dbo.tbl_days sd"
                 + ", sappifiscalcalendar.dbo.tbl_months sm"
                 + ", sappifiscalcalendar.dbo.tbl_month_names smn"
                 + ", sappifiscalcalendar.dbo.tbl_years sy"
                 + " where d.KPICODE = kd.KpiCode"
                 + " and   d.flid = p.flid"
                 + " and   d.millcode = m.SiteAbbrev"
                 + " and   m.millkey = p.millkey"
                 + " and  d.sappidayid = sd.day_id"
                 + " and   sd.month_id = sm.month_id"
                 + " and sm.month_name_id = smn.month_name_id"
                 + " and   sm.year_id = sy.year_id"
                 + " and sy.year = " + iSappiYear
                 + " and   sm.month_of_year = " + currMonth
                 + " and d.MILLCODE = '" + sSite + "'"
                 + " and   d.FLID = " + sAreaID
                 + " and   d.kpicode  = 'BPCS'"
                 + " and sd.day_start < '" + sDailyStartDate + "'"
                 + " group by d.MillCode, d.FLID, d.KpiCode"
                 + "";

                DataTable dt = cSQL.GetDataTable(sql);
                foreach (DataRow item in dt.Rows)
                {
                   mtdBPCS = cmath.getd(item["KPIValue"].ToString());
                }

                int daysLeft = (numOfDaysMonth - dayOfMonth + 1);

                if (daysLeft <= daysToSplit)
                {
                   dayBPCS = (monthBPCS - mtdBPCS) / daysLeft;
                }

                arr[zBP_BPCS, FROM_MONTH] = dayBPCS;


                SaveData(zBP_BPCS, "BPCS");
                SumYTD_QTR(zBP_BPCS, ROUND5);
                SaveQuarter(zBP_BPCS, "BPCS");

                cSQL.CloseDB();


          }
     * 
     * */



    /*
     *      private void DoBPOSDaily()
          {

                //monthBPTS1 contains the month value / calendar hours
                int daysToSplit = (int)(Math.Round(monthBPOS, 2) / 24);

                double mtdBPOS = 0;
                double dayBPOS = 0;

                //if (sDailyStartDate == "2025-01-25")
                //{
                //   dayBPTS1 = 0;
                //}

                cSQL.OpenDB("ZAMDW");

                int dayOfMonth = cal.GetDayOfMonth(sDailyStartDate);

                //get mtd value of daily bpts1
                string sql = "select d.MillCode, d.FLID, d.kpicode, sum(d.KPIValue) KPIValue  from KPIDaily d"
                 + ", kpidefinition kd "
                 + ", DimPlant p"
                 + ", DimMill m"
                 + ", sappifiscalcalendar.dbo.tbl_days sd"
                 + ", sappifiscalcalendar.dbo.tbl_months sm"
                 + ", sappifiscalcalendar.dbo.tbl_month_names smn"
                 + ", sappifiscalcalendar.dbo.tbl_years sy"
                 + " where d.KPICODE = kd.KpiCode"
                 + " and   d.flid = p.flid"
                 + " and   d.millcode = m.SiteAbbrev"
                 + " and   m.millkey = p.millkey"
                 + " and  d.sappidayid = sd.day_id"
                 + " and   sd.month_id = sm.month_id"
                 + " and sm.month_name_id = smn.month_name_id"
                 + " and   sm.year_id = sy.year_id"
                 + " and sy.year = " + iSappiYear
                 + " and   sm.month_of_year = " + currMonth
                 + " and d.MILLCODE = '" + sSite + "'"
                 + " and   d.FLID = " + sAreaID
                 + " and   d.kpicode  = 'BPOS'"
                 + " and sd.day_start < '" + sDailyStartDate + "'"
                 + " group by d.MillCode, d.FLID, d.KpiCode"
                 + "";

                DataTable dt = cSQL.GetDataTable(sql);
                foreach (DataRow item in dt.Rows)
                {
                   mtdBPOS = cmath.getd(item["KPIValue"].ToString());
                }

                int daysLeft = (numOfDaysMonth - dayOfMonth + 1);

                if (daysLeft <= daysToSplit)
                {
                   dayBPOS = (monthBPOS - mtdBPOS) / daysLeft;
                }

                arr[zBP_BPOS, FROM_MONTH] = dayBPOS;


                SaveData(zBP_BPOS, "BPOS");
                SumYTD_QTR(zBP_BPOS, ROUND5);
                SaveQuarter(zBP_BPOS, "BPOS");

                cSQL.CloseDB();

          }

     * 
     * 
     * 
     * */




    /*
     *      private void DoBPTPDaily()
         {
            //monthBPTS1 contains the month value / calendar hours
            int daysToSplit = (int)(Math.Round(monthBPTP, 2) / 24);

            double mtdBPTP = 0;
            double dayBPTP = 0;

            //if (sDailyStartDate == "2025-01-25")
            //{
            //   dayBPTS1 = 0;
            //}

            cSQL.OpenDB("ZAMDW");

            int dayOfMonth = cal.GetDayOfMonth(sDailyStartDate);

            //get mtd value of daily bpts1
            string sql = "select d.MillCode, d.FLID, d.kpicode, sum(d.KPIValue) KPIValue  from KPIDaily d"
             + ", kpidefinition kd "
             + ", DimPlant p"
             + ", DimMill m"
             + ", sappifiscalcalendar.dbo.tbl_days sd"
             + ", sappifiscalcalendar.dbo.tbl_months sm"
             + ", sappifiscalcalendar.dbo.tbl_month_names smn"
             + ", sappifiscalcalendar.dbo.tbl_years sy"
             + " where d.KPICODE = kd.KpiCode"
             + " and   d.flid = p.flid"
             + " and   d.millcode = m.SiteAbbrev"
             + " and   m.millkey = p.millkey"
             + " and  d.sappidayid = sd.day_id"
             + " and   sd.month_id = sm.month_id"
             + " and sm.month_name_id = smn.month_name_id"
             + " and   sm.year_id = sy.year_id"
             + " and sy.year = " + iSappiYear
             + " and   sm.month_of_year = " + currMonth
             + " and d.MILLCODE = '" + sSite + "'"
             + " and   d.FLID = " + sAreaID
             + " and   d.kpicode  = 'BPTP'"
             + " and sd.day_start < '" + sDailyStartDate + "'"
             + " group by d.MillCode, d.FLID, d.KpiCode"
             + "";

            DataTable dt = cSQL.GetDataTable(sql);
            foreach (DataRow item in dt.Rows)
            {
               mtdBPTP = cmath.getd(item["KPIValue"].ToString());
            }

            int daysLeft = (numOfDaysMonth - dayOfMonth + 1);

            if (daysLeft <= daysToSplit)
            {
               dayBPTP = (monthBPTP - mtdBPTP) / daysLeft;
            }

            arr[zBP_BPTP, FROM_MONTH] = dayBPTP;


            SaveData(zBP_BPTP, "BPTP");
            SumYTD_QTR(zBP_BPTP, ROUND5);
            SaveQuarter(zBP_BPTP, "BPTP");

            cSQL.CloseDB();


         }

     * 
     * 
     * */



    //private void GetMEMBgtDailySai(string pMillCode, int pFLID, string pRunDate)
    //{
    //  //budgets for saiccor only
    //  //double dBGP = 0;
    //  //double dBQR = 0;
    //  double dBNPP = 0;

    //  try
    //  {
    //    string sql = "select k.id, k.sappidayid, sd.day_start, sm.month_of_year, k.millcode, k.flid"
    //      + " ,p.plantname, p.plantcode, p.altplantcode, k.kpicode, k.kpivalue, kd.kpidesc, sd.day_of_month"
    //      + " from kpidaily k"
    //      + " , kpidefinition kd"
    //      + " , dimplant p"
    //      + " , dimmill m"
    //      + " , sappifiscalcalendar.dbo.tbl_days sd"
    //      + " , sappifiscalcalendar.dbo.tbl_months sm"
    //      + " , sappifiscalcalendar.dbo.tbl_month_names smn"
    //      + " , sappifiscalcalendar.dbo.tbl_years sy"
    //      + " where   k.kpicode = kd.kpicode"
    //      + " and k.flid = p.flid"
    //      + " and k.millcode = m.siteabbrev"
    //      + " and m.millkey = p.millkey"
    //      + " and k.sappidayid = sd.day_id"
    //      + " and sd.month_id = sm.month_id"
    //      + " and sm.month_name_id = smn.month_name_id"
    //      + " and sm.year_id = sy.year_id"
    //      + " and k.millcode = '" + pMillCode + "'"
    //      + " and k.flid = " + pFLID
    //      + " and k.kpicode in('BPOH', 'BPNPT', 'KPI0163','BNPP')"
    //      + " and cast(sd.day_start as date) = '" + pRunDate + "'"
    //      + " ";

    //    cSQL.OpenDB("ZAMDW");

    //    DataTable dt = cSQL.GetDataTable(sql);

    //    dBPAvailTime = 0;
    //    dBPProdTime = 0;
    //    dBPNetTons = 0;
    //    dBPBrokeTons = 0;
    //    dBPSpeed = 0;


    //    foreach (DataRow item in dt.Rows)
    //    {
    //      if (item["KPICode"].ToString() == "BPOH")
    //        dBPAvailTime = cmath.getd(item["KPIValue"].ToString());

    //      if (item["KPICode"].ToString() == "BPNPT")
    //        dBPProdTime = cmath.getd(item["KPIValue"].ToString());

    //      if (item["KPICode"].ToString() == "KPI0163")
    //        dBPNetTons = cmath.getd(item["KPIValue"].ToString());

    //      if (item["KPICode"].ToString() == "BNPP")
    //        dBNPP = cmath.getd(item["KPIValue"].ToString());

    //    }

    //    ///2023-11-14  this overides the POR calculation, we cant have a budget figure dependent on actual
    //    if (dBPNetTons == 0)
    //    {
    //      dBPAvailTime = 0;
    //      dBPProdTime = 0;
    //    }
    //    else
    //    {
    //      dBPAvailTime = 24;
    //      dBPProdTime = (24 *  dBNPP) / 100;
    //    }

    //  }
    //  catch (Exception er)
    //  {
    //    //throw er;
    //    cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
    //    cFG.LogIt(pMillCode + " " + pFLID);
    //  }
    //  finally
    //  {
    //    cSQL.CloseDB();
    //  }


    //}//




  }
}
