﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Schedules")]
[assembly: AssemblyDescription("ETL for the MEM Report, SSA Daily Report, Global Report")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Sappi")]
[assembly: AssemblyProduct("Schedules")]
[assembly: AssemblyCopyright("Copyright ©  2020")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("f22eaa6b-a26b-4997-a388-3b9bcf802313")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.2.26")]
[assembly: AssemblyFileVersion("1.0.2.26")]


//1.0.1.0 2020-12-14    C-2012-0419 see email from Eric
//1.0.1.2 2021-01-27    I-2101-12082 MEM calculation changes
//1.0.1.3 2021-02-01    C-2102-0026 MEM calculation changes
//1.0.1.4 2021-02-03    KPI0073 = (NRFTP / RFT) * (NRFTP / RFT) * (PPAG / NSPPM) from Eric
//1.0.1.5 2021-02-05    I-2102-2915 MEM new KPI's in preparation for forthcoming reports
//1.0.1.6 2021-02-16    I-2102-7550 Add Saiccor China BGrade to the schedule KPI0136
//1.0.1.7 2021-02-17    I-2102-8563 Adjusted calculations for when Budgets BGP or POR is zero(Saiccor)
//        2021-02-18    Re-engineer the YTD and Quarters calculation : now stored in table KPIQM by year and month
//1.0.1.8 2021-02-17    Added PLAP planned shut>24hrs Hrs (hrs)
//1.0.1.9 2021-02-22    Added TCSS and TISS and othe calcs
//1.0.1.10 2021-02-23   I-2102-10845 MEM Changes to the daily calculations 
//1.0.1.11 2021-02-24   I-2102-11941 Further calculation changes for monthly figures
//1.0.1.12 2021-02-25   I-2102-11858 Weighted avarage calculation required for CEO Report
//1.0.1.13 2021-02-27   I-2103-0623 MEM new calculations in preparation for Mafube report
//1.0.1.14 2021-03-04   I-2103-2208 New KPI’s for Waterfall Charts 
//1.0.1.15 2021-03-08   C-2103-0214 Mafube report
//1.0.1.16 2021-03-09   C-2103-0263 Changes to the ZA Daily Report [ReportProdMTDZA]
//1.0.1.17 2021-03-10   NGX UT3 now using the stored procedure
//1.0.1.18 2021-03-16   C-2103-0214 Mafube report update calcs
//1.0.1.19 2021-03-17   I-2103-9209 Mem Calculation change on Mafube Report
//1.0.1.20 2021-03-18   I-2103-10229 Mem calculation changes and description changes
//1.0.1.21 2021-06-29   Changed table Name ReportProdDailyZA to ReportProdMTDZA
//1.0.1.22 2021-06-30   I-2106-13686 ZA Modify the MTD section of the "ZA Daily and MTD Report" to reflect the MTD budget and produciton figures for digesters
//                        rather than daily figure
//1.0.1.23 2021-07-23   Fixed the PLO kpi 'unforeseen' category text mispelt
//1.0.1.24 2021-08-16   Changed quarter/ytd calcs for K29 and K40
//1.0.1.25 2021-08-18   I-2107-13369 Publish the cZAdailyMTDReport Job
//1.0.1.26 2021-09-02   I-2108-10661  Za MTD and daily report and weekly Jobs
//1.0.1.27 2021-09-07   I-2109-3321 MEM various KPI Changes (calculations)
//1.0.1.28 2021-09-18   I-2109-9128 Add new KPI Items to the MEM Monthly, Daily and Weekly screens
//1.0.1.29 2021-10-06   I-2109-9128 Add new KPI Items to the MEM Monthly, Daily and Weekly screens
//1.0.1.30 2021-10-20   I-2110-10118 MEM Calculation for POR revised
//1.0.2.0  2021-11-02   C-2110-0242 OEE Digesters
//1.0.2.1  2022-01-20   C-2110-0242 OEE Digesters Added MgO3 Digester for Saiccor
//1.0.2.2  2022-01-27   C-2110-0242 Weighted average for digesters
//1.0.2.3  2022-02-03   C-2201-0420 Count of sheet breaks and count of grade changes 
//1.0.2.4  2022-02-09   I-2202-1772 Only pull through events >= 5 min from OEE into MEM from January 2022 onwards for all Saiccor Plant in MEM
//1.0.2.5  2022-03-02   I-2203-1102 MEM change calculation for Digesters KPI0043
//1.0.2.6  2022-03-11   I-2203-6324  Reverse KPI0043 to original calculation
//1.0.2.7  2022-06-09   Changed zADailyMTDReport Stanger PulpNetProdBgt to read from MEM Daily rather that Fact Table
//1.0.2.8  2022-06-28   C-2203-0482 Regional Performance Summary Report Lomati OEE % and substandard figures
//1.0.2.9  2022-07-19   I-2207-7440 MEM quarterly calculation - change to actual rather than weighted average (Themba)
//1.0.2.9  2022-07-26   Added naresh.tiribeni@sappi.com to the email global report list
//1.0.2.10 2022-08-11   I-2208-6864 Verify MEM weighted average calculations for the Continuous digesters OEE and OME report
//1.0.2.11 2022-10-05   C-2210-0105 MEM calculations to the YTD and Quarters for KPI0057 and KPI0047

//1.0.2.12 2022-11-11   I-2211-7295 - Please change KPI0053 to take its reference from KPI0040 instead of KPI 0043 (Kugan)
//1.0.2.13 2022-12-01   //reversed the calculation above I-2211-7295 to original as per Kugan 2022-12-01
//1.0.2.14              C-2211-0787  OME Summary for Lomati
//1.0.2.15 2023-04-20   C-2304-0326 MEM daily figures please change Saiccor’s PM3, PM4 and  PM5 to pull the their  daily 	
//                        production budget from Daniels data source MESQualitySystem
//1.0.2.16 2023-05-15   C-2305-0046 Align MEM and Global Reports - cGlobalReport.cs
//1.0.2.17 2023-05-30   C-2305-0629 MEM Saiccor Figures please change Saiccor’s PM3, PM4 and  PM5 to pull the their weekly and monthly
//                        production budget from Daniels data source MESQualitySystem
//1.0.2.17 2023-06-13   C-2306-0137 MEM New KPI for Daily Report KPI0163
//1.0.2.18 2023-07-11   C-2306-0489  SSA regional mapping to Global Report - daily budgets
//                      Alignment with all SSA mills to provide all mills with a default daily budget figure based on automatic calculation.
//                      Daily budgets captured in OEE
//1.0.2.19 2023-09-07  C-2309-0057 MEM calculation changes for the YTD and Quarters
//1.0.2.20 2023-10-16  C-2310-0329 Global Report Mapping - Update the Global Report mapping from MEM :  Table [FactProdDaily] - [BrokeTons]
//                      Total quality loss = ((100-KPI0040)/100)*KPI0035 + TBS
//                      The old mapping for BrokeTons in FactProdDaily was from the daily "TBS" figure from MEM  ( Extracted directly from PTS)
//                      Fixed the daily DoWAGRR figure - execute before DoBGP in the order of execution

//1.0.2.21 2024-03-12   I-2403-6663 OME/OEE WA Calculation
//                      Change the OEE/OME weighted average calculation for quarter and ytd.  published 2024-03-20

//1.0.2.22 2024-10-03   I-2410-3350 Please add Zama to the email distribution list mailto: Zamakhosi.Mthethwa@sappi.com

//1.0.2.23 2025-02-14   C-2502-0280 - MEM Daily Budgets 
//1.Global report upload FactProdBudgetDaily   table with daily NSPPM values instead of KPI0163 in BPNetTons 
//2.Aportion BPTS1 Monthy figure to BPTS1 Daily in order to balance for the month.  

//1.0.2.25 2025-02-19   C-2502-0280 - MEM Daily Budgets - apply the above to Saiccor as well.
//1.0.2.26 2025-02-27   C-2502-0541 MEM Preparations Synergy






