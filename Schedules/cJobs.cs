﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;


namespace Schedules
{

  //user defined ad-hoc jobs schedules
  class cJobs
  {


    public int id;
    public string millCode;
    public int FLID;
    public byte jobStatus;
    public byte jobType;
    public int sappiYear;
    public int sappiMonthStart;
    public DateTime nextRunDate;
    public DateTime completedDate;


    public void getJob(string jobIdx)
    {
      string sql = "select id, millcode, flid, jobstatus, jobtype, sappiyear, sappimonthstart, nextrundate, completeddate"
        + "   from [jobs]"
        + " where id = " + jobIdx
        + "";

      cSQL.OpenDB("ZAMDW");

      try
      {
        DataTable dt = cSQL.GetDataTable(sql);
        foreach (DataRow item in dt.Rows)
        {
          id = cmath.geti(item["id"].ToString());
          millCode = item["millcode"].ToString();
          FLID = cmath.geti(item["flid"].ToString());
          jobStatus = cmath.getbyte(item["jobstatus"].ToString());
          jobType = cmath.getbyte(item["jobtype"].ToString());
          sappiYear = cmath.geti(item["sappiyear"].ToString());
          sappiMonthStart = cmath.geti(item["sappimonthstart"].ToString());
          nextRunDate = cmath.getDateTime(item["nextrundate"].ToString());
          completedDate = cmath.getDateTime(item["completeddate"].ToString());
        }
      }
      catch (Exception er)
      {
        throw er;
      }
      finally
      {
        cSQL.CloseDB();
      }

    }//



    public void updJob(string idx)
    {

      string sql = " update jobs set millcode = '" + millCode + "'"
         + " , flid = " + FLID
         + " , jobstatus = " + jobStatus
         + " , jobtype = " + jobType
         + " , sappiyear = " + sappiYear
         + " , sappimonthstart = " + sappiMonthStart
         + " , nextrundate = '" + cmath.getDateTimeStr(nextRunDate) + "'"
         + " , completeddate = '" + cmath.getDateTimeStr(completedDate) + "'"
         + " where id = " + idx
         + "";

      cSQL.OpenDB("ZAMDW");

      try
      {
        cSQL.ExecuteQuery(sql);
      }
      catch (Exception er)
      {
        throw er;
      }
      finally
      {
        cSQL.CloseDB();
      }

    }//




  }///
}///
