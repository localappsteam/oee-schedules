﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;




namespace Schedules
{
  class cCal
  {

    public int iSappiYearID = 0;
    public int iSappiYear = 0;
    public string sYearStartDate = "";
    public string sYearEndDate = "";

    public int iCurrMonthID = 0;
    public int iCurrMonthOfYear = 0;
    public string sMonthStartDate = "";
    public string sMonthEndDate = "";
    public string sMonthShortName = "";
    public int iDaysInMonth = 0;

    public int iCurrWeekOfYear = 0;
    public int iCurrWeekID = 0;
    public string sWeekStartDate = "";
    public string sWeekEndDate = "";
    public int iDaysInWeek = 0;

    public int iDayID = 0;
    public string sDayStart = "";
    public string sDayEnd = "";
    public int iDayOfMonth = 0;
    public int iDayOfWeek = 0;
    public int iDayOfYear = 0;

    public int iQuarterID = 0;
    public int iQuarterOfYear = 0;
    public string sQuartertartDate = "";
    public string sQuarterEndDate = "";

    public void ClearVar()
    {
      iSappiYear = 0;
      iSappiYearID = 0;
      sYearStartDate = "";
      sYearEndDate = "";

      iCurrMonthID = 0;
      iCurrMonthOfYear = 0;
      sMonthStartDate = "";
      sMonthEndDate = "";
      sMonthShortName = "";
      iDaysInMonth = 0;

      iCurrWeekID = 0;
      iCurrWeekOfYear = 0;
      sWeekStartDate = "";
      sWeekEndDate = "";
      iDaysInWeek = 0;

      iDayID = 0;
      sDayStart = "";
      sDayEnd = ""; 
      iDayOfMonth = 0;
      iDayOfWeek = 0 ;
      iDayOfYear = 0 ;

      iQuarterID = 0;
      iQuarterOfYear = 0;
      sQuartertartDate = "";
      sQuarterEndDate = "";
    }

    public void GetCal(string selectedDate = "")
    {

      string sql = " select   sy.year_id, sy.year, sy.year_start, sy.year_end"
        + " 	,sm.month_id, sm.month_of_year, smn.name_short, sm.month_days, sm.month_start, sm.month_end"
        + "   ,sw.week_id, sw.week_start, sw.week_end, sw.week_of_year, sq.quarter_id, sq.quarter_start, sq.quarter_end, sq.quarter_of_year"
        + "   , DATEDIFF(w, sw.week_start, sw.week_end)+1 numDaysInWeek"
        + "   , sd.day_id, sd.day_start, sd.day_end, sd.day_of_month, sd.day_of_week, sd.day_of_year"
        + "   from sappifiscalcalendar.dbo.tbl_years sy"
        + "      , sappifiscalcalendar.dbo.tbl_months sm"
        + "      , sappifiscalcalendar.dbo.tbl_month_names smn"
        + "      , sappifiscalcalendar.dbo.tbl_weeks sw"
        + "      , sappifiscalcalendar.dbo.tbl_quarters sq"
        + "      ,sappifiscalcalendar.dbo.tbl_days sd"
        + "   where   sm.month_name_id = smn.month_name_id"
        + "           and sm.year_id = sy.year_id"
        + "           and sw.month_id = sm.month_id"
        + "";

      
      if (selectedDate == "")
      {
        sql += "   and getdate() between sw.week_start and sw.week_end"
            + "    and getdate() between sm.month_start and sm.month_end"
            + "    and getdate() between sq.quarter_start and sq.quarter_end"
            + "    and getdate() between sd.day_start and sd.day_end"
            + "";
      }
      else
      {
        sql += " and '" + selectedDate + "' between sw.week_start and sw.week_end  "
            + "  and '" + selectedDate + "' between sm.month_start and sm.month_end"
            + "  and '" + selectedDate + "' between sq.quarter_start and sq.quarter_end"
            + "  and '" + selectedDate + "' between sd.day_start and sd.day_end"
            + "";
      }

      try
      {
        ClearVar();

        cSQL.OpenDB("ZAMDW");

        DataTable dt = cSQL.GetDataTable(sql);
        foreach (DataRow item in dt.Rows)
        {

          iSappiYear = cmath.geti(item["year"].ToString());
          iSappiYearID = cmath.geti(item["year_id"].ToString());
          sYearStartDate = item["year_start"].ToString();
          sYearEndDate = item["year_end"].ToString();

          iCurrMonthID = cmath.geti(item["month_id"].ToString());
          iCurrMonthOfYear = cmath.geti(item["month_of_year"].ToString());
          sMonthStartDate = item["month_start"].ToString();
          sMonthEndDate = item["month_end"].ToString();
          sMonthShortName = item["name_short"].ToString();
          iDaysInMonth = cmath.geti(item["month_days"].ToString());

          iCurrWeekID = cmath.geti(item["week_id"].ToString());
          iCurrWeekOfYear = cmath.geti(item["week_of_year"].ToString());
          sWeekStartDate = item["week_start"].ToString();
          sWeekEndDate = item["week_end"].ToString();
          iDaysInWeek = cmath.geti(item["numDaysInWeek"].ToString());

          iDayID = cmath.geti(item["day_id"].ToString());
          sDayStart = item["day_start"].ToString();
          sDayEnd = item["day_end"].ToString();
          iDayOfMonth = cmath.geti(item["day_of_month"].ToString());
          iDayOfWeek = cmath.geti(item["day_of_week"].ToString());
          iDayOfYear = cmath.geti(item["day_of_year"].ToString()); 

          iQuarterID = cmath.geti(item["quarter_id"].ToString());
          iQuarterOfYear = cmath.geti(item["quarter_of_year"].ToString());
          sQuartertartDate = item["quarter_start"].ToString();
          sQuarterEndDate = item["quarter_end"].ToString();
        }
      }
      catch (Exception er)
      {
        throw er;
      }
      finally
      {
        cSQL.CloseDB();
      }

    } //




    
    public int GetFirstMonthID(int pSappiYear)
    {

      int rv = 0;

      try
      {

        cSQL.OpenDB("ZAMDW");      


        string sql = "select sm.month_id, sm.month_of_year, smn.name_short, sy.year"
            + " from   sappifiscalcalendar.dbo.tbl_months sm"
            + " , sappifiscalcalendar.dbo.tbl_month_names smn"
            + " , sappifiscalcalendar.dbo.tbl_years sy"
            + " where sm.month_name_id = smn.month_name_id"
            + " and sm.year_id = sy.year_id"
            + " and sy.year = " + pSappiYear
            + " order by sm.month_id";            // 2025-02-25 added this 


        //get the first month id
        DataTable dt = cSQL.GetDataTable(sql);
        foreach (DataRow item in dt.Rows)
        {
          rv = cmath.geti(item["month_id"].ToString());
          break;
        }

        return rv;

      }
      catch (Exception er)
      {
        throw new Exception("GetFirstMonth: " + er.Message);
      }
      finally
      {
        cSQL.CloseDB();
      }

    }//









    public void GetWeekMSSQL(int finYear, int weekNum, ref int currMonth, ref string startDate, ref string endDate, ref int numDaysWeek)
    {

     
      try
      {

        cSQL.OpenDB("ZAMDW");

        string sql = "select cast(sw.week_start as date) week_start, cast (sw.week_end as date) week_end"
          + ", sw.week_of_month, sw.week_of_year, sw.week_id, sw.month_id, sy.year, sm.month_of_year "
          + ", DATEDIFF(w, sw.week_start, sw.week_end)+1 numDaysInWeek"
          + " from sappifiscalcalendar.dbo.tbl_weeks sw, sappifiscalcalendar.dbo.tbl_months sm, sappifiscalcalendar.dbo.tbl_years sy"
          + " where sw.year_id = sy.year_id"
          + " and sw.month_id = sm.month_id"
          + " and sy.year  = " + finYear
          + " and sw.week_of_year = " + weekNum
          + "";

        DataTable dt = cSQL.GetDataTable(sql);
        foreach (DataRow item in dt.Rows)
        {
          currMonth = cmath.geti(item["month_of_year"].ToString());
          startDate = item["week_start"].ToString();
          endDate = item["week_end"].ToString();
          numDaysWeek = cmath.geti(item["numDaysInWeek"].ToString());

        }

      }
      catch (Exception ee)
      {
        throw ee;
      }
      finally
      { 
        cSQL.CloseDB(); 
      }


    }//



    public void GetMonthMSSQL(int finYear, int currMonth, ref string startDate, ref string endDate)
      {

         try
         {
            

            string sql = "select mm.month_start, mm.month_end"
              + " from [SappiFiscalCalendar].[dbo].[tbl_months] mm "
              + " join [SappiFiscalCalendar].[dbo].[tbl_years] yy on mm.year_id = yy.year_id"
              + " where mm.month_of_year = " + currMonth
              + " and  yy.year  = " + finYear
              + "";

            DataTable dt = cSQL.GetDataTable(sql);
            foreach (DataRow item in dt.Rows)
            {
               startDate = item["month_start"].ToString();
               endDate = item["month_end"].ToString();
            }

         }
         catch (Exception ee)
         {
            throw ee;
         }
         finally
          {
          }

      }//



    public int GetSappiMonthID(int pSappiYear, int monthNum)
    {
      int rv = 0;

      try
      {

        cSQL.OpenDB("ZAMDW");    

        string sql = "select sm.month_id, sm.month_of_year, smn.name_short, sy.year"
         + " from   sappifiscalcalendar.dbo.tbl_months sm"
         + " , sappifiscalcalendar.dbo.tbl_month_names smn"
         + " , sappifiscalcalendar.dbo.tbl_years sy"
         + " where sm.month_name_id = smn.month_name_id"
         + " and sm.year_id = sy.year_id"
         + " and sy.year = " + pSappiYear
         + "  and sm.month_of_year = " + monthNum
         + "";

        DataTable dt = cSQL.GetDataTable(sql);
        foreach (DataRow item in dt.Rows)
        {
          rv =  cmath.geti(item["month_id"].ToString());
        }

        return rv;
      }
      catch (Exception er)
      {
        throw new Exception("GetSappiMonthID: " + er.Message);
      }
      finally
      {
        cSQL.CloseDB();
      }

    }//



    public int GetSappiWeekID(int pSappiYear, int pWeekNum)
    {
        //get the sappiWeekID from a week nummber
      int rv = 0;

      try
      {

        cSQL.OpenDB("ZAMDW");

        string sql = "select sw.week_start, sw.week_end, sw.week_of_month, sw.week_of_year, sw.week_id,  sy.year"
          + " from  sappifiscalcalendar.dbo.tbl_weeks sw"
          + "      ,sappifiscalcalendar.dbo.tbl_years sy"
          + "  where sw.year_id = sy.year_id"
          + "        and sy.year = " + pSappiYear
          + "        and sw.week_of_year = " + pWeekNum
          + "";

        DataTable dt = cSQL.GetDataTable(sql);
        foreach (DataRow item in dt.Rows)
        {
          rv = cmath.geti(item["week_id"].ToString());
        }

        return rv;
      }
      catch (Exception er)
      {
        throw new Exception("GetSappiWeekID: " + er.Message);
      }
      finally
      {
        cSQL.CloseDB();
      }

    }//

    
    public void GetSappiMonthDates(string thisDate, ref string pDateFrom, ref string pDateTo)
    {

      //returns string dates in the format  2021-06-28 00:00:00.000   to  2021-07-25 23:59:59.997 

      try
      {

        cSQL.OpenDB("ZAMDW");

        //sappi months start and end dates
        string sql = "select sm.month_id "
          + "    , sm.month_of_year"
          + "    , smn.name_short"
          + "    , sy.year"
          + "    , sm.month_days"
          + "    , sm.month_start"
          + "    , sm.month_end"
          + "    , sm.month_id"
          + "    , sy.year_start"
          + "    , sy.year_end"
          + "  from"
          + "     sappifiscalcalendar.dbo.tbl_months sm"
          + "   , sappifiscalcalendar.dbo.tbl_month_names smn"
          + "   , sappifiscalcalendar.dbo.tbl_years sy"
          + " where"
          + " sm.month_name_id = smn.month_name_id"
          + " and sm.year_id = sy.year_id"
          + "     and '" + thisDate + "' between sm.month_start and sm.month_end"
          //+ "     and getdate() between sm.month_start and sm.month_end"
          + " ";



        DataTable dt = cSQL.GetDataTable(sql);
        foreach (DataRow item in dt.Rows)
        {
          pDateFrom = item["month_start"].ToString();
          pDateTo = item["month_end"].ToString();
        }

      }
      catch (Exception er)
      {
        throw new Exception("GetSappiDates: " + er.Message);
      }
      finally
      {
        cSQL.CloseDB();
      }

    }//

    
    public int GetSappiDayID(string pDate = "")
    {
      //from SQLSERVER
      //return sappi day id for today or for the day in param

      int rv = 0;

      try
      {

        cSQL.OpenDB("ZAMDW");     

        string sql = "Select   sd.day_id,  sd.day_of_month, sd.day_start, sd.day_end, sd.day_of_year , sm.month_of_year, sy.year"
          + " from  sappifiscalcalendar.dbo.tbl_days sd"
          + " , sappifiscalcalendar.dbo.tbl_months sm"
          + " , sappifiscalcalendar.dbo.tbl_years sy";

        if (pDate == "")
          sql += " where sd.day_start = CAST(GETDATE() as date)";
        else
          sql += " where sd.day_start = '" + cmath.getDateStr(pDate) + "'";

        sql += " and sd.month_id = sm.month_id"
            + " and sd.year_id = sy.year_id"
            + "";

        DataTable dt = cSQL.GetDataTable(sql);
        foreach (DataRow item in dt.Rows)
        {
          rv = cmath.geti(item["day_id"].ToString());
        }

        return rv;
      }
      catch (Exception er)
      {
        throw new Exception("GetSappiDayID: " + er.Message);
      }
      finally
      {
        cSQL.CloseDB();
      }

    }//


    public void GetThisMonthHrs(ref int thisYear, ref double dHrs, ref int thisMonth)
    {

      try
      {

        cSQL.OpenDB("ZAMDW");     

        string sql = "select sm.month_start, sm.month_end, sm.month_days , sm.month_of_year, sy.year"
          + ", (DATEDIFF(day, sm.month_start, cast(GETDATE() as date))) * 24 as Hrs"
          + " from sappifiscalcalendar.dbo.tbl_months sm"
          + "     ,sappifiscalcalendar.dbo.tbl_years sy"
          + " where getdate() between sm.month_start and sm.month_end"
          + "   and sm.year_id = sy.year_id";

       
        DataTable dt = cSQL.GetDataTable(sql);
        foreach (DataRow item in dt.Rows)
        {
          dHrs = cmath.geti(item["Hrs"].ToString());
          thisMonth = cmath.geti(item["month_of_year"].ToString());
          thisYear = cmath.geti(item["year"].ToString());
        }


      }
      catch (Exception er)
      {
        throw new Exception("GetThisMonthHrs: " + er.Message);
      }
      finally
      {
        cSQL.CloseDB();
      }

    }//


    public void GetThisWeekHrs(string sRunDate, ref double dHrs)
    {

      try
      {
        
        sRunDate = cmath.getDateStr(sRunDate);

        cSQL.OpenDB("ZAMDW");

        string sql = "select sw.week_start,  sw.week_end,  sw.week_of_year,  sy.year" 
          + ", (DATEDIFF(day, sw.week_start, cast('" + sRunDate + "'  as date)) + 1) * 24 as Hrs"
          + " from sappifiscalcalendar.dbo.tbl_weeks sw     ,sappifiscalcalendar.dbo.tbl_years sy"
          + " where '" + sRunDate + "' between sw.week_start and sw.week_end and sw.year_id = sy.year_id"
          + "";

        DataTable dt = cSQL.GetDataTable(sql);

        foreach (DataRow item in dt.Rows)
        {
          dHrs = cmath.geti(item["Hrs"].ToString());
        }


      }
      catch (Exception er)
      {
        throw new Exception("GetThisWeekHrs: " + er.Message);
      }
      finally
      {
        cSQL.CloseDB();
      }

    }//


    public int GetDayOfMonth(string sDate)
      {
         int rv = 0;
         try
         {

            string sql = " select sd.day_Start, sd.day_end, day_of_month"
                + " from sappifiscalcalendar.dbo.tbl_days sd where '" + sDate + "'  between sd.day_start and sd.day_end";


            DataTable dt = cSQL.GetDataTable(sql);
            foreach (DataRow item in dt.Rows)
            {
               rv = cmath.geti(item["day_of_month"].ToString());
            }

            return rv;

         }
         catch (Exception ee)
         {
            throw ee;
         }


      }







  }///
}///



